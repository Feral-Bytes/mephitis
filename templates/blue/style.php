<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

class style{

	var $tpl_path = 'templates/blue/';

	function __construct(){
	
	}
	
	function draw_site($content){
		
		global $user, $lang, $config, $libs, $msg_stack, $searchengine, $phproot, $tpl_vars, $default_tpl_vars, $httpRootPath;
		
		$nav = new nav(nav_conf(0, -1, 'all'));
		$echo = '';
		
		$logo = $httpRootPath.$this->tpl_path.'/files/14934696244_ac7bea9a4c_o.jpg';
		$styleCss = $httpRootPath.$this->tpl_path.'style.css';
		if(isset($default_tpl_vars))
		{
			$styleCss = $httpRootPath.'index.php?file='.$this->tpl_path.'style.css&mode=text&filemodule=text/css';
			
			if($tpl_vars['picture']['header_picture'] != '')$logo = $httpRootPath.'index.php?file='.$tpl_vars['picture']['header_picture'].'&mode=full';
		}
		
		$footer = getvar('footer');
		
		if(trim($footer) != '')
		{
			$footer .= '<br>';
		}
		
		$echo .= '<!doctype html>
		<html>
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset='.$config['charset'].'" />
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<meta name="viewport" content="width=device-width, initial-scale=1">
				
				<title>
					'.getvar('title').'
				</title>
				
				<link rel="stylesheet" type="text/css" href="'.$httpRootPath.'libs/bootstrap/css/bootstrap.min.css">
				<link rel="stylesheet" type="text/css" href="'.$httpRootPath.'libs/font-awesome/css/font-awesome.min.css">
				<link rel="stylesheet" type="text/css" href="'.$httpRootPath.$this->tpl_path.'jquery-ui/jquery-ui.min.css">
				'.build_lib($libs['html_header']).'
				<link rel="stylesheet" type="text/css" href="'.$styleCss.'">
				
				<meta name="robots" content="'.getvar('meta_robots').'">
				<meta name="author" content="'.getvar('meta_author').'" >
				<meta name="description" content="'.getvar('meta_description').'">
				<meta name="keywords" content="'.getvar('meta_keywords').'">
				'; 
				if(getvar('shortcut_icon') != ''){$echo .= '<link rel="shortcut icon" href="'.getvar('shortcut_icon').'">';}
				$echo .= '
				
			</head>
		<body>
		<div id="wrap">
			<a name="top"></a>
			<header class="header hidden-xs">
				<a class="header" style="background-image: url('.$logo.');" href="'.$phproot.'"></a>
			</header>

			<nav class="navbar navbar-default" role="navigation">
			  <div class="container-fluid">
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainnav">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				  <a class="navbar-brand visible-xs" href="'.$phproot.'">'.getvar('title').'</a>
				</div>

				<div class="collapse navbar-collapse" id="mainnav">
					'.draw_nav($nav, 'mainnav').'
					'.$searchengine->draw_form('navbar-form navbar-right').'
				</div>
			  </div>
			</nav>
			'.draw_action_link('#bottom', 'to_bottom', '', 'pull-right pagenav-top').'
			<div id="wrap-page">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="msg_stack">
							'; 
							if(isset($msg_stack))$echo .= $msg_stack;
							$echo .= '
							</div>
						</div>
					</div>
				</div>			
				'.$content.'
			</div>
			'.draw_action_link('#top', 'to_top', '', 'pull-right pagenav-bottom').'
		</div>	
		<footer class="footer">
			<div class="container">
			'.$footer.'
			'.$lang['footer'].'
			</div>
		</footer>
		<a name="bottom"></a>
		<script src="'.$httpRootPath.'libs/jquery/jquery.min.js"></script>
		<script src="'.$httpRootPath.$this->tpl_path.'jquery-ui/jquery-ui.min.js"></script>
		<script src="'.$httpRootPath.'libs/bootstrap/js/bootstrap.min.js"></script>
		<script src="'.$httpRootPath.'templates/mephitis1/js/basic.js"></script>
		<script>
			var msg_box_fadeout_timer = '.((getvar('msg_box_fadeout_timer'))*1000).';
		</script>
		'.build_lib($libs['html_footer']).'
		</body>
		</html>
		';
		
		return $echo;
		
	}
	
	function draw_iframe($content){
		
		global $user, $lang, $config, $libs, $msg_stack, $searchengine, $phproot, $tpl_vars, $default_tpl_vars, $httpRootPath;
		
		$echo = '';
		
		$styleCss = $httpRootPath.$this->tpl_path.'style.css';
		if(isset($default_tpl_vars))
		{
			$styleCss = $httpRootPath.'index.php?file='.$this->tpl_path.'style.css&mode=text&filemodule=text/css';
		}
		
		$echo .= '<!doctype html>
		<html>
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset='.$config['charset'].'" />
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<meta name="viewport" content="width=device-width, initial-scale=1">
				
				<title>
					'.getvar('title').'
				</title>
				
				<link rel="stylesheet" type="text/css" href="'.$httpRootPath.'libs/bootstrap/css/bootstrap.min.css">
				<link rel="stylesheet" type="text/css" href="'.$httpRootPath.'libs/font-awesome/css/font-awesome.min.css">
				<link rel="stylesheet" type="text/css" href="'.$httpRootPath.$this->tpl_path.'jquery-ui/jquery-ui.min.css">
				'.build_lib($libs['html_header']).'
				<link rel="stylesheet" type="text/css" href="'.$styleCss.'">
				
			</head>
		<body>
			
		'.$content.'

		<script src="'.$httpRootPath.'libs/jquery/jquery.min.js"></script>
		<script src="'.$httpRootPath.$this->tpl_path.'jquery-ui/jquery-ui.min.js"></script>
		<script src="'.$httpRootPath.'libs/bootstrap/js/bootstrap.min.js"></script>
		<script src="'.$httpRootPath.'templates/mephitis1/js/basic.js"></script>
		<script>
			var msg_box_fadeout_timer = '.((getvar('msg_box_fadeout_timer'))*1000).';
		</script>		
		'.build_lib($libs['html_footer']).'
		</body>
		</html>
		';
		
		return $echo;
		
	}
}

?>