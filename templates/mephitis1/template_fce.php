<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function DrawGrid($ce, $pos = 'main')
{
	global $pageContent, $page;
	
	$echo = '';
	
	$parent = 0;

	if(isset($ce['type']) && $ce['type'] == 'page')
	{
		$parent = 0;
	}
	else
	{
		$parent = $ce['id'];
	}

	$contents = $pageContent->GetContent($pos, $parent, $page['id'], $ce['isBranch']);
	
	$contentsHTML = '';
	
	$count = count($contents);
	$i = 0;
	
	if(is_array($contents))
	foreach($contents as $content)
	{
		$i++;
		$contentsHTML .= $content['html'];
	}

	$echo = '
		'.$contentsHTML.'
	';
	
	return $echo;

}

?>