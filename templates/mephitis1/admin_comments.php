<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_admin_comments($data)
{
	global $phproot, $user, $link;
	
	$echo = '';
	$list = '';
	
	if(is_array($data))
	{
		$offline_items = '';
		foreach($data as $element)
		{
			if($element['count_offline'] > 0)
			$offline_items .= '
				<a href="'.$phproot.'?'.$link.'&module='.$element['object'].'&get=offline" class="btn btn-default btn-lg btn-block">'.get_label($element['object']).'&nbsp<span class="badge">'.$element['count_offline'].'</span></a>
			';
		}
		$list .= '
				<b>'.get_label('offline_comments').'</b>
				'.$offline_items.'
				<hr>
		';
		
		$online_items = '';
		foreach($data as $element)
		{
			if($element['count_online'] > 0)
			$online_items .= '
				<a href="'.$phproot.'?'.$link.'&module='.$element['object'].'&get=online" class="btn btn-default btn-lg btn-block">'.get_label($element['object']).'&nbsp<span class="badge">'.$element['count_online'].'</span></a>
			';
		}
		$list .= '
				<b>'.get_label('online_comments').'</b>
				'.$online_items.'
				<hr>
		';		
	}
	
	$echo .= $list;
	
	
	return $echo;
}

function draw_admin_comments_actions($conf, $row)
{
	global $lang, $phproot;
	
	$echo = '';
	$echo .= '
		<a href="'.$phproot.'?'.$conf['link'].'&action=edit&itemid='.$row['id'].'" class="btn btn-warning" title="'.get_label('edit').'"><i class="fa fa-wrench"></i></a>&nbsp;
		<a href="'.$phproot.'?'.$conf['link'].'&action=del&itemid='.$row['id'].'" class="btn btn-danger" title="'.get_label('del').'"><i class="fa fa-trash-o"></i></a>&nbsp;
	';
	if($row['online'] == '0')
	{
		$echo .= '
			<a href="'.$phproot.'?'.$conf['link'].'&comment=setonline&itemid='.$row['id'].'#comment_'.$row['id'].'" class="btn btn-info" title="'.get_label('set_public').'"><i class="fa fa-eye"></i></a>				
		';
	}
	else
	{
		$echo .= '
			<a href="'.$phproot.'?'.$conf['link'].'&comment=unsetonline&itemid='.$row['id'].'#comment_'.$row['id'].'" class="btn btn-warning" title="'.get_label('unset_public').'"><i class="fa fa-eye-slash"></i></a>				
		';				
	}	
	
	return $echo;
}

?>