<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

class style extends StyleHTML{

	function __construct()
	{

		global $assetService, $phpRootPath;
	
		$pageJS = '
			var cms_cookie = "'.getvar('cookie_name ').'";
		';
		$assetService->Add(
			array(
				'name' => 'page.cookie_vars.js',
				'type' => 'js',
				'content' => $pageJS
			)
		);
		
		parent::__construct();
		
		$styleCss = $phpRootPath.$this->tpl_path.'admin_style.css';
		$assetService->Add(
			array(
				'name' => $this->tpl_name.'_admin',
				'type' => 'css',
				'path' => $styleCss,
				'standalone' => true
			)
		);		
	
		$assetService->Add(
			array(
				'name' => 'page_admin.js',
				'type' => 'js',
				'path' => $phpRootPath.$this->tpl_path.'js/admin_basic.js'
			)
		);
		
		$assetService->Add(
			array(
				'name' => 'page_cache.js',
				'type' => 'js',
				'path' => $phpRootPath.$this->tpl_path.'js/admin_cache.js'
			)
		);

	}

	function draw_site($content){
		
		global $user, $lang, $links, $config, $libs, $msg_stack, $phproot, $httpRootPath, $assetService;
		
		$echo = '';
		
		$nav_admin = admin_nav($links['admin_nav']);
		$nav_user =  nav_usercp($links['usercp_nav']);
		
		$echo .= '<!doctype html>
		<html>
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset='.$config['charset'].'" />
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<meta name="viewport" content="width=device-width, initial-scale=1">
				
				<title>
					'.$lang['title'].'
				</title>
				<link rel="shortcut icon" href="'.$httpRootPath.'favicon.ico">
				<script>
					
				</script>
				'.$assetService->Render('css').'
				'.build_lib($libs['html_header']).'
			</head>
		<body>
		<div id="wrap">
			<nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0" role="navigation">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainnav">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="'.$phproot.'">'.$lang['title'].' v'.$config['version'].'</a>
				</div>
				<div class="sidebarnav collapse navbar-collapse" id="mainnav">
					<ul class="nav">
						'.$nav_admin.'
					</ul>
				</div>

				<ul class="nav navbar-top-links navbar-right">
					<li>
						<a href="'.$phproot.'?cache_action=clear" class="btn btn-info" id="btnClearCache"><i class="fa fa-cogs" aria-hidden="true"></i>&nbsp;<span>'.get_label('clear_cache').'</span></a>
					</li>
					'.$nav_user.'
				</ul>
			</nav>
		
			<div id="wrap-page">
				<div class="row">
					<div class="cold-md-12">
					<br>
					</div>
				</div>
				<div class="row">
					<div class="cold-md-12">
						<div class="msg_stack">
						'; 
						if(isset($msg_stack))$echo .= $msg_stack;
						$echo .= '
						</div>
						'.$content.'
					</div>
				</div>
				<div class="row">
					<div class="cold-md-12 text-center">
						&copy; by Feral Bytes<br>
						<a href="https://www.feralbytes.ch/">www.feralbytes.ch</a>
					</div>
				</div>				
			</div>
		</div>
	
		'.$assetService->Render('js').'
		'.build_lib($libs['html_footer']).'
		</body>
		</html>
		';
		
		return $echo;
		
	}
	
}

?>