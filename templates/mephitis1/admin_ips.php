<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_ip_list($ips, $queryadd, $order_link){
	
	global $phproot, $lang;
	
	$echo = '';
	if(is_array($ips))
	foreach($ips as $ip){
		$user = user_info($ip['user']);
		$echo .= '
			<tr>
				<td style="font-size: 10px;">
					'.$ip['id'].'
				</td>
				<td style="font-size: 10px;">
					'.$ip['host'].'
				</td>
				<td style="font-size: 10px;">
					'.$ip['ip'].'';
					if(is_array($user)){$echo .= ' ('.$user['name'].')';}
					$echo .='
					<br>
					<hr>
					'.$ip['http_user_agent'].'
				</td>
				<td style="font-size: 10px;">
					'.$ip['http_referer'].'<hr>'.$ip['request_method'].'<hr>'.$ip['php_self'].'
				</td>
				<td style="white; font-size: 10px;">
					'.date(getvar('norm_date'), $ip['time']).'
				</td>
			</tr>
		';
	}
	
	$echo = '
		<a href="'.$phproot.'?admin=ips&action=del">'.$lang['ips_del'].'</a><br><br>
		'.pages('draw', 'ips', 'id != "0" '.$queryadd.'', $order_link, 'ip_page').'
		<table class="norm_tab table table-bordered">
			<thead>
				<tr>
					<th class="head">
						<a href="'.$phproot.'?admin=ips&order=id">'.$lang['id'].'</a>
					</th>
					<th class="head">
						'.$lang['host'].'
					</th>
					<th class="head">
						<a href="'.$phproot.'?admin=ips&order=ip">'.$lang['ip'].'</a> ('.$lang['user'].') / '.$lang['http_user_agent'].'
					</th>
					<th class="head">
						'.$lang['http_referer'].' / '.$lang['request_method'].' / '.$lang['php_self'].'
					</th>
					<th class="head">
						<a href="'.$phproot.'?admin=ips&order=time">'.$lang['lastaccess'].'</a>
					</th>
				</tr>
			</thead>
			<tbody>
				'.$echo.'
			</tbody>
		</table>
		<br>
		'.pages('draw', 'ips', 'id != "0" '.$queryadd.'', $order_link, 'ip_page').'
		<br>
		<a href="'.$phproot.'?admin=ips&action=del">'.$lang['ips_del'].'</a>
	';
	
	return $echo;
	
}

?>