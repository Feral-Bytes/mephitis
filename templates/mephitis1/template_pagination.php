<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_pages($pages, $num_rows, $num_pages, $per_page, $link, $varname){

	global $phproot, $lang;
	
	$list = '';

	$field	= $num_pages; 
	
	if(isset($_GET[$varname])){
		$viewer = $_GET[$varname];
	}else{
		$viewer = 1;
	}

	if(isset($_POST['gotopage']) AND isset($_POST[$varname])){
		$viewer = $_POST[$varname];
	}

	$dropdown_pages = '';
	if(is_array($pages))foreach($pages as $page){
		if($page == $viewer){
			$active = ' selected';
		}else{
			$active = '';
		}
		$dropdown_pages .= '<option value="'.$page.'"'.$active.'>'.$page.'</option>';
	}

	if($num_pages > 10){

		$pages		= '';
		$range		= 1;
		$endrange	= 3;

		$view['a'] = $viewer-$range;
		$view['b'] = $viewer+$range;

		for($i=1;$i<=$num_pages;$i++){
			if($i <= $endrange){
				$pages[] = $i;
			}else if($i >= $view['a'] AND $i <= $view['b']){
				$pages[] = $i;
			}else if($i > $num_pages-$endrange){
				$pages[] = $i;
			}else if($i == $endrange + 1 OR $i ==  $field-$endrange){
				$pages[] = '';
			}else{
				
			}
		}

	}

	if(is_array($pages))foreach($pages as $page){
		if(is_numeric($page)){
			if($viewer == $page){
				$list .= '<li class="active"><a href="'.$phproot.'?'.$link.'&'.$varname.'='.$page.'">'.$page.' <span class="sr-only">(current)</span></a></li>';
			}else{
				$list .= '<li><a href="'.$phproot.'?'.$link.'&'.$varname.'='.$page.'">'.$page.'</a></li>';
			}
			
		}else{
			$list .= '<li class="disabled"><a>...</a></li>';
		}
	}
	
	if($viewer-1 > 0){
		$nav['back'] = '<li><a href="'.$phproot.'?'.$link.'&'.$varname.'='.($viewer-1).'" title="'.$lang['back'].'">&laquo;</a></li>';
	}else{
		$nav['back'] = '<li class="disabled"><a href="#">&laquo;</a></li>';
	}

	if($viewer+1 <= $field){
		$nav['next'] = '<li><a href="'.$phproot.'?'.$link.'&'.$varname.'='.($viewer+1).'" title="'.$lang['next'].'">&raquo;</a></li>';
	}else{
		$nav['next'] = '<li class="disabled"><a href="#">&raquo;</a></li>';
	}
	
	$list = '<ul class="pagination mep_pagination">'.$nav['back'].$list.$nav['next'].'</ul>';
	
	$dropdown = '
		<form action="'.$phproot.'?'.$link.'" method="POST" class="pagination mep_pagination">
			<div class="input-group">
				<select class="form-control" name="'.$varname.'">
					'.$dropdown_pages.'
				</select>
				<span class="input-group-btn">
					<input type="submit" class="btn btn-default" name="gotopage" value="'.$lang['go_to_page'].'">				
				</span>			
			</div>			
		</form>
	';

	if(is_array($pages)){
		$echo = '
			'.$list.'
			'.$dropdown.'
		';
	}else{
		$echo = '';
	}

	return $echo;

}

?>