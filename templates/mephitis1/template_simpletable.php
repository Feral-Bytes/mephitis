<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_simpletable($conf, $header, $data){
	
	global $phproot, $lang, $libs;
	
	$echo = '';
	
	$tab_header = '';
	$tab_data = '';
	
	$x_h = 0;
	foreach($header as $td){
		if(isset($td['Field']) && isset($conf['fields'][$td['Field']]['display']) AND ($conf['fields'][$td['Field']]['display'] == 'no' || $conf['fields'][$td['Field']]['display'] == false)){
			continue;
		}

		$x_h++;
	
		$actions = '';
		
		if(isset($td['Field']) && isset($conf['link']) AND (!isset($conf['template']['show_order_btns']) || $conf['template']['show_order_btns'])){
			$actions .= '<br>';
			if(isset($_GET['orderby']) AND $_GET['orderby'] == $td['Field'] AND isset($_GET['ordermode']) AND $_GET['ordermode'] == 'ASC'){
				$actions .= '<span class="order_active">'.draw_action_link($phproot.'?'.$conf['link'].'&orderby='.$td['Field'].'&ordermode=ASC', 'asc', '').'</span> | ';
			}else{
				$actions .= ''.draw_action_link($phproot.'?'.$conf['link'].'&orderby='.$td['Field'].'&ordermode=ASC', 'asc').' | ';
			}
			
			if(isset($_GET['orderby']) AND $_GET['orderby'] == $td['Field'] AND isset($_GET['ordermode']) AND $_GET['ordermode'] == 'DESC'){
				$actions .= '<span class="order_active">'.draw_action_link($phproot.'?'.$conf['link'].'&orderby='.$td['Field'].'&ordermode=DESC', 'desc', '').'</span> | ';
			}else{
				$actions .= ''.draw_action_link($phproot.'?'.$conf['link'].'&orderby='.$td['Field'].'&ordermode=DESC', 'desc').' | ';
			}
			$actions = substr($actions, 0, -2);
		}else{
			$actions .= '<br><br>';
		}
		if(isset($td['Field']) && isset($conf['fields'][$td['Field']]['tooltip']) && trim($conf['fields'][$td['Field']]['tooltip']) != '')
		{
			$td['name'] .= ' <a title="'.$conf['fields'][$td['Field']]['tooltip'].'" href="#">?</a>';
		}
		
		
		$tab_header .= '
			<th class="head">
				'.$td['name'].'
				'.$actions.'
			</th>
		';
	}
	$new_btn = '';
	if($conf['ajaxkey'] != '' AND (!isset($conf['template']['show_new_modal_btns']) || $conf['template']['show_new_modal_btns'])){
		
		$link = '';
		
		if(isset($conf['template']['new_link_modal_renderfunction']))
		{
			$link .= $conf['template']['new_link_modal_renderfunction']($conf);
		}
		else
		{
			$link .= '
				'.draw_modalformlink(
					array(
						'label' => '<b>+</b>',
						'mode' => 'new',
						'modekey' => 'action',
						'formlink' => $phproot.'?'.$conf['link'].'&ajax=getform',
						'dellink' => $phproot.'?'.$conf['link'].'&action=del&ajax=del',
						'updatelink' => $phproot.'?'.$conf['link'].$conf['orderlink'].'&ajax=get_'.$conf['ajaxkey'],
						'idkey' => 'itemid',
						'id' => 0,
						'key' => $conf['ajaxkey'],
						'modal_label' => $conf['modal_label']
					)
				).'
			';
		}
		
		$new_btn = $link;
		/*
		$new_btn_row = '
			<tr>
				<td colspan="'.($x_h+1).'">
					'.$link.'
				</td>
			</tr>
		';
		*/
	}	
	
	//$tab_data .= $new_btn_row;
	
	$y = 0;
	if(is_array($data))
	foreach($data as $key => $row){
		
		if(isset($conf['template']['func_draw_tr']))
		{
			$tab_data .= $conf['template']['func_draw_tr']($conf, $header, $row);
		}
		else
		{
			$conf['key'] = $key;
			$y++;
			$x = 0;
			$tds = '';
			foreach($header as $td){
				
				if(isset($td['Field']))
				{
					if(isset($conf['fields'][$td['Field']]['display']) AND ($conf['fields'][$td['Field']]['display'] == 'no' || $conf['fields'][$td['Field']]['display'] == false)){
						continue;
					}
					$x++;
					
					$tds .= '<td>';

					if(isset($conf['fields'][$td['Field']]['date'])){
						$row[$td['Field']] = date($conf['fields'][$td['Field']]['date'], $row[$td['Field']]);
					}
					
					if($x == 1 AND $conf['ajaxkey'] != ''){
						$tds .= draw_modalformlink(
							array(
								'label' => $row[$td['Field']],
								'mode' => 'edit',
								'modekey' => 'action',
								'formlink' => $phproot.'?'.$conf['link'].'&ajax=getform',
								'dellink' => $phproot.'?'.$conf['link'].'&action=del&ajax=del',
								'updatelink' => $phproot.'?'.$conf['link'].$conf['orderlink'].'&ajax=get_'.$conf['ajaxkey'],
								'idkey' => 'itemid',
								'id' => $row['id'],
								'key' => $conf['ajaxkey'],
								'modal_label' => $conf['modal_label']
							)
						);
					}else{
						
						if(isset($conf['fields'][$td['Field']]['renderfunction']))
						{
							if(isset($conf['fields'][$td['Field']]['renderfunction_args']))
							{
								$tds .= $conf['fields'][$td['Field']]['renderfunction']($row[$td['Field']], $conf['fields'][$td['Field']]['renderfunction_args'], $row['id']);
							}
							else
							{
								$tds .= $conf['fields'][$td['Field']]['renderfunction']($row[$td['Field']], $row['id']);
							}
						}
						else
						{
							if(isset($td['renderfunction']))
							{
								if(isset($td['renderfunction_args']))
								{
									$tds .= $td['renderfunction']($row[$td['Field']], $td['renderfunction_args'], $row['id']);
								}
								else
								{
									$tds .= $td['renderfunction']($row[$td['Field']], $row['id']);
								}
							}
							else
							{
								$tds .= $row[$td['Field']];
							}
						}
						
					}
					
					$tds .= '</td>';
				}
				else
				{
					
					$tds .= '<td>';
					if(isset($td['renderfunction']))
					{
						if(isset($td['renderfunction_args']))
						{
							$tds .= $td['renderfunction']($row, $td['renderfunction_args']);
						}
						else
						{
							$tds .= $td['renderfunction']($row);
						}
					}
					else
					{
						$tds .= '';
					}
					$tds .= '</td>';
				}
				
			}
			
			if(isset($conf['link']) AND isset($conf['form']) AND isset($row['id']) AND (!isset($conf['template']['show_actions_btns']) || $conf['template']['show_actions_btns'])){
				$x++;
				$tds .= '<td>';
				
				if(isset($conf['template']['action_renderfunction']))
				{
					$tds .= $conf['template']['action_renderfunction']($conf, $row);
				}
				else
				{	
					$css_edit = '';
					$css_del = '';
					
					if(!isset($conf['template']['action_bsbtns']) || $conf['template']['action_bsbtns'])
					{
						$tds .= '
							'.draw_action_link($phproot.'?'.$conf['link'].'&action=edit&itemid='.$row['id'], 'edit', '' , 'btn btn-warning').'
							'.draw_action_link($phproot.'?'.$conf['link'].'&action=del&itemid='.$row['id'], 'del', '' , 'btn btn-danger').'
						';
						if(isset($conf['rank']) && $conf['rank'] == true)
						{
							if($key > 0)
							{
								$tds .= '
									'.draw_action_link($phproot.'?'.$conf['link'].'&itemid='.$row['id'].'&order=up', 'up', '' , 'btn btn-info').'
								';
							}
							
							if($key+1 < $conf['count'])
							{
								$tds .= '
									'.draw_action_link($phproot.'?'.$conf['link'].'&itemid='.$row['id'].'&order=down', 'down', '' , 'btn btn-info').'
								';
							}
						}
					}
					else
					{
						$tds .= '
							'.draw_action_link($phproot.'?'.$conf['link'].'&action=edit&itemid='.$row['id'], 'edit').'
							|
							'.draw_action_link($phproot.'?'.$conf['link'].'&action=del&itemid='.$row['id'], 'del').'
						';
						if(isset($conf['rank']) && $conf['rank'] == true)
						{
							if($key > 0)
							{
								$tds .= '
									|
									'.draw_action_link($phproot.'?'.$conf['link'].'&itemid='.$row['id'].'&order=up', 'up').'
								';
							}
							
							if($key+1 < $conf['count'])
							{
								$tds .= '
									|
									'.draw_action_link($phproot.'?'.$conf['link'].'&itemid='.$row['id'].'&order=down', 'down', '').'
								';
							}
						}
					}
					

				}
				$tds .= '</td>';
			}

			
			
			if(isset($_GET['ajax']) AND $_GET['ajax'] == 'get_'.$conf['ajaxkey'].'_'.$row['id']){
				echo $tds;
				exit;
			}
			
			$css = '';
			
			if(isset($_GET['itemid']) && $_GET['itemid'] == $row['id'])
			{
				$css .= ' selected';
			}
			
			$tab_data .= '
				<tr id="'.$conf['ajaxkey'].'_'.$row['id'].'" class="'.$css.'">
					'.$tds.'
				</tr>
			';
		}
	}
	
	//$tab_data .= $new_btn_row;
	
	if(isset($_GET['ajax']) AND $_GET['ajax'] == 'get_'.$conf['ajaxkey'].''){
		echo $tab_data;
		exit;
	}
	
	$pagination = '';
	if(isset($conf['link'])){
		if($conf['perpage'] != 0)
		$pagination = pages('draw', $conf['tab'], '', $conf['link'].$conf['orderlink'], 'page', $conf['perpage'], FALSE);
	}
	
	if(isset($conf['link']) AND isset($conf['form']) AND (!isset($conf['template']['show_actions_btns']) || $conf['template']['show_actions_btns'])){
		$tab_header .= '
			<th class="head">
				'.$lang['action'].'
				<br>
				<br>
			</th>
		';
	}
	
	if(!isset($conf['template']['show_new_btns']) || $conf['template']['show_new_btns'])
	{
		if(isset($conf['template']['new_btns_renderfunction']))
		{
			if(isset($conf['template']['new_btns_renderfunction_args']))
			{
				$new_btn .= $conf['template']['new_btns_renderfunction']($phproot.'?'.$conf['link'].'&action=new', $conf['template']['new_btns_renderfunction_args']);
			}
			else
			{
				$new_btn .= $conf['template']['new_btns_renderfunction']($phproot.'?'.$conf['link'].'&action=new');
			}
		}
		else
		{
			if(!isset($conf['template']['action_bsbtns']) || $conf['template']['action_bsbtns'])
			{
				$new_btn .= draw_action_link($phproot.'?'.$conf['link'].'&action=new', 'new', '', 'btn btn-success').'<br>';
			}
			else
			{
				$new_btn .= draw_action_link($phproot.'?'.$conf['link'].'&action=new', 'new').'<br>';
			}
		}
		$new_btn .= '&nbsp;';
	}
	
	$truncateBtn = '';
	if(isset($conf['truncate']) && $conf['truncate'])
	{
		if(!isset($conf['template']['action_bsbtns']) || $conf['template']['action_bsbtns'])
		{
			$truncateBtn .= draw_action_link($phproot.'?'.$conf['link'].'&action=truncate', 'truncate', '', 'btn btn-danger').'<br>';
		}
		else
		{
			$truncateBtn .= draw_action_link($phproot.'?'.$conf['link'].'&action=truncate', 'truncate').'<br>';
		}
		$truncateBtn .= '&nbsp;';
	}
	
	$backlink = '';
	if(isset($conf['backlink']) && $conf['backlink']!= '')
	{
		if(!isset($conf['backlinkCSS']))
		{
			$conf['backlinkCSS'] = '';
		}
		if(!isset($conf['backlinkLabel']))
		{
			$conf['backlinkLabel'] = get_label('back');
		}		
		$backlink = '<a href="'.$phproot.'?'.$conf['backlink'].'" class="btn btn-primary '.$conf['backlinkCSS'].'" title="'.get_label('back').'"><i class="fa fa-undo"></i></a><br>';
		//$backlink = '<a href="'.$phproot.'?'.$conf['backlink'].'" class="'.$conf['backlinkCSS'].'">'.$conf['backlinkLabel'].'</a><br>';
	}
	
	$infobox = '';
	if(isset($conf['infobox']) && $conf['infobox']!= '')
	{	
		$infobox = $conf['infobox'].'<br>';
	}
	
	$scrolllinks = array();
	$scrolllinks['top']['anchor'] = '<a name="top"></a>';
	$scrolllinks['top']['link'] = draw_action_link('#bottom', 'to_bottom', '', 'pull-right');
	$scrolllinks['bottom']['anchor'] = '<a name="bottom"></a>';
	$scrolllinks['bottom']['link'] = draw_action_link('#top', 'to_top', '', 'pull-right');
	
	if(!isset($conf['template']['scrolllinks']) || $conf['template']['scrolllinks'] == false)
	{	
		$scrolllinks['top']['anchor'] = '';
		$scrolllinks['top']['link'] = '';
		$scrolllinks['bottom']['anchor'] = '';
		$scrolllinks['bottom']['link'] = '';
	}
	
	$echo .= '
	'.$scrolllinks['top']['anchor'].'
	'.$infobox.'
	'.$backlink.'
	'.$pagination.'
	'.$scrolllinks['top']['link'].'
	<br>
	'.$new_btn.$truncateBtn.'
	<br><br>
	<table id="'.$conf['ajaxkey'].'" class="norm_tab table table-bordered">
		<thead>
			<tr>
				'.$tab_header.'
			</tr>
		</thead>
		<tbody>
				'.$tab_data.'
		</tbody>
	</table>
	'.$pagination.'
	'.$scrolllinks['bottom']['link'].'
	<br>
	'.$new_btn.$truncateBtn.'
	'.$scrolllinks['bottom']['anchor'].'
	<br>
	';
		
	
	if(isset($conf['template']['func_draw_content']) && $conf['template']['func_draw_content'] == false)
	{
		$echo = $echo;
	}
	else
	{
		$echo = draw_content($conf['header'], $echo);
	}

	if(isset($conf['ajax_update_time']))
	$libs['html_footer']['simple_table'.$conf['ajaxkey']] = '
		<script>
			function '.$conf['ajaxkey'].'_update(){ 
				
				var update = $.ajax({
					type: "POST",
					url: "'.$phproot.'?'.$conf['link'].$conf['orderlink'].'&ajax=get_'.$conf['ajaxkey'].'"
				});
				
				update.done(function ( msg ){
					$("#'.$conf['ajaxkey'].'").find("tbody").html(msg);
				});
				
				update.fail(function( msg ) {
					alert_message("alert-danger", "'.$lang['error'].'", msg.responseText);
				});
				
				setTimeout('.$conf['ajaxkey'].'_update, '.$conf['ajax_update_time'].');
				
			}
			'.$conf['ajaxkey'].'_update();
		</script>
	';
	
	return $echo;

}

?>