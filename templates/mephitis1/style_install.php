<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

class style extends StyleHTML{

	var $tpl_path = 'templates/mephitis1/';

	function __construct(){
	
		global $assetService, $phpRootPath, $httpRootPath, $tpl_vars;
		
		$assetService->SetVars($tpl_vars);
		
		$assetService->Add(
			array(
				'name' => 'bootstrap',
				'type' => 'css',
				'path' => $phpRootPath.'libs/bootstrap/scss/_bootstrap.scss',
				'replace' => array(
					'bootstrap/' => $phpRootPath.'libs/bootstrap/scss/bootstrap/'
				),
				'replaceAfter' => array(
					'../fonts/bootstrap/' => $httpRootPath.'libs/bootstrap/fonts/'
				),
				'standalone' => true
			)
		);
		
		$assetService->Add(
			array(
				'name' => 'font-awesome',
				'type' => 'css',
				'path' => $phpRootPath.'libs/font-awesome/css/font-awesome.min.css',
				'replace' => array(
					'../fonts/' => $httpRootPath.'libs/font-awesome/fonts/'
				),
			)
		);
		
		$assetService->Add(
			array(
				'name' => 'jquery-ui',
				'type' => 'css',
				'path' => $httpRootPath.$this->tpl_path.'jquery-ui/jquery-ui.min.css',
				'cache' => false
			)
		);
		
		$styleCss = $phpRootPath.$this->tpl_path.'style.scss';
		$assetService->Add(
			array(
				'name' => $this->tpl_name,
				'type' => 'css',
				'path' => $styleCss,
				'standalone' => true
			)
		);
	
	
		$assetService->Add(
			array(
				'name' => 'jquery',
				'type' => 'js',
				'path' => $phpRootPath.'libs/jquery/jquery.min.js'
			)
		);
		
		$assetService->Add(
			array(
				'name' => 'jquery-ui.js',
				'type' => 'js',
				'path' => $phpRootPath.$this->tpl_path.'jquery-ui/jquery-ui.min.js'
			)
		);
		
		$assetService->Add(
			array(
				'name' => 'bootstrap.js',
				'type' => 'js',
				'path' => $phpRootPath.'libs/bootstrap/js/bootstrap.min.js'
			)
		);
		
		$assetService->Add(
			array(
				'name' => 'page.js',
				'type' => 'js',
				'path' => $phpRootPath.$this->tpl_path.'js/basic.js'
			)
		);
	
	}
	
	function draw_site($content){
		
		global $lang, $actions, $config, $libs, $msg_stack, $httpRootPath, $assetService;
		
		$echo = '';
		
		$echo .= '<!doctype html>
		<html>
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset='.$config['charset'].'" />
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<meta name="viewport" content="width=device-width, initial-scale=1">
				
				<title>
					'.$lang['title'].'
				</title>
				

				<link rel="shortcut icon" href="'.$httpRootPath.'favicon.ico">
				
				'.$assetService->Render('css').'
				'.build_lib($libs['html_header']).'
			</head>
		<body>

		<nav class="navbar navbar-default" role="navigation">
		  <div class="container-fluid">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainnav">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand visible-xs" href="#">'.$lang['title'].'</a>
			</div>

			<div class="collapse navbar-collapse" id="mainnav">
				<ul class="nav navbar-nav">
					';
						foreach($actions as $action){
							if(isset($_GET['action']) AND $_GET['action'] == $action['action']){
								$echo .= '<li class="active"><a href="install.php?admin='.$action['action'].'">'.$action['lang'].'</a></li>';
							}else{ 
								if(isset($action['phproot']))
								{
									$echo .= '<li><a href="'.$action['phproot'].'" target="_blank">'.$action['lang'].'</a></li>';
								}
								else
								{	
									$echo .= '<li><a href="install.php?admin='.$action['action'].'">'.$action['lang'].'</a></li>';
								}
							}
						}
					$echo .= '
				</ul>
			</div>
		  </div>
		</nav>

		<div id="main">
			
			
			<div class="container">
				<div class="row">
					<div class="cold-md-12">
						
					</div>
				</div>
				<div class="row">
					<div class="cold-md-12">
						<div class="msg_stack">
						'; 
						if(isset($msg_stack))$echo .= $msg_stack;
						$echo .= '
						</div>
						'.$content.'
					</div>
				</div>
			</div>
			
		</div>
		
		'.$assetService->Render('js').'
		'.build_lib($libs['html_footer']).'
		
		</body>
		</html>
		';
		
		return $echo;
		
	}
	
	function draw_already_installed(){
	
		global $lang, $actions;
		
		$echo = '';
		
		$echo .= $lang['already_installed'].'<br>';
		
		foreach($actions as $action){
		
			if(isset($action['phproot']))
			{
				$echo .= '<a href="'.$action['phproot'].'?admin='.$action['action'].'" target="_BLANK">'.$action['lang'].'</a><br>';
			}
			else
			{
				$echo .= '<a href="install.php?admin='.$action['action'].'">'.$action['lang'].'</a><br>';
			}
		}
		
		return $echo;
	
	}
	
	function draw_not_installed(){
	
		global $lang, $actions;
		
		$echo = '';
		
		$echo .= $lang['not_installed'].'<br>';
		
		foreach($actions as $action){
		
			$echo .= '<a href="install.php?admin='.$action['action'].'">'.$action['lang'].'</a><br>';
		
		}
		
		return $echo;
	
	}
	
	function draw_need_auth($data){
	
		global $lang, $auth_file, $phproot;
		
		$echo = '';
		
		$echo .= $lang['need_auth_file'].'<br>
		<pre>'.$auth_file['name'].'</pre>
		<br>
		'.$lang['need_auth_file_content'].'
		<br>
		<pre>'.$data.'</pre>
		<br>
		<a href="'.$phproot.'" title="'.$lang['need_auth_file_next'].'"><button type="button" class="btn btn-success">'.$lang['need_auth_file_next'].'</button></a>
		';
			
		return $echo;
	
	}
	
	function draw_need_special_auth($data){
	
		global $lang, $auth_file, $phproot;
		
		$echo = '';
		
		$echo .= $lang['need_special_auth_file'].'<br>
		<pre>'.$auth_file['name'].'</pre>
		<br>
		'.$lang['need_special_auth_file_content'].'
		<br>
		<pre>'.$data.'</pre>
		<br>
		<a href="'.$phproot.'" title="'.$lang['need_auth_file_next'].'"><button type="button" class="btn btn-success">'.$lang['need_auth_file_next'].'</button></a>
		';
			
		return $echo;
	
	}
	
	function draw_logout(){
	
		global $lang, $auth_file;
		
		$echo = '';
		
		$echo .= $lang['deleted_auth_file'].'<br>
		<pre>
			'.$auth_file['name'].'
		</pre>
		';
			
		return $echo;
	
	}
	
	function draw_install($mode, $hiddenvars = null){
	
		global $lang;
		
		$echo = '';
		
		if($mode == 'form1'){
			$echo .= '
				<h3>'.$lang['step_1'].'</h3>
				'.$lang['step_1_info'].'
				<hr>
				<form action="install.php?admin=install" method="POST" class="form-horizontal" role="form">
					<div class="form-group">
						<label for="db_host" class="col-sm-2 control-label">'.$lang['db_host'].'</label>
						<div class="col-sm-10">
							<input type="text" name="db_host" class="form-control" id="db_host" placeholder="'.$lang['db_host'].'" value="localhost">
						</div>
					</div>
					
					<div class="form-group">
						<label for="db_name" class="col-sm-2 control-label">'.$lang['db_name'].'</label>
						<div class="col-sm-10">
							<input type="text" name="db_name" class="form-control" id="db_name" placeholder="'.$lang['db_name'].'">
						</div>
					</div>
					
					<div class="form-group">
						<label for="db_user" class="col-sm-2 control-label">'.$lang['db_user'].'</label>
						<div class="col-sm-10">
							<input type="text" name="db_user" class="form-control" id="db_user" placeholder="'.$lang['db_user'].'">
						</div>
					</div>
					
					<div class="form-group">
						<label for="db_pass" class="col-sm-2 control-label">'.$lang['db_pass'].'</label>
						<div class="col-sm-10">
							<input type="text" name="db_pass" class="form-control" id="db_pass" placeholder="'.$lang['db_pass'].'">
						</div>
					</div>
					
					<div class="form-group">
						<label for="prefix" class="col-sm-2 control-label">'.$lang['prefix'].'</label>
						<div class="col-sm-10">
							<input type="text" name="prefix" class="form-control" id="prefix" placeholder="'.$lang['prefix'].'" value="cms_">
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<input type="submit" class="btn btn-default" value="'.$lang['next'].'" name="send1">
						</div>
					</div>
				</form>
			';
		}
		if($mode == 'form2'){
						
			$echo .= '
				<h3>'.$lang['step_2'].'</h3>
				'.$lang['step_2_info'].'
				<hr>
				<form action="install.php?admin=install" method="POST" class="form-horizontal" role="form">
					';
					
					foreach($hiddenvars as $key => $value){
						$echo .= '<input type="hidden" name="'.$key.'" value="'.$value.'">';
					}
					
					$echo .= '
					<div class="form-group">
						<label for="name" class="col-sm-2 control-label">'.$lang['name'].'</label>
						<div class="col-sm-10">
							<input type="text" name="name" class="form-control" id="name" placeholder="'.$lang['name'].'" value="admin">
						</div>
					</div>
					
					<div class="form-group">
						<label for="pass" class="col-sm-2 control-label">'.$lang['pass'].'</label>
						<div class="col-sm-10">
							<input type="text" name="pass" class="form-control" id="pass" placeholder="'.$lang['pass'].'" value="123456">
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<input type="submit" class="btn btn-default" value="'.$lang['next'].'" name="send2">
						</div>
					</div>
				</form>
			';
			
		}
		if($mode == 'form3'){
			
			$tzlist = new timezones();
			$tzlist_echo = $tzlist->getlist('groups', 'name');

			
			$echo .= '
				<h3>'.$lang['step_3'].'</h3>
				'.$lang['step_3_info'].'
				<hr>
				<form action="install.php?admin=install" method="POST" class="form-horizontal" role="form">
					';
					
					foreach($hiddenvars as $key => $value){
						$echo .= '<input type="hidden" name="'.$key.'" value="'.$value.'">';
					}
					
					$echo .= '
					
					<div class="form-group">
						<label for="timezone" class="col-sm-2 control-label">'.$lang['timezone'].'</label>
						<div class="col-sm-10">
							<select name="timezone" class="form-control" id="timezone">
								';
								
								foreach($tzlist_echo as $continent => $citys){
								
									$echo .= '<optgroup label="'.$continent.'">';
									
									foreach($citys as $city => $zone){
									
										if($zone['zone'] == 'Europe/Berlin'){
											$echo .= '<option value="'.$zone['zone'].'" selected>'.$city.'</option>';;
										}else{
											$echo .= '<option value="'.$zone['zone'].'">'.$city.'</option>';;
										}
									}
									
									$echo .= '</optgroup>';
								
								}
								
								$echo .= '
							</select>
							
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<input type="submit" class="btn btn-default" value="'.$lang['next'].'" name="send3">
						</div>
					</div>
				</form>
			';
			
		}
		
		if($mode == 'installed'){
		
			$echo .= $lang['installed'];
			
		}
		
		return $echo;
	
	}
	
	function draw_install_dberror($mode){
	
		global $lang;
		
		$errormsg = get_label('install_dberror_'.$mode);
		
		if($errormsg == 'install_dberror_'.$mode)
		{
			$errormsg = $mode;
		}
		
		$echo = $lang['install_dberror'].'<br>'.$errormsg.'<br><a href="install.php?admin=install">'.$lang['back'].'</a>';
		
		return $echo;
		
	}
	
	function draw_wrong_login($user){
	
		global $lang;
	
		$echo = $lang['wrong_login_try'].'<br>';
		
		$echo .= $lang['ip'].': '.$user['ip'].'<br>';
		$echo .= $lang['host'].': '.$user['host'].'<br>';
		$echo .= $lang['request_method'].': '.$user['request_method'].'<br>';
		$echo .= $lang['time'].': '.date('Y-m-d, h:i:s', $user['time']).' ('.$user['time'].')<br>';
		$echo .= $lang['session_id'].': '.$user['session_id'].'<br>';
		
		return $echo;
	}
	
}

?>