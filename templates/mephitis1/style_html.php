<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

class StyleHTML{

	var $tpl_name = 'mephitis1';
	var $tpl_path = 'templates/mephitis1/';
	
	function __construct(){
		
		global $assetService, $phpRootPath, $httpRootPath, $tpl_vars;
		
		$assetService->SetVars($tpl_vars);
		
		$assetService->Add(
			array(
				'name' => 'bootstrap',
				'type' => 'css',
				'path' => $phpRootPath.'libs/bootstrap/scss/_bootstrap.scss',
				'replace' => array(
					'bootstrap/' => $phpRootPath.'libs/bootstrap/scss/bootstrap/'
				),
				'replaceAfter' => array(
					'../fonts/bootstrap/' => $httpRootPath.'libs/bootstrap/fonts/'
				),
				'standalone' => true
			)
		);
		
		$assetService->Add(
			array(
				'name' => 'font-awesome',
				'type' => 'css',
				'path' => $phpRootPath.'libs/font-awesome/css/font-awesome.min.css',
				'replace' => array(
					'../fonts/' => $httpRootPath.'libs/font-awesome/fonts/'
				),
			)
		);
		
		$assetService->Add(
			array(
				'name' => 'jquery-ui',
				'type' => 'css',
				'path' => $httpRootPath.$this->tpl_path.'jquery-ui/jquery-ui.min.css',
				'cache' => false
			)
		);
		
		$styleCss = $phpRootPath.$this->tpl_path.'style.scss';
		$assetService->Add(
			array(
				'name' => $this->tpl_name,
				'type' => 'css',
				'path' => $styleCss,
				'standalone' => true
			)
		);
		
		
		
		
		//----------------------------------------------
		
		$assetService->Add(
			array(
				'name' => 'jquery',
				'type' => 'js',
				'path' => $phpRootPath.'libs/jquery/jquery.min.js'
			)
		);
		
		$assetService->Add(
			array(
				'name' => 'jquery-ui.js',
				'type' => 'js',
				'path' => $phpRootPath.$this->tpl_path.'jquery-ui/jquery-ui.min.js'
			)
		);
		
		$assetService->Add(
			array(
				'name' => 'bootstrap.js',
				'type' => 'js',
				'path' => $phpRootPath.'libs/bootstrap/js/bootstrap.min.js'
			)
		);
		
		$assetService->Add(
			array(
				'name' => 'page.js',
				'type' => 'js',
				'path' => $phpRootPath.$this->tpl_path.'js/basic.js'
			)
		);
		
		$pageJS = '
			var msg_box_fadeout_timer = '.((getvar('msg_box_fadeout_timer'))*1000).';
		';
		
		$assetService->Add(
			array(
				'name' => 'page.addition.js',
				'type' => 'js',
				'content' => $pageJS
			)
		);
		

	}
	
	function draw_site($content){
		
		global $user, $lang, $config, $libs, $msg_stack, $searchengine, $phproot, $tpl_vars, $default_tpl_vars, $httpRootPath, $assetService;
		
		$echo = '';
		
		$logo = '<i class="fa fa-paw fa-5x"></i>';
		if(isset($default_tpl_vars))
		{
			if($tpl_vars['picture']['header_picture'] != '')$logo = '<img src="'.$httpRootPath.'index.php?file='.$tpl_vars['picture']['header_picture'].'&mode=full">';
		}
		
		$footer = getvar('footer');
		
		/*if(trim($footer) != '')
		{
			$footer .= '<br>';
		}*/
		
		$echo .= '<!doctype html>
		<html>
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset='.$config['charset'].'" />
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<meta name="viewport" content="width=device-width, initial-scale=1">
				
				<title>
					'.getvar('title').'
				</title>
				
				'.$assetService->Render('css').'
				'.build_lib($libs['html_header']).'
				
				<meta name="robots" content="'.getvar('meta_robots').'">
				<meta name="author" content="'.getvar('meta_author').'" >
				<meta name="description" content="'.getvar('meta_description').'">
				<meta name="keywords" content="'.getvar('meta_keywords').'">			
				
				'; 
				if(getvar('shortcut_icon') != ''){$echo .= '<link rel="shortcut icon" href="'.getvar('shortcut_icon').'">';}
				$echo .= '
				
			</head>
		<body>
		<div id="wrap">
			<a name="top"></a>
			<header class="header hidden-xs">
				<a href="'.$phproot.'">'.$logo.'</a>
			</header>

			<nav class="navbar navbar-default" role="navigation">
			  <div class="container-fluid">
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainnav">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				  <a class="navbar-brand visible-xs" href="'.$phproot.'">'.getvar('title').'</a>
				</div>

				<div class="collapse navbar-collapse" id="mainnav">
					'.draw_nav().'
					'.$searchengine->draw_form('navbar-form navbar-right').'
				</div>
			  </div>
			</nav>
			'.draw_action_link('#bottom', 'to_bottom', '', 'pull-right pagenav-top').'
			<div id="wrap-page">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="msg_stack">
							'; 
							if(isset($msg_stack))$echo .= $msg_stack;
							$echo .= '
							</div>
						</div>
					</div>
				</div>			
				'.$content.'
			</div>
			'.draw_action_link('#top', 'to_top', '', 'pull-right pagenav-bottom').'
		</div>
		
		<footer class="footer">
			<div class="container">
				<div class="col-xs-6">
					'.$lang['footer'].'
				</div>
				<div class="col-xs-6 text-right">
					'.$footer.'
				</div>
			</div>
		</footer>
		
		<a name="bottom"></a>
		
		'.$assetService->Render('js').'
		'.build_lib($libs['html_footer']).'
		</body>
		</html>
		';
		
		return $echo;
		
	}
	
	function draw_iframe($content){
		
		global $user, $lang, $config, $libs, $msg_stack, $searchengine, $phproot, $tpl_vars, $default_tpl_vars, $httpRootPath, $assetService;
		
		$echo = '';
		
		
		$echo .= '<!doctype html>
		<html>
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset='.$config['charset'].'" />
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<meta name="viewport" content="width=device-width, initial-scale=1">
				
				<title>
					'.getvar('title').'
				</title>
				
				'.$assetService->Render('css').'
				'.build_lib($libs['html_header']).'

				<script>
					var msg_box_fadeout_timer = '.((getvar('msg_box_fadeout_timer'))*1000).';
				</script>
			</head>
		<body>
			
		'.$content.'

		'.$assetService->Render('js').'
		'.build_lib($libs['html_footer']).'
		</body>
		</html>
		';
		
		return $echo;
		
	}
}

?>