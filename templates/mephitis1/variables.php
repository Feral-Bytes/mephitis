<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

$tpl_vars = array();

$tpl_vars['picture']['header_picture'] = '';

$tpl_vars['color']['tpl_gradient_color1'] = '#C0C0C0';
$tpl_vars['color']['tpl_gradient_color2'] = '#F3F3F3';

$tpl_vars['size']['font'] = '10px';

?>