<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_mediaselect($name, $id, $link, $files, $mode = '')
{
	global $phproot, $libs, $last_upload;
	
	$echo = '';
	$form = '';
	
	if(isset($_GET['ajax']) && isset($_GET['var_name']))
	{
		$name = htmlspecialchars($_GET['var_name']);
		
	}
	
	$value = '';
	$thumb = '';
	if($id != '' || $id != 0 || isset($_GET['ajax_select_id']) || $last_upload)
	{
		$value = $id;
		if(isset($_GET['ajax_select_id']))
		{
			$id = $_GET['ajax_select_id'];
		}
		if(isset($last_upload))
		{
			if(is_array($last_upload))
			{
				foreach($last_upload as $upload)
				{
					$id = $upload;
				}
			}
			else
			{
				$id = $last_upload;
			}
		}
		if($id != '' && $id != 0)
		{
			$file = file_info($id);
			
			if(is_array($file))
			{
				$thumb = draw_mediaselect_thumb($file);
			}
			else
			{
				$thumb = get_label('404');
			}
			$thumb .= '<a id="mediaselect_'.$name.'_remove" class="btn btn-default">'.get_label('remove').'</a>';
		}
	}
	
	$form .= '
			<div class="mediaselect_thumb_'.$name.'">
				'.$thumb.'
			</div>
			<hr>
			<a href="#" id="mediaselect_'.$name.'" class="btn btn-default" data-toggle="modal" data-target="#Modal_'.$name.'">'.get_label('media_filebrowser').'</a>
	';
	
	$content = ''.$form.'<input id="'.$name.'" name="'.$name.'" type="hidden" value="'.$id.'">';
	
	if(isset($_GET['ajax']))
	{
		if($_GET['ajax'] == 'get_files_'.$name)
		{
			echo draw_mediaselect_filebrowser($name, $files, $link);
			echo '
				<script>
					var dir_'.$name.' = '.$files['dir'].';
				</script>				
			';
			exit;
		}		
		if($_GET['ajax'] == 'mediaselect' || $_GET['ajax'] == 'upload')
		{
			echo $content;
			exit;
		}		
	}
	
	draw_mediaselect_modal_js($name, $link, $mode);
	draw_mediaselect_lib_dir_list($name, $link);
	
	$echo = '
		<div class="mediaselect mediaselect_'.$name.' alert alert-info">
			'.$content.'
		</div>
	';
	
	return $echo;
	
}

function draw_mediaselect_modal_js($name, $link, $mode)
{
	
	global $libs;
	
	$libs['html_footer'][$name] = '
		<div class="modal fade" id="Modal_'.$name.'" tabindex="-1" role="dialog" aria-labelledby="Modal_'.$name.'_Label" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="close_'.$name.'" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<h4 class="modal-title" id="Modal_'.$name.'_Label">'.get_label('media_filebrowser').'</h4>
					</div>
					<div class="modal-body">
						
					</div>
					<div class="modal-footer">
						<button type="button" id="close_'.$name.'" class="btn btn-default" data-dismiss="modal">'.get_label('close').'</button>
					</div>
				</div>
			</div>
		</div>
		<script>
		
			$(document).on("click", "a#mediaselect_'.$name.'_remove", function(){ 
				$(".mediaselect_thumb_'.$name.'").empty();
				$("input#'.$name.'").val("");
			});			
		
			$(document).on("click", "a#mediaselect_'.$name.'", function(){ 
				
				var target =  "#Modal_'.$name.'";
				
				var request  = $.ajax({
					type: "POST",
					url: "'.$link.'&ajax=get_files_'.$name.'"
				});
				
				$(target).find(".modal-body").empty();			
				
				$(target).find(".modal-body").html(\'<i class="fa fa-spinner fa-pulse"></i>\'); 
				
				request.done(function( msg ) {
					$(target).find(".modal-body").html(msg); 
				});
			});		
		
			$(document).on("click", "a.mediaselect_'.$name.'", function(){ 

				var target =  ".mediaselect_'.$name.'";
				
				var request  = $.ajax({
					type: "POST",
					url: "'.$link.'&ajax=mediaselect&var_name='.$name.'&ajax_select_id=" + $(this).attr("id")
				});			
				
				$(target).html(\'<i class="fa fa-spinner fa-pulse"></i>\'); 
				
				request.done(function( msg ) {
					$(target).html(msg); 
				});
			});
			
			$(document).on("click", "button#'.$name.'_upload", function(){ 

				var target =  "#Modal_'.$name.'";
				
				var formData = new FormData($("form#upload_form_'.$name.'")[0]);
				
				$(target).find(".modal-body").empty();
				
				$(".mediaselect_thumb_'.$name.'").html( "'.get_label('uploading').'<br><br><div class=\"progress\"><div id=\"bar_mediaselect_'.$name.'\" class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"0\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"min-width: 2em;\"></div></div>" );
				$(target).find(".modal-body").html( "'.get_label('uploading').'<br><br><div class=\"progress\"><div id=\"bar_modal_'.$name.'\" class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"0\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"min-width: 2em;\"></div></div>" );

				var bar_modal_'.$name.' = $("#bar_modal_'.$name.'");
				var bar_mediaselect_'.$name.' = $("#bar_mediaselect_'.$name.'");
				
				var request  = $.ajax({
					type: "POST",
					url: "'.$link.'&ajax=upload&var_name='.$name.'&file_dir=" + dir_'.$name.' + "&ajax_select_id=0",
					processData: false,
					contentType: false,					
					data: formData,
					xhr: function() {
						var xhr = new window.XMLHttpRequest();
						
						xhr.upload.addEventListener("progress", function(evt) {
							if (evt.lengthComputable) {
								var percentComplete = evt.loaded / evt.total;
								percentComplete = parseInt(percentComplete * 100);
								
								bar_modal_'.$name.'.attr(\'aria-valuenow\', percentComplete);
								bar_modal_'.$name.'.width(percentComplete+"%");
								bar_modal_'.$name.'.html(percentComplete+"%");
								
								bar_mediaselect_'.$name.'.attr(\'aria-valuenow\', percentComplete);
								bar_mediaselect_'.$name.'.width(percentComplete+"%");
								bar_mediaselect_'.$name.'.html(percentComplete+"%");
								
								
								if (percentComplete === 100) {
									
								}
							}
						}, false);
						return xhr;
					},					
				});			
				
				request.done(function( msg ) {
					$(target).modal("hide");
					$(".mediaselect_'.$name.'").html(msg); 
				});
				
				
			});			
			
		</script>
	';
	
	if($mode == 'modal')
	{
		$libs['html_footer'][$name.'_modal_jumper'] = '
			<script>
				$("#Modal_'.$name.'").appendTo("body");
			</script>
		';
	}
	
}

function draw_media($info, $path = false)
{
	global $httpRootPath;
	
	if(is_numeric($info))
	{
		$info = mediainfo($info);
	}
	
	if(!is_array($info))
	{
		$info = mediainfo($info);
	}
	
	if($path === true)
	{
		return 'index.php?file='.$info['id'].'&mode=full';
	}
	
	$echo = '';
	
	if(is_array($info))
	{
		$type = explode('/', $info['type']);
		
		if($type[0] == 'image')
		{
			$echo = '<a href="'.$httpRootPath.'index.php?file='.$info['id'].'&mode=full" target="_BLANK">'.draw_mediaselect_thumb($info).'</a>';
		}
		else
		{
			$echo = '<a href="'.$httpRootPath.'index.php?file='.$info['id'].'&mode=full" target="_BLANK">'.draw_mediaselect_thumb($info).'</a>';
		}
		return $echo;
	}
	else
	{
		return '';
	}
}

function draw_mediaselect_thumb($info, $css = '')
{
	
	global $httpRootPath;
	
	$echo = '';
	
	$type = explode('/', $info['type']);
	
	if(!isset($info['title']))
	{
		$info['title'] = $info['file_name'];
	}
	
	if($type[0] == 'image')
	{
		$echo = '<img src="'.$httpRootPath.'index.php?file='.$info['id'].'&mode=user_thumb" class="thumbnail img-responsive '.$css.'" alt="'.$info['title'].'" title="'.$info['title'].'">';
	}
	else
	{
		$echo = '<i class="fa fa-download fa-2x" title="'.$info['title'].'"></i> '.$info['title'].'<br>';
	}
	
	return $echo;
}

function draw_mediaselect_filebrowser_tinymce($name, $files, $link)
{
	global $libs, $httpRootPath;
	
	$echo = '';
	
	$pictures = '';
	foreach($files['files'] as $file)
	{	
		$pictures .= '
			<div class="tinymce_uploader_thumb">
				<a href="#" id="'.$file['id'].'" class="mediaselect_file_'.$name.'_thumb_link">'.draw_mediaselect_thumb($file, 'pull-left').'</a>
				<a id="'.$file['id'].'" class="mediaselect_file_'.$name.'_thumb_link" href="#">'.get_label('thumb_link').'</a>
				<br>
				<a id="'.$file['id'].'" class="mediaselect_file_'.$name.'_thumb" href="#">'.get_label('thumb_nolink').'</a>
				<br>
				<a id="'.$file['id'].'" class="mediaselect_file_'.$name.'_full" href="#">'.get_label('fullsize').'</a>
			</div>
		';
	}
	
	$echo .= '
		<div id="tiny_mce_bar_'.$name.'">
			'.draw_mediaselect_uploadform($name, $link.'&file_dir='.$files['dir']).'
		</div>
		<hr>
		'.draw_mediaselect_dir_list($name, $files, $link).'
		<hr>
		<div id="pictures" class="clearfix">
			'.$pictures.'
		</div>
		<script>
			var dir_'.$name.' = '.$files['dir'].';
		</script>
	';		
	
	if(isset($_GET['ajax']))
	{
		if($_GET['ajax'] == 'upload')
		{
			echo $echo;
		}
		if($_GET['ajax'] == 'get_files_'.$name.'')
		{
			echo $echo;
		}
		exit;
	}
	
	$echo = '
		<div class="tinymce_uploader" id="filelist_'.$name.'">
			'.$echo.'
		</div>
	';	
	
	$libs['html_footer']['tinymce'] = '<script type="text/javascript" src="'.$httpRootPath.'libs/tinymce/tinymce.min.js"></script>';
	$libs['html_footer']['tinymce_vars'] = '<script>var tinymceHttpRootPath = "'.$httpRootPath.'";</script>';
	$libs['html_footer']['tinymce_uploader'] = '
		<script>
			$(document).on("click", ".mediaselect_file_'.$name.'_thumb_link", function(){
				top.tinymce.activeEditor.selection.setContent("<a href=\"'.$httpRootPath.'index.php?file=" + this.id + "&mode=full\" target=\"_BLANK\"><img src=\"'.$httpRootPath.'index.php?file=" + this.id + "&mode=user_thumb\"></a>");
				top.tinymce.activeEditor.windowManager.close();
				
			});
			
			$(document).on("click", ".mediaselect_file_'.$name.'_thumb", function(){
				top.tinymce.activeEditor.selection.setContent("<img src=\"'.$httpRootPath.'index.php?file=" + this.id + "&mode=user_thumb\">");
				top.tinymce.activeEditor.windowManager.close();
			});	
			
			$(document).on("click", ".mediaselect_file_'.$name.'_full", function(){
				top.tinymce.activeEditor.selection.setContent("<img src=\"'.$httpRootPath.'index.php?file=" + this.id + "&mode=full\">");
				top.tinymce.activeEditor.windowManager.close();
			});			
			
			$(document).on("click", "button#'.$name.'_upload", function(){ 
				
				var formData = new FormData($("form#upload_form_'.$name.'")[0]);
				
				$(".tinymce_uploader").html( "'.get_label('uploading').'<br><br><div class=\"progress\"><div id=\"bar_mediaselect_'.$name.'\" class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"0\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"min-width: 2em;\"></div></div>" );

				var bar_mediaselect_'.$name.' = $("#bar_mediaselect_'.$name.'");
				
				var request  = $.ajax({
					type: "POST",
					url: "'.$link.'&ajax=upload&file_dir=" + dir_'.$name.' + "&ajax_select_id=0",
					processData: false,
					contentType: false,					
					data: formData,
					xhr: function() {
						var xhr = new window.XMLHttpRequest();
						
						xhr.upload.addEventListener("progress", function(evt) {
							if (evt.lengthComputable) {
								var percentComplete = evt.loaded / evt.total;
								percentComplete = parseInt(percentComplete * 100);
								
								bar_mediaselect_'.$name.'.attr(\'aria-valuenow\', percentComplete);
								bar_mediaselect_'.$name.'.width(percentComplete+"%");
								bar_mediaselect_'.$name.'.html(percentComplete+"%");
								
								if (percentComplete === 100) {
									
								}
							}
						}, false);
						return xhr;
					},					
				});	
				request.done(function( msg ) {
					$(".tinymce_uploader").html(msg); 
				});
				
				
			});				
		</script>
	';
	
	draw_mediaselect_lib_dir_list($name, $link);
	
	return $echo;
}

function draw_mediaselect_filebrowser($name, $files, $link)
{
	
	$echo = '';
	
	foreach($files['files'] as $file)
	{
		$echo .= '<a href="#" id="'.$file['id'].'" class="mediaselect_'.$name.'" data-dismiss="modal">'.draw_mediaselect_thumb($file, 'pull-left').'</a>';
	}
	
	$echo = '
		<div id="filelist_'.$name.'">
			'.draw_mediaselect_uploadform($name, $link, $link.'&file_dir='.$files['dir']).'
			<hr>
			'.draw_mediaselect_dir_list($name, $files, $link).'
			<hr>
			<div class="clearfix">
				'.$echo.'
			</div>
		</div>
	';
	
	return $echo;
}

function draw_mediaselect_uploadform($name, $link)
{	
	$echo = '';
	
	if(!check_group_access(getvar('file_upload_groups'), 'whitelist'))
	{
		$echo .= '
			<div class="alert alert-info">
				'.get_label('no_upload_permission').'
			</div>
		';
		return $echo;
	}
	
	$echo .= '
		<div class="upload_'.$name.'">
			<form id="upload_form_'.$name.'" enctype="multipart/form-data" action="'.$link.'&ajax=upload" method="POST">
				<input name="'.$name.'[]" id="'.$name.'" type="file" multiple>
				<br>
				<button class="btn btn-primary" type="button" id="'.$name.'_upload">'.get_label('upload').'</button>
			</form>
		</div>
	';
	
	return $echo;
}

function draw_mediaselect_lib_dir_list($name, $link)
{
	global $libs;
	
	$libs['html_footer']['mediaselect_file_dirs_'.$name] = '
		<script>
			
			$(document).on("click", "a.file_dir_link_'.$name.'", function(){ 
			
				var request  = $.ajax({
					type: "POST",
					url: "'.$link.'&ajax=get_files_'.$name.'&file_dir="	+ $(this).attr("id")				
				});			

				request.done(function( msg ) {
					$("#filelist_'.$name.'").html(msg); 
				});
			});				
		</script>
	';	
}

function draw_mediaselect_dir_list($name, $files, $link)
{
	global $phproot, $libs;
	
	
	$echo = '
		<ul class="cms_content_tree" id="cms_content_tree">
			<li>
			<a href="#" class="file_dir_link_'.$name.'" id="'.$files['root'].'"><span class="cms_content_tree_dir">'.$files['root_name'].'</span>&nbsp;<span class="badge">'.$files['rootcount'].'</span></a>
			'.draw_mediaselect_files_tree($name, $files['tree']->GetTree(), 'dir', null, $link).'
			</li>
		</ul>
	';
	return $echo;
}

function draw_mediaselect_files_tree($name, $tree, $mode = 'dir', $parent, $link){

	global $phproot, $lang;

	$echo = '';

	if(is_array($tree))
	foreach($tree as $element){
		$echo .= '<li>';
		if($mode == 'dir'){
			if(isset($_GET['file_dir']) && $_GET['file_dir'] == $element['id'])
			{
				$echo .= '<i class="fa fa-folder-open-o"></i>&nbsp;<span class="cms_content_tree_dir">'.$element['name'].'&nbsp;<span class="badge">'.CountDirFiles($element['id']).'</span></span>&nbsp;';
			}
			else
			{
				$echo .= '<a href="#" id="'.$element['id'].'" class="file_dir_link_'.$name.'"><i class="fa fa-folder-o"></i>&nbsp;<span class="cms_content_tree_dir">'.$element['name'].'&nbsp;<span class="badge">'.CountDirFiles($element['id']).'</span></span></a>&nbsp;';
			}
		}
		if(isset($element['subs']) AND is_array($element['subs'])){
			$echo .= draw_mediaselect_files_tree($name, $element['subs'], 'dir', $element, $link);
		}	
		$echo .= '</li>';
	}
	

	$echo = '<ul>'.$echo.'</ul>';
	
	return $echo;
}



?>