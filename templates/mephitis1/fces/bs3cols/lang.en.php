<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */
//

$lang[$element][$element] = 'Bootstrap 2 Columns';

$lang[$element]['col1'] = 'Left ';
$lang[$element]['col2'] = 'Middle';
$lang[$element]['col3'] = 'Right';

$lang[$element]['cssCol1'] = 'Left Column sizes';
$lang[$element]['cssCol2'] = 'Middle Column sizes';
$lang[$element]['cssCol3'] = 'Right Column sizes';

?>