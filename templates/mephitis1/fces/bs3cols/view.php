<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_view_bs3cols($ce){

	global $lang;

	$echo = '';
	
	$echo = '
		<div class="row">
			<div class="'.str_replace(',', ' ', @$ce['content']->cssCol1).'">
				'.DrawGrid($ce, 'Col1').'
			</div>
			<div class="'.str_replace(',', ' ', @$ce['content']->cssCol2).'">
				'.DrawGrid($ce, 'Col2').'
			</div>
			<div class="'.str_replace(',', ' ', @$ce['content']->cssCol3).'">
				'.DrawGrid($ce, 'Col3').'
			</div>
		</div>
	';
	
	return $echo;
}

?>