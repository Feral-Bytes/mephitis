<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */
//

$lang[$element][$element] = 'Bootstrap 3 Spaltem';

$lang[$element]['col1'] = 'Linke Spalte';
$lang[$element]['col2'] = 'Mittlere Spalte';
$lang[$element]['col3'] = 'Rechte Spalte';

$lang[$element]['cssCol1'] = 'Linke Spalte Gr&ouml;&szlig;en';
$lang[$element]['cssCol2'] = 'Mittlere Spalte Gr&ouml;&szlig;en';
$lang[$element]['cssCol3'] = 'Rechte Spalte Gr&ouml;&szlig;en';


?>