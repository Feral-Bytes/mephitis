<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function fce_settings_bscol($data)
{
	
	$fields = array();
	
	$fields['name'] = array(
		'type' => 'text'
	);
	
	$cssOptions = array();
	
	$cssCols = array('xs', 'sm', 'md', 'lg');
	$cssColWidths = array(1,2,3,4,5,6,7,8,9,10,11,12);
	
	foreach($cssCols as $cssCol)
	{
		foreach($cssColWidths as $cssColWidth)
		{
			$colCss = 'col-'.$cssCol.'-'.$cssColWidth;
			$cssOptions[$colCss] = array(
				'lang' => $colCss
			);
		}
	}
	
	
	$fields['css'] = array(
		'type' => 'select',
		'multiple' => '1',
		'options' => $cssOptions
	);
	
	return array(
		'fields' => $fields
	);
}

?>