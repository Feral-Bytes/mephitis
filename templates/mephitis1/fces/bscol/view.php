<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_view_bscol($ce){

	global $lang;

	$echo = '';
	
	$css = @(string)$ce['content']->css;
	$css = str_replace(',', ' ', $css);
	
	$echo = '
		<div class="'.$css.'">
			'.DrawGrid($ce, 'main').'
		</div>
	';
	
	return $echo;
}

?>