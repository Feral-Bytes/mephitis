<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_view_module($ce){

	global $installedModules, $modulename, $moduleargs, $moduleinstance, $mysqli, $config, $lang, $phpRootPath, $phpRootPath, $phproot, $link, $user, $msg_stack, $autoform_success, $row;

	$echo = '';
	
	$content = '';
	$modulecontent = '';
	
	$moduleinstance = @trim((string)$ce['content']->moduleinstance);
	$modulename = @trim((string)$ce['content']->module);
	$moduleargs = @trim((string)$ce['content']->modulearguments);
	$border = @(string)$ce['content']->border;
	$cssClasses = @(string)$ce['content']->cssClasses;
	
	if(isset($installedModules[$modulename]) && $installedModules[$modulename]['active'] == "1")
	{
		include(include_file('module_main', $modulename));
		if(!isset($_GET['ajax']) OR $_GET['ajax'] == ''){
			if($modulecontent != '' || !isset($_GET['action']) || !($_GET['action'] == 'new' || $_GET['action'] == 'edit' || $_GET['action'] == 'del'))
			{
				$content .= $modulecontent;
			}
		}
	}
	
	if($border){
		$echo = '
			<div class="container">
				'.draw_item($ce['name'], $content, $cssClasses).'
			</div>
		';
	}else{

		$echo = '
			'.$content.'
		';
	}
	
	return $echo;
}

?>