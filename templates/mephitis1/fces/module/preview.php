<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_preview_module($ce){

	global $lang;

	$echo = '';
	
	$moduleinstance = @trim((string)$ce['content']->moduleinstance);
	$modulename = @trim((string)$ce['content']->module);
	$moduleargs = @trim((string)$ce['content']->modulearguments);
	$border = @(string)$ce['content']->border;
	
	
	$echo = '
		'.get_label('module').': '.$modulename.'<br>
		'.get_label('modulearguments').': '.$moduleargs.'<br>
		'.get_label('moduleinstance').': '.$moduleinstance.'
	';
	
	return $echo;
}

?>