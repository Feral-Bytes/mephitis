<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function fce_settings_module($data)
{
	
	global $installedModules;
	
	$fields['name'] = array(
		'type' => 'text',
	);
	
	$fields['border'] = array(
		'type' => 'checkbox',
	);
	
	$modules = array();
	foreach($installedModules as $module)
	{
		if($module['active'])
		{
			$modules[$module['name']] = array('lang' => get_label($module['name'], $module['name']));
		}
	}
	
	$fields['module'] = array(
		'type' => 'select',
		'options' => $modules
	);
	
	$fields['moduleinstance'] = array(
		'type' => 'text',
	);
	
	$fields['modulearguments'] = array(
		'type' => 'text',
	);
	
	
	return array(
		'fields' => $fields
	);
}

?>