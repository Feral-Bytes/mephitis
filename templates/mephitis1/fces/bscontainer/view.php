<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_view_bscontainer($ce){

	global $lang;

	$echo = '';
	
	$echo = '
		<div class="container">

			'.DrawGrid($ce, 'main').'

		</div>
	';
	
	return $echo;
}

?>