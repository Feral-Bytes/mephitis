<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_view_bscarouselitem($ce){

	global $lang;

	$echo = '';
	
	$css = array();
	$css[] = 'item';
	if($ce['i'] == 1)
	{
		$css[] = 'active';	
	}
	
	$echo = '
		<div class="'.implode(' ', $css).'" style="background-image: url(\''.draw_media(@(int)$ce['content']->picture, true).'\')">
			<div class="carousel-caption">
				'.@(string)$ce['content']->caption.'
			</div>
		</div>
	';
	
	return $echo;
}

?>