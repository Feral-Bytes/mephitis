<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function fce_settings_bscarouselitem($data)
{
	
	$fields['name'] = array(
		'type' => 'text',
	);
	
	$fields['picture'] = array(
		'type' => 'media',
		'path' => 'files/content/',
		'module' => 'content'
	);
	
	$fields['caption'] = array(
		'type' => 'textarea',
		'wysiwyg' => 'html'
	);
	
	
	return array(
		'fields' => $fields,
		'allowedParents' => 'bscarousel'
	);
}

?>