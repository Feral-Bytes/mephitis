<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function fce_settings_text($data)
{
	
	$fields['name'] = array(
		'type' => 'text',
	);
	
	$fields['content'] = array(
		'type' => 'textarea',
		'wysiwyg' => 'html'
	);
	
	$fields['border'] = array(
		'type' => 'checkbox',
	);
	
	
	return array(
		'fields' => $fields
	);
}

?>