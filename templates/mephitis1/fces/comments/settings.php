<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function fce_settings_comments($data)
{
	
	global $installedModules;
	
	$fields['name'] = array(
		'type' => 'text',
	);
	
	$fields['accsess_control'] = array(
		'type' => 'select',
		'options' => array(
			'global' => array(
				'lang' => get_label('global')
			),
			'local' => array(
				'lang' => get_label('local')
			)
		)
	);
	
	$fields['guests_can_write'] = array(
		'type' => 'checkbox',
		'category' => 'cat_access'
	);
	
	$fields['members_can_write'] = array(
		'type' => 'checkbox',
		'category' => 'cat_access'
	);
	
	$fields['requires_mod_check'] = array(
		'type' => 'checkbox',
		'category' => 'cat_access'
	);	
	
	
	$fields['draw_mode'] = array(
		'type' => 'select',
		'options' => array(
			'content' => array(
				'lang' => get_label('content')
			),
			'item' => array(
				'lang' => get_label('item')
			)
		)
	);
	
	$fields['object'] = array(
		'type' => 'text',
	);
	
	$fields['object_id'] = array(
		'type' => 'text',
	);
	
	
	return array(
		'fields' => $fields
	);
}

?>