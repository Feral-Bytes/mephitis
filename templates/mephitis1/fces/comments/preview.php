<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_preview_comments($ce){

	global $lang;

	$echo = '';
	
	$conf = array();
	
	$conf['header'] = @trim((string)$ce['name']);
	
	$conf['accsess_control'] = @trim((string)$ce['content']->accsess_control);
	if($conf['accsess_control'] == 'local')
	{
		$conf['guests_can_write'] = @trim((string)$ce['content']->guests_can_write);
		$conf['members_can_write'] = @trim((string)$ce['content']->members_can_write);
	}
	$conf['requires_mod_check'] = @trim((string)$ce['content']->requires_mod_check);
	$conf['draw_mode'] = @trim((string)$ce['content']->draw_mode);
	$conf['object'] = @trim((string)$ce['content']->object);
	$conf['object_id'] = @trim((string)$ce['content']->object_id);
	
	
		
	$echo = '
		'.get_label('comments').': '.$conf['header'] .'<br>
		'.get_label('object').': '.$conf['object'].'<br>
		'.get_label('object_id').': '.$conf['object_id'].'
	';
	
	return $echo;
}

?>