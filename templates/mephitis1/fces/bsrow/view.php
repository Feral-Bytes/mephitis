<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_view_bsrow($ce){

	global $lang;

	$echo = '';
	
	$echo = '
		<div class="row">
			'.DrawGrid($ce, 'main').'
		</div>
	';
	
	return $echo;
}

?>