<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_view_bscarousel($ce)
{

	global $config;
	
	$echo = '';
	
	$indicators	= '';
	
	
	$childContents = db_get(
		$ce['tab'],
		array(
			'parent' => $ce['id'],
			'page' => $ce['page'],
			'pos' => 'main'
		),
		'ORDER BY rank ASC'
	);
	
	for($i = 0; $i < count($childContents); $i++)
	{
		if($i == 0)
		{
			$indicators .= '<li data-target="#carousel-'.$ce['name'].'" data-slide-to="'.$i.'" class="active"></li>';
		}
		else
		{
			$indicators .= '<li data-target="#carousel-'.$ce['name'].'" data-slide-to="'.$i.'"></li>';
		}
	}
	
	$echo = '	
		<div id="carousel-'.$ce['name'].'" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				'.$indicators.'
			</ol>

			
			<div class="carousel-inner">
				'.DrawGrid($ce, 'main').'
			</div>


			<a class="left carousel-control" href="#carousel-'.$ce['name'].'" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left"></span>
			</a>
			<a class="right carousel-control" href="#carousel-'.$ce['name'].'" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right"></span>
			</a>
		</div>
	';

	
	return $echo;
}

?>