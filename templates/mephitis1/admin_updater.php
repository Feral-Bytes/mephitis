<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_updater($updates)
{
	global $phproot, $libs;
	
	$update_list = '';
	if(is_array($updates))
	{
		
		foreach($updates as $key => $update)
		{
			
			$actions = '
				<a href="#" id="'.$key.'" class="btn btn-danger mep_del_update" title="'.get_label('del').'"><i class="fa fa-trash-o"></i></a>&nbsp;
				<a href="'.$phproot.'?admin=updater&update='.$key.'" id="'.$key.'" class="btn btn-info mep_install_update" title="'.get_label('install').'"><i class="fa fa-download"></i></a>&nbsp;
			';
			
			$update_list .= '
				<tr>
					<td>
						'.$update['name'].'
					</td>
					<td>
						'.$actions.'
					</td>					
				</tr>
			';
		}
		
		$update_list = '
			<table class="norm_tab table table-bordered">
				<thead>
					<tr>
						<th class="head">
							'.get_label('name').'
						</th>					
						<th class="head">
							'.get_label('action').'
						</th>
					</tr>
				</thead>
				<tbody>
					'.$update_list.'
				</tbody>
			</table>
		';
	}
	
	$echo = '
		<div class="updater">
			<form id="updater" enctype="multipart/form-data" action="'.$phproot.'?admin=updater&ajax=upload" method="POST">
				<input name="file" id="file" type="file">
				<br>
				<button class="btn btn-primary" type="button" id="file_upload">'.get_label('upload').'</button>
			</form>
			<hr>
			'.$update_list.'
		</div>
	';
	
	if(isset($_GET['ajax']))
	{
		if($_GET['ajax'] == 'upload' || $_GET['ajax'] == 'update')
		{
			echo $echo;
		}
		exit;
	}

	
	$libs['html_footer']['update_uploader'] = '
		<script>
			
			$(document).on("click", "button#file_upload", function(){ 
				
				var formData = new FormData($("form#updater")[0]);
				
				$(".updater").html( "'.get_label('uploading').'<br><br><div class=\"progress\"><div id=\"bar_update_upload\" class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"0\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"min-width: 2em;\"></div></div>" );

				var bar_update_upload = $("#bar_update_upload");
				
				var request  = $.ajax({
					type: "POST",
					url: "'.$phproot.'?admin=updater&ajax=upload",
					processData: false,
					contentType: false,					
					data: formData,
					xhr: function() {
						var xhr = new window.XMLHttpRequest();
						
						xhr.upload.addEventListener("progress", function(evt) {
							if (evt.lengthComputable) {
								var percentComplete = evt.loaded / evt.total;
								percentComplete = parseInt(percentComplete * 100);
								
								bar_update_upload.attr(\'aria-valuenow\', percentComplete);
								bar_update_upload.width(percentComplete+"%");
								bar_update_upload.html(percentComplete+"%");
								
								if (percentComplete === 100) {
									
								}
							}
						}, false);
						return xhr;
					},					
				});	
				request.done(function( msg ) {
					$(".updater").html(msg); 
				});
			});
			
			$(document).on("click", "a.mep_del_update", function(){ 
				
				var request  = $.ajax({
					type: "POST",
					url: "'.$phproot.'?admin=updater&ajax=update&del_update=" + $(this).attr("id")
				});	
				request.done(function( msg ) {
					$(".updater").html(msg); 
				});				
				
			});			
		</script>
	';	
	
	return $echo;
}

function draw_update_install_check($update)
{
	global $phproot;
	
	$echo = '
		<form action="'.$phproot.'?admin=updater&update='.$update['key'].'&action=install" method="POST">
			<div class="form-group">
				<label for="md5_checksum">'.get_label('md5_checksum').'</label>
				<input type="text" class="form-control" name="md5_checksum" placeholder="'.get_label('md5_checksum').'">
			</div>
			<div class="form-group">
				<label for="admin_pw">'.get_label('admin_pw').'</label>
				<input type="password" class="form-control" name="admin_pw" placeholder="'.get_label('admin_pw').'">
			</div>
			<input type="hidden" name="file_name" value="'.$update['name'].'">
			<button type="submit" class="btn btn-danger"><i class="fa fa-download"></i>&nbsp;'.get_label('install').'</button>
		</form>
	';
	
	return $echo;
}

function draw_update_install($update, $runkey)
{
	global $phproot;
	
	$echo = '
		<div class="alert alert-danger">
			'.get_label('install_update_warning').'
			<hr>
			<div class="row">
				<div class="col-md-12">
					<div class="pull-left">
						<a href="'.$phproot.'?admin=updater&update='.$update['key'].'&action=install&runkey='.$runkey.'" class="btn btn-danger"><i class="fa fa-download"></i>&nbsp;'.get_label('install').'</a>
					</div>
					<div class="pull-right">
						<a href="'.$phproot.'?admin=updater" class="btn btn-success"><i class="fa fa-long-arrow-left"></i>&nbsp;'.get_label('cancel').'</a>
					</div>	
				</div>				
			</div>
		</div>
	';
	
	return $echo;
}

function draw_update_install_success($update, $runkey)
{
	global $phproot;
	
	$echo = '
		<div class="alert alert-success">
			'.get_label('install_update_success').'
			<hr>
			<a href="'.$phproot.'?admin=updater" class="btn btn-success"><i class="fa fa-long-arrow-left"></i>&nbsp;'.get_label('back').'</a>
		</div>
	';
	
	return $echo;
}

function draw_update_install_fail($update, $runkey)
{
	global $phproot;
	
	$echo = '
		<div class="alert alert-danger">
			'.get_label('install_update_fail').'
			<hr>
			<a href="'.$phproot.'?admin=updater" class="btn btn-success"><i class="fa fa-long-arrow-left"></i>&nbsp;'.get_label('back').'</a>
		</div>
	';
	
	return $echo;
}

?>