<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function admin_nav($nav_data, $parent = null)
{
	global $phproot;
	$echo = '';

	if(is_array($nav_data))
	foreach($nav_data as $key => $value){
		
		$value['key'] = $key;
		
		$active = '';
		if(isset($_GET['admin']) AND $_GET['admin'] == $key){
			$active = 'active';
		}
		
		if(isset($_GET['subadmin']) AND $_GET['subadmin'] == $key){
			$active = 'active';
		}			
		
		if(!isset($value['icon'])){
			$value['icon'] = 'fa fa-cube';
		}
		if((isset($value['modules']) AND is_array($value['modules'])) || (isset($value['submodules']) AND is_array($value['submodules']))){
			if(isset($value['modules']) && is_array($value['modules']))
			{
				$echo .= '
					<li>
						<a id="'.$active.'" href="#" class="submenu_link"><span class="'.$value['icon'].'"></span> '.$value['lang'].'<span id="controll_icon_nav_'.$key.'" class="controll_icon"></span></a>
						<ul id="nav_'.$key.'" class="subnav nav '.$active.'">'.admin_nav($value['modules']).'</ul>
					</li>
				';
			}
			else if(isset($value['submodules']) && is_array($value['submodules']))
			{
				$echo .= '
					<li>
						<a id="'.$active.'" href="#" class="submenu_link"><span class="'.$value['icon'].'"></span> '.$value['lang'].'<span id="controll_icon_nav_'.$key.'" class="controll_icon"></span></a>
						<ul id="nav_'.$key.'" class="subnav nav '.$active.'">'.admin_nav($value['submodules'], $value).'</ul>
					</li>
				';					
			}
		}else{
			if(isset($value['module']) && $parent == null){
				if($value['module'] != '')
				{
					$echo .= '<li class="'.$active.'"><a href="'.$phproot.'?'.$value['module'].'='.$key.'"><span class="'.$value['icon'].'"></span> '.$value['lang'].'</a></li>';
				}
				else
				{
					$echo .= '<li class="'.$active.'"><a href="'.$phproot.'"><span class="'.$value['icon'].'"></span> '.$value['lang'].'</a></li>';
				}
			}
			if($parent != null){
				$echo .= '<li class="'.$active.'"><a href="'.$phproot.'?'.$parent['module'].'='.$parent['key'].'&subadmin='.$key.'"><span class="'.$value['icon'].'"></span> '.$value['lang'].'</a></li>';
			}				
		}
	}

	return $echo;
}

?>