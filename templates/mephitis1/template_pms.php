<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

class draw_pms{

	var $pms;

	function __construct($pms){
	//function draw_pms($pms)
		$this->pms = $pms;
	}
	
	
	function PmsLinkLabel()
	{
		$echo = '';
		$echo .= get_label('pms');
		if($this->pms->unreadCount > 0)
		{
			$echo .= ' <span class="badge">'.$this->pms->unreadCount.'</span>';
		}
		return $echo;
	}
	
	function nav($box)
	{
		global $phproot, $lang;
		
		$echo = '';
		$inboxAdd = '';
		$outboxAdd = '';
		$newpmAdd = '';
		
		if($box == 'inbox')
		{
			$inboxAdd .= 'active';
		}
		if($box == 'outbox')
		{
			$outboxAdd .= 'active';
		}
		if($box == 'newpm')
		{
			$newpmAdd .= 'active';
		}		
		
		$echo .= '
			<a href="'.$phproot.'?pms=inbox" class="btn btn-default btn-block '.$inboxAdd.'">'.get_label('inbox').' <span class="badge">'.$this->pms->inboxCount.'</span></a>
			<a href="'.$phproot.'?pms=outbox" class="btn btn-default btn-block '.$outboxAdd.'">'.get_label('outbox').' <span class="badge">'.$this->pms->outboxCount.'</span></a>
			<hr>
			<a href="'.$phproot.'?pms=action&pm_action=new" class="btn btn-default btn-block '.$newpmAdd.'">'.get_label('new').'</a>
		';
		
		$lang['posted'] = get_label('msg_pm_send');
		/*
		$echo .= draw_modalformlink(
			array(
				'label' => ''.get_label('new').'',
				'mode' => 'new',
				'cssLink' => 'btn btn-success btn-block',			
				'modekey' => 'pmaction',
				'formlink' => $phproot.'?pms=action&pm_action=new&ajax=getform',
				'updatelink' => $phproot.'?pms=outbox&ajax=getpms',
				'idkey' => 'itemid',
				'id' => 0,
				'key' => 'pms'.$box
			)
		);
		*/
		return $echo;
	}
	
	function Box($data, $box)
	{
		global $phproot;
		
		$echo = '';
		if(count($data) > 0)
		{
			$tableData = '';
			foreach($data as $pm)
			{
				$userId = 0;
				$actions = '
					<a href="'.$phproot.'?pms=show&pm='.$pm['id'].'" class="btn btn-primary" title="'.get_label('show').'"><i class="fa fa-eye"></i></a>&nbsp;
					<a href="'.$phproot.'?pms='.$box.'&pm_action=del&pm='.$pm['id'].'" class="btn btn-danger" title="'.get_label('del').'"><i class="fa fa-trash"></i></a>&nbsp;
				';
				
				$subject = $pm['subject'];
				
				if($box == 'inbox')
				{
					$userId = $pm['from_id'];
					if($pm['read_to'] == "0")
					{
						$subject = '<b>'.$subject.'</b>';
					}
				}
				if($box == 'outbox')
				{
					$userId = $pm['to_id'];
				}
				
				$userInfo = $this->pms->UserInfo($userId);
				
				
				
				$tableData .= '
					<tr>
						<td>
							'.$subject.'
						</td>
						<td>
							'.$userInfo['name'].'
						</td>
						<td>
							'.date(getvar('norm_date'), $pm['time']).'
						</td>
						<td>
							'.$actions.'
						</ts>
					</tr>
				';
			}
			
			if(isset($_GET['ajax']) && $_GET['ajax'] == 'getpms')
			{
				echo $tableData;
			}
			
			$table = '
				<table class="norm_tab table table-bordered" id="pms'.$box.'">
					<thead>
						<tr>
							<th class="head">
								'.get_label('subject').'
							</th>
							<th class="head">
								'.get_label('from_to_'.$box.'').'
							</th>
							<th class="head">
								'.get_label('time').'
							</th>
							<th class="head">
								'.get_label('action').'
							</th>							
						</tr>
					</thead>
					<tbody>
						'.$tableData.'
					</tbody>
				</table>
			';
			
			$echo .= $table;
		}
		else
		{
			$echo .= draw_alert(get_label('msg_box_empty'), 'alert alert-info', false);
		}
		
		return $echo;
	}
	
	function Pm($pm, $box)
	{
		global $phproot;
		
		$echo = '';
		
		$actions = '';
		
		if($box == 'inbox')
		{
			
			$userId = $pm['from_id'];
			$actions .= '
				<a href="'.$phproot.'?pms=action&pm_action=new&reply='.$pm['id'].'" class="btn btn-info" title="'.get_label('reply').'"><i class="fa fa-reply"></i></a>&nbsp;				
			';
		}
		if($box == 'outbox')
		{
			$userId = $pm['to_id'];
		}
		
		$actions .= '
			<a href="'.$phproot.'?pms='.$box.'&pm_action=del&pm='.$pm['id'].'" class="btn btn-danger" title="'.get_label('del').'"><i class="fa fa-trash"></i></a>&nbsp;		
		';		
		
		$userInfo = $this->pms->UserInfo($userId);
		
		$echo .= '
			<div class="pm">
				'.get_label('subject').': '.$pm['subject'].'
				<br>
				'.get_label('from_to_'.$box.'').': '.$userInfo['name'].'
				<br>
				'.get_label('time').': '.date(getvar('norm_date'), $pm['time']).'
				<hr>
				'.mk_post($pm['content'], true, true).'
				<hr>
				'.$actions.'
			</div>
		';
		
		return $echo;
	}
	
	function Pms($box, $data, $form_success = false)
	{
		global $phproot, $autoform_success;
		
		$echo = '';	
		
		$content = '';
		if(is_array($data))
		{
			if(isset($data['id']))
			{
				$content = $this->Pm($data, $box);
			}
			else
			{
				$content = $this->Box($data, $box);
			}
		}
		else
		{
			if(isset($_GET['pm_action']))
			{
			$this->pms->CheckBoxes();
				if($_GET['pm_action'] == 'new')
				{
					if(isset($_POST['send']) && $form_success)
					{
						$content = draw_alert(get_label('msg_pm_send'), 'alert alert-info', false, $phproot.'?pms=action&pm_action=new', get_label('newpm'));
					}
					else
					{
						$content = $data;
					}
				}
			}
		}
		$echo .= '
			<div class="container">
				<div class="row">
					<div class="col-md-2">
						'.draw_item(get_label('pms_nav'), $this->nav($box)).'
					</div>
					<div class="col-md-10">
						'.draw_item(get_label($box), $content).'
					</div>					
				</div>
			</div>
		';
		
		return $echo;
	}
	
	function DelDialogue($id, $pms)
	{
		global $phproot;
		
		$pm = $this->pms->Pm($id);
		
		if($pms == 'inbox')
		{
			$userId = $pm['from_id'];
		}
		if($pms == 'outbox')
		{
			$userId = $pm['to_id'];
		}		
		$userInfo = $this->pms->UserInfo($userId);		

		$msg = get_label('msg_'.$pms.'_confirm_del');
		$msg = str_replace('{subject}', '<b>'.$pm['subject'].'</b>', $msg);
		$msg = str_replace('{user}', '<b>'.$userInfo['name'].'</b>', $msg);
		
		return draw_confirm_dialog($msg, $phproot.'?pms='.$pms.'&pm_action=del&pm='.$pm['id'].'&sure=yes', $phproot.'?pms='.$pms);
	}
	
	function MsgDelSuccess($pm, $pms)
	{
		global $phproot;
		
		if($pms == 'inbox')
		{
			$userId = $pm['from_id'];
		}
		if($pms == 'outbox')
		{
			$userId = $pm['to_id'];
		}		
		$userInfo = $this->pms->UserInfo($userId);		

		$msg = get_label('msg_'.$pms.'_success_del');
		$msg = str_replace('{subject}', '<b>'.$pm['subject'].'</b>', $msg);
		$msg = str_replace('{user}', '<b>'.$userInfo['name'].'</b>', $msg);
		
		return draw_alert($msg, 'alert alert-success');
	}
	
	function Unread()
	{
		global $msg_stack, $phproot;
		
		$msg = get_label('msg_unread_pms');
		$msg = str_replace('{count}', $this->pms->unreadCount, $msg);
		$msg = '<a href="'.$phproot.'?pms=inbox">'.$msg.'</a>';
		$msg_stack .= draw_alert($msg, 'alert alert-info');
	}
	
	function EMail($pm, $to_user, $from_user, $linkToPm)
	{
		$echo = '
			<html>
			<body>
				'.get_label('subject').': '.$pm['subject'].'
				<br>
				'.get_label('from_to_inbox').': '.$from_user['name'].'
				<br>
				'.get_label('time').': '.date(getvar('norm_date'), $pm['time']).'
				<br>
				-------------------------------------------------------------
				<br>
				'.mk_post($pm['content'], true, false).'
				<br>
				-------------------------------------------------------------
				<br>
				<a href="'.$linkToPm.'">'.get_label('pms_view_in_cms').'</a>
			</body>
			</html>
		';
		
		return $echo;
	}
	
}

?>