<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_modalformlink($cnf){
	
	global $libs, $temp_modal, $lang;
	
	if(!isset($cnf['formname'])){
		$cnf['formname'] = 'post';
	}

	if(!isset($cnf['link'])){
		$cnf['link'] = $cnf['label'];
	}
	
	if(!isset($cnf['cssLink'])){
		$cnf['cssLink'] = '';
	}
	
	$echo = '<a id="'.$cnf['key'].'_'.$cnf['id'].'_'.$cnf['modekey'].'_'.$cnf['mode'].'_'.$cnf['label'].'" title="'.$cnf['label'].'" class="openmodel_'.$cnf['key'].' '.$cnf['cssLink'].'" href="#" data-toggle="modal" data-target="#Modal_'.$cnf['key'].'">'.$cnf['link'].'</a>';

	if(!isset($temp_modal[$cnf['key']])){
		$temp_modal[$cnf['key']] = 1;
		
		if(!isset($cnf['modal_label']))
		{
			$cnf['modal_label'] = get_label($cnf['key']);
		}
		
		$modal = '
			
			<div class="modal fade" id="Modal_'.$cnf['key'].'" tabindex="-1" role="dialog" aria-labelledby="Modal_'.$cnf['key'].'_Label" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" id="close_'.$cnf['key'].'" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							<h4 class="modal-title" id="Modal_'.$cnf['key'].'_Label"></h4>
						</div>
						<div class="modal-body">

						</div>
						<div class="modal-footer">

						</div>
					</div>
				</div>
			</div>
			<script>
			
				var request_link_'.$cnf['key'].' = "";
				var request_'.$cnf['key'].'_id = "";
				var modekey_'.$cnf['key'].' = "";
				var mode_'.$cnf['key'].' = "";
				
				function close_modal_'.$cnf['key'].'()
				{
					var target =  "#Modal_'.$cnf['key'].'";
					
					$(target).find(".modal-title").empty();
					$(target).find(".modal-body").empty();
					$(target).find(".modal-footer").empty();
					
					var request  = $.ajax({
						type: "POST",
						url: "'.$cnf['updatelink'].'&'.$cnf['idkey'].'=" + request_'.$cnf['key'].'_id + "&action=modal_close"
					});
					
				}
				
				$(document).on("click", "button#close_'.$cnf['key'].'", function(){
					close_modal_'.$cnf['key'].'();
				});
				
				$(document).on("click", "button#submit_'.$cnf['key'].'", function(){ 
					
					var target =  "#Modal_'.$cnf['key'].'";
					
					if(typeof tinyMCE !=  "undefined")
					{
						tinyMCE.triggerSave();
					}
				
					var request  = $.ajax({
						 type: "POST",
						 url: request_link_'.$cnf['key'].',
						 data: $(target).find("form#'.$cnf['formname'].'").serialize()
					});
					
					request.done(function( msg ) {
						$(target).modal("hide"); 
						
						if(mode_'.$cnf['key'].' == "edit")
						{						
							var update = $.ajax({
								type: "POST",
								url: "'.$cnf['updatelink'].'_" + request_'.$cnf['key'].'_id
							});
							
							update.done(function ( msg ){
								$("#'.$cnf['key'].'_" + request_'.$cnf['key'].'_id).html(msg);
								alert_message("alert-success", "'.$lang['edited'].'", "");
							});
							
							
							
						}
						if(mode_'.$cnf['key'].' == "new")
						{						
							var update = $.ajax({
								type: "POST",
								url: "'.$cnf['updatelink'].'"
							});
							
							update.done(function ( msg ){
								$("#'.$cnf['key'].'").find("tbody").html(msg);
								alert_message("alert-success", "'.$lang['posted'].'", "");
							});
						}
					});
					
					request.fail(function( msg ) {
						$(target).find(".modal-body").html(msg.responseText); 
						
						//alert_message("alert-danger", "'.$lang['error'].'", msg.responseText);
						
						//$(target).find(".modal-footer").empty();
						//$(target).find(".modal-footer").append("<button type=\"button\" id=\"close_'.$cnf['key'].'\" class=\"btn btn-default\" data-dismiss=\"modal\">'.$lang['close'].'</button>");
					});
				});
				';
				
				if(isset($cnf['dellink']))
				$modal .= '
				$(document).on("click", "button#delete_'.$cnf['key'].'", function(){ 
					var target =  "#Modal_'.$cnf['key'].'";
					$(target).find(".modal-body").html("'.$lang['del_confirm'].'");
					$(target).find(".modal-footer").empty();
					$(target).find(".modal-footer").append("<button type=\"button\" id=\"delete_'.$cnf['key'].'_no\" class=\"btn btn-success\" data-dismiss=\"modal\">'.$lang['no'].'</button>");
					$(target).find(".modal-footer").append("<button type=\"button\" id=\"delete_'.$cnf['key'].'_yes\" class=\"btn btn-danger\">'.$lang['yes'].'</button>");
				});
			
				$(document).on("click", "button#delete_'.$cnf['key'].'_no", function(){ 
					close_modal_'.$cnf['key'].'();
				});
				
				$(document).on("click", "button#delete_'.$cnf['key'].'_yes", function(){ 
					var target =  "#Modal_'.$cnf['key'].'";
					
					var request  = $.ajax({
						type: "POST",
						url: "'.$cnf['dellink'].'&'.$cnf['idkey'].'=" + request_'.$cnf['key'].'_id + "&sure=yes"
					});
					
					request.done(function ( msg ){

						$(target).find(".modal-body").html("'.$lang['deleted'].'");
						$(target).find(".modal-footer").empty();
						$(target).find(".modal-footer").append("<button type=\"button\" id=\"close_'.$cnf['key'].'\" class=\"btn btn-default\" data-dismiss=\"modal\">'.$lang['close'].'</button>");
						
						
						var update = $.ajax({
							type: "POST",
							url: "'.$cnf['updatelink'].'"
						});
						
						update.done(function ( msg ){
							$("#'.$cnf['key'].'").find("tbody").html(msg);
							alert_message("alert-success", "'.$lang['deleted'].'", "");
						});
						
						
					});
					
					request.fail(function( msg ) {
						$(target).find(".modal-body").html(msg.responseText); 
						
						alert_message("alert-danger", "'.$lang['error'].'", msg.responseText);
						
						$(target).find(".modal-footer").empty();
						$(target).find(".modal-footer").append("<button type=\"button\" id=\"close_'.$cnf['key'].'\" class=\"btn btn-default\" data-dismiss=\"modal\">'.$lang['close'].'</button>");
					});
					
				});
				
				';
				
				$modal .= '
				
				$(document).on("click", "a.openmodel_'.$cnf['key'].'", function(){ 
					
					var target =  "#Modal_'.$cnf['key'].'";
					var request_id = this.id.split("_");
					
					modekey_'.$cnf['key'].' = request_id[2];
					mode_'.$cnf['key'].' = request_id[3];
					
					request_link_'.$cnf['key'].' = "'.$cnf['formlink'].'&'.$cnf['idkey'].'=" + request_id[1] + "&" + modekey_'.$cnf['key'].' + "=" + mode_'.$cnf['key'].';
					request_'.$cnf['key'].'_id = request_id[1];
					
					var request  = $.ajax({
						type: "POST",
						url: request_link_'.$cnf['key'].'
					});
					$(target).find(".modal-body").html(\'<i class="fa fa-spinner fa-pulse"></i>\'); 
					
					request.done(function( msg ) {

						$(target).find(".modal-title").empty();
						$(target).find(".modal-body").empty();
						$(target).find(".modal-footer").empty();
					
						$(target).find(".modal-title").html( "'.get_label($cnf['modal_label']).': " + request_id[4]); 
						$(target).find(".modal-body").html(msg); 
						
						';

						if(isset($cnf['dellink']))
						$modal .= '
						if(mode_'.$cnf['key'].' != "new")
						$(target).find(".modal-footer").append("<button type=\"button\" id=\"delete_'.$cnf['key'].'\" class=\"btn btn-danger\">'.$lang['del'].'</button>");
						';
						
						$modal .= '
						
						$(target).find(".modal-footer").append("<button type=\"button\" id=\"close_'.$cnf['key'].'\" class=\"btn btn-default\" data-dismiss=\"modal\">'.$lang['close'].'</button>");
						$(target).find(".modal-footer").append("<button type=\"button\" id=\"submit_'.$cnf['key'].'\" class=\"btn btn-primary\">'.$lang['norm_send'].'</button>");
						
					});
					
					request.fail(function( msg ) {
						$(target).find(".modal-body").html(msg.responseText); 
						
						alert_message("alert-danger", "'.$lang['error'].'", msg.responseText);
						
						$(target).find(".modal-footer").empty();
						$(target).find(".modal-footer").append("<button type=\"button\" id=\"close_'.$cnf['key'].'\" class=\"btn btn-default\" data-dismiss=\"modal\">'.$lang['close'].'</button>");
					});
					
				});
			
				var enforceModalFocusFn = $.fn.modal.Constructor.prototype.enforceFocus;

				$.fn.modal.Constructor.prototype.enforceFocus = function() {};

			
			</script>
			
		';
		$libs['html_footer']['modal_'.$cnf['key']] = $modal;
	}
	return $echo;

}

function draw_alert($txt, $mode = 'success', $dismissible = true, $link = '', $linklabel = '')
{
	global $lang;
	
	$echo = '';
	$classadd = '';
	
	if($dismissible)
	{
		$txt = '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$txt;
		
		if(($link == '' || $linklabel == '') && !($mode == 'danger' || $mode == 'warning' || $mode == 'info'))
		{
			$classadd .= ' static_msg_box_fadeout ';
		}
		$classadd .= ' alert-dismissible ';
	}
	if($link != '')
	{
		if($link == 'back2')
		{
			$link = '<br><a href="#" class="btn-sm btn btn-default" onclick="history.go(-2)">'.$lang['back'].'</a>';
		}else if($link == 'back1'){
			$link = '<br><a href="#" class="btn-sm btn btn-default" onclick="history.go(-1)">'.$lang['back'].'</a>';
		}else if($linklabel != ''){
			$link = '<br><a href="'.$link.'" class="btn-sm btn btn-default">'.$linklabel.'</a>';
		}else{
			$link = '';
		}
	}
	$echo .='
		<div class="alert alert-'.$mode.' '.$classadd.'" role="alert">
			'.$txt.'
			'.$link.'
		</div>
	';
	
	return $echo;
}

?>