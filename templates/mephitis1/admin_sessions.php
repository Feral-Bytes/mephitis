<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_sessions($sessions, $queryadd, $order_link){
	
	global $phproot, $lang;
	
	$echo = '';
	if(is_array($sessions))
	foreach($sessions as $session){
		$echo .= '
			<tr>
				<td>
					'.$session['id'].'
				</td>
				<td>
					'.$session['session_id'].'
				</td>
				<td>
					'.$session['user_name'].'
				</td>
				<td>
					'.date(getvar('norm_date'), $session['time']).'
				</td>
			</tr>
		';
	}
	
	$echo = '
		<a href="'.$phproot.'?admin=sessions&action=del">'.$lang['ips_del'].'</a><br><br>
		'.pages('draw', 'sessions', 'id != "0"', $order_link, 'session_page').'
		<table class="norm_tab table table-bordered">
			<thead>
				<tr>
					<th class="head">
						<a href="'.$phproot.'?admin=sessions&order=id">'.$lang['id'].'</a>
					</th>
					<th class="head">
						'.$lang['session_id'].'
					</th>
					<th class="head">
						<a href="'.$phproot.'?admin=sessions&order=user">'.$lang['user'].'</a>
					</th>
					<th class="head">
						<a href="'.$phproot.'?admin=sessions&order=time">'.$lang['lastaccess'].'</a>
					</th>
				</tr>
			</thead>
			<tbody>
				'.$echo.'
			</tbody>
		</table>
		<br>
		'.pages('draw', 'sessions', 'id != "0"', $order_link, 'session_page').'
		<br>
		<a href="'.$phproot.'?admin=sessions&action=del">'.$lang['ips_del'].'</a>
	';
	
	return $echo;
	
}

?>