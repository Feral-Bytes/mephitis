<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_comments($data){

	global $phproot, $lang, $user, $link;
	
	$content = '';
	$echo = '';
	
	$i = 0;
	if(is_array($data['comments']))foreach($data['comments'] as $comment)
	{
		$i++;
		$ip = '';
		$modactions = '';
		$css = '';
		if($user['id'] != '0' AND check_group_access(getvar('comment_mod_groups'), 'whitelist'))
		{
			$ip = get_label('ip').': '.$comment['ip'].'';
			$modactions .= '
				<hr>
				<a href="'.$phproot.'?'.$link.'&comment=edit&itemid='.$comment['id'].'#post" class="btn btn-warning" title="'.get_label('edit').'"><i class="fa fa-wrench"></i></a>
				<a href="'.$phproot.'?'.$link.'&comment=del&itemid='.$comment['id'].'" class="btn btn-danger" title="'.get_label('del').'"><i class="fa fa-trash"></i></a>
			';
			if($comment['online'] == '0')
			{
				$modactions .= '
					<a href="'.$phproot.'?'.$link.'&comment=setonline&itemid='.$comment['id'].'#comment_'.$comment['id'].'" class="btn btn-info" title="'.get_label('set_public').'"><i class="fa fa-eye"></i></a>				
				';
				$css .= ' comment_hidden ';
			}
			else
			{
				$modactions .= '
					<a href="'.$phproot.'?'.$link.'&comment=unsetonline&itemid='.$comment['id'].'#comment_'.$comment['id'].'" class="btn btn-warning" title="'.get_label('unset_public').'"><i class="fa fa-eye-slash"></i></a>				
				';				
			}
		}
		
		if($comment['online'] == '0')
		{
			$css .= ' comment_hidden ';
		}
		
		if(!isset($comment['user']))
		{
			$comment['user'] = $comment['user_name'].' ('.get_label('guest').')<br>';
		}
		else
		{
			$comment['user'] = draw_user_info($comment['user']);
		}
		
		$content .= '
			<div class="comment '.$css.' clearfix">
				<a name="comment_'.$comment['id'].'"></a>
				<div class="subject">
					'.$comment['subject'].'
				</div>
				<div class="user_info text-center">
					'.$comment['user'].$ip.'					
				</div>
				<div class="comment_post">
					'.mk_post($comment['content'], getvar('bbcode'), getvar('emojis')).'
				</div>
				<div class="clearfix"></div>
				<div class="info">
						'.date(getvar('norm_date'), $comment['time']).'
						'.$modactions.'
				</div>
			</div>
		';
		
	}

	$form = '';
	if($data['form'] != '')
	{
		$form .= '
			<hr>
			'.$data['form'] .'
		';
	}
	
	$pagination = pages('draw', 'comments', 'object = "'.$data['conf']['object'].'" AND object_id = "'.$data['conf']['object_id'].'"', $link, 'comment_page'); 
	
	$newbtn = '';
	if(($data['conf']['can_write'] || check_group_access(getvar('comment_mod_groups'), 'whitelist')) && $form == ''){
		$newbtn .= '<a href="'.$phproot.'?'.$link.'&comment=new#post" class="btn btn-success">'.$lang['new_comment'].'</a><br>';
	}
	if(!is_array($data['comments'])){
		$content = $lang['no_comments'].'<br>';
	}
	
	$echo .= '
		'.$newbtn.'
		'.$pagination.'
		<br>
		<br>
		<div>
		'.$content.'
		</div>
		'.$pagination.'
		<div>
		'.$form.'
		</div>
	';

	
	if($data['conf']['draw_mode'] == 'item')
	{
		$echo = draw_item(''.$data['conf']['header'].' <span class="badge">'.$data['count'].'</span>', $echo);
	}
	else
	{
		$echo = draw_content(''.$data['conf']['header'].' <span class="badge">'.$data['count'].'</span>', $echo);
	}
	
	
	
	return $echo;

}

?>