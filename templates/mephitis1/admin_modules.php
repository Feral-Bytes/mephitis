<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_admin_modules_actions($conf, $row)
{
	global $phproot, $user, $modules, $installedModules, $phpRootPath;
	
	$actions = '';
	
	
	
	$actions .= '
		'.draw_action_link($phproot.'?admin=modules&action=edit&itemid='.$row['id'], 'edit', '', 'btn btn-warning').'
		'.draw_action_link($phproot.'?admin=modules&action=del&itemid='.$row['id'], 'del', '', 'btn btn-danger').'
	';

	
	if(isset($installedModules[$row['name']]) && $installedModules[$row['name']]['active'] == '1')
	{
		//$module = $modules[$row['name']];
		if(file_exists($phpRootPath.'modules/'.$row['name'].'/mod_db.php'))
		{
			$actions .= '<a href="'.$phproot.'?admin=modules&action=fix_db&itemid='.$row['id'].'" class="btn btn-info" title="'.get_label('mod_fix_db').'"><i class="fa fa-database"></i></a>&nbsp;';
		}
		
		if(isset($modules[$row['name']]['update_func']))
		{
			$actions .= '<a href="'.$phproot.'?admin=modules&action=update&itemid='.$row['id'].'" class="btn btn-info" title="'.get_label('mod_update').'"><i class="fa fa-cogs"></i></a>&nbsp;';
		}
	}
	
	
	
	return $actions;
}


?>