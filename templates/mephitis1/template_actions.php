<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_action_link($target, $action, $txt = '', $cssClass = ''){

	global $lang;

	$echo = '';
	
	if($txt != '')
	{
		$txt = ' '.$txt;
	}
	
	if($action == 'up'){
		$echo .= '<a href="'.$target.'" title="'.$lang['up'].'" class="'.$cssClass.'"><i class="fa fa-arrow-up"></i>'.$txt.'</a>';
	}else if($action == 'down'){
		$echo .= '<a href="'.$target.'" title="'.$lang['down'].'" class="'.$cssClass.'"><i class="fa fa-arrow-down"></i>'.$txt.'</a>';
	}else if($action == 'to_top'){
		$echo .= '<a href="'.$target.'" title="'.$lang['to_top'].'" class="'.$cssClass.' hidden-xs"><i class="fa fa-arrow-up"></i>'.$txt.'</a>';
	}else if($action == 'to_bottom'){
		$echo .= '<a href="'.$target.'" title="'.$lang['to_bottom'].'" class="'.$cssClass.' hidden-xs"><i class="fa fa-arrow-down"></i>'.$txt.'</a>';
	}else if($action == 'asc'){
		$echo .= '<a href="'.$target.'" title="'.$lang['asc'].'" class="'.$cssClass.'"><i class="fa fa-arrow-up"></i>'.$txt.'</a>';
	}else if($action == 'desc'){
		$echo .= '<a href="'.$target.'" title="'.$lang['desc'].'" class="'.$cssClass.'"><i class="fa fa-arrow-down"></i>'.$txt.'</a>';
	}else if($action == 'new'){
		$echo .= '<a href="'.$target.'" title="'.$lang['new'].'" class="'.$cssClass.'"><i class="fa fa-file-o"></i>'.$txt.'</a>';
	}else if($action == 'new_site'){
		$echo .= '<a href="'.$target.'" title="'.$lang['site_new'].'" class="'.$cssClass.'"><i class="fa fa-file-text-o"></i>'.$txt.'</a>';
	}else if($action == 'new_dir'){
		$echo .= '<a href="'.$target.'" title="'.$lang['dir_new'].'" class="'.$cssClass.'"><i class="fa fa-folder-o"></i>'.$txt.'</a>';
	}else if($action == 'edit'){
		$echo .= '<a href="'.$target.'" title="'.$lang['edit'].'" class="'.$cssClass.'"><i class="fa fa-wrench"></i>'.$txt.'</a>';
	}else if($action == 'del' OR $action == 'delete'){
		$echo .= '<a href="'.$target.'" title="'.$lang['del'].'" class="'.$cssClass.'"><i class="fa fa-trash-o"></i>'.$txt.'</a>';
	}else if($action == 'logout'){
		$echo .= '<a href="'.$target.'" title="'.$lang['logout'].'" class="'.$cssClass.'"><i class="fa fa-power-off"></i>'.$txt.'</a>';
	}else if($action == 'select'){
		$echo .= '<a href="'.$target.'" title="'.$lang['select'].'" class="'.$cssClass.'"><i class="fa fa-square-o"></i>'.$txt.'</a>';
	}else if($action == 'copypastehere'){
		$echo .= '<a href="'.$target.'" title="'.$lang['copypastehere'].'" class="'.$cssClass.'"><i class="fa fa-files-o"></i>'.$txt.'</a>';
	}else if($action == 'movehere'){
		$echo .= '<a href="'.$target.'" title="'.$lang['movehere'].'" class="'.$cssClass.'"><i class="fa fa-arrow-right"></i>'.$txt.'</a>';
	}else if($action == 'truncate'){
		$echo .= '<a href="'.$target.'" title="'.$lang['truncate'].'" class="'.$cssClass.'"><i class="fa fa-trash-o"></i>'.$txt.'</a>';
	}else{
		$echo .= '<a href="'.$target.'" title="'.get_label($action).'" class="'.$cssClass.'">'.get_label($action).''.$txt.'</a>';
	}

	
	return $echo;
	
}

function draw_confirm_dialog($txt, $yesLink = '', $noLink = '')
{
	global $lang;
	
	$echo = '';
	$yesLinkAtr = '';
	$noLinkAtr = '';
	
	if($noLink == '')
	{
		$noLink = '#';
		$noLinkAtr = ' onclick="history.go(-1)" ';
	}
	
	$echo .='
		<p>'.$txt.'</p>
		<a href="'.$yesLink.'" '.$yesLinkAtr.' class="btn-sm btn btn-default">'.$lang['yes'].'</a>
		<a href="'.$noLink.'" '.$noLinkAtr.' class="btn-sm btn btn-default">'.$lang['no'].'</a>
	';	
	
	return $echo;
}

?>