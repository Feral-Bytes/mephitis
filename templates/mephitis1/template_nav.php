<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_nav()
{
	global $pageTree, $links;
	
	$echo = '
		<ul class="nav navbar-nav">
			'.draw_nav_elements($pageTree->GetTree()).'
			'.nav_usercp($links['usercp_nav']).'
		</ul>
	';
	
	return $echo;
}

function draw_page_link($page, $attr = '', $symbol = '', $lvl = 0)
{
	global $phproot;
	
	$echo = '';
	
	$css = array();
	
	$id = $page['id'];
	$getVar = 'site';
	if(trim($page['shortlink']) != '')
	{
		$id = $page['shortlink'];
		$getVar = 'show';
	}
	
	$icon = '';
	if(trim($page['icon']) != '')
	{
		$icon = '<i class="'.$page['icon'].'"></i>&nbsp;';
	}
	
	if($page['isActive'])
	{
		$css[] = 'active';
	}

	if($page['isCurrent'])
	{
		$css[] = 'current';
	}
	
	$content_link_target = $page['content_link_target'];
	if(is_numeric($content_link_target) && $lvl <= 10)
	{
		$targetPage = GetPage($content_link_target);
		if(isset($targetPage['id']))
		{
			return draw_page_link($targetPage, $attr, $symbol, $lvl+1);
		}

	}
	
	switch($page['content_kind'])
	{
		case 'link':
			$echo .= '<a href="'.$content_link_target.'" class="'.implode(' ', $css).'" '.$attr.'>'.$icon.''.$page['name'].$symbol.'</a>';
			break;
		case 'link_blank':
			$echo .= '<a href="'.$content_link_target.'" target="_BLANK" class="'.implode(' ', $css).'" '.$attr.'>'.$icon.''.$page['name'].$symbol.'</a>';
			break;
		default:
			$echo .= '<a href="'.$phproot.'?'.$getVar.'='.$id.'" class="'.implode(' ', $css).'" '.$attr.'>'.$icon.''.$page['name'].$symbol.'</a>';
			break;
	}
	
	
	return $echo;
}

function draw_nav_elements($elements, $lvl = 0)
{
	if(!is_array($elements))
	{
		return;
	}
	
	$elementsHTML = '';
	
	foreach($elements as $element)
	{
		if(!$element['hasAccess'] || $element['isHidden'])
		{
			continue;
		}
		
		$css = array();
		
		if($element['isActive'])
		{
			$css[] = 'active';
		}

		if($element['isCurrent'])
		{
			$css[] = 'current';
		}
		
		$symbol = '';
		if($element['hasChildren'] && $lvl < 1)
		{
			$symbol = '<span class="caret"></span>';
		}
		
		$attr = '';
		$childrenHTML = '';
		if($element['hasChildren'])
		{
			$dropdownCSS = array();
			
			if($lvl >= 1)
			{
				$dropdownCSS[] = 'dropdown-menu';
				$css[] = 'dropdown-submenu';
			}
			else
			{
				$dropdownCSS[] = 'dropdown-menu';
				$css[] = 'dropdown';
				
				$attr = 'data-toggle="dropdown"';
			}
			
			$childrenHTML = '
				<ul class="'.implode(' ', $dropdownCSS).'">
					'.draw_nav_elements($element['children'], $lvl+1).'
				</ul>
			';
			
		}
		
		$elementHTML = '
			<li class="'.implode(' ', $css).'">
				'.draw_page_link($element, $attr, $symbol).'
				'.$childrenHTML.'
			</li>
		';
		
		$elementsHTML .= $elementHTML;
	}
	
	return $elementsHTML;
}


function nav_usercp($links)
{
	global $user, $phproot;
	
	if(is_array($user) AND isset($user['id']) AND $user['id'] != '0')
	{
	
		$nav_user = '
		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-user"></span> '.get_label('usercp').'<span class="caret"></span></a>
			<ul class="dropdown-menu" role="menu">
			'; 
			foreach($links as $key => $value){
					if(isset($value['phproot']))
					{
						if(isset($value['module']))
						{
							$nav_user .= '<li><a href="'.$value['phproot'].'?'.$value['module'].'='.$key.'"><span class="'.$value['icon'].'"></span> '.$value['lang'].'</a></li>';
						}
						else
						{
							$nav_user .= '<li><a target="_BLANK" href="'.$value['phproot'].'"><span class="'.$value['icon'].'"></span> '.$value['lang'].'</a></li>';
						}
						
					}
					else
					{
						$nav_user .= '<li><a href="'.$phproot.'?'.$value['module'].'='.$key.'"><span class="'.$value['icon'].'"></span> '.$value['lang'].'</a></li>';
					}				
			}
			$nav_user .= '
			</ul>
		</li>';	
		
	}
	else
	{
		if(getvar('public_show_login') == '1'){
			$nav_user = '
			'; 
			foreach($links as $key => $value){
				$icon = '';
				if($value['icon'] != '')
				{
					$icon = '<span class="'.$value['icon'].'"></span> ';
				}					
				$nav_user .= '<li><a href="'.$phproot.'?'.$value['module'].'='.$key.'">'.$icon .''.$value['lang'].'</a></li>';
			}
			$nav_user .= '
			';
		}else{
			$nav_user = '';
		}
	}
	
	
	return $nav_user;
}

?>