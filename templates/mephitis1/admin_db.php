<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_db($db, $mode){

	global $phproot, $mysqli, $lang;
	
	$header = '';
	
	$backup_form = '
		<form action="'.$phproot.'?admin=db" method="POST">
			<table>
				<tr>
					<td>
						<input type="checkbox" name="drop_table_if_exists" checked>
					</td>
					<td>
						'.$lang['db_drop_table_if_exists'].'
					</td>
				</tr>
				<tr>
					<td>
						<input type="text" size="3" name="breakat" value="1000">
					</td>
					<td>
						'.$lang['db_breakat_rows'].'
					</td>
				</tr>
				<tr>
					<td>
						<input type="checkbox" name="send_as_file" checked>
					</td>
					<td>
						'.$lang['db_send_as_file'].'
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="submit" name="backup_send" value="'.$lang['db_backup'].'">
					</td>
				</tr>
			</table>
		</form>
	';
	
	if(isset($db['db_errors']) AND is_array($db['db_errors']) AND count($db['db_errors']) > 0){
		
		$repair = '<a href="'.$phproot.'?admin=db&db_action=repair">'.$lang['db_repair'].'</a>';
		
		$toinstall = '';
		$toinstalltabs = '';
		foreach($db['db_errors'] as $key => $value){
			if($value == 'not_found'){
				$toinstall = 1;
				$toinstalltabs[] = $key;
			}
		}
		if($toinstall == '1'){
			$tab_tex = '';
			if(is_array($toinstalltabs)){
				foreach($toinstalltabs as $tab){
					$tab_tex .= $tab.'<br>';
				}
				$to_install = $lang['db_toinstall'].': <br>'.$tab_tex;
			}else{
				$to_install = '';
			}
		}else{
			$to_install = '';
		}
	}else{
		$to_install = '';
		$repair = '';
	}

	$echo = '';
	if($mode == 'db'){
	
		$tables = '';
		if(is_array($db['db']))foreach($db['db'] as $table => $data){
			
			$status = '';
			$action = '';
			if(isset($data['check']) AND $data['field_check']){
				if($data['check'] == 'ok')$status = $lang['db_ok'];
				if($data['check'] == 'error' OR $data['field_check'] == 'error')$status = $lang['db_error'];
				if($data['check'] == 'sort_error' OR $data['field_check'] == 'sort_error')$status = $lang['db_sort_error'];
				if($data['check'] == 'collation_error' OR $data['field_check'] == 'collation_error')$status = $lang['db_collation_error'];
				if($data['check'] == 'engine_error' OR $data['field_check'] == 'engine_error')$status = $lang['db_engine_error'];
				if(isset($data['var_check']) AND $data['var_check'] == 'error')$status = $lang['db_var_error'];
			}else{
				$status = $lang['db_unknown'];
				$action = ' | <a href="'.$phproot.'?admin=db&db_del_table='.$data['Name'].'">'.$lang['del'].'</a>';
			}
			
			
			
			$tables .= '
				<tr>
					<td>
						'.$data['Name'].'
					</td>
					<td>
						'.$status.'
					</td>
					<td>
						<a href="'.$phproot.'?admin=db&db_action=show&table='.$data['Name'].'">'.$lang['db_table_show'].'</a>
						'.$action.'
					</td>
				</tr>
				';
		}

		$echo = '
			<table width="100%">
				<tr>
					<td width="50%" style="vertical-align: top;">
						'.$to_install.'
						<table class="norm_tab table table-bordered">
							<thead>
								<tr>
									<th class="head">
										'.$lang['db_tables'].'
									</th>
									<th class="head">
										'.$lang['db_status'].'
									</th>
									<th class="head">
										'.$lang['action'].'
									</th>
								</tr>
							</thead>
							<tbody>
								'.$tables.'
							</tbody>
						</table>
						'.$repair.'
					</td>
					<td width="50%" style="vertical-align: top; padding-left:10px;">
						'.$backup_form.'
					</td>
				</tr>
			</table>
		';
	
	}
	
	if($mode == 'table'){
		
		if(!isset($db['db'][$mysqli->real_escape_string($_GET['table'])]))die(draw_error($lang['error'], $lang['error']));
		$header = $header.' '.$lang['db_table'].': '.$db['db'][$mysqli->real_escape_string($_GET['table'])]['Name'];
		$fields = '';
		if(is_array($db['db'][$mysqli->real_escape_string($_GET['table'])]['fields']))foreach($db['db'][$mysqli->real_escape_string($_GET['table'])]['fields'] as $field => $data){
			$status = '';
			$action = '';
			if(isset($data['check'])){
				if($data['check'] == 'ok')$status = $lang['db_ok'];
				if($data['check'] == 'error')$status = $lang['db_error'];
				if($data['check'] == 'sort_error')$status = $lang['db_sort_error'];
				if($data['check'] == 'collation_error')$status = $lang['db_collation_error'];
			}else{
				$status = $lang['db_unknown'];
				$action = '<a href="'.$phproot.'?admin=db&db_action=show&table='.$db['db'][$mysqli->real_escape_string($_GET['table'])]['Name'].'&db_del_field='.$data['Field'].'">'.$lang['del'].'</a>';
			}
			
			
			
			$fields .= '
				<tr>
					<td>
						'.$data['Field'].'
					</td>
					<td>
						'.$status.'
					</td>
					<td>
						'.$action.'
					</td>
				</tr>
				';
		}
		
		$vartab = '';
		if(isset($db['db'][$mysqli->real_escape_string($_GET['table'])]['vars']) AND is_array($db['db'][$mysqli->real_escape_string($_GET['table'])]['vars'])){
			$vartabrows = '';
			foreach($db['db'][$mysqli->real_escape_string($_GET['table'])]['vars'] as $var => $check){
				$action = '';
				if($check == 'ok'){
					$status = $lang['db_ok'];
					$action = '';
				}
				if($check == 'not_found'){
					$status = $lang['db_not_found'];
					$action = $lang['db_need_repair'];
				}
				if($check == ''){
					$status = $lang['db_unknown'];
					$action = '<a href="'.$phproot.'?admin=db&db_action=show&table='.$_GET['table'].'&db_del_var='.$var.'">'.$lang['del'].'</a>';
				}
				$vartabrows .= '
						<tr>
							<td>
								'.$var.'
							</td>
							<td>
								'.$status.'
							</td>
							<td>
								'.$action.'
							</td>
						</tr>
				';
			}
				$vartab = '
					<table class="norm_tab table table-bordered">
						<thead>
							<tr>
								<th class="head">
									'.$lang['db_var'].'
								</th>
								<th class="head">
									'.$lang['db_status'].'
								</th>
								<th class="head">
									'.$lang['action'].'
								</th>
							</tr>
						</thead>
						<tbody>
							'.$vartabrows.'
						</tbody>
					</table>
				';
		}
		
		$echo = '
			<table width="100%">
				<tr>
					<td width="50%" style="vertical-align: top;">
						'.$to_install.'
						<table class="norm_tab table table-bordered">
							<thead>
								<tr>
									<th class="head">
										'.$lang['db_fields'].'
									</th>
									<th class="head">
										'.$lang['db_status'].'
									</th>
									<th class="head">
										'.$lang['action'].'
									</th>
								</tr>
							</thead>
							<tbody>
								'.$fields.'
							</tbody>
						</table>
						'.$repair.'
					</td>
					<td width="50%" style="vertical-align: top;">
					'.$vartab.'
					</td>
				</tr>
			</table>
			<a href="'.$phproot.'?admin=db">'.$lang['back'].'</a>
		';
		
	}
	
	if($mode == 'msg'){
		if(isset($_GET['db_del_field']) AND $_GET['db_del_field'] != ''){
			if(isset($_GET['ok']) AND $_GET['ok'] == 'ok'){
				$echo = ''.$lang['db_field_deled1'].' <b>'.$_GET['db_del_field'].'</b> '.$lang['db_field_deled2'].' <b>'.$_GET['table'].'</b> '.$lang['db_field_deled3'].'';
			}else{
				$echo = ''.$lang['db_field_del1'].' <b>'.$_GET['db_del_field'].'</b> '.$lang['db_field_del2'].' <b>'.$_GET['table'].'</b> '.$lang['db_field_del3'].'<br>
					<a href="'.$phproot.'?admin=db&db_action=show&table='.$_GET['table'].'&db_del_field='.$_GET['db_del_field'].'&ok=ok">'.$lang['yes'].'</a><br>
					<a href="'.$phproot.'?admin=db&db_action=show&table='.$_GET['table'].'">'.$lang['no'].'</a><br>
					
				';
			}
		}

		if(isset($_GET['db_del_var']) AND $_GET['db_del_var'] != ''){
			if(isset($_GET['ok']) AND $_GET['ok'] == 'ok'){
				$echo = ''.$lang['db_var_deled1'].' <b>'.$_GET['db_del_var'].'</b> '.$lang['db_var_deled2'].' <b>'.$_GET['table'].'</b> '.$lang['db_var_deled3'].'';
			}else{
				$echo = ''.$lang['db_var_del1'].' <b>'.$_GET['db_del_var'].'</b> '.$lang['db_var_del2'].' <b>'.$_GET['table'].'</b> '.$lang['db_var_del3'].'<br>
					<a href="'.$phproot.'?admin=db&db_action=show&table='.$_GET['table'].'&db_del_var='.$_GET['db_del_var'].'&ok=ok">'.$lang['yes'].'</a><br>
					<a href="'.$phproot.'?admin=db&db_action=show&table='.$_GET['table'].'">'.$lang['no'].'</a><br>
					
				';
			}
		}

		if(isset($_GET['db_del_table']) AND $_GET['db_del_table'] != ''){
			if(isset($_GET['ok']) AND $_GET['ok'] == 'ok'){
				$echo = ''.$lang['db_table_deled1'].' <b>'.$_GET['db_del_table'].'</b> '.$lang['db_table_deled2'].'';
			}else{
				$echo = ''.$lang['db_table_del1'].' <b>'.$_GET['db_del_table'].'</b> '.$lang['db_table_del2'].'<br>
					<a href="'.$phproot.'?admin=db&db_del_table='.$_GET['db_del_table'].'&ok=ok">'.$lang['yes'].'</a><br>
					<a href="'.$phproot.'?admin=db">'.$lang['no'].'</a><br>
					
				';
			}
		}
	
	}
	
	if($mode == 'backup'){
		$header .= ' '.$lang['db_backup'];
		$echo = '<textarea style="width: 100%; height: 1000px;">'.$db.'</textarea><br><a href="'.$phproot.'?admin=db">'.$lang['back'].'</a>';
	}
	
	if(!isset($_POST['send_as_file'])){
		
		if($mode != 'msg'){
			$echo = '
				'.$header.'
				<hr>
				'.$echo.'
			';
		}
		return $echo;
		
	}

}

?>