<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_admin_user_list_actions($conf, $row)
{
	global $phproot, $user;
	
	$actions = '';
	
	$actions .= '
		'.draw_action_link($phproot.'?admin=user&action=edit&itemid='.$row['id'], 'edit', '', 'btn btn-warning').'
	';
	
	if($user['id'] != $row['id'])
	{
		$actions .= '
			'.draw_action_link($phproot.'?admin=user&action=del&itemid='.$row['id'], 'del', '', 'btn btn-danger').'
		';
	}
	
	if(get_session($row, false) != '0')
	{
		$actions .= draw_action_link($phproot.'?admin=user&logout_user='.$row['id'], 'logout', '', 'btn btn-info').'';
	}
	
	return $actions;
}


?>