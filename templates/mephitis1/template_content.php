<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_item($header, $content, $css = ''){

	
	$echo = '
	<div class="post '.$css.'">
		<div class="post_header">
			<h1>'.$header.'</h1>
		</div>
		<div class="post_body">
			'.$content.'
		</div>
	</div>
	';
	/*
	$echo = '
		<div class="panel panel-default">
			<div class="panel-heading">
				'.$header.'
			</div>
			<div class="panel-body">
				'.$content.'
			</div>
		</div>	
	';
	*/
	return $echo;
}

function draw_content($header, $content, $border = 1, $comments = 0, $site_id = null, $site_dir = null, $css = '', $frontend_admin = false){
	
	global $nav, $user, $phproot;

	$echo = '';
	
	if($frontend_admin)
	{
		$echo .= draw_action_link($phproot.'?frontend_admin=sites&action=edit&itemid='.$site_id, 'edit');
	}
	
	if($border == '1'){
		$echo .= draw_item($header, $content, $css);
	}else{
		$echo .= $content;
	}

	$echo = '
		<div class="row">
			<div class="container">
				<div class="col-lg-12">
					'.$echo.'
				</div>
			</div>
		</div>
	';
	
	if($comments == '1')
	{
		$comments_conf = array(
			'object' => 'sites',
			'object_id' => $site_id
		);
		$echo .= comments($comments_conf);
	}
	
	return $echo;
	
}

function draw_fces($fces){

	global $lang;
	
	$echo = '';
	if(is_array($fces)){
		foreach($fces as $fce){
		
			$func = 'draw_view_'.$fce['kind'];
			$fce['echo_content'] = $func($fce['data']);
			
			if(is_array($fce['echo_content'])){
			
				$echo[] = $fce['echo_content'];
			
			}else{
			
				$echo .= $fce['echo_content'];
			
			}
			
		}
		
		
	}else{
		$echo .= '';
	}
	
	
	
	return $echo;

}

?>