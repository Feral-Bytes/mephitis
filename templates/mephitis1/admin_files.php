<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_files($info, $table, $tree, $rootcount)
{
	
	global $phproot, $libs;
	
	$echo = '';
	
	$link = $phproot.'?admin=files&dir='.$info['dir'].'';
	$name = $info['name'];
	
	$templateFileTree = new DrawFileTreeAdmin($tree);
	
	$echo .= '
		<div class="fileadmin">
			'.draw_mediaselect_uploadform($info['name'], $link).'
			<hr>
				'.$templateFileTree->Render().'
			<hr>
			'.$table.'
		</div>
	';
	
	if(isset($_GET['ajax']))
	{
		if($_GET['ajax'] == 'upload')
		{
			echo $echo;
			
		}
		exit;
	}
	
	$libs['html_footer']['fileupload'] = '
		<script>
			$(document).on("click", "button#'.$name.'_upload", function(){ 
				
				var formData = new FormData($("form#upload_form_'.$name.'")[0]);
				
				$(".upload_'.$name.'").html( "'.get_label('uploading').'<br><br><div class=\"progress\"><div id=\"bar_form_'.$name.'\" class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"0\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"min-width: 2em;\"></div></div>" );

				var bar_form_'.$name.' = $("#bar_form_'.$name.'");
				
				var request  = $.ajax({
					type: "POST",
					url: "'.$link.'&ajax=upload&orderby=time&ordermode=DESC",
					processData: false,
					contentType: false,					
					data: formData,
					xhr: function() {
						var xhr = new window.XMLHttpRequest();
						
						xhr.upload.addEventListener("progress", function(evt) {
							if (evt.lengthComputable) {
								var percentComplete = evt.loaded / evt.total;
								percentComplete = parseInt(percentComplete * 100);
								
								bar_form_'.$name.'.attr(\'aria-valuenow\', percentComplete);
								bar_form_'.$name.'.width(percentComplete+"%");
								bar_form_'.$name.'.html(percentComplete+"%");
								
								if (percentComplete === 100) {
									
								}
							}
						}, false);
						return xhr;
					},					
				});			
				
				request.done(function( msg ) {
					$(".fileadmin").html(msg); 
				});
			});
		</script>
	';
	
	return $echo;
}

class DrawFileTreeAdmin
{
	var $tree;
	
	function __construct($tree)
	{
		$this->tree = $tree;
	}
	
	private function BuildList($tree, $lvl = 0)
	{
		global $phproot;
		$echo = '';
		
		$parent = 0;
		$isBranch = false;
		$i = 0;
		$list = array();
		$count = count($tree);
		
		if(is_array($tree));
		foreach($tree as $element)
		{
			$i++;
			
			$parent = $element['sub_of'];
			$isBranch = $element['isBranch'];
			$icons = array();
			//$icons[] = '<a href="#" class="submenu_link"><span id="controll_icon_nav_dir_'.$element['id'].'" class="controll_icon"></span></a>';
			$icons[] = '<i class="fa fa-folder-open-o" title="'.$element['id'].'"></i>';
			
			$actions = array();
			$actions[] = '<a href="'.$phproot.'?admin='.$_GET['admin'].'&dir_action=new&parent='.$element['id'].'"><i class="fa fa-level-down" title="'.get_label('dir_new_next_lvl').'"></i></a>';
			$actions[] = '<a href="'.$phproot.'?admin='.$_GET['admin'].'&dir_action=edit&parent='.$element['sub_of'].'&itemid='.$element['id'].'"><i class="fa fa-wrench" title="'.get_label('dir_edit').'"></i></a>';
			$actions[] = '<a href="'.$phproot.'?admin='.$_GET['admin'].'&dir_action=del&parent='.$element['sub_of'].'&itemid='.$element['id'].'"><i class="fa fa-trash" title="'.get_label('dir_del').'"></i></a>';
			
			
			//$actions[] = '<a href="'.$phproot.'?admin='.$_GET['admin'].'&dir='.$element['id'].'"><i class="fa fa-square-o" title="'.get_label('dir_select').'"></i></a>';
			/*
			if($count != 1)
			{
				
				$uplink = draw_action_link($phproot.'?admin='.$_GET['admin'].'&order_dir=up&itemid='.$element['id'].'', 'up');
				$downlink = draw_action_link($phproot.'?admin='.$_GET['admin'].'&order_dir=down&itemid='.$element['id'].'', 'down');
				
				if($i == 1){
					$actions[] = $downlink;
				}else if($i == $count){
					$actions[] = $uplink;
				}else{
					$actions[] = $uplink;
					$actions[] = $downlink;
				}
			}
			*/
			if(isset($_GET['dir']) && !$element['isBranch'])
			{
				$actions[] = '<a href="'.$phproot.'?admin='.$_GET['admin'].'&move_dir_in='.$element['id'].'&dir='.$_GET['dir'].'"><i class="fa fa-arrow-left text-success" title="'.get_label('dir_move_here').'"></i></a>';	
				//$actions[] = '<a href="'.$phproot.'?admin='.$_GET['admin'].'&move_dir_in='.$element['sub_of'].'&after='.$element['id'].'&dir='.$_GET['dir'].'"><i class="fa fa-arrow-down text-success" title="'.get_label('dir_move_here').'"></i></a>';	
			}
			
			$elementHTML = implode('&nbsp;', $icons).'&nbsp;<a href="'.$phproot.'?admin='.$_GET['admin'].'&dir='.$element['id'].'">'.$element['name'].'</a>&nbsp;<span class="badge">'.CountDirFiles($element['id']).'</span>&nbsp;'.implode('&nbsp;|&nbsp;' ,$actions);
			
			if($element['hasChildren'])
			{
				$elementHTML .= $this->BuildList($element['children'], $lvl+1);
			}
			
			$css = array();
			
			if((isset($_GET['dir_action']) && (isset($_GET['itemid']) && $_GET['itemid'] == $element['id']) || (isset($_GET['parent']) && $_GET['parent'] == $element['id'])) || (isset($_GET['dir']) && $_GET['dir'] == $element['id']))
			{
				$css[] = 'selected';
			}
			
			$list[] = '
				<li class="'.implode(' ', $css).'">
					'.$elementHTML.'
				</li>
			';
		}
		
		$css = array();
		$actions = array();
		$actions[] = '<a href="'.$phproot.'?admin='.$_GET['admin'].'&dir_action=new&parent='.$parent.'"><i class="fa fa-folder-o" title="'.get_label('dir_new_this_lvl').'"></i></a>';
		
		if(isset($_GET['dir']) && !$isBranch)
		{
			$actions[] = '<a href="'.$phproot.'?admin='.$_GET['admin'].'&move_dir_in='.$parent.'&dir='.$_GET['dir'].'"><i class="fa fa-arrow-left text-success" title="'.get_label('dir_move_here').'"></i></a>';	
			//$actions[] = '<a href="'.$phproot.'?admin='.$_GET['admin'].'&move_dir_in='.$parent.'&after=0&dir='.$_GET['dir'].'"><i class="fa fa-arrow-down text-success" title="'.get_label('dir_move_here_after').'"></i></a>';	
			
		}
		
		if(isset($_GET['dir']))
		{
			//$actions[] = '<a href="'.$phproot.'?admin='.$_GET['admin'].'&dir_copy_in='.$parent.'&dir='.$_GET['dir'].'"><i class="fa fa-files-o text-success" title="'.get_label('dir_copy_here').'"></i></a>';	
		}
		
		if($lvl == 0 && isset($_GET['dir_action']) && isset($_GET['parent']) && $_GET['parent'] == $parent)
		{
			//$css[] = 'selected';
		}
		if($lvl != 0)
		{
			$css[] = '';
		}
		$echo .= '
			<ul class="'.implode(' ', $css).'" id="dir_'.$parent.'">
				<li>'.implode('&nbsp;|&nbsp;' ,$actions).'</li>
				'.implode('', $list).'
				
			</ul>
		';

		return $echo;
	}
	
	public function Render()
	{
		global $phproot;
		$echo = '
			
			<ul class="cms_content_tree" id="cms_file_tree">
				<li>
					<span class="cms_file_tree_dir"><i class="fa fa-folder-open-o"></i>&nbsp;<a href="'.$phproot.'?admin='.$_GET['admin'].'&dir=0">'.get_label('root_dir').'</a>&nbsp;<span class="badge">'.CountDirFiles(0).'</span></span>
					'.$this->BuildList($this->tree->GetTree()).'
				</li>
			</ul>
		';
		
		return $echo;
	}
	
}

?>