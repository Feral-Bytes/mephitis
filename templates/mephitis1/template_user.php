<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_user_info($info)
{
	global $lang, $httpRootPath;
	
	if(!is_array($info) && is_numeric($info))
	{
		$info = user_info($info);
	}
	
	$echo = '';
	
	$avatar = '';
	
	if($info['avatar'] != 0)
	{
		$avatar = '<img src="'.$httpRootPath.'index.php?file='.$info['avatar'].'&mode=full&size=150" class="img-responsive img-rounded" alt="'.$lang['avatar'].'">';
	}
	
	$echo .= '
		'.$info['name'].'
		<br>
		'.$avatar.'
	';
	
	return $echo;
}

function draw_login($formdata){

	global $phproot, $lang;

	$echo = '
		<form name="'.$formdata['name'].'" action="'.$formdata['action'].'" method="'.$formdata['method'].'" class="form-horizontal" role="form">
			<div class="form-group">
				<label for="name" class="col-sm-2 control-label">'.$lang['login_name'].'</label>
				<div class="col-sm-4">
					<input type="text" name="name" class="form-control" id="name" placeholder="'.$lang['login_name'].'">
				</div>
			</div>
			<div class="form-group">
				<label for="pw" class="col-sm-2 control-label">'.$lang['user_pass'].'</label>
				<div class="col-sm-4">
					<input type="password" name="pass" class="form-control" id="pw" placeholder="'.$lang['user_pass'].'">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<div class="checkbox">
						<label>
							<input type="checkbox" name="login_remember"> '.$lang['login_remember'].'
						</label>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<input type="submit" class="btn btn-success" name="login" value="Login">
					';
					if(getvar('public_register') == '1'){
						$echo .= '<a href="'.$phproot.'?usercp=register" class="btn btn-danger" role="button">'.$lang['register'].'</a>';
					}
					$echo .= '
					<a href="'.$phproot.'?usercp=forgotpassword" class="btn btn-primary" role="button">'.$lang['forgotpassword'].'</a>
				</div>
			</div>
		</form>
	';

	return $echo;

}

function draw_groups($groups)
{
	
	$echo_groups = array();
	
	foreach(explode(';', $groups) as $user_group)
	{
		if(is_numeric($user_group))
		{
			$group = group_info($user_group);
			if(is_array($group))
			{
				$echo_groups[] = $group['name'];
			}
			else
			{
				$echo_groups[] = '';
			}
		}
		else
		{
			$echo_groups[] = '';
		}
	}
	
	return implode(', ', $echo_groups);
}

?>