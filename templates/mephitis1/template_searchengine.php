<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

class draw_searchengine
{
	
	var $searchengine;
	
	function __construct()
	{
		
	}
	
	function form($cssForm = '')
	{
		global $lang, $query;
		
		$echo = '';
		
		
		
		$prefill = '';
		
		if(isset($_REQUEST['search_query']))
		{
			$prefill = htmlspecialchars($_REQUEST['search_query']);
		}
		
		$echo .= '
			<form class="'.$cssForm.'" method="POST" action="'.$this->searchengine->actionlink.'">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="'.$lang['search_placeholder'].'" name="search_query" value="'.$prefill.'">
					<span class="input-group-btn">
						<button class="btn btn-default" title="'.$lang['search_btn'].'" type="submit"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
					</span>
				</div>
			</form>
		';
		return $echo;
	}
	
	function serp()
	{
		global $lang, $phproot;
		
		
		$echo = '';
		
		$query = $_REQUEST['search_query'];
		$results_list = array();
		if(is_array($this->searchengine->results))
		{
			foreach($this->searchengine->results as $module => $results)
			{
				if(isset($results['config']['results_render_function']))
				{
					$echo .= $results['config']['results_render_function']($results);
				}
				else
				{
					if(isset($results['results']))
					foreach($results['results'] as $result)
					{
						if(isset($results['config']['result_render_function']))
						{
							$results_list[] = $results['config']['result_render_function']($result, $results['config']);
						}
						else
						{
							if(!isset($result['content']))
							{
								$result['content'] = '';
							}
							
							$link = '';
							if(is_array($results['config']['link']))
							{
								if(trim($results['config']['link']['shortlink']) != '')
								{
									$link = 'show='.trim($results['config']['link']['shortlink']).'';
								}
								else
								{
									$link_id = $results['config']['link']['id'];
									$link_kind = $results['config']['link']['kind'];
									$link = ''.$link_kind.'='.$link_id .'';
								}
							}
							else
							{
								if($results['config']['link'] == 'site')
								{
									
									$idField = 'id';
									if(isset($results['config']['IdField']))
									{
										$idField = $results['config']['IdField'];
									}
									
									if(isset($result['shortlink']) && trim($result['shortlink']) != '')
									{
										$link = 'show='.$result['shortlink'];
									}
									else
									{
										$link = 'site='.$result[$idField];
									}
								}
							}
							
							$linkadd = '';
							
							if(isset($results['config']['linkadd']) && is_array($results['config']['linkadd']))
							{
								foreach($results['config']['linkadd'] as $key => $value)
								{
									$value = str_replace('{resultid}', $result['id'], $value);
									$linkadd .= '&'.$key.'='.$value;
								}
							}
							
							$results_list[] = '
								<div class="search_result">
									<p>
									<h4><a href="'.$phproot.'?'.$link.''.$linkadd.'">'.$result['title'].'</a></h4>
									<p>
									'.$result['content'].'
									</p>
									<hr>
								</div>
							';
						}
					}
				}
			}
			
		}
		else
		{
			$echo = $lang['search_nothingfound'];
		}
		if(count($results_list) < 1)
		{
			$echo = $lang['search_nothingfound'];
		}
		
		$echo .= implode('', $results_list);
		
		if(isset($this->searchengine->conf['template']['func_draw_content']) && $this->searchengine->conf['template']['func_draw_content'] == false)
		{
			$echo = $echo;
		}
		else
		{	
			$echo = draw_content($lang['search_result'], $echo);
		}
		
		return $echo;
		
	}
	
}

?>