<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_form($inputs, $header, $formdata){

	//base formular Template
	global $assetService, $phpRootPath, $httpRootPath;
	global $lang, $user, $link, $libs, $config, $modulename, $msg_stack, $httpRootPath;

	$jquery = '';
	
	$mode = '';
	
	$base_category = 'root';
	
	if(!isset($formdata['actionname'])){
		$formdata['actionname'] = 'action';
	}
	
	if(!isset($formdata['form_msgs'])){
		$formdata['form_msgs'] = '';
	}	
	
	if(isset($formdata['mode'])){
		if($formdata['mode'] == 'modal') $mode = 'modal';
		if($formdata['mode'] == 'noframe') $mode = 'noframe';
	}
	
	//Make inputs html parts
	foreach($inputs as $input){
		
		$atr = '';
		$cssvalid = '';
		
		if(isset($input['name']) AND isset($_POST[$input['name']]) AND ($input['type'] != 'file' AND !is_array($_POST[$input['name']]))){

			$_POST[$input['name']] = htmlspecialchars($_POST[$input['name']]);
			$input['value'] = $_POST[$input['name']];
		}
		
		if(!isset($input['value']))
		{
			$input['value'] = '';
		}
		else
		{
			$input['value'] = $input['value'];
		}
		
		if(!isset($input['placeholder']))
		{
			if(isset($input['lang']))
			{
				$input['placeholder'] = $input['lang'];
			}
			else
			{
				$input['placeholder'] = '';
			}
		}
		
		if(!isset($input['category']) || $input['category'] == '')
		{
			$input['category'] = $base_category;
		}
		
		if(isset($input['required']) AND $input['required'] == '1'){
			$required = '<font color="red">*</font>';
			if(isset($_POST['send']) AND (!isset($_POST[$input['name']]) || $_POST[$input['name']] == ''))
			{
				$cssvalid = 'alert-danger';
			}
		}else{
			$required = '';
		}
		
		if((isset($input['disabled']) AND  $input['disabled'] != false))
		{
			$atr .= ' disabled ';
		}

		if(isset($input['validation_func']) && trim($input['validation_func']) != '' && isset($_POST[$input['name']]) && $_POST[$input['name']] != '')
		{
			$validation = $input['validation_func']($_POST[$input['name']]);
			if(!$validation)
			{
				$cssvalid = 'alert-danger';
			}
		}

		if($input['type'] == 'text'){
			$ajax_class = '';
			if(isset($input['autocomplete'])){
				
				$ajax_class .= $input['autocomplete']['class'].' ';
				
				$jquery .= '
					$(\'.'.$input['autocomplete']['class'].'\').autocomplete({source:\'index.php?'.$link.'&'.$formdata['actionname'].'='.$_GET[$formdata['actionname']].'&ajax=get'.$input['name'].'ByLetters\', minLength:'.$input['autocomplete']['minlength'].'});
				';
				
			}
			
			if(isset($input['datepicker']) && isset($input['value']) && $input['value'] != 0)
			{
				$input['value'] = date(get_label('dateFormat'), strtotime($input['value']));
			}
			
			if(isset($input['datepicker'])){
				$ajax_class .= $input['datepicker']['class'].' ';
				
				$dateformat = get_label('dateFormatJs');
				if(isset($input['datepicker']['dateFormat']))
				{
					$dateformat = $input['datepicker']['dateformat'];
				}
				
				if(file_exists($phpRootPath.'libs/jquery-ui/i18n/datepicker-'.$user['lang'].'.js'))
				{
					$assetService->Add(
						array(
							'name' => 'datepicker-'.$user['lang'].'.js',
							'type' => 'js',
							'path' => $phpRootPath.'libs/jquery-ui/i18n/datepicker-'.$user['lang'].'.js'
						)
					);
				}
				
				$jquery .= '
					$(\'.'.$input['datepicker']['class'].'\').datepicker({
						dateFormat: "'.$dateformat.'",
						changeMonth: true,
						changeYear: true,
						showOtherMonths: true,
						selectOtherMonths: true,
						yearRange: "-100:+100"
					});
				';
			}
			if(isset($input['entertime']) AND $input['entertime'] == '1'){
				if($_GET[$formdata['actionname']] == 'edit'){
					$input['value'] = date(getvar('norm_date'), $input['value']);
				}
			}
			$html = '<input type="text" class="form-control  '.$cssvalid.'  '.$ajax_class.'" id="'.$input['name'].'" name="'.$input['name'].'" value="'.$input['value'].'" placeholder="'.$input['placeholder'].'" '.$atr.'>';
			
			if(isset($input['timepicker']) AND $input['timepicker'] == '1'){
				if(isset($input['value']) && $input['value'] > 0)
				{
					if(isset($input['timepicker_atr']['days']))
					{
						$i_d = floor($input['value']/(3600*24));
						$input['value'] -= $i_d*(3600*24);
					}
					$i_h = floor($input['value']/3600);
					$i_m = floor((($input['value']/3600)-$i_h)*60);
					$i_s = $input['value']-($i_h*3600)-($i_m*60);
				}
				$options_d = '';
				if(isset($input['timepicker_atr']['days']))
				{
					for($i=0;$i<=$input['timepicker_atr']['days'];$i++)
					{
						$atr = '';
						if(isset($i_d) && $i_d == $i)
						{
							$atr .= ' selected';
						}
						$options_d .= '<option value="'.$i.'" '.$atr.'>'.sprintf("%02d", $i).'</option>';
					}
				}
				
				$options_h = '';
				for($i=0;$i<24;$i++)
				{
					$atr = '';
					if(isset($i_h) && $i_h == $i)
					{
						$atr .= ' selected';
					}
					$options_h .= '<option value="'.$i.'" '.$atr.'>'.sprintf("%02d", $i).'</option>';
				}
				$options_m = '';
				$options_s = '';
				
				for($i=0;$i<60;$i++)
				{
					$atr_m = '';
					if(isset($i_m) && $i_m == $i)
					{
						$atr_m .= ' selected';
					}
					
					$atr_s = '';
					if(isset($i_s) && $i_s == $i)
					{
						$atr_s .= ' selected';
					}					
					
					$options_m .= '<option value="'.$i.'" '.$atr_m.'>'.sprintf("%02d", $i).'</option>';
					$options_s .= '<option value="'.$i.'" '.$atr_s.'>'.sprintf("%02d", $i).'</option>';
				}				
				
				$options_placeholder_atr = '';
				
				if(!isset($i_h))
				{
					$options_placeholder_atr .= ' selected';
				}
				
				$html = '';
				
				if(isset($input['timepicker_atr']['days']))
				{	
					$day_value = '';
					if(isset($i_d))$day_value = $i_d;
					$html .= '
						<input type="text" size="3" class=" '.$ajax_class.'" id="'.$input['name'].'_d" name="'.$input['name'].'_d" value="'.$day_value.'" placeholder="DD">			
					';
				}
				
				
				$html .= '
					<select type="text" class=" '.$ajax_class.'" id="'.$input['name'].'_h" name="'.$input['name'].'_h">
						<option value="0" class="placeholder" disabled '.$options_placeholder_atr.'>HH</option>
						<option value="0" class="placeholder" disabled>---</option>
						'.$options_h.'
					</select>
					<select type="text" class=" '.$ajax_class.'" id="'.$input['name'].'_m" name="'.$input['name'].'_m">
						<option value="0" class="placeholder" disabled '.$options_placeholder_atr.'>MM</option>
						<option value="0" class="placeholder" disabled>---</option>
						'.$options_m.'
					</select>
					<select type="text" class=" '.$ajax_class.'" id="'.$input['name'].'_s" name="'.$input['name'].'_s">
						<option value="0" class="placeholder" disabled '.$options_placeholder_atr.'>SS</option>
						<option value="0" class="placeholder" disabled>---</option>
						'.$options_s.'
					</select>					
				';
			}
			
			if(isset($input['tooltip']) && trim($input['tooltip']) != '')
			{
				$input['lang'] = ''.$input['lang'].' <a title="'.$input['tooltip'].'" href="#">?</a>';
			}
			
			$echo_inputs[$input['name']] = array(
				'lang' => $input['lang'].$required,
				'html' => $html,
				'category' => $input['category']
			);
		}else if($input['type'] == 'textarea'){
			$extra = '';
			$css = '';
			
			if(isset($input['wysiwyg'])){
				if($input['wysiwyg'] == 'classic')
				{
					$input['bbcode'] = getvar('bbcode');
					$input['emojis'] = getvar('emojis');
				}
				else
				{
					draw_TinyMCE($input['wysiwyg']);
					$css .= ' wysiwyg_'.$input['wysiwyg'].'';
				}
			}				
			
			if(isset($input['bbcode']) AND $input['bbcode'] == '1'){
				$extra .= draw_bbcode_btns($input['name']).'<br>';
			}
			if(isset($input['emojis']) AND $input['emojis'] == '1'){
				$extra .= draw_emoji_btns(emoji_list('db'), $input['name']).'<br>';
			}		
			
			if(isset($input['tooltip']) && trim($input['tooltip']) != '')
			{
				$input['lang'] = ''.$input['lang'].' <a title="'.$input['tooltip'].'" href="#">?</a>';
			}			
			
			$echo_inputs[$input['name']] = array(
				'lang' => $input['lang'].$required,
				'html' => $extra.'<textarea class="form-control  '.$cssvalid.'  '.$css.'" style="height: 120px;" rows="8" name="'.$input['name'].'" '.$atr.'>'.$input['value'].'</textarea>',
				'category' => $input['category']
			);
		}else if($input['type'] == 'radio'){
			$options = '';
			
			if(isset($input['template']) && $input['template'] == 'bsbtn')
			{
				if(is_array($input['options']))foreach($input['options'] as $value => $option){
					$option['activeClass'] = '';
					if(!isset($option['active']))$option['active'] = '';
					if((isset($_GET[$formdata['actionname']]) AND $_GET[$formdata['actionname']] == 'new' AND isset($option['default']) AND $option['default'] == '1' AND !isset($_POST[$input['name']])) OR (isset($input['value']) AND $input['value'] == $value) OR (isset($_POST[$input['name']]) AND $_POST[$input['name']] == $value)){
						$option['active'] = ' checked';
					}
					if($option['active'] != '')
					{
						$option['activeClass'] = ' active';
					}
					
					if(isset($input['disabled']))
					{
						$options = '
							<div class="btn btn-default '.$option['activeClass'].' disabled">
								<label>
									<input type="radio" name="'.$input['name'].'" id="'.$input['name'].'" value="'.$value.'"'.$option['active'].' '.$atr.'>
									'.$option['lang'].'
								</label>
							</div>
						';
						break;						
					}
					else
					{
						$options .= '
							<div class="btn btn-default '.$option['activeClass'].'">
								<label>
									<input type="radio" name="'.$input['name'].'" id="'.$input['name'].'" value="'.$value.'"'.$option['active'].' '.$atr.'>
									'.$option['lang'].'
								</label>
							</div>
						';
					}
				}
				
				$options = '
					<div class="btn-group" data-toggle="buttons">
						'.$options.'
					</div>
				';	
			}
			else
			{
				if(is_array($input['options']))foreach($input['options'] as $value => $option){
					if(!isset($option['active']))$option['active'] = '';
					if((isset($_GET[$formdata['actionname']]) AND $_GET[$formdata['actionname']] == 'new' AND isset($option['default']) AND $option['default'] == '1' AND !isset($_POST[$input['name']])) OR (isset($input['value']) AND $input['value'] == $value) OR (isset($_POST[$input['name']]) AND $_POST[$input['name']] == $value)){
						$option['active'] = ' checked';
					}
					$options .= '
						<div class="radio">
							<label>
								<input type="radio" name="'.$input['name'].'" id="'.$input['name'].'" value="'.$value.'"'.$option['active'].' '.$atr.'>
								'.$option['lang'].'
							</label>
						</div>
					';
				}
			}			
			
			if(isset($input['tooltip']) && trim($input['tooltip']) != '')
			{
				$input['lang'] = ''.$input['lang'].' <a title="'.$input['tooltip'].'" href="#">?</a>';
			}			
			
			$echo_inputs[$input['name']] = array(
				'lang' => $input['lang'].$required,
				'html' => $options,
				'category' => $input['category']
			);
		}else if($input['type'] == 'select'){
			$options = '';
			
			if(isset($input['multiple']) AND $input['multiple'] == '1'){
				$options .= '<select class="" name="'.$input['name'].'[]" multiple '.$atr.'>';
			}else{
				$options .= '<select class="form-control  '.$cssvalid.' " name="'.$input['name'].'" '.$atr.'>';
			}
			
			if(!isset($input['value']))
			{
				$input['value'] = '';
			}
			else
			{
				if(isset($input['multiple']) && isset($input['value']))
				{
					$input['value'] = explode(';', $input['value']);
				}
			}

			if(isset($input['groups']) AND $input['groups'] == '1'){
				foreach($input['options'] as $grp => $grp_array){
					if(is_array($grp_array['options'])){
						$options .= '<optgroup label="'.$grp_array['lang'].'">';
						foreach($grp_array['options'] as $value => $option){
							if(!isset($option['lang']))
							{
								$option['lang'] = $value;
							}
							if(!isset($option['active']) OR isset($_POST[$input['name']]))$option['active'] = '';
							
							if(isset($_GET[$formdata['actionname']]) AND $_GET[$formdata['actionname']] == 'new' AND isset($option['default']) AND $option['default'] == '1' AND !isset($_POST[$input['name']])){
								$option['active'] = ' selected';
							}
							if(isset($_POST[$input['name']]))
							{
								if(isset($input['multiple']) && $input['multiple'] == '1')
								{
									for($i = 0; $i < count($_POST[$input['name']]); $i++)
									{
										if(isset($_POST[$input['name']][$i]) && $_POST[$input['name']][$i] == $value)
										{
											$option['active'] = ' selected';
											break;
										}
									}
								}
								else
								{
									if($_POST[$input['name']] == $value)
									{
										$option['active'] = ' selected';
									}
								}
							}
							
							if($input['value'] == $value OR (is_array($input['value']) AND in_array($value, $input['value'])))
							{
								
							}
							
							$options .= '<option value="'.$value.'"'.$option['active'].'>'.$option['lang'].'</option>';
						}
						$options .= '</optgroup>';
					}
				}
			}else{
				if(is_array($input['options']))foreach($input['options'] as $value => $option){
					if(!isset($option['active']) OR isset($_POST[$input['name']]))$option['active'] = '';
					if(!isset($option['lang']))
					{
						$option['lang'] = $value;
					}
					
					if((isset($_GET[$formdata['actionname']]) AND $_GET[$formdata['actionname']] == 'new' AND isset($option['default']) AND $option['default'] == '1' AND !isset($_POST[$input['name']])) OR (isset($input['value']) AND ($input['value'] == $value OR (is_array($input['value']) AND in_array($value, $input['value']))) AND !isset($_POST[$input['name']])) OR (isset($_POST[$input['name']]) AND (!is_array($_POST[$input['name']]) AND ($_POST[$input['name']] == $value) OR (is_array($_POST[$input['name']]) AND in_array($value, $_POST[$input['name']]))))){
						$option['active'] = ' selected';
					}
					
					if(isset($input['multiple']) && $input['multiple'] == '1' && isset($_POST[$input['name']]))
					{
						for($i = 0; $i < count($_POST[$input['name']]); $i++)
						{
							if(isset($_POST[$input['name']][$i]) && $_POST[$input['name']][$i] == $value)
							{
								$option['active'] = ' selected';
								break;
							}
						}
					}
					
					$options .= '<option value="'.$value.'"'.$option['active'].'>'.$option['lang'].'</option>';
				}
			}
			
			$options .= '</select>';
			
			if(isset($input['tooltip']) && trim($input['tooltip']) != '')
			{
				$input['lang'] = ''.$input['lang'].' <a title="'.$input['tooltip'].'" href="#">?</a>';
			}			
			
			$echo_inputs[$input['name']] = array(
				'lang' => $input['lang'].$required,
				'html' => $options,
				'category' => $input['category']
			);	
		}else if($input['type'] == 'checkbox'){
			
			if(!isset($input['active']) OR isset($_POST['send']))
			{
				$input['active'] = '';
			}			
			
			if((isset($_GET[$formdata['actionname']]) AND $_GET[$formdata['actionname']] == 'new' AND isset($input['default']) AND $input['default'] == '1' AND !isset($_POST['send'])) OR isset($_POST[$input['name']])){
				
				$input['active'] = ' checked';
			}
			
			if(isset($input['value']) && $input['value'] != '' && $input['value'] != '0')
			{
				$input['active'] = ' checked';
			}
			
			if(!isset($input['atr']))
			{
				$input['atr'] = '';
			}
			
			if(isset($input['template']) && $input['template'] == 'bsswitch')
			{
				
				$assetService->Add(
					array(
						'name' => 'bootstrap-switch.css',
						'type' => 'css',
						'path' => $phpRootPath.'libs/bootstrap-switch/bootstrap-switch.min.css'
					)
				);
				
				$assetService->Add(
					array(
						'name' => 'bootstrap-switch.js',
						'type' => 'js',
						'path' => $phpRootPath.'libs/bootstrap-switch/bootstrap-switch.min.js'
					)
				);
				
				$assetService->Add(
					array(
						'name' => $formdata['name'].'_bsswitch_'.$input['name'],
						'type' => 'js',
						'content' => '$("[name=\''.$input['name'].'\']").bootstrapSwitch();'
					)
				);
				
				$input['atr'] = 'data-label-text="'.$input['lang'].'" data-on-text="'.$lang['yes'].'" data-off-text="'.$lang['no'].'"';
				$input['lang'] = '';
			}			
			
			$tooltip = '';
			if(isset($input['tooltip']) && trim($input['tooltip']) != '')
			{
				$tooltip = ' <a title="'.$input['tooltip'].'" href="#">?</a>';
			}			
			
			$echo_inputs[$input['name']] = array(
				'type' => $input['type'],
				'lang' => $input['lang'].$required,
				'html' => '
					<div class="checkbox">
						<label>
							<input type="checkbox" name="'.$input['name'].'"'.$input['active'].' '.$input['atr'].' '.$atr.'>
							'.$input['lang'].''.$tooltip.'
						</label>
					</div>
				',
				'category' => $input['category']
			);
		}else if($input['type'] == 'password'){
			if(isset($_GET[$formdata['actionname']]) AND $_GET[$formdata['actionname']] != 'new')$required = '';
			$echo_inputs[$input['name']] = array(
				'lang' => $input['lang'].$required,
				'html' => '<input type="password" class="form-control  '.$cssvalid.' " id="'.$input['name'].'" name="'.$input['name'].'" placeholder="'.$input['lang'].'" '.$atr.' autocomplete="off">',
				'category' => $input['category']
			);
			$echo_inputs[$input['name'].'_v'] = array(
				'lang' => $input['lang_v'].$required,
				'html' => '<input type="password" class="form-control  '.$cssvalid.' " id="'.$input['name'].'_v" name="'.$input['name'].'_v" placeholder="'.$input['lang_v'].'" '.$atr.' autocomplete="off">',
				'category' => $input['category']
			);
		}else if($input['type'] == 'file'){
			if(isset($_GET[$formdata['actionname']]) && $_GET[$formdata['actionname']] != 'new')$required = '';
			
			$thumb = ''.draw_media($input['value']).'';
			
			if(isset($input['multiple']))
			{
				$atr .= ' multiple="multiple" ';
				$input['name'] = $input['name'].'[]';
			}
			
			$extra = '';
			
			if(isset($input['delbtn']) AND $input['value'] != 0 AND $input['value'] != '')
			{
				$extra .= '<br><input type="checkbox" name="'.$input['name'].'_delbtn"> '.$lang['del'];
			}
			
			if(isset($input['tooltip']) && trim($input['tooltip']) != '')
			{
				$input['lang'] = ''.$input['lang'].' <a title="'.$input['tooltip'].'" href="#">?</a>';
			}			
			
			$echo_inputs[$input['name']] = array(
				'lang' => $input['lang'].$required,
				'html' => '<input type="file" name="'.$input['name'].'" '.$atr.'>'.$thumb.$extra,
				'category' => $input['category']
			);
		}else if($input['type'] == 'media'){
			if(isset($_GET[$formdata['actionname']]) && $_GET[$formdata['actionname']] != 'new')$required = '';
			if(!isset($input['files']))
			{
				$input['files'] = mediaselect_filelist($input);
			}
			
			if(isset($input['tooltip']) && trim($input['tooltip']) != '')
			{
				$input['lang'] = ''.$input['lang'].' <a title="'.$input['tooltip'].'" href="#">?</a>';
			}
			
			if((isset($input['disabled']) AND  $input['disabled'] != false))
			{
				$echo_inputs[$input['name']] = array(
					'lang' => $input['lang'].$required,
					'html' => draw_media($input['value']),
					'category' => $input['category']
				);				
			}
			else
			{
				$echo_inputs[$input['name']] = array(
					'lang' => $input['lang'].$required,
					'html' => draw_mediaselect($input['name'], $input['value'], $formdata['action'], $input['files'], $mode),
					'category' => $input['category']
				);	
			}
		
		}else if($input['type'] == 'timepicker'){
			if($_GET[$formdata['actionname']] != 'new')$required = '';
			
			if(!isset($input['value']) || trim($input['value']) == '' || !is_numeric($input['value']))
			{
				$input['value'] = 0;
			}
			
			$dateformat = get_label('dateFormatJs');
			$phpdate = 'Y-m-d, H:i:s';

			$datetime = date($phpdate , $input['value']);
			$datetime = explode(', ', $datetime); 
			$date = $datetime[0];
			$time = $datetime[1];
			
			if(file_exists($phpRootPath.'libs/jquery-ui/i18n/datepicker-'.$user['lang'].'.js'))
			{
				$assetService->Add(
					array(
						'name' => 'datepicker-'.$user['lang'].'.js',
						'type' => 'js',
						'path' => $phpRootPath.'libs/jquery-ui/i18n/datepicker-'.$user['lang'].'.js'
					)
				);
			}
			
			$jquery .= '
				$(\'.'.$input['name'].'_date\').datepicker({
					dateFormat: "'.$dateformat.'",
					changeMonth: true,
					changeYear: true,
					showOtherMonths: true,
					selectOtherMonths: true,
					yearRange: "-100:+100"
				});
			';
			
			if(isset($input['value']))
			{	
				if($input['value'] != 0)
				{
					$time = explode(':', $time);
					$i_h = $time[0];
					$i_m = $time[1];
					$i_s = $time[2];
				}
				if(isset($_POST[$input['name'].'_h']))$i_h = $_POST[$input['name'].'_h'];
				if(isset($_POST[$input['name'].'_m']))$i_m = $_POST[$input['name'].'_m'];
				if(isset($_POST[$input['name'].'_s']))$i_s = $_POST[$input['name'].'_s'];
			}
			
			$options_h = '';
			for($i=0;$i<24;$i++)
			{
				$atr = '';
				if(isset($i_h) && $i_h == $i)
				{
					$atr .= ' selected';
				}
				$options_h .= '<option value="'.$i.'" '.$atr.'>'.sprintf("%02d", $i).'</option>';
			}
			$options_m = '';
			$options_s = '';
			
			for($i=0;$i<60;$i++)
			{
				$atr_m = '';
				if(isset($i_m) && $i_m == $i)
				{
					$atr_m .= ' selected';
				}
				
				$atr_s = '';
				if(isset($i_s) && $i_s == $i)
				{
					$atr_s .= ' selected';
				}					
				
				$options_m .= '<option value="'.$i.'" '.$atr_m.'>'.sprintf("%02d", $i).'</option>';
				$options_s .= '<option value="'.$i.'" '.$atr_s.'>'.sprintf("%02d", $i).'</option>';
			}				
			
			$options_placeholder_atr = '';
			
			if(!isset($i_h))
			{
				$options_placeholder_atr .= ' selected';
			}
			
			$timepicker = '
				<select type="text" class=" '.$ajax_class.'" id="'.$input['name'].'_h" name="'.$input['name'].'_h">
					<option value="0" class="placeholder" disabled '.$options_placeholder_atr.'>HH</option>
					<option value="0" class="placeholder" disabled>---</option>
					'.$options_h.'
				</select>
				<select type="text" class=" '.$ajax_class.'" id="'.$input['name'].'_m" name="'.$input['name'].'_m">
					<option value="0" class="placeholder" disabled '.$options_placeholder_atr.'>MM</option>
					<option value="0" class="placeholder" disabled>---</option>
					'.$options_m.'
				</select>
				<select type="text" class=" '.$ajax_class.'" id="'.$input['name'].'_s" name="'.$input['name'].'_s">
					<option value="0" class="placeholder" disabled '.$options_placeholder_atr.'>SS</option>
					<option value="0" class="placeholder" disabled>---</option>
					'.$options_s.'
				</select>					
			';
			
			$date = date(get_label('dateFormat') ,strtotime($date));
			if($input['value'] == 0)$date = '';
			
			$input['html'] = '<input type="text" name="'.$input['name'].'_date" placeholder="'.$dateformat.'" class="form-control  '.$cssvalid.'  '.$input['name'].'_date" value="'.$date.'"><br>'.$timepicker;
			
			$echo_inputs[$input['name']] = array(
				'lang' => $input['lang'].$required,
				'html' => $input['html'],
				'category' => $input['category']
			);	
			
		
		}else if($input['type'] == 'emoji'){
			if($_GET[$formdata['actionname']] != 'new')$required = '';
			if($_GET['action'] != 'new')
			{
				$thumb = '<br>'.show_emoji($_GET['itemid'], 'mk_emoji').'';
			}
			else
			{
				$thumb = '';
			}
		
			
			if(isset($input['tooltip']) && trim($input['tooltip']) != '')
			{
				$input['lang'] = ''.$input['lang'].' <a title="'.$input['tooltip'].'" href="#">?</a>';
			}			
			
			$echo_inputs[$input['name']] = array(
				'lang' => $input['lang'].$required,
				'html' => '<input type="file" name="'.$input['name'].'">'.$thumb,
				'category' => $input['category']
			);
		}else if($input['type'] == 'custom'){
			
			if(isset($input['input'])){
				$input['html'] = '<input class="form-control  '.$cssvalid.' " type="'.$input['input'].'" name="'.$input['input_name'].'" '.$atr.'>';
			}
			
			if(isset($input['divider'])){
				$input['html'] = '<hr>';
			}
			
			$echo_inputs[] = $input;
			/*
			$echo_inputs[] = array(
				'type' => $input['type'],
				'lang' => $input['lang'],
				'cols' => $input['cols'],
				'html' => $input['html'],
				'category' => $input['category']
			);
			*/
		}
	}
	
	if((isset($_GET[$formdata['actionname']]) AND ($_GET[$formdata['actionname']] == 'new' OR $_GET[$formdata['actionname']] == 'register' OR $_GET[$formdata['actionname']] == 'mail') AND $user['id'] == '0') OR (isset($formdata['captcha'])) AND $formdata['captcha'] == true){
		if(getvar('reCAPTCHA') == '1')
		{
			$echo_inputs[$input['name'].'_1'] = array(
				'lang' => $lang['captcha'],
				'html' => chk_captcha('draw', 'reCAPTCHA'),
				'category' => $input['category']
			);
		}
		else
		{
			$echo_inputs[$input['name'].'_1'] = array(
				'lang' => chk_captcha('draw', 'lang'),
				'html' => chk_captcha('draw', 'img'),
				'category' => $input['category']
			);
			$echo_inputs[$input['name'].'_2'] = array(
				'lang' => chk_captcha('draw', 'lang_confirm'),
				'html' => chk_captcha('draw', 'input'),
				'category' => $input['category']
			);
		}
	}
	
	// echo the formular
	
	if($jquery != '')
	{
		$assetService->Add(
			array(
				'name' => $formdata['name'].'_jquery',
				'type' => 'js',
				'content' => '
					jQuery(document).ready(function($){	
						'.$jquery.'
					});
				'
			)
		);
	}
	
	if(!isset($formdata['fileadd']))
	{
		$formdata['fileadd'] = '';
	}
	
	if(!isset($formdata['infobox']))
	{
		$formdata['infobox'] = '';
	}
	
	$colWidthLabel = '2';
	$colWidthInput = '10';
	
	if($mode == 'modal')
	{
		$colWidthLabel = '4';
		$colWidthInput = '8';
	}
	
	$echo = '
		<a id="'.$formdata['name'].'"></a>
		'.$formdata['infobox'].'
		<form name="'.$formdata['name'].'" id="'.$formdata['name'].'" class="form-horizontal" role="form" action="'.$formdata['action'].'" method="'.$formdata['method'].'"'.$formdata['fileadd'].'>

		';
		
			$echo_form = array();
			if(isset($echo_inputs) && array($echo_inputs))
			foreach($echo_inputs as $key => $input){
				
				$echo_form_input = '';
				
				if(!isset($input['type'])){
					$echo_form_input .= '
						<div class="form-group form-group-sm">
							<label for="'.$key.'" class="col-sm-'.$colWidthLabel.' control-label">'.$input['lang'].'</label>
							<div class="col-sm-'.$colWidthInput.'">
								'.$input['html'].'
							</div>
						</div>
					';
				}else if($input['type'] == 'custom'){
					if(isset($input['cols']) AND $input['cols'] == '1'){
						$echo_form_input .= '
							<div class="form-group form-group-sm">
								<label for="'.$key.'" class="col-sm-'.$colWidthLabel.' control-label">'.$input['lang'].'</label>
								<div class="col-sm-'.$colWidthInput.'">
									'.$input['html'].'
								</div>
							</div>
						';
					}else{
						if(isset($input['lang']))
						{
							$echo_form_input .= '
								<div class="form-group form-group-sm">
									<div class="col-sm-12">
									<label for="'.$key.'" class="control-label">'.$input['lang'].'</label>
										'.$input['html'].'
									</div>
								</div>
							';
						}
						else
						{
							$echo_form_input .= '
								<div class="form-group form-group-sm">
									<div class="col-sm-12">
										'.$input['html'].'
									</div>
								</div>
							';
						}
					}
				}else if($input['type'] == 'checkbox'){
					$echo_form_input .= '
						<div class="form-group form-group-sm">
							<div class="col-sm-offset-'.$colWidthLabel.' col-sm-'.$colWidthInput.'">
								'.$input['html'].'
							</div>
						</div>
					';
				}
				
				$echo_form[$input['category']][] = $echo_form_input;
			}
			
			$category_count = count($echo_form);
			$categorys_nav = '';
			$base_category_form_elements = '';
			$category_form = '';
			
			$i = 0;
			foreach($echo_form as $category => $form_element)
			{
				$i++;
				
				if($category_count > 1)
				{
					if($category != $base_category)
					{
						$css = '';
						if($i <= 1)
						{
							$css .= 'active';
						}						
						
						$categorys_nav .= '
							<li role="presentation" class="'.$css.'">
								<a href="#'.str_replace(' ', '_', trim($category)).'" aria-controls="'.get_label($category).'" role="tab" data-toggle="tab">'.get_label($category).'</a>
							</li>						
						';
						
						$category_form .= '
							<div role="tabpanel" class="tab-pane '.$css.'" id="'.str_replace(' ', '_', trim($category)).'">
								<br>
								'.implode('', $form_element).'
							</div>
						';
					}
					else
					{
						$base_category_form_elements .= implode('', $form_element);
					}
				}
				else
				{
					$echo .= implode('', $form_element);
				}
			}
			
			if($category_count > 1 )
			{
				$echo .= '
					<ul class="nav nav-tabs" role="tablist">
						'.$categorys_nav.'
					</ul>
				';
				
				$echo .= '
					<div class="tab-content">
						'.$category_form.'
					</div>
					<hr>
				';
				
				if(isset($echo_form[$base_category]))
				{
					$echo .= '
						'.$base_category_form_elements.'
					';
				}
			}
			
			
			if($mode != 'modal'){
				$echo .= '
				<div class="form-group">
					<div class="col-sm-offset-'.$colWidthLabel.' col-sm-'.$colWidthInput.'">
						<input type="submit" class="btn btn-default" value="'.$lang['norm_send'].'" name="send">
					</div>
				</div>
				'; 
			}else{			
				
				$echo .= '
					<input type="hidden" value="'.$lang['norm_send'].'" name="send">
				'; 
			}
			$echo .= '
		</form>

	';
	
	if($mode == 'item'){

		if(isset($formdata['form_msgs']))
		{
			$msg_stack .= $formdata['form_msgs'];
		}
	
		$echo = draw_item($header, $echo);
		
	}else if($mode != 'modal' AND $mode != 'noframe'){
		
		if(isset($formdata['form_msgs']))
		{
			$msg_stack .= $formdata['form_msgs'];
		}
		$echo = draw_content($header, $echo);

	}else{
		if($mode == 'noframe')
		{
			$echo = $formdata['form_msgs'].$echo;
		}
	}
	
	if($mode == 'modal')
	{
		$echo = $formdata['form_msgs'].$echo;
		$echo .= '
			'.$assetService->Render('js').'
		';
	}
	
	return $echo;

}

function draw_bbcode_btns($field){

	global $lang;
	
	$echo = '
		<script type="text/javascript">
		<!--
		function bbcode_'.$field.'(v){
			if (document.selection) // IE
			{
				var str = document.selection.createRange().text;
				document.post.'.$field.'.focus();
				var sel = document.selection.createRange();
				sel.text = "[" + v + "]" + str + "[/" + v + "]";
				return;
			}else if (document.getElementById && !document.all) // Mozilla
			{
				var txtarea = document.forms[\'post\'].elements[\''.$field.'\'];
				var selLength = txtarea.textLength;
				var selStart = txtarea.selectionStart;
				var selEnd = txtarea.selectionEnd;
				if (selEnd == 1 || selEnd == 2)
				selEnd = selLength;
				var s1 = (txtarea.value).substring(0,selStart);
				var s2 = (txtarea.value).substring(selStart, selEnd)
				var s3 = (txtarea.value).substring(selEnd, selLength);
				txtarea.value = s1 + \'[\' + v + \']\' + s2 + \'[/\' + v + \']\' + s3;
				return;
			}else{
				input_'.$field.'(\'[\' + v + \'][/\' + v + \'] \');
			}
		}
		
		//-->
		</script>

		<input type="button" class="btn btn-default" onclick="javascript:bbcode_'.$field.'(\'b\');" value="B" id="bbcode_btn" style="font-weight: bold;">
		<input type="button" class="btn btn-default" onclick="javascript:bbcode_'.$field.'(\'u\');" value="U" id="bbcode_btn" style="text-decoration: underline;">
		<input type="button" class="btn btn-default" onclick="javascript:bbcode_'.$field.'(\'i\');" value="I" id="bbcode_btn" style="font-style: italic;">
		<input type="button" class="btn btn-default" onclick="javascript:bbcode_'.$field.'(\'center\');" value="center" id="bbcode_btn" style="">
		<input type="button" class="btn btn-default" onclick="javascript:bbcode_'.$field.'(\'img\');" value="img" id="bbcode_btn" style="">
		<input type="button" class="btn btn-default" onclick="javascript:bbcode_'.$field.'(\'video\');" value="video" id="bbcode_btn" style="">
		<br> 
	';

	return $echo;

}

function draw_emoji_btns($emojis, $field){
	
	global $lang, $phproot;
	
	$echo = '
		<script type="text/javascript">
		<!--
		function input_'.$field.'(what){
			if (document.post.'.$field.'.createTextRange){
				document.selection.createRange().duplicate().text = what;
			}
			else if (document.getElementById && !document.all) // Mozilla
			{
				var tarea = document.forms[\'post\'].elements[\''.$field.'\'];
				var selEnd = tarea.selectionEnd;
				var txtLen = tarea.value.length;
				var txtbefore = tarea.value.substring(0,selEnd);
				var txtafter =  tarea.value.substring(selEnd, txtLen);
				tarea.value = txtbefore + what + txtafter;
			}
			else{
				document.entryform.text.value += what;
			}
			document.forms[\'post\'].'.$field.'.focus();
		}
		//-->
		</script>
	';

	if(is_array($emojis))foreach($emojis as $code => $emoji){
		$echo .= '<a href="#'.$field.'" onclick="javascript:input_'.$field.'(\' '.$code.' \')">'.$emoji.'</a> ';
	}
	
	$echo = '<div id="emoji_list">'.$echo.' <a href="#'.$field.'" onclick="window.open(\''.$phproot.'?popup=emojis&field='.$field.'\',\''.$lang['emojis'].'\',\'HEIGHT=500,resizable=yes,scrollbars=yes,WIDTH=400\');">'.$lang['more'].'</a></div>
		<a name="'.$field.'"></a>
	';
	
	return $echo;
	
}

function draw_emojis_popup($field){

	global $lang;

	$js = '
		<script language="javascript" type="text/javascript">
		<!--
		function input_'.$field.'(text) {
			text = \' \' + text + \' \';
			if (opener.document.forms[\'post\'].'.$field.'.createTextRange && opener.document.forms[\'post\'].'.$field.'.caretPos) {
				var caretPos = opener.document.forms[\'post\'].'.$field.'.caretPos;
				caretPos.text = caretPos.text.charAt(caretPos.text.length - 1) == \' \' ? text + \' \' : text;
				opener.document.forms[\'post\'].post.focus();
			} else {
			opener.document.forms[\'post\'].'.$field.'.value  += text;
			opener.document.forms[\'post\'].'.$field.'.focus();
			}
		}
		//-->
		</script>
	';
	
	$emojis = emoji_list('all');
	
	$tab = '';
	if(is_array($emojis))foreach($emojis as $code => $emoji){
		$tab .= '
			<tr>
				<td class="head">
					<a href="#" onclick="input_'.$field.'(\''.$code.'\')">'.$emoji.'</a>
				</td>
				<td class="head">
					'.$code.'
				</td>
			</tr>
		';
	}
	
	$echo = '
		'.$js.'
		<table class="emoji_list_popup">
			<tr>
				<td>
					<b>'.$lang['emoji'].'</b>
				</td>
				<td>
					<b>'.$lang['code'].'</b>
				</td>
			</tr>
			'.$tab.'
		</table>
	';
	
	return $echo;	
}

function draw_TinyMCE($mode)
{
	global $libs, $user, $httpRootPath, $phpRootPath, $assetService;
	
	$plugins = '';
	
	
	$assetService->Add(
		array(
			'name' => 'tinymce',
			'type' => 'js',
			'path' => $httpRootPath.'libs/tinymce/tinymce.min.js',
			'cache' => false
		)
	);
	
	$assetService->Add(
		array(
			'name' => 'tinymce_vars',
			'type' => 'js',
			'content' => '
				var tinymceHttpRootPath = "'.$httpRootPath.'";
			'
		)
	);
	
	
	
	$emoji_list = emoji_list_raw(100);
	$emoji_list_js_y = array();
	
	$i = 0;
	for($e_y = 0; $e_y < floor(sqrt(count($emoji_list)))+1; $e_y++)
	{
		$emoji_list_js_y[$e_y] = '';
		$emoji_list_js_y[$e_y] .= '[';
		
		$emoji_list_js_x = array();
		for($e_x = 0; $e_x < floor(sqrt(count($emoji_list)))+1; $e_x++)
		{
			$emoji_list_js_x[$e_x] = '';
			$emoji_list_js_x[$e_x] .= '"';
			if(isset($emoji_list[$i]['id']))
			{
				$emoji_list_js_x[$e_x] .= $emoji_list[$i]['id'];
			}
			else
			{
				$emoji_list_js_x[$e_x] .= 0;
			}
			$emoji_list_js_x[$e_x] .= '"';
			$i++;
		}
		$emoji_list_js_y[$e_y] .= implode(', ', $emoji_list_js_x);
		$emoji_list_js_y[$e_y] .= ']';
	}
	
	$emoji_list_js = '['.implode(',', $emoji_list_js_y).']';
	

	$assetService->Add(
		array(
			'name' => 'tinymce_plugin_mepemojis_list',
			'type' => 'js',
			'content' => '
				var emojis = '.$emoji_list_js.';
			'
		)
	);
	
	$assetService->Add(
		array(
			'name' => 'tinymce_plugin_mepemojis',
			'type' => 'js',
			'path' => $phpRootPath.'libs/mephitis/js/tinymce/plugin.mepemojis.js'
		)
	);
	
	$assetService->Add(
		array(
			'name' => 'tinymce_plugin_mepbb',
			'type' => 'js',
			'path' => $phpRootPath.'libs/mephitis/js/tinymce/plugin.mepbbcode.js'
		)
	);
	
	$assetService->Add(
		array(
			'name' => 'tinymce_plugin_mepyoutube',
			'type' => 'js',
			'path' => $phpRootPath.'libs/mephitis/js/tinymce/plugin.mepyoutube.js'
		)
	);
	
	$assetService->Add(
		array(
			'name' => 'tinymce_plugin_mepvimeo',
			'type' => 'js',
			'path' => $phpRootPath.'libs/mephitis/js/tinymce/plugin.mepvimeo.js'
		)
	);
	
	$assetService->Add(
		array(
			'name' => 'tinymce_plugin_mepsoundcloud',
			'type' => 'js',
			'path' => $phpRootPath.'libs/mephitis/js/tinymce/plugin.mepsoundcloud.js'
		)
	);
	

	
	if($mode == 'bbcode')
	{
		
		$assetService->Add(
			array(
				'name' => 'tinymce_'.$mode,
				'type' => 'js',
				'content' => '
					tinymce.init({
						selector: "textarea.wysiwyg_'.$mode.'",
						height : 250,
						language : "'.$user['lang'].'",
						plugins : [
							"emoticons code link image textcolor paste contextmenu",
							"mepbbcode mepyoutube mepvimeo mepsoundcloud mepemojis"
						],
						toolbar : "mepemojis emoticons | bold italic underline | alignleft aligncenter alignright alignjustify | undo redo | link unlink image | fontsizeselect forecolor removeformat | mepyoutube mepvimeo mepsoundcloud | code",
						fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
						menubar: false,
						entity_encoding : "raw",
						add_unload_trigger : false,
						remove_linebreaks : false,
						force_p_newlines : false,
						force_br_newlines : false,
						inline_styles : false,
						convert_fonts_to_spans : false,
						paste_as_text: true,
						relative_urls : false
					});
				'
			)
		);
	
	}
	else if($mode == 'html')
	{
		$assetService->Add(
			array(
				'name' => 'tinymce_plugin_meppic',
				'type' => 'js',
				'path' => $phpRootPath.'libs/mephitis/js/tinymce/plugin.meppic.js'
			)
		);
		
		$plugins .= ' meppic';
		
		$assetService->Add(
			array(
				'name' => 'tinymce_'.$mode,
				'type' => 'js',
				'content' => '
					tinymce.init({
						selector: "textarea.wysiwyg_'.$mode.'",
						height : 250,
						language : "'.$user['lang'].'",
						plugins: [
							"advlist autolink link image lists charmap print preview hr anchor pagebreak",
							"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
							"save table contextmenu directionality emoticons template paste textcolor",
							"mepyoutube mepvimeo mepsoundcloud mepemojis '.$plugins.'"
					   ],
					   //content_css: "css/content.css",
					   toolbar: "insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | fontsizeselect forecolor backcolor emoticons | mepemojis mepyoutube mepvimeo mepsoundcloud meppic ", 
					   fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
					   style_formats: [
							{title: \'Bold text\', inline: \'b\'},
							{title: \'Red text\', inline: \'span\', styles: {color: \'#ff0000\'}},
							{title: \'Red header\', block: \'h1\', styles: {color: \'#ff0000\'}},
							{title: \'Example 1\', inline: \'span\', classes: \'example1\'},
							{title: \'Example 2\', inline: \'span\', classes: \'example2\'},
							{title: \'Table styles\'},
							{title: \'Table row 1\', selector: \'tr\', classes: \'tablerow1\'}
						],
						paste_as_text: true,
						relative_urls : false
					 });
				'
			)
		);

	}
}

?>