/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */


function alert_message(css, subject, msg, target = ".msg_stack"){

	var count = $(target).children().length;
	

	$(target).append("<div id=\"msg_box_" + count + "\" class=\"alert " + css + " alert-dismissable\"></div>");
	var msg_box = $(target).find("#msg_box_" + count);
	msg_box.append("<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\"> &times; </button>");
	
	msg_box.append("<strong>" + subject + "</strong>&nbsp;");
	msg_box.append(msg);
	
	msg_box_fadeout(msg_box);
	
}

function msg_box_fadeout(msg_box)
{
	window.setTimeout(function() {
		msg_box.fadeTo(500, 0).slideUp(500, function(){
			$(this).remove(); 
		});
	}, msg_box_fadeout_timer);	
}

function msg_box_static_fadeout(count)
{
	if($(".static_msg_box_fadeout").length)
	{
		if(count != 0)
		{
			$(".static_msg_box_fadeout").first().fadeTo(500, 0).slideUp(500, function(){
				$(this).remove(); 
			});
		}
		count++;
		setTimeout(msg_box_static_fadeout, msg_box_fadeout_timer, count);	
	}
}

window.onload = function () {

	msg_box_static_fadeout(0);

}

$(document).on('hidden.bs.modal', '.modal', function () {
    $('.modal:visible').length && $(document.body).addClass('modal-open');
});