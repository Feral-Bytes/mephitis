/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */


$( document ).ready(function() {
	var btnCache = $('#btnClearCache');
	var clearLink = btnCache.attr('href') + "&ajax=1";
	var icon = btnCache.find("i");
	
	btnCache.attr('href', '#');

	$(document).on("click", "a#btnClearCache", function(){

		var request  = $.ajax({
			 type: "POST",
			 url: clearLink
		});
		
		btnCache.addClass("disabled");
		
		icon.addClass("fa-spinner");
		icon.addClass("fa-spin");
		icon.removeClass("fa-cogs");
		
		request.done(function( msg ) {
			btnCache.removeClass("disabled");
			
			icon.removeClass("fa-spinner");
			icon.removeClass("fa-spin");
			icon.addClass("fa-cogs");
			
			alert_message("alert-success", "", msg);
		});
		
	});
	
	
});

