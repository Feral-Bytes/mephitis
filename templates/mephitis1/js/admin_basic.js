/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

var icon_open	= 'fa-minus-square-o';
var icon_closed	= 'fa-plus-square-o';

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function nav_cookie(cname, mode, data){
	
	var content = '';
	if(getCookie(cname) != ""){
		var content = getCookie(cname);
	}
	
	var contents = content.split(',');
	var check = false;
	
	if(mode == 'add'){
		$.each(contents, function(key, field){
			if(field == data){
				check = true;
			}
		});
		if(check == false){
			if(content == ''){
				content = data;
			}else{
				content += ","+data;
			}
		}
		setCookie(cname, content, 10);
	}else if(mode == 'del'){
		content = '';
		$.each(contents, function(key, field){
			if(field != data){
				content += field+",";
			}
		});
		content = content.substring(0, content.length - 1);
		setCookie(cname, content, 10);
		check = true;
	}else if(mode == 'read'){
		$.each(contents, function(key, field){
			if(field == data){
				check = true;
			}
		});
	}
	return check;
}
	
$('.submenu_link').click(function() {
	var subMenu		= $(this).siblings('ul');
	var icon		= $(this).find('.controll_icon');
	
	if ($(subMenu).hasClass('open'))
	{
		$(subMenu).fadeOut();
		$(subMenu).removeClass('open').addClass('closed');
		
		nav_cookie(cms_cookie + '_admin_nav', 'del', subMenu.attr('id'));
		
		if($(icon).hasClass(icon_open)){
			$(icon).removeClass(icon_open).addClass(icon_closed);
		}
	}
	else 
	{
		$(subMenu).fadeIn();
		$(subMenu).removeClass('closed').addClass('open');
		
		nav_cookie(cms_cookie + '_admin_nav', 'add', subMenu.attr('id'));
		
		$(icon).removeClass(icon_closed).addClass(icon_open);
	}
});

$( document ).ready(function() {
	$('.subnav').each(function(){
		var id = $(this).attr('id');
		var icon = $(this).siblings('a').find('.controll_icon');
		if(!nav_cookie(cms_cookie + '_admin_nav', 'read', id) == '1'){
			$(this).addClass('closed');
			$(icon).addClass('fa ' + icon_closed);
		}else{
			$(this).addClass('open');
			$(icon).addClass('fa ' + icon_open);
		}
	});
	
});
