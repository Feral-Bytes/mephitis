<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_fce_list($fces, $parent_idkey, $parent, $pos = 0){

	global $phproot, $mysqli, $lang, $linkadd, $config;

	$echo = '';
	if(is_array($fces)){
		foreach($fces as $fce){
		
			if(isset($fce['uplink']) AND $fce['uplink'] == '1'){
				$uplink = draw_action_link($phproot.'?admin=fce&action=show&itemid='.$fce['id'].'&'.$parent_idkey.'='.$parent['id'].'&pos='.$pos.'&order=up'.$linkadd, 'up').' | ';
			}else{
				$uplink = '';
			}
			if(isset($fce['downlink']) AND $fce['downlink'] == '1'){
				$downlink = draw_action_link($phproot.'?admin=fce&action=show&itemid='.$fce['id'].'&'.$parent_idkey.'='.$parent['id'].'&pos='.$pos.'&order=down'.$linkadd, 'down').' | ';
			}else{
				$downlink = '';
			}
			
			$select_style = '';
			if(isset($_GET['select']) AND $mysqli->real_escape_string($_GET['select']) == $fce['id']){
				$select_style .= ' fce_selected';
			}
			
			if(isset($_GET['itemid']) AND $mysqli->real_escape_string($_GET['itemid']) == $fce['id'] && isset($_GET['action']) && $_GET['action'] == 'del'){
				$select_style .= ' fce_selected_to_delete';
			}			
			
			if(isset($_GET['select']) AND fce_deps($_GET['select'], $parent['id']) == 1){
			
				$loopcheck = new loopcheck($config['prefix'].'content_elements', $_GET['select'], $fce['id'], 'fce');
				if($loopcheck->detection != 'loop'){
					$move = ''.draw_action_link($phproot.'?admin=fce&action=show&siteid='.$_GET['siteid'].'&move='.$_GET['select'].'&'.$parent_idkey.'='.$parent['id'].'&pos='.$pos.'&after='.$fce['id'].'#'.$fce['id'], 'movehere').' | ';
				}else{
					$move = '';
				}
			
				$copy = ''.draw_action_link($phproot.'?admin=fce&action=show&siteid='.$_GET['siteid'].'&copy='.$_GET['select'].'&'.$parent_idkey.'='.$parent['id'].'&pos='.$pos.'&after='.$fce['id'].'#'.$fce['id'], 'copypastehere').' | ';
				
				$selectactions = '
					<div class="fce_select_actions">
						'.$copy.'
						'.$move.'
					</div>
				';
			}else{
				$selectactions = '';
			}
			
			$func = 'draw_preview_'.$fce['kind'];
			$fce['echo_content'] = $func($fce['data']);

			$echo .= '
				<a id="'.$fce['id'].'"></a>
				<div class="fce_preview'.$select_style.'">
					<div class="fce_preview_header">
						<div class="fce_name">'.$fce['name'].'</div>
						<div class="fce_controlls">
							'.draw_action_link($phproot.'?admin=fce&action=show&siteid='.$_GET['siteid'].'&select='.$fce['id'].'#'.$fce['id'], 'select').'
							|
							'.$uplink.''.$downlink.'
							'.draw_action_link($phproot.'?admin=fce&action=edit&itemid='.$fce['id'].'&fce='.$fce['kind'].'&pos='.$pos.'&'.$parent_idkey.'='.$parent['id'].$linkadd, 'edit').'
							|
							'.draw_action_link($phproot.'?admin=fce&action=del&itemid='.$fce['id'].'&pos='.$pos.'&'.$parent_idkey.'='.$parent['id'].$linkadd, 'del').'
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="fce_preview_content">
						'.$fce['echo_content'].'
					</div>
				</div>
			';
			$echo .= '<br>'.$selectactions.draw_action_link($phproot.'?admin=fce&action=new&'.$parent_idkey.'='.$parent['id'].'&pos='.$pos.'&after='.$fce['id'].$linkadd, 'new').'<br><br>';
			
		}
		
		if(isset($_GET['select']) AND fce_deps($_GET['select'], $parent['id']) == 1){
				$loopcheck = new loopcheck($config['prefix'].'content_elements', $_GET['select'], $parent['id'], 'fce');
				if($loopcheck->detection != 'loop'){
					$move = ''.draw_action_link($phproot.'?admin=fce&action=show&siteid='.$_GET['siteid'].'&move='.$_GET['select'].'&'.$parent_idkey.'='.$parent['id'].'&pos='.$pos.'&after=0#'.$fce['id'], 'movehere').' | ';
				}else{
					$move = '';
				}
			
				$copy = ''.draw_action_link($phproot.'?admin=fce&action=show&siteid='.$_GET['siteid'].'&copy='.$_GET['select'].'&'.$parent_idkey.'='.$parent['id'].'&pos='.$pos.'&after=0#'.$fce['id'], 'copypastehere').' | ';
				
				$selectactions = '
					<div class="fce_select_actions">
						'.$copy.'
						'.$move.'
					</div>
				';
		}else{
			$selectactions = '';
		}
		
		$echo = '
			'.$selectactions.'
			'.draw_action_link($phproot.'?admin=fce&action=new&'.$parent_idkey.'='.$parent['id'].'&pos='.$pos.'&after=0'.$linkadd, 'new').'
			<br><br>
			'.$echo.'
			
		';
	}else{
	
		if(isset($_GET['select']) AND fce_deps($_GET['select'], $parent['id']) == 1){
				$loopcheck = new loopcheck($config['prefix'].'content_elements', $_GET['select'], $parent['id'], 'fce');
				if($loopcheck->detection != 'loop'){
					$move = ''.draw_action_link($phproot.'?admin=fce&action=show&siteid='.$_GET['siteid'].'&move='.$_GET['select'].'&'.$parent_idkey.'='.$parent['id'].'&pos='.$pos.'&after=0#'.$parent['id'], 'movehere').' | ';
				}else{
					$move = '';
				}
			
				$copy = ''.draw_action_link($phproot.'?admin=fce&action=show&siteid='.$_GET['siteid'].'&copy='.$_GET['select'].'&'.$parent_idkey.'='.$parent['id'].'&pos='.$pos.'&after=0#'.$parent['id'], 'copypastehere').' | ';
				
				$selectactions = '
					<div class="fce_select_actions">
						'.$copy.'
						'.$move.'
					</div>
				';
		}else{
			$selectactions = '';
		}
	
		$echo .= $lang['no_fce'];
		$echo .= '<br>'.$selectactions.''.draw_action_link($phproot.'?admin=fce&action=new&'.$parent_idkey.'='.$parent['id'].'&pos='.$pos.$linkadd, 'new');
	}
	
	if(isset($_GET['select']) && $parent_idkey == 'siteid')
	{
		$echo = '
			<a href="'.$phproot.'?admin=fce&select='.$_GET['select'].'">'.$lang['back'].'</a><hr>
		'.$echo;
	}	

	
	return $echo;

}

function draw_fce_containers($container_list){

	global $phproot, $lang;
	
	$echo = ''.$lang['possible_fce_containers'].'';
	
	if(is_array($container_list))
	foreach($container_list as $container => $list){
		
		$linkadd = '';
		
		if(isset($_GET['select']))
		{
			$linkadd = '&select='.$_GET['select'].'';
		}		
		
		$echo_tab = '';
		if(is_array($list))
		foreach($list as $item){
			$echo_tab .= '
				<tr>
					<td>
						'.$item['id'].'
					</td>
					<td>
						<a href="'.$phproot.'?admin=fce&action=show&siteid='.$item['id'].''.$linkadd.'">'.$item['name'].'</a>
					</td>
				</tr>
			';
		}
		
		$echo .= '
			<h3>'.$lang[$container].'</h3>
			<table class="norm_tab table table-bordered">
				<thead>
					<tr>
						<th class="head">
							'.$lang['id'].'
						</th>
						<th class="head">
							'.$lang['name'].'
						</th>
					</tr>
				</thead>
				<tbody>
					'.$echo_tab.'
				</tbody>
			</table>
			<hr>
		';
	
	}
	return $echo;

}

function draw_fce_select_list($fce_list, $parent_idkey, $parent, $pos, $linkadd){

	global $phproot, $lang;
	

	$echo = '';
	
	
	if(is_array($fce_list))
	foreach($fce_list as $template => $fces){
		if(is_array($fces)){
			$sitelink = '';
			if(isset($_GET['siteid']))
			{
				$sitelink = 'siteid='.$_GET['siteid'];
			}
			$echo .= '
				<a href="'.$phproot.'?admin=fce&action=show&'.$sitelink.'">'.$lang['back'].'</a>
				<hr>
				<h3>'.get_label('template_'.$template).'</h3>
				<br>
			';
			foreach($fces as $key => $fce){
				$echo .= '<a href="'.$phproot.'?admin=fce&action=new&'.$parent_idkey.'='.$parent['id'].'&fce='.$key.'&pos='.$pos.'&'.$sitelink.''.$linkadd.'">'.get_label($fce['info']->name).'</a><br>';
			}
			$echo .= '<hr>';
		}
	}

	return $echo;

}

?>