<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_time_of_day($time)
{
	return date('H:i:s', strtotime('today')+$time);
}

function draw_date($date)
{
	$echo = '';
	if($date != '' AND $date != '0000-00-00' AND $date != '1970-01-01')
	{
		$echo .= date(getvar("norm_date_notime"), strtotime($date));
	}
	return $echo;
}

function draw_time($time)
{
	if($time > 0)
	{
		$date = date(getvar('norm_date'), $time);
	}
	else
	{
		$date = '';
	}
	return $date;
}

function draw_time_date($time)
{
	if($time > 0)
	{
		$date = date(getvar('norm_date_notime'), $time);
	}
	else
	{
		$date = '';
	}
	return $date;
}

function draw_online_time($data, $bscss = 'text')
{
	$time = draw_time_duration($data['time']);
	
	$echo = '';
	$time_text = '';
	$css = $bscss.' '.$bscss.'-';
	
	if($data['mode'] == 'online_in')
	{
		$css .= 'info';
		$time_text = get_label('online_in').' '.$time;
	}
	if($data['mode'] == 'online_since')
	{
		$css .= 'success';
		$time_text = get_label('online_since').' '.$time;
	}
	if($data['mode'] == 'offline_in')
	{
		$css .= 'warning';
		$time_text = get_label('offline_in').' '.$time;
	}
	if($data['mode'] == 'offline_since')
	{
		$css .= 'danger';
		$time_text = get_label('offline_since').' '.$time;
	}
	
	$echo = '<a title="'.$time_text.'" class="'.$css.'"><i class="fa fa-clock-o"></i></a>';
	
	return $echo;
}

function draw_time_duration($time)
{
	$echo = '';
	
	$y = floor($time/(3600*24*365.25));
	$time -= $y*3600*24*365.25;
	$d = floor($time/(3600*24));
	$time -= $d*3600*24;
	
	$h = floor($time/3600);
	$m = floor((($time/3600)-$h)*60);
	$s = $time-($h*3600)-($m*60);
	
	if($y > 0)$echo .= $y.':';
	if($d > 0)$echo .= sprintf("%02d", $d).':';
	
	$echo .= sprintf("%02d", $h).':'.sprintf("%02d", $m).':'.sprintf("%02d", $s);
	
	return $echo;
}

function draw_time_duration_text($time, $delimiter = ', ')
{
	$echo = '';
	
	$y = floor($time/(3600*24*365.25));
	$time -= $y*3600*24*365.25;
	$d = floor($time/(3600*24));
	$time -= $d*3600*24;
	
	$h = floor($time/3600);
	$m = floor((($time/3600)-$h)*60);
	$s = $time-($h*3600)-($m*60);
	
	$to_echo = array();
	
	$year = '';
	if($y > 0)
	{
		if($y > 1)
		{
			$year = get_label('time_duration_years');
		}
		else
		{
			$year = get_label('time_duration_year');
		}
		$to_echo[] = str_replace('{num}', $y, $year);
	}
	
	$day = '';
	if($d > 0)
	{
		if($d > 1)
		{
			$day = get_label('time_duration_days');
		}
		else
		{
			$day = get_label('time_duration_day');
		}
		$to_echo[] = str_replace('{num}', $d, $day);
	}
	
	$hour = '';
	if($h > 0)
	{
		if($h > 1)
		{
			$hour = get_label('time_duration_hours');
		}
		else
		{
			$hour = get_label('time_duration_hour');
		}
		$to_echo[] = str_replace('{num}', $h, $hour);
	}
	
	$min = '';
	if($m > 0)
	{
		if($m > 1)
		{
			$min = get_label('time_duration_mins');
		}
		else
		{
			$min = get_label('time_duration_min');
		}
		$to_echo[] = str_replace('{num}', $m, $min);
	}
	
	$sec = '';
	if($s > 0)
	{
		if($s > 1)
		{
			$sec = get_label('time_duration_secs');
		}
		else
		{
			$sec = get_label('time_duration_sec');
		}
		$to_echo[] = str_replace('{num}', $s, $sec);
	}
	
	
	$echo = implode($delimiter, $to_echo);
	
	
	return $echo;
}


?>