<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_admin_email_mailqueue_infobox()
{
	global $phproot, $link;
	
	$echo = '
		<a href="'.$phproot.'?'.$link.'&action=sendqueue" class="btn btn-info">'.get_label('email_send_queue').'</a>
		<hr>
	';
	
	return $echo;

}

?>