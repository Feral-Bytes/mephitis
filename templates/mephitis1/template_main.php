<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_msg($header, $content, $autolink = ''){

	global $msg_stack, $autoformuse;
	
	$msg_echo = '
		'.$autolink.'

		'.$content.'
	';
	$msg_stack .= draw_content($header, $msg_echo);
	
}

function draw_error($header, $content){

	$echo = draw_content($header, $content);
	$error_site = new style();
	
	if(isset($_GET['ajax'])){

		header('HTTP/1.1 500 Internal Server Error');
		echo $content;
		
	}else{
	
		return $error_site->draw_site($echo);

	}
}


function draw_popup(){
	
	global $tpl_data, $lang, $libs, $module;

	$echo = '
	<html>
		<head>
			<title>
				'.getvar('title').'
			</title>
				<link rel="stylesheet" type="text/css" href="templates/mephitis1/style.css">
				<meta http-equiv="Content-Type" content="text/html; charset='.$tpl_data['charset'].'">
				'.build_lib($libs['html_header']).'
		</head>
	<body>

	<div align="center">
	'; 
	if($_GET['popup'] == 'emojis')$echo .= draw_emojis_popup($_GET['field']);
	$echo .= '
	</div>

	</body>
	</html>
	';
	
	echo $echo;

}

function draw_bool($bool)
{
	
	global $lang;
	
	if($bool == "1")
	{
		return $lang['yes'];
	}
	else
	{
		return $lang['no'];
	}
}

?>