<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_cms_content($pageTree, $pageContent, $page)
{
	
	$echo = '';

	$templatePageTree= new DrawPageTreeAdmin($pageTree);
	
	$pageContent = $pageContent->GetPageContent($page);
	
	$pageContentHtml = '';
	if(is_array($page) && $page['id'] != 0)
	{
		$pageContentHtml = draw_content($page['name'], $pageContent);
	}
	
	$echo .= '
		<div class="row page">
			<div class="col-sm-4 pageTree">
				'.$templatePageTree->Render().'
			</div>
			<div class="col-sm-8 pageContent">
				'.$pageContentHtml.'
			</div>
		</div>
	';
	
	return $echo;
}

function draw_cms_content_element_select($page, $contentElements)
{
	global $pageContent, $phproot;
	
	$echo = '';
	
	$list = array();	
	
	foreach($contentElements as $contentElement => $template)
	{
		$list[] = '
			<li>
				<a href="'.$phproot.'?admin='.$_GET['admin'].'&content_action=new&page='.$page['id'].'&pos='.$_GET['pos'].'&parent='.$_GET['parent'].'&after='.$_GET['after'].'&content_element='.$contentElement.'">'.get_label($contentElement, $contentElement).'</a>
			</li>
		';
	}
	
	$echo .= '
		<ul>
			'.implode('', $list).'
		</ul>
	';
	
	return draw_content($page['name'].': '.get_label('page_content_element_select'), $echo);
}

class DrawPageTreeAdmin
{
	var $tree;
	
	function __construct($tree)
	{
		$this->tree = $tree;
	}
	
	private function BuildList($tree, $lvl = 0)
	{
		global $phproot;
		$echo = '';
		
		$parent = 0;
		$isBranch = false;
		$i = 0;
		$list = array();
		$count = count($tree);
		
		if(is_array($tree));
		foreach($tree as $element)
		{
			$i++;
			$parent = $element['parent'];
			$isBranch = $element['isBranch'];
			$icons = array();
			$icons[] = '<a href="#" class="submenu_link"><span id="controll_icon_nav_page_'.$element['id'].'" class="controll_icon"></span></a>';
			$icons[] = '<i class="fa fa-file-text-o" title="'.$element['id'].'"></i>';
			
			if($element['online'])
			{
				if($element['isHidden'])
				{
					$icons[] = '<i class="fa fa-eye text text-warning" title="'.get_label('hidden').'"></i>';
				}
				else
				{
					$icons[] = '<i class="fa fa-eye text text-success" title="'.get_label('visible').'"></i>';
				}
			}
			else
			{
				$icons[] = '<i class="fa fa-eye-slash text text-danger" title="'.get_label('offline').'"></i>';
			}
			
			if($element['online_time_mode'] != '')
			{
				$icons[] = draw_online_time(online_time($element), 'text');
			}
			
			$actions = array();
			$actions[] = '<a href="'.$phproot.'?admin='.$_GET['admin'].'&page_action=new&parent='.$element['id'].'"><i class="fa fa-level-down" title="'.get_label('page_new_next_lvl').'"></i></a>';
			$actions[] = '<a href="'.$phproot.'?admin='.$_GET['admin'].'&page_action=edit&parent='.$element['parent'].'&itemid='.$element['id'].'"><i class="fa fa-wrench" title="'.get_label('page_edit').'"></i></a>';
			$actions[] = '<a href="'.$phproot.'?admin='.$_GET['admin'].'&page_action=del&parent='.$element['parent'].'&itemid='.$element['id'].'"><i class="fa fa-trash" title="'.get_label('page_del').'"></i></a>';
			
			
			$actions[] = '<a href="'.$phproot.'?admin='.$_GET['admin'].'&select_page='.$element['id'].'"><i class="fa fa-square-o" title="'.get_label('page_select').'"></i></a>';
			
			if($count != 1)
			{
				
				$uplink = draw_action_link($phproot.'?admin='.$_GET['admin'].'&order_page=up&itemid='.$element['id'].'', 'up');
				$downlink = draw_action_link($phproot.'?admin='.$_GET['admin'].'&order_page=down&itemid='.$element['id'].'', 'down');
				
				if($i == 1){
					$actions[] = $downlink;
				}else if($i == $count){
					$actions[] = $uplink;
				}else{
					$actions[] = $uplink;
					$actions[] = $downlink;
				}
			}
			
			if(isset($_GET['select_page']) && !$element['isBranch'])
			{
				$actions[] = '<a href="'.$phproot.'?admin='.$_GET['admin'].'&move_page_in='.$element['id'].'&select_page='.$_GET['select_page'].'"><i class="fa fa-arrow-left text-success" title="'.get_label('page_move_here').'"></i></a>';	
				$actions[] = '<a href="'.$phproot.'?admin='.$_GET['admin'].'&move_page_in='.$element['parent'].'&after='.$element['id'].'&select_page='.$_GET['select_page'].'"><i class="fa fa-arrow-down text-success" title="'.get_label('page_move_here').'"></i></a>';	
			}
			
			$elementHTML = implode('&nbsp;', $icons).'&nbsp;<a href="'.$phproot.'?admin='.$_GET['admin'].'&page='.$element['id'].'">'.$element['name'].'</a>&nbsp;'.implode('&nbsp;|&nbsp;' ,$actions);
			
			if($element['hasChildren'])
			{
				$elementHTML .= $this->BuildList($element['children'], $lvl+1);
			}
			
			$css = array();
			
			if((isset($_GET['page_action']) && (isset($_GET['itemid']) && $_GET['itemid'] == $element['id']) || (isset($_GET['parent']) && $_GET['parent'] == $element['id'])) || (isset($_GET['select_page']) && $_GET['select_page'] == $element['id']))
			{
				$css[] = 'selected';
			}
			
			$list[] = '
				<li class="'.implode(' ', $css).'">
					'.$elementHTML.'
				</li>
			';
		}
		
		$css = array();
		$actions = array();
		$actions[] = '<a href="'.$phproot.'?admin='.$_GET['admin'].'&page_action=new&parent='.$parent.'"><i class="fa fa-file-text-o" title="'.get_label('page_new_this_lvl').'"></i></a>';
		
		if(isset($_GET['select_page']) && !$isBranch)
		{
			$actions[] = '<a href="'.$phproot.'?admin='.$_GET['admin'].'&move_page_in='.$parent.'&select_page='.$_GET['select_page'].'"><i class="fa fa-arrow-left text-success" title="'.get_label('page_move_here').'"></i></a>';	
			$actions[] = '<a href="'.$phproot.'?admin='.$_GET['admin'].'&move_page_in='.$parent.'&after=0&select_page='.$_GET['select_page'].'"><i class="fa fa-arrow-down text-success" title="'.get_label('page_move_here_after').'"></i></a>';	
			
		}
		
		if(isset($_GET['select_page']))
		{
			$actions[] = '<a href="'.$phproot.'?admin='.$_GET['admin'].'&page_copy_in='.$parent.'&select_page='.$_GET['select_page'].'"><i class="fa fa-files-o text-success" title="'.get_label('page_copy_here').'"></i></a>';	
		}
		
		if($lvl == 0 && isset($_GET['page_action']) && isset($_GET['parent']) && $_GET['parent'] == $parent)
		{
			$css[] = 'selected';
		}
		if($lvl != 0)
		{
			$css[] = 'subnav';
		}
		$echo .= '
			<ul class="'.implode(' ', $css).'" id="page_'.$parent.'">
				<li>'.implode('&nbsp;|&nbsp;' ,$actions).'</li>
				'.implode('', $list).'
				
			</ul>
		';

		return $echo;
	}
	
	public function Render()
	{
		$echo = '
			
			<ul class="cms_content_tree" id="cms_content_tree">
				<li>
					<span class="cms_content_tree_dir"><i class="fa fa-globe"></i>&nbsp;'.get_label('root_dir').'</span>
					'.$this->BuildList($this->tree->GetTree()).'
				</li>
			</ul>
		';
		
		return $echo;
	}
	
}

function GridAdmin($ce, $pos)
{
	global $phproot, $pageContent, $page, $newContentElementId, $selectedContent;

	$parent = 0;
	
	$rootActions = array();
	
	if(isset($ce['type']) && $ce['type'] == 'page')
	{
		$parent = 0;
		$ce['fce'] = 'page';
		$rootActions[] = '<a href="'.$phproot.'?admin='.$_GET['admin'].'&content_action=edit&page='.$page['id'].'&pos='.$pos.'&content_element=page" class="btn btn-warning btn-sm"><i class="fa fa-wrench" title="'.get_label('page_edit_content').'"></i></a>';
	}
	else
	{
		$parent = $ce['id'];
	}
	
	$btnCss = array();
	
	if(isset($_GET['content_action']))
	{
		if($_GET['content_action'] == 'new' && $_GET['page'] == $page['id'] && $_GET['pos'] == $pos && $_GET['parent'] == $parent && $_GET['after'] == 0)
		{
			$btnCss[] = 'selected';
		}
	}
	
	$btnNew = '<a href="'.$phproot.'?admin='.$_GET['admin'].'&content_action=new&page='.$page['id'].'&pos='.$pos.'&parent='.$parent.'&after=0" class="btn btn-default btn-sm '.implode(' ', $btnCss).'"><i class="fa fa-plus" title="'.get_label('content_new').'"></i></a>';
	
	$contents = $pageContent->GetContent($pos, $parent, $page['id'], $ce['isBranch']);
	
	$contentsHTML = '';
	
	$count = count($contents);
	$i = 0;
	
	if(is_array($contents))
	foreach($contents as $content)
	{
		$i++;
		$btnCss = array();
		$elemntCSS = array();
		
		if(isset($_GET['content_action']))
		{
			if($_GET['content_action'] == 'new' && $_GET['page'] == $page['id'] && $_GET['pos'] == $pos && $_GET['parent'] == $parent && $_GET['after'] == $content['contentElement']['id'])
			{
				$btnCss[] = 'selected';
			}
			if(
				(isset($_GET['itemid']) && $_GET['itemid'] == $content['contentElement']['id'])
				||
				(isset($newContentElementId) && $newContentElementId == $content['contentElement']['id'])
			)
			{
				$elemntCSS[] = 'selected';
			}
		}
		
		if(isset($_GET['select_content']) && $_GET['select_content'] == $content['contentElement']['id'])
		{
			$elemntCSS[] = 'selected';
		}
		
		$btnNewAfter = '<a href="'.$phproot.'?admin='.$_GET['admin'].'&content_action=new&page='.$page['id'].'&pos='.$pos.'&parent='.$parent.'&after='.$content['contentElement']['id'].'" class="btn btn-default btn-sm '.implode(' ', $btnCss).'"><i class="fa fa-plus" title="'.get_label('content_new').'"></i></a>';
		
		$actions = array();
		
		$actions[] = '<a href="'.$phproot.'?admin='.$_GET['admin'].'&content_action=edit&page='.$page['id'].'&pos='.$pos.'&itemid='.$content['contentElement']['id'].'&content_element='.$content['contentElement']['fce'].'" class="btn btn-warning btn-sm"><i class="fa fa-wrench" title="'.get_label('content_edit').'"></i></a>';
		$actions[] = '<a href="'.$phproot.'?admin='.$_GET['admin'].'&content_action=del&page='.$page['id'].'&pos='.$pos.'&itemid='.$content['contentElement']['id'].'&content_element='.$content['contentElement']['fce'].'" class="btn btn-danger btn-sm"><i class="fa fa-trash" title="'.get_label('content_del').'"></i></a>';
		
		$actions[] = '<a href="'.$phproot.'?admin='.$_GET['admin'].'&page='.$page['id'].'&select_content='.$content['contentElement']['id'].'" class="btn btn-info btn-sm"><i class="fa fa-square-o" title="'.get_label('content_select').'"></i></a>';
		
		if($count != 1)
		{
			
			$uplink = draw_action_link($phproot.'?admin='.$_GET['admin'].'&page='.$page['id'].'&order_content=up&itemid='.$content['contentElement']['id'].'', 'up', '', 'btn btn-info btn-sm');
			$downlink = draw_action_link($phproot.'?admin='.$_GET['admin'].'&page='.$page['id'].'&order_content=down&itemid='.$content['contentElement']['id'].'', 'down', '', 'btn btn-info btn-sm');
			
			if($i == 1){
				$actions[] = $downlink;
			}else if($i == $count){
				$actions[] = $uplink;
			}else{
				$actions[] = $uplink;
				$actions[] = $downlink;
			}
		}
		
		$moveHere = '';
		
		if(isset($_GET['select_content']) && !$content['contentElement']['isBranch'] && CheckContentDependencies($selectedContent, $ce))
		{
			$moveHere = '<a href="'.$phproot.'?admin='.$_GET['admin'].'&page='.$page['id'].'&move_content_in='.$content['contentElement']['parent'].'&pos='.$pos.'&select_content='.$_GET['select_content'].'&after='.$content['contentElement']['id'].'" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left text-success" title="'.get_label('content_move_here').'"></i></a>';
		}
		
		$content['html'] .= '
			<hr>
			'.implode('&nbsp;', $actions).'
		';
		$contentsHTML .= '
			'.draw_item($content['contentElement']['name'], $content['html'], implode(' ', $elemntCSS)).'
			<br>
			'.$btnNewAfter.' '.$moveHere.'
			<br>
			<br>
		';
	}
	
	$moveHere = '';
	if(isset($_GET['select_content']) && !$ce['isBranch'] && CheckContentDependencies($selectedContent, $ce))
	{
		$moveHere = '<a href="'.$phproot.'?admin='.$_GET['admin'].'&page='.$page['id'].'&move_content_in='.$ce['id'].'&pos='.$pos.'&select_content='.$_GET['select_content'].'&after=0" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left text-success" title="'.get_label('content_move_here').'"></i></a>';
	}
	
	$rootActionsHTML = '';
	if(count($rootActions) > 0)
	{
		$rootActionsHTML = implode('&nbsp;', $rootActions);
	}
	
	$echo = '
		'.$btnNew.' '.$moveHere.'
		<hr>
		'.$contentsHTML.'
		'.$rootActionsHTML.'
	';
	
	return $echo;
}

?>