<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function draw_templates_list($templates)
{
	global $phproot, $lang, $httpRootPath;
	
	$echo = '';
	
	if(is_array($templates))
	{
		foreach($templates as $key => $tpl)
		{
			$actions = '';
			
			if(isset($tpl['db']))
			{
				$actions .= '<a href="'.$phproot.'?admin=templates&template='.$key.'&action=edit_db_vars"  class="btn btn-success"><i class="fa fa-database"></i>&nbsp;'.get_label('tpl_edit_db_vars').'</a>';
			}
			else if(isset($tpl['vars']))
			{
				$actions .= '<a href="'.$phproot.'?admin=templates&template='.$key.'&action=create_db_vars" class="btn btn-primary"><i class="fa fa-database"></i>&nbsp;'.get_label('tpl_create_db_vars').'</a>';
			}
			
			$css = '';
			if(isset($_GET['template']) && $_GET['template'] == $key && isset($_GET['action']) && $_GET['action'] == 'del_db_vars' && !isset($_GET['sure']))
			{
				$css .= ' selected_to_delete';
			}
			
			$tpl_info = '
				<div class="row">
					<div class="col-md-2">
						<a href="'.$httpRootPath.'templates/'.$key.'/thumb.png" target="_blank" title="'.get_label('name', $key).'"><img src="'.$httpRootPath.'templates/'.$key.'/thumb.png" class="img-thumbnail" alt="'.get_label('name', $key).'"></a>
					</div>
					<div class="col-md-10">
						'.get_label('info', $key).'
					</div>					
				</div>
				<hr>
				<div class="row">
					<div class="col-md-12">
						'.$actions.'
					</div>				
				</div>				
			';
			$echo .= draw_item(get_label('name', $key), $tpl_info, $css);
		}
	}
	else
	{
		$echo .= get_label('template_error');
	}

	
	return $echo;
}

function draw_template($tpl)
{
	global $phproot, $lang;
	
	$echo = '';
	$tpl_name = $tpl['db']['name'];
	$form = '';
	$nav = '';
	$i = 0;
	
	//prerender for ajax fileuploader
	foreach($tpl['vars'] as $cat_key => $cat)
	{
		foreach($cat as $var_key => $var)
		{
			if($cat_key == 'picture')
			{
				if(isset($_GET['ajax']) && $_GET['ajax'] == 'upload')
				{
					break;
				}
				$info = array(
					'path'	=> 'files/templates/',
					'name'	=> $cat_key.'_'.$var_key,
					'module' => 'templates'
				);
				$input = mediaselect_filelist($info);
			}
		}
	}

	foreach($tpl['vars'] as $cat_key => $cat)
	{
		$classes = '';
		if($i < 1)
		{
			$classes .= ' active';
		}
		$nav .= '<li role="presentation" class="'.$classes.'"><a href="#'.$cat_key.'" aria-controls="'.$cat_key.'" role="tab" data-toggle="tab">'.get_label($cat_key).'</a></li>';
		$inputs = '';
		foreach($cat as $var_key => $var)
		{
			if(isset($tpl['db']['vars'][$cat_key][$var_key]))
			{
				$var = $tpl['db']['vars'][$cat_key][$var_key];
			}
			$type = 'text';
			if($cat_key == 'color')
			{
				$type = 'color';
			}
			$input = '<input type="'.$type.'" class="form-control" id="'.$var_key.'" name="vars['.$cat_key.']['.$var_key.']" value="'.$var.'" placeholder="'.get_label($var_key, $tpl_name).'">';
			
			if($cat_key == 'picture')
			{
				$info = array(
					'path'	=> 'files/templates/',
					'name'	=> $cat_key.'_'.$var_key,
					'module' => 'templates'
				);
				$input = draw_mediaselect($info['name'], $var, $phproot.'?admin=templates&template='.$tpl['db']['name'].'&action=edit_db_vars', mediaselect_filelist($info));
			}
			
			$inputs .= '
				<div class="form-group">
					<label for="'.$var_key.'" class="col-sm-2 control-label">'.get_label($var_key, $tpl_name).'</label>
					<div class="col-sm-10">
						'.$input.'
					</div>
				</div>	
			';
		}
		$form .= '
			<div role="tabpanel" class="tab-pane '.$classes.'" id="'.$cat_key.'">
				<br>
				'.$inputs.'
			</div>
		';
		
		$i++;
	}
	
	$form = '
		<form class="form-horizontal" href="'.$phproot.'?admin=templates&template='.$tpl['db']['name'].'&action=edit_db_vars" method="POST">
			<ul class="nav nav-tabs" role="tablist">
				'.$nav.'
			</ul>
			<div class="tab-content">
				'.$form.'
			</div>
			<hr>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-8">
					<button type="submit" name="send" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;'.get_label('norm_send').'</button>
				</div>
				<div class="col-sm-2">
					<a href="'.$phproot.'?admin=templates&template='.$tpl['db']['name'].'&action=del_db_vars" class="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;'.get_label('del').'</a>
				</div>				
			</div>			
		</form>
	';
	
	$echo .= '
		<a href="'.$phproot.'?admin=templates">'.get_label('back').'</a>
		<hr>
		'.$form.'
	';
	
	return $echo;
}

?>