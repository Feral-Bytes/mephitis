Mephitis CMS
========

This is the offical Mephitis CMS repository, which is owned by [Feral Bytes](https://www.feralbytes.ch).
The software is licensed under [GNU Affero General Public License 3.0](https://www.feralbytes.ch/index.php?show=agpl). Third party libraries, images, fonts and scripts are under their own licenses.

The documentation and contributions are licensed under [CC-by-nc-sa 4.0](http://creativecommons.org/licenses/by-nc-sa/4.0).


######Download actual version 1.0
* [As .zip](https://bitbucket.org/Feral-Bytes/mephitis/get/release-1.0.zip)
* [As .tar.gz](https://bitbucket.org/Feral-Bytes/mephitis/get/release-1.0.tar.gz)
* [As .tar.bz2](https://bitbucket.org/Feral-Bytes/mephitis/get/release-1.0.tar.bz2)


######Information

This repository is under development. All branches apart of the release branch are unstable! Never use them, but you know what you do.


######Branches organization
* ***master*** - main master branch (it will be synced with the dev branch)
* ***development*** - development branch
* ***release-1.0*** - lastest stable release (includes bug fixes)


######How to contribute
* fork the repository. If you are not used to Bitbucket, please check out [making a pull request](https://www.atlassian.com/git/tutorials/making-a-pull-request) and there examples.
* branch your repository, to commit the desired changes.
* sign-off your commits, to acknowledge your submission under the license of the project.
 * It is enough to include in your commit comment "Signed-off by: " followed by your name and email address (for example: `Signed-off-by: James Anderson <janderson@live.com>`)
* send a pull request to us.


######How to submit a pull request
* If you want to send a bug fix for actual version, send it to the branch ***release-1.0***
* If you want to send a new feature, use the branch ***development***
* You should never send any pull request against the master branch
For more informations, the ideal branching we would like to follow is the one described in [this article](http://nvie.com/posts/a-successful-git-branching-model/)