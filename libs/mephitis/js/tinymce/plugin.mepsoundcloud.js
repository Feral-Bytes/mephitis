/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

tinymce.PluginManager.add('mepsoundcloud', function(editor, url) {
    // Add a button that opens a window
	editor.addButton('mepsoundcloud', {
		tooltip: 'Soundcloud',
		icon: false,
		image: tinymceHttpRootPath+'libs/brands/soundcloud/black_white_24.png',
		onclick: function() {
			// Open window
			editor.windowManager.open({
				title: 'Soundcloud',
				body: [
					{type: 'textbox', name: 'link', label: 'Sound Link'}
				],
				onsubmit: function(e) {
					editor.insertContent('<iframe class="embed-responsive-item" width="400" height="150" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=' + e.data.link + '&amp;auto_play=false&amp;visual=true"></iframe>');
				}
			});
		}
	});
});