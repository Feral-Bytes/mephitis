/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

tinymce.PluginManager.add('meppic', function(editor, url) {
	
    // Add a button that opens a window
	editor.addButton('meppic', {
		tooltip: 'File',
		text: 'File',			
		icon: 'image',
		onclick: function() {
			// Open window
			editor.windowManager.open({
				title: 'File',
				width: 700,
				height: 500,
				url: tinymceHttpRootPath+'index.php?filemanager=tinymce&iframe=1'
			});
		}
	});
});