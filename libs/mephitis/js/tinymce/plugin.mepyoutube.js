/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

tinymce.PluginManager.add('mepyoutube', function(editor, url) {
    // Add a button that opens a window
	editor.addButton('mepyoutube', {
		tooltip: 'Youtube',
		icon: false,
		image: tinymceHttpRootPath+'libs/brands/YouTube-social_icon_pack-dark-PNG/48px/YouTube-social-squircle_dark_48px.png',
		onclick: function() {
			// Open window
			editor.windowManager.open({
				title: 'Youtube',
				body: [
					{type: 'textbox', name: 'link', label: 'Video Link'}
				],
				onsubmit: function(e) {
					
					var video_id = e.data.link.split('v=')[1];
					if(typeof video_id != 'undefined')	{
						var ampersandPosition = video_id.indexOf('&');
						if(ampersandPosition != -1){
							video_id = video_id.substring(0, ampersandPosition);
						}
					}else{
						video_id = e.data.link;
					}
					editor.insertContent('<iframe class="embed-responsive-item" id="ytplayer" width="425" height="344" frameborder="0" type="text/html" src="https://www.youtube.com/embed/' + video_id + '"></iframe>');
				}
			});
		}
	});
});