/*global tinymce:true */

(function() {
	tinymce.create('tinymce.plugins.MepBBcodePlugin', {
		init: function(ed) {
			
			var self = this;
			ed.on('beforeSetContent', function(e) {
				e.content = self["_mepbb_bbcode2html"](e.content);
			});

			ed.on('postProcess', function(e) {
				if (e.set) {
					e.content = self["_mepbb_bbcode2html"](e.content);
				}

				if (e.get) {
					e.content = self["_mepbb_html2bbcode"](e.content);
				}
			});
		},

		getInfo: function() {
			return {
				longname: 'BBCode Plugin',
				author: 'Ephox Corp (Modded by Feral Bytes)',
				authorurl: 'http://www.tinymce.com',
				infourl: 'http://www.tinymce.com/wiki.php/Plugin:bbcode'
			};
		},

		// Private methods

		// HTML -> BBCode in mepbb dialect
		_mepbb_html2bbcode: function(s) {
			s = tinymce.trim(s);

			function rep(re, str) {
				s = s.replace(re, str);
			}
			
			// example: <strong> to [b]
			
			rep(/<p style=\"text-align: (.*?);\">(.*?)<\/p>/gi, "[$1\]$2[/$1]\n");
			rep(/<iframe.*?src=\"https:\/\/www.youtube.com\/embed\/(.*?)\".*?><\/iframe>/gi, "[youtube]$1[/youtube]");
			rep(/<iframe.*?src=\"\/\/player.vimeo.com\/video\/(.*?)\".*?><\/iframe>/gi, "[vimeo]$1[/vimeo]");
			rep(/<iframe.*?src=\"https:\/\/w.soundcloud.com\/player\/\?url=(.*?)&amp;auto_play=false&amp;visual=true\".*?><\/iframe>/gi, "[soundcloud]$1[/soundcloud]");
			
			
			rep(/<a.*?href=\"(.*?)\".*?>(.*?)<\/a>/gi, "[url=$1]$2[/url]");
			rep(/<font.*?color=\"(.*?)\".*?class=\"codeStyle\".*?>(.*?)<\/font>/gi, "[code][color=$1]$2[/color][/code]");
			rep(/<font.*?color=\"(.*?)\".*?class=\"quoteStyle\".*?>(.*?)<\/font>/gi, "[quote][color=$1]$2[/color][/quote]");
			rep(/<font.*?class=\"codeStyle\".*?color=\"(.*?)\".*?>(.*?)<\/font>/gi, "[code][color=$1]$2[/color][/code]");
			rep(/<font.*?class=\"quoteStyle\".*?color=\"(.*?)\".*?>(.*?)<\/font>/gi, "[quote][color=$1]$2[/color][/quote]");
			rep(/<span style=\"color: ?(.*?);\">(.*?)<\/span>/gi, "[color=$1]$2[/color]");
			rep(/<font.*?color=\"(.*?)\".*?>(.*?)<\/font>/gi, "[color=$1]$2[/color]");
			rep(/<span style=\"font-size:(.*?);\">(.*?)<\/span>/gi, "[size=$1]$2[/size]");
			rep(/<font>(.*?)<\/font>/gi, "$1");
			rep(/<img.*?src=\"(.*?)\".*?\/>/gi, "[img]$1[/img]");
			rep(/<span class=\"codeStyle\">(.*?)<\/span>/gi, "[code]$1[/code]");
			rep(/<span class=\"quoteStyle\">(.*?)<\/span>/gi, "[quote]$1[/quote]");
			rep(/<strong class=\"codeStyle\">(.*?)<\/strong>/gi, "[code][b]$1[/b][/code]");
			rep(/<strong class=\"quoteStyle\">(.*?)<\/strong>/gi, "[quote][b]$1[/b][/quote]");
			rep(/<em class=\"codeStyle\">(.*?)<\/em>/gi, "[code][i]$1[/i][/code]");
			rep(/<em class=\"quoteStyle\">(.*?)<\/em>/gi, "[quote][i]$1[/i][/quote]");
			rep(/<u class=\"codeStyle\">(.*?)<\/u>/gi, "[code][u]$1[/u][/code]");
			rep(/<u class=\"quoteStyle\">(.*?)<\/u>/gi, "[quote][u]$1[/u][/quote]");
			rep(/<\/(strong|b)>/gi, "[/b]");
			rep(/<(strong|b)>/gi, "[b]");
			rep(/<\/(em|i)>/gi, "[/i]");
			rep(/<(em|i)>/gi, "[i]");
			rep(/<\/u>/gi, "[/u]");
			rep(/<span style=\"text-decoration: ?underline;\">(.*?)<\/span>/gi, "[u]$1[/u]");
			rep(/<u>/gi, "[u]");
			rep(/<blockquote[^>]*>/gi, "[quote]");
			rep(/<\/blockquote>/gi, "[/quote]");
			rep(/<br \/>/gi, "\n");
			rep(/<br\/>/gi, "\n");
			rep(/<br>/gi, "\n");
			rep(/<p>/gi, "");
			rep(/<\/p>/gi, "\n");
			rep(/&nbsp;|\u00a0/gi, " ");
			rep(/&quot;/gi, "\"");
			rep(/&lt;/gi, "<");
			rep(/&gt;/gi, ">");
			rep(/&amp;/gi, "&");

			return s;
		},

		// BBCode -> HTML from mepbb dialect
		_mepbb_bbcode2html: function(s) {
			s = tinymce.trim(s);

			function rep(re, str) {
				s = s.replace(re, str);
			}
						
			
			// example: [b] to <strong>
			rep(/\[left\]/gi, "<p data-mce-style=\"text-align: left;\" style=\"text-align: left;\">");
			rep(/\[\/left\]<br \/>/gi, "</p>");
			rep(/\[\/left\]<br>/gi, "</p>");
			rep(/\[\/left\]\n\n/gi, "</p>");
			rep(/\[\/left\]\n/gi, "</p>");
			rep(/\[\/left\]/gi, "</p>");
			
			
			rep(/\[right\]/gi, "<p data-mce-style=\"text-align: right;\" style=\"text-align: right;\">");
			rep(/\[\/right\]<br \/>/gi, "</p>");
			rep(/\[\/right\]<br>/gi, "</p>");
			rep(/\[\/right\]\n\n/gi, "</p>");
			rep(/\[\/right\]\n/gi, "</p>");
			rep(/\[\/right\]/gi, "</p>");
			
			
			rep(/\[center\]/gi, "<p data-mce-style=\"text-align: center;\" style=\"text-align: center;\">");
			rep(/\[\/center\]<br \/>/gi, "</p>");
			rep(/\[\/center\]<br>/gi, "</p>");
			rep(/\[\/center\]\n\n/gi, "</p>");
			rep(/\[\/center\]\n/gi, "</p>");
			rep(/\[\/center\]/gi, "</p>");
			
			rep(/\[justify\]/gi, "<p data-mce-style=\"text-align: justify;\" style=\"text-align: justify;\">");
			rep(/\[\/justify\]<br \/>/gi, "</p>");
			rep(/\[\/justify\]<br>/gi, "</p>");
			rep(/\[\/justify\]\n\n/gi, "</p>");
			rep(/\[\/justify\]\n/gi, "</p>");
			rep(/\[\/justify\]/gi, "</p>");
			
			
			rep(/\[youtube\]https:\/\/www\.youtube\.com\/watch\?v\=/gi, "<iframe class=\"embed-responsive-item\" id=\"ytplayer\" width=\"425\" height=\"344\" frameborder=\"0\" type=\"text/html\" src=\"https://www.youtube.com/embed/");
			rep(/\[youtube\]/gi, "<iframe class=\"embed-responsive-item\" id=\"ytplayer\" width=\"425\" height=\"344\" frameborder=\"0\" type=\"text/html\" src=\"http://www.youtube.com/embed/");
			rep(/\[\/youtube\]<br \/>/gi, "\"></iframe>");
			rep(/\[\/youtube\]<br>/gi, "\"></iframe>");
			rep(/\[\/youtube\]\n\n/gi, "\"></iframe>");
			rep(/\[\/youtube\]\n/gi, "\"></iframe>");
			rep(/\[\/youtube\]/gi, "\"></iframe>");			

			rep(/\[vimeo\]https:\/\/www\.youtube\.com\/watch\?v\=/gi, "<iframe class=\"embed-responsive-item\" id=\"vimeoplayer\" width=\"425\" height=\"344\" frameborder=\"0\" type=\"text/html\" src=\"//player.vimeo.com/video/");
			rep(/\[vimeo\]/gi, "<iframe class=\"embed-responsive-item\" id=\"vimeoplayer\" width=\"425\" height=\"344\" frameborder=\"0\" type=\"text/html\" src=\"//player.vimeo.com/video/");
			rep(/\[\/vimeo\]<br \/>/gi, "\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>");
			rep(/\[\/vimeo\]<br>/gi, "\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>");
			rep(/\[\/vimeo\]\n\n/gi, "\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>");
			rep(/\[\/vimeo\]\n/gi, "\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>");
			rep(/\[\/vimeo\]/gi, "\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>");

			rep(/\[soundcloud\]/gi, "<iframe class=\"embed-responsive-item\" width=\"400\" height=\"150\" scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?url=");
			rep(/\[\/soundcloud\]<br \/>/gi, "&amp;auto_play=false&amp;visual=true\"></iframe>");
			rep(/\[\/soundcloud\]<br>/gi, "&amp;auto_play=false&amp;visual=true\"></iframe>");
			rep(/\[\/soundcloud\]\n\n/gi, "&amp;auto_play=false&amp;visual=true\"></iframe>");
			rep(/\[\/soundcloud\]\n/gi, "&amp;auto_play=false&amp;visual=true\"></iframe>");
			rep(/\[\/soundcloud\]/gi, "&amp;auto_play=false&amp;visual=true\"></iframe>");
			
			rep(/\[size=(.*?)\](.*?)\[\/size\]/gi, "<span style=\"font-size: $1;\">$2</span>");
			
			
			rep(/\r\n\r\n/gi, "<br />");
			rep(/\r\n/gi, "<br />");
			rep(/\n\n/gi, "<br />");
			
			
			rep(/\n/gi, "<br />");
			rep(/\[b\]/gi, "<strong>");
			rep(/\[\/b\]/gi, "</strong>");
			rep(/\[i\]/gi, "<em>");
			rep(/\[\/i\]/gi, "</em>");
			rep(/\[u\]/gi, "<u>");
			rep(/\[\/u\]/gi, "</u>");
			rep(/\[url=([^\]]+)\](.*?)\[\/url\]/gi, "<a href=\"$1\">$2</a>");
			rep(/\[url\](.*?)\[\/url\]/gi, "<a href=\"$1\">$1</a>");
			rep(/\[img\](.*?)\[\/img\]/gi, "<img src=\"$1\" />");
			rep(/\[color=(.*?)\](.*?)\[\/color\]/gi, "<font color=\"$1\">$2</font>");
			rep(/\[code\](.*?)\[\/code\]/gi, "<span class=\"codeStyle\">$1</span>&nbsp;");
			rep(/\[quote.*?\](.*?)\[\/quote\]/gi, "<span class=\"quoteStyle\">$1</span>&nbsp;");				
			
			
			return s;
		}
	});

	// Register plugin
	tinymce.PluginManager.add('mepbbcode', tinymce.plugins.MepBBcodePlugin);
})();