/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

tinymce.PluginManager.add('mepvimeo', function(editor, url) {
    // Add a button that opens a window
	editor.addButton('mepvimeo', {
		tooltip: 'Vimeo',
		icon: false,
		image: tinymceHttpRootPath+'libs/brands/vimeo_icons/vimeo_icon_dark.png',
		onclick: function() {
			// Open window
			editor.windowManager.open({
				title: 'Vimeo',
				body: [
					{type: 'textbox', name: 'link', label: 'Video Link'}
				],
				onsubmit: function(e) {
					
					var regExp = /(www\.)?vimeo.com\/(\d+)($|\/)/;

					var match = e.data.link.match(regExp);

					if (match){
						video_id = match[2];
					}else{
						video_id = e.data.link;
					}					
					editor.insertContent('<iframe src="//player.vimeo.com/video/' + video_id + '" width="425" height="344" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
				}
			});
		}
	});
});