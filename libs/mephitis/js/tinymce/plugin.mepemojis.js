tinymce.PluginManager.add('mepemojis', function(editor, url) {
	

	function getHtml() {
		var emojisHtml;

		emojisHtml = '<table role="list" class="mce-grid">';

		tinymce.each(emojis, function(row) {
			emojisHtml += '<tr>';

			tinymce.each(row, function(icon) {
				
				if(icon != 0){
				var emoticonUrl =  tinymceHttpRootPath + 'index.php?emoji=' + icon;
				
				emojisHtml += '<td><a href="#" data-mce-url="' + emoticonUrl + '" data-mce-alt="' + icon + '" tabindex="-1" ' +
					'role="option" aria-label="' + icon + '"><img src="' +
					emoticonUrl + '" role="presentation" /></a></td>';
				}else{
					emojisHtml += '<td></td>';
				}
			});

			emojisHtml += '</tr>';
		});

		emojisHtml += '</table>';

		return emojisHtml;
	}

	editor.addButton('mepemojis', {
		type: 'panelbutton',
		text: '^.^',
		tooltip: 'Emojis',
		panel: {
			role: 'application',
			text: 'Emojis',
			icon: false,
			autohide: true,
			html: getHtml,
			onclick: function(e) {
				var linkElm = editor.dom.getParent(e.target, 'a');

				if (linkElm) {
					editor.insertContent(
						'<img src="' + linkElm.getAttribute('data-mce-url') + '" alt="' + linkElm.getAttribute('data-mce-alt') + '" />'
					);

					this.hide();
				}
			}
		}
	});
});