<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

if(!check_rootline_groups($rootline))
{
	die(draw_error($lang['error'], $lang['access_denied']));
}	

if(isset($_GET['usercp'])  && !isset($_POST['login']) && $_GET['usercp'] != 'logout')
{
	include($phpRootPath.'sys/usercp.php');
}
else if(isset($_GET['pms']))
{
	include($phpRootPath.'sys/pms.php');
}
else if(isset($_GET['filemanager']))
{
	include($phpRootPath.'sys/filemanager.php');
}
else if(isset($_GET['action']) AND $_GET['action'] == 'search' AND isset($_REQUEST['search_query']))
{

	$searchengine->search($_REQUEST['search_query']);
	
	$content .= $searchengine->draw_serp();

}else{
	
	$buildContent = true;
	
	if(isset($_GET['frontend_admin']))
	{
		include($phpRootPath.'sys/admin_frontend.php');
	}
	
	if($buildContent)
	{	
		
		if(is_array($page) && isset($page['id']))
		{
			$content .= $pageContent->GetPageContent($page);
		}
		else
		{
			$content .= draw_msg($lang['error'], $lang['404'],'');
		}
	}
}

if(getvar('global_tracking_enable') == 1)
{
	$libs['html_footer']['global_tracking_code'] = getvar('global_tracking_code');
}


?>