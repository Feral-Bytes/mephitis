<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

if($user['id'] != '0' && $_GET['filemanager'] == 'tinymce')
{
	$info = array(
		'path'	=> 'files/content/',
		'name'	=> 'tinymce_file',
		'module' => 'content'
	);
	$content .= draw_mediaselect_filebrowser_tinymce($info['name'], mediaselect_filelist($info), $phproot.'?filemanager=tinymce&iframe=1');
}



?>