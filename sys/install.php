<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

define('MephitisCMS', 'install');

session_start();

header('Content-Type: text/html; charset=utf-8');
header("Cache-Control: no-cache, no-store, must-revalidate, private");
header("Pragma: no-cache");
header("X-XSS-Protection: 1; mode=block");
header("X-Content-Type-Options: nosniff");
header("X-Frame-Options: SAMEORIGIN");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

$rootDir = dirname(dirname($_SERVER['PHP_SELF']));
if(strlen($rootDir) <= 1)
{
	$rootDir = '';
}
 
$httpRootPath = $rootDir.'/';
$phproot = $rootDir.'/sys/install.php';

$phpRootPath = dirname(__DIR__).'/';


//system modules
include($phpRootPath.'sys/functions.php');
include($phpRootPath.'sys/classes.php');
$assetService = new AssetService();

//config
$config['charset'] = 'UTF-8';
$config['default_template'] = 'mephitis1';
$config['debugmode'] = 0;


$config['template'] = $config['default_template'];

$libs				= array();
$libs['html_header']	= '';
$libs['html_footer']	= '';

//language
$user['lang'] = browser_lang(availablelangs());

include(include_file('lang', 'lang_main'));
include(include_file('lang', 'lang_admin'));
include(include_file('lang', 'lang_install'));

//style
$stylefile = 'style_install.php';
include($phpRootPath.'sys/templates.php');
include($phpRootPath.'sys/admin_templates.php');

include_once(include_file('lang', 'lang_admin'));
//plugins
include($phpRootPath.'libs/libs.php');

$msg_stack = '';

function set_user_file($user_file){
	
	global $phpRootPath;
	
	$ip					= $_SERVER['REMOTE_ADDR'];
	$host				= gethostbyaddr($ip); 
	$request_method		= $_SERVER['REQUEST_METHOD'];
	$http_user_agent	= $_SERVER['HTTP_USER_AGENT'];
	$time				= time();
	
	$file = fopen($phpRootPath.$user_file['name'],"w");
	$file_data = '<?php
		
		$user[\'ip\'] = '.var_export($ip, TRUE).';
		$user[\'host\'] = '.var_export($host, TRUE).';
		$user[\'request_method\'] = '.var_export($request_method, TRUE).';
		$user[\'http_user_agent\'] = '.var_export($http_user_agent, TRUE).';
		$user[\'time\'] = '.var_export($time, TRUE).';
		$user[\'session_id\'] = '.var_export($_SESSION['mephitis_install']['session_id'], TRUE).';
		?>';
	
	fwrite($file, $file_data);
	fclose($file);

}

//set content variables
$content = '';
$echo = new style();

$auth_file = array(
	'name' => 'authentication.php',
	'content' => ''
);

$user_file = array(
	'name'	=> 'temp_user_install.php'
);

$actions = array();

//check session
if(!isset($_SESSION['mephitis_install'])){
	$_SESSION['mephitis_install'] = array(
		'session_id' => session_id()
	);
}

//authentication
if(!file_exists($phpRootPath.$user_file['name'])){
	set_user_file($user_file);
}
include($phpRootPath.$user_file['name']);

$auth_file_data = '
<?php
$auth_file[\'key\'] = \''.$_SESSION['mephitis_install']['session_id'].'\';
?>';
$auth_file_data = htmlspecialchars($auth_file_data);

if(!file_exists($phpRootPath.$auth_file['name'])){
	die ($echo->draw_site(draw_content($lang['title'], $echo->draw_need_auth($auth_file_data))));
}else{

	include($phpRootPath.$auth_file['name']);

	if(!isset($auth_file['key'])){
		die ($echo->draw_site(draw_content($lang['title'], $echo->draw_need_auth($auth_file_data))));
	}
	
}

//check user
if($_SESSION['mephitis_install']['session_id'] != $auth_file['key']){
	unlink($phpRootPath.$auth_file['name']);
	die ($echo->draw_site(draw_content($lang['title'], $echo->draw_need_auth($auth_file_data))));
}

if($user['session_id'] != $_SESSION['mephitis_install']['session_id']){
	draw_msg($lang['warning'], $echo->draw_wrong_login($user));
}

if (version_compare(phpversion(), $minPhpVersion, '<')) {
    $msg_stack .= draw_alert('<h2>Warning you are using PHP '.phpversion().' which is not supported, malfunction iminent!<h2>', 'danger');
}

//database and module template
$modules	= array();
$database	= array();

if(isset($_GET['admin']) AND $_GET['admin'] == 'install' AND (isset($_POST['send1']) OR isset($_POST['send2']) OR isset($_POST['send3']))){
	
	$config['db_host']	= $_POST['db_host'];
	$config['db_name']	= $_POST['db_name'];
	$config['db_user']	= $_POST['db_user'];
	$config['db_pass']	= $_POST['db_pass'];
	$config['prefix']	= $_POST['prefix'];
	
	if($config['db_host'] == '' || $config['db_name'] == '' || $config['db_user'] == '' || $config['prefix'] == '')
	{
		die ($echo->draw_site(draw_content($lang['title'], $echo->draw_install_dberror('no_connection'))));
	}
	
	@$mysqli = new mysqli($config['db_host'], $config['db_user'], $config['db_pass'], $config['db_name']) or die ($echo->draw_site(draw_content($lang['title'], $echo->draw_install_dberror('no_connection'))));
	@$mysqli->query("SET NAMES 'utf8'");
	@$mysqli->set_charset("utf8");	
	
	if ($mysqli->connect_errno) {
		die ($echo->draw_site(draw_content($lang['title'], $echo->draw_install_dberror($mysqli->connect_error))));
	}
}

if(file_exists($phpRootPath.'config.php')){

	include($phpRootPath.'config.php');

	//mysql connection
	$mysqli = new mysqli($config['db_host'], $config['db_user'], $config['db_pass'], $config['db_name']);
	$mysqli->query("SET NAMES 'utf8'");
	$mysqli->set_charset("utf8");
	
	//timezone
	date_default_timezone_set($config['timezone']);
	
}

//database template
if(isset($config['prefix'])){
	
	include_once($phpRootPath.'sys/db.php');
	/*
	//module loading
	$sysmoduleloader	= array();
	$moduleloader		= array();
	if ($handle = opendir('./modules')) {
		while (false !== ($file = readdir($handle))) {
			if ($file != "." && $file != ".." && is_dir('./modules/'.$file)) {
				if(substr($file, 0, strlen('sys_')) === 'sys_'){
					//system module will be loaded before normal modules
					$sysmoduleloader[]	= $file;
				}else{
					$moduleloader[]		= $file;
				}
				
			}
		}
		closedir($handle);
	}

	//modules: only database stuff is interesting
	$mod_files = array(
		'mod_db'
	);

	foreach(array_merge($sysmoduleloader, $moduleloader) as $modulename){
		foreach($mod_files as $mod_file){
			$file = 'modules/'.$modulename.'/'.$mod_file.'.php';
			if(file_exists($file)){
				include($file);
			}
		}
	}
*/
}

$link = '';

if(isset($_GET['action'])){
	$link .= 'action='.$_GET['action'];
}

$header = $lang['title'];

$actions[] = array(
	'lang'		=> $lang['status'],
	'action'	=> 'status'
);

if(file_exists($phpRootPath.'config.php')){
	
	$actions[] = array(
		'lang'		=> $lang['db'],
		'action'	=> 'db'
	);
	
	$actions[] = array(
		'lang'		=> $lang['user'],
		'action'	=> 'user'
	);
	
	$actions[] = array(
		'lang'		=> $lang['admin'],
		'action'	=> 'admin',
		'phproot'	=> 'index.php'
	);	
	
	if(!isset($_GET['admin']) OR (isset($_GET['admin']) AND $_GET['admin'] == 'status')){
		$header .= ': '.$lang['status'];
		$content .= draw_content($header, $echo->draw_already_installed());
	}
	
}else{

	$actions[] = array(
		'lang'		=> $lang['install'],
		'action'	=> 'install'
	);

	if(!isset($_GET['admin']) OR (isset($_GET['admin']) AND $_GET['admin'] == 'status')){
		$header .= '';
		$content .= draw_content($header,$echo->draw_not_installed());
	}
}

$actions[] = array(
	'lang'		=> $lang['logout'],
	'action'	=> 'logout'
);

if(isset($_GET['admin']) AND $_GET['admin'] == 'install'){

	$header .= ': '.$lang['install'];
	
	if(isset($_POST['send1'])){
		
		$hidden = $config;
		$content .= draw_content($header, $echo->draw_install('form2', $hidden));
		
		
	}else if(isset($_POST['send2'])){
		
		$hidden = $config;
		$hidden['name']	= $_POST['name'];
		$hidden['pass']	= $_POST['pass'];
		
		$content .= draw_content($header, $echo->draw_install('form3', $hidden));
		
		
	}else if(isset($_POST['send3'])){
		
		$config['timezone']	= $mysqli->real_escape_string($_POST['timezone']);
		
		$admin = array(
			'name'	=> $mysqli->real_escape_string($_POST['name']),
			'pass'	=> $mysqli->real_escape_string(password_crypt($_POST['pass']))
		);
		
		
		$file = fopen($phpRootPath."config.php","w");
		
		$file_data = '<?php
			$config[\'db_host\']	= \''.$config['db_host'].'\';
			$config[\'db_name\']	= \''.$config['db_name'].'\';
			$config[\'db_user\']	= \''.$config['db_user'].'\';
			$config[\'db_pass\']	= \''.$config['db_pass'].'\';
			$config[\'prefix\']	= \''.$config['prefix'].'\';

			$config[\'charset\']	= \''.$config['charset'].'\';
			$config[\'timezone\']	= \''.$config['timezone'].'\';

			$config[\'default_template\'] = \''.$config['default_template'].'\';
			$config[\'debugmode\'] = 0;
			?>';
		
		fwrite($file, $file_data);
		fclose($file);
		
		
		$db = new dbcheck($config, $database);
		$db->load_db();
		$db->check();
		$db->repair();
		
		$db->load_db();
		$db->check();
		$db->repair();
		
		$new = 'INSERT INTO '.$config['prefix'].'user 
			(name, pass, time, reg_time, perm, email, active, ref, ref_time, timezone, lang, avatar, groups, pms_sendemail) 
			VALUES ("'.$admin['name'].'", "'.$admin['pass'].'", "'.time().'", "'.time().'", "admin", "", "1", "", "'.time().'", "'.$config['timezone'].'", "'.$user['lang'].'", "0", "", "0")
		';
		
		$post = $mysqli->query($new) or die($echo->draw_site(draw_content($lang['title'], $echo->draw_install_dberror('admin'))));
				
		$sql = '';
		include($phpRootPath.'sys/prefills/sql/emojis.php');
		$post = $mysqli->query($sql) or die($echo->draw_site(draw_content($lang['title'], $echo->draw_install_dberror($mysqli->error))));
		
		//unlink($phpRootPath.$user_file['name']);
		//unlink($phpRootPath.$auth_file['name']);
		
		$content .= draw_content($header, $echo->draw_install('installed'));
		
		
	}else{
		$content .= draw_content($header, $echo->draw_install('form1'));
	}
	
	

}

if(isset($_GET['admin']) AND $_GET['admin'] == 'db'){

	include($phpRootPath.'sys/admintools/admin_db.php');

	$header .= ': '.$lang['db'];

}

if(isset($_GET['admin']) AND $_GET['admin'] == 'user'){

$auth_file_data = '
<?php
$auth_file[\'key\'] = \''.$_SESSION['mephitis_install']['session_id'].'\';
$auth_file[\'mode\'] = \'user\';
?>';
$auth_file_data = htmlspecialchars($auth_file_data);

	if(isset($auth_file['mode']) AND $auth_file['mode'] == 'user'){
		
		$user['id'] = 1;
		$config['availablelangs'] = availablelangs();
		
		include($phpRootPath."sys/admintools/admin_user.php");
	
	}else{
		die ($echo->draw_site(draw_content($lang['title'], $echo->draw_need_special_auth($auth_file_data))));
	}
	
	$header .= ': '.$lang['user'];

}

if(isset($_GET['admin']) AND $_GET['admin'] == 'logout'){
	
	unlink($phpRootPath.$user_file['name']);
	unlink($phpRootPath.$auth_file['name']);
	$header .= ': '.$lang['logout'];
	$content .= draw_content($header, $echo->draw_logout());

}


if(!isset($_GET['ajax'])){
	echo $echo->draw_site($content);
}

//debug mode
if($config['debugmode'] == 1){
	debug($_SESSION);
	debug($_COOKIE);
	debug($_SERVER);
	debug($_GET);
	debug($_POST);
	debug($database);
}

?>