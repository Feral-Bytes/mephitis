<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

define('MephitisCMS', 'frontend');
 
$rootDir = dirname($_SERVER['PHP_SELF']);
if(strlen($rootDir) <= 1)
{
	$rootDir = '';
}
$httpRootPath = $rootDir.'/';
$phproot = $rootDir.'/index.php';
 
$stylefile = 'style.php';

$phpRootPath = dirname(__DIR__).'/';

include($phpRootPath.'sys/init.php');

header('Content-Type: text/html; charset=utf-8');
header("Cache-Control: no-cache, no-store, must-revalidate, private");
header("Pragma: no-cache");
header("X-XSS-Protection: 1; mode=block");
header("X-Content-Type-Options: nosniff");
header("X-Frame-Options: SAMEORIGIN");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

//usertools
user_tools();

include_once(include_file('template', 'overrides_user.php'));

if((isset($_GET['ajax']) AND $_GET['ajax'] != '') OR (isset($_GET['file']) AND $_GET['file'] != '') OR (isset($_GET['emoji']) AND $_GET['emoji'] != '') OR isset($_REQUEST['send_as_file'])){
	
	if(isset($_GET['file'])){
		
		if(!isset($_GET['mode']))
		{
			$_GET['mode'] = 'full';
		}
		
		if(isset($_GET['filemodule']))
		{
			show_file($_GET['file'], $_GET['mode'], $_GET['filemodule']);
		}
		else
		{
			show_file($_GET['file'], $_GET['mode']);
		}
		
		exit;
	}
	if(isset($_GET['emoji'])){
		show_emoji($_GET['emoji']);
		exit;
	}
	include($phpRootPath.'sys/main.php');
}else{
	
	//main code and templates
	include($phpRootPath.'sys/main.php');
	
	if(isset($_GET['popup'])){
		draw_popup();
	}else{
		
		if(isset($_GET['iframe']))
		{
			if(method_exists($echo, "draw_iframe"))
			{
				echo $echo->draw_iframe($content);
			}
			else
			{
				echo $echo->draw_site($content);
			}
		}
		else
		{
			echo $echo->draw_site($content);
		}
	}
}



//debug mode
if($config['debugmode'] == 1){

	$query = "SHOW SESSION STATUS LIKE 'Questions'";
	$result = $mysqli->query($query);
	$row = $result->fetch_assoc();
	
	debug ($row);

	$endtime = microtime(TRUE);
	debug(round($endtime-$starttime, 3));

	debug($_SESSION);
	debug($_COOKIE);
	debug($_SERVER);
	debug($_GET);
	debug($_POST);
	debug($user);
}

?>