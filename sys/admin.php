<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

if((isset($user['perm']) AND $user['perm'] != 'admin') || !isset($user['perm'])){
	die(draw_error($lang['admin'], $lang['access_denied']));
}

if(isset($_GET['admin']) AND $_GET['admin'] == 'content'){

	include($phpRootPath.'sys/admintools/admin_content.php');
	
}else if(isset($_GET['admin']) AND $_GET['admin'] == 'common'){

	include($phpRootPath.'sys/admintools/admin_common.php');
	
}else if(isset($_GET['admin']) AND $_GET['admin'] == 'email'){

	include($phpRootPath.'sys/admintools/admin_email.php');
	
}else if(isset($_GET['admin']) AND $_GET['admin'] == 'templates'){

	include($phpRootPath.'sys/admintools/admin_templates.php');
	
}else if(isset($_GET['admin']) AND $_GET['admin'] == 'user'){

	include($phpRootPath.'sys/admintools/admin_user.php');
	
}else if(isset($_GET['admin']) AND $_GET['admin'] == 'groups'){

	include($phpRootPath.'sys/admintools/admin_groups.php');
	
}else if(isset($_GET['admin']) AND $_GET['admin'] == 'files'){
	
	include($phpRootPath.'sys/admintools/admin_files.php');
	
}else if(isset($_GET['admin']) AND $_GET['admin'] == 'emojis'){
	
	include($phpRootPath.'sys/admintools/admin_emojis.php');
	
}else if(isset($_GET['admin']) AND $_GET['admin'] == 'ips'){
	
	include($phpRootPath.'sys/admintools/admin_ips.php');
	
}else if(isset($_GET['admin']) AND $_GET['admin'] == 'sessions'){
	
	include($phpRootPath.'sys/admintools/admin_sessions.php');
	
}else if(isset($_GET['admin']) AND $_GET['admin'] == 'modules'){
	
	include($phpRootPath.'sys/admintools/admin_modules.php');	
	
}else if(isset($_GET['admin']) AND $_GET['admin'] == 'updater'){
	
	include($phpRootPath.'sys/admintools/admin_updater.php');	
	
}else if(isset($_GET['admin']) AND $_GET['admin'] == 'db'){
	
	include($phpRootPath.'sys/admintools/admin_db.php');
	
}else if(isset($_GET['admin']) AND $_GET['admin'] == 'credits'){
	
	include($phpRootPath.'sys/admintools/admin_credits.php');	
	
}else if(isset($_GET['admin']) AND $_GET['admin'] == 'taskqueue'){
	
	include($phpRootPath.'sys/admintools/admin_taskqueue.php');
	
}else if(isset($_GET['admin'])){
	
	$modulename = $mysqli->real_escape_string($_GET['admin']);
	$subadmin = '';
	$file = $phpRootPath.'modules/'.$modulename.'/mod_admin.php';
	if(isset($installedModules[$modulename]) && $installedModules[$modulename]['active'] == "1" && file_exists($file)){
		$link .= 'admin='.$modulename;
		if(isset($_GET['subadmin']))
		{
			$subadmin = $mysqli->real_escape_string($_GET['subadmin']);
			$link .= '&subadmin='.$subadmin;
		}
		include($file);
		if(isset($modulecontent) && $modulecontent != '')
		{
			$content .= $modulecontent;
		}
	}else{
		draw_msg($lang['error'], $lang['nothing']);
	}

}else if(isset($_GET['frontend_admin'])){
	if(isset($_GET['frontend_admin']) AND $_GET['frontend_admin'] == 'comments')
	{
		include($phpRootPath.'sys/admintools/admin_frontend_comments.php');	
	}
	else
	{
		$modulename = $mysqli->real_escape_string($_GET['frontend_admin']);
		$subadmin = '';
		if(isset($installedModules[$modulename]) && $installedModules[$modulename]['active'] == 1 && isset($modules[$modulename]['frontend_admin']) && $modules[$modulename]['frontend_admin'] == true)
		{
			$file = $phpRootPath.'modules/'.$modulename.'/mod_frontend_admin.php';
			if(isset($installedModules[$modulename]) && $installedModules[$modulename]['active'] == "1" && file_exists($file)){
				$link .= 'frontend_admin='.$modulename;
				if(isset($_GET['subadmin']))
				{
					$subadmin = $mysqli->real_escape_string($_GET['subadmin']);
					$link .= '&subadmin='.$subadmin;
				}
				include($file);
				if(isset($modulecontent) && $modulecontent != '')
				{
					$content .= $modulecontent;
				}
			}
		}
	}
}else{
	include($phpRootPath.'sys/admintools/admin_dashboard.php');	
}


?>