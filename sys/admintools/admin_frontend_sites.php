<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */
 
if($_GET['action'] == 'del' || $_GET['action'] == 'new'){
	die(draw_error($lang['error'], $lang['error']));
}

$modules = array_merge(array(array('lang' => $lang['nomodule'])), $modules);

$result = $mysqli->query('SELECT * FROM '.$config['prefix'].'sites WHERE id = "'.$mysqli->real_escape_string($_GET['itemid']).'"') or die(draw_error($lang['mysql_error'], $mysqli->error));
$row = $result->fetch_assoc();
$dirs = $row;

if(!check_frontend_admin($row))
{
	die(draw_error($lang['admin'], $lang['access_denied']));
}
	

$inputs[] = array(
	'type'	=> 'checkbox',
	'name'	=> 'border',
	'lang'	=> $lang['border'],
	'default' => '1',
	'active' => ''
);

$inputs[] = array(
	'type'	=> 'text',
	'name'	=> 'name',
	'value'	=> '',
	'required' => '1',
	'required_msg' => $lang['required_msg_name'],
	'lang'	=> $lang['name']
);

$inputs[] = array(
	'type'	=> 'textarea',
	'name'	=> 'content',
	'wysiwyg' => 'html',
	'value'	=> '',
	'lang'	=> $lang['content']
);

$inputs[] = array(
	'type'	=> 'checkbox',
	'name'	=> 'online',
	'lang'	=> $lang['online'],
	'default' => '1',
	'active' => ''
);

$inputs[] = array(
	'type'	=> 'text',
	'name'	=> 'shortlink',
	'value'	=> '',
	'lang'	=> $lang['shortlink']
);

if($_GET['action'] == 'edit' OR $_GET['action'] == 'del'){
	$info = site_info($_GET['itemid']);
	$header = $lang['sites'].': '.$info['name'];
}else{
	$header = $lang['sites'];
}

$content .= autoform($config['prefix'].'sites', $inputs, $phproot, 'site='.$_GET['itemid'].'&frontend_admin=sites&', $header, NULL, NULL, 'action', $phproot.'?site='.$_GET['itemid'], '', true);
if(isset($autoform_success))
{
	unset($_GET['action']);
	unset($_POST['send']);
	$buildContent = true;
}
else
{
	$buildContent = false;
}
?>