<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function get_form_admin_email_settings()
{
	
	global $config, $mysqli, $lang;
	
	$settings = array();
	unset($config['settings']);
	
	if(getvar('email_smtp') == '1'){
		$active = ' checked';
	}else{
		$active = '';
	}
	$settings[] = array(
		'type'	=> 'checkbox',
		'name'	=> 'email_smtp',
		'lang'	=> get_label('email_smtp'),
		'value'	=> getvar('email_smtp'),
		'active' => $active
	);
	
	$settings[] = array(
		'name'	=> 'email_smtp_host',
		'type'	=> 'text',
		'lang'	=> get_label('email_smtp_host'),
		'value'	=> getvar('email_smtp_host')
	);
	
	if(getvar('email_smtp_auth') == '1'){
		$active = ' checked';
	}else{
		$active = '';
	}
	$settings[] = array(
		'type'	=> 'checkbox',
		'name'	=> 'email_smtp_auth',
		'lang'	=> get_label('email_smtp_auth'),
		'value'	=> getvar('email_smtp_auth'),
		'active' => $active
	);
	
	$settings[] = array(
		'name'	=> 'email_smtp_user',
		'type'	=> 'text',
		'lang'	=> get_label('email_smtp_user'),
		'value'	=> getvar('email_smtp_user')
	);
	
	$settings[] = array(
		'name'	=> 'email_smtp_pass',
		'type'	=> 'text',
		'lang'	=> get_label('email_smtp_pass'),
		'value'	=> getvar('email_smtp_pass')
	);
	
	
	$options = array(
		'' => array(
			'lang' => get_label('none'),
			'active' => ''
		),
		'tls' => array(
			'lang' => 'tls',
			'active' => ''
		),
		'ssl' => array(
			'lang' => 'ssl',
			'active' => ''
		)
	);
	foreach($options as $key => $option)
	{
		if(getvar('email_smtp_secure') == $key)
		{
			$options[$key]['active'] = ' selected';
		}
	}
	$settings[] = array(
		'name'	=> 'email_smtp_secure',
		'type'	=> 'select',
		'lang'	=> get_label('email_smtp_secure'),
		'value'	=> getvar('email_smtp_secure'),
		'options' => $options
	);
	
	$settings[] = array(
		'name'	=> 'email_smtp_port',
		'type'	=> 'text',
		'lang'	=> get_label('email_smtp_port'),
		'value'	=> getvar('email_smtp_port')
	);
	
	$settings[] = array(
		'name'	=> 'email_smtp_from_name',
		'type'	=> 'text',
		'lang'	=> get_label('email_smtp_from_name'),
		'value'	=> getvar('email_smtp_from_name')
	);
	
	$settings[] = array(
		'name'	=> 'email_smtp_from_email',
		'type'	=> 'text',
		'lang'	=> get_label('email_smtp_from_email'),
		'value'	=> getvar('email_smtp_from_email')
	);
	
	
	return $settings;
}

if(isset($_POST['send'])){
	
	update_settings(get_form_admin_email_settings());
	unset($config['settings']);
	
	$mail = new PHPMailer();

	$mail->isSMTP();
	//$mail->SMTPDebug  = 4; 
	$mail->Host = getvar('email_smtp_host');
	$mail->SMTPAuth = getvar('email_smtp_auth');
	$mail->Username = getvar('email_smtp_user');
	$mail->Password = getvar('email_smtp_pass');
	$mail->SMTPSecure = getvar('email_smtp_secure');
	$mail->Port = getvar('email_smtp_port');
	
	if($mail->smtpConnect())
	{
		$mail->smtpClose();
		$msg_stack .= draw_alert(get_label('email_settings_ok'), 'success');
	}
	else
	{
		$msg_stack .= draw_alert(get_label('email_settings_notok'), 'danger');
	}
	
	
}

$formdata = array(
	'name' => 'email_settings',
	'action' => $phproot.'?'.$link,
	'method' => 'POST',
	'fileadd' => ''
);

$content .= draw_form(get_form_admin_email_settings(), get_label('email_settings'), $formdata);




?>