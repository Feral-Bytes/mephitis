<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */
 
$inputs[] = array(
	'name'	=> 'name',
	'type'	=> 'text',
	'lang'	=> $lang['name'],
	'required' => '1',
	'required_msg' => $lang['required_msg_name'],
	'value'	=> ''
);

$groups = new simpletable(
	$config['prefix'].'groups',
	array(
		'link' => 'admin=groups',
		'header' => $lang['groups'],
		'form' => $inputs,
		'cols'	=> 'all',
		'fields' => array(
			'member_count' => array(
				'query' => '
					SELECT
						count('.$config['prefix'].'user.id)
					FROM
						'.$config['prefix'].'user
					WHERE
						FIND_IN_SET('.$config['prefix'].'groups.id ,REPLACE(groups, ";", ",") )
				',
				'after' => 'name',
				'name' => get_label('members')
			)
		)
	)
);

$content .= $groups->show();

?>