<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

if(isset($_GET['action']))
{
	if($_GET['action'] == 'edit' OR $_GET['action'] == 'del')
	{
		$info = user_info($_GET['itemid']);
		$header = $lang['user'].': '.$info['name'];
		
		if($_GET['action'] == 'del' AND $user['id'] == $_GET['itemid'])
		{
			die(draw_error($lang['error'], $lang['selfdelete_error']));
			$autoform_lock = true;
		}
	}
}

$inputs[] = array(
	'name'	=> 'time',
	'type'	=> 'fix',
	'value'	=> time()
);

$inputs[] = array(
	'name'	=> 'reg_time',
	'type'	=> 'fix',
	'value'	=> time()
);

$inputs[] = array(
	'name'	=> 'name',
	'type'	=> 'text',
	'lang'	=> $lang['user_name'],
	'required' => '1',
	'required_msg' => $lang['required_msg_name'],
	'value'	=> ''
);

$inputs[] = array(
	'name'	=> 'email',
	'type'	=> 'text',
	'lang'	=> $lang['email'],
	'required' => 1,
	'value'	=> ''
);

$inputs[] = array(
	'name'	=> 'pass',
	'type'	=> 'password',
	'lang' => $lang['user_pass'],
	'lang_v' => $lang['user_pass_v'],
	'required' => '1',
	'required_msg' => '',
	'value'	=> '',
);

if(!isset($_GET['itemid']) OR $user['id'] != $_GET['itemid']){
	$inputs[] = array(
		'type'	=> 'select',
		'name'	=> 'perm',
		'lang'	=> $lang['perm'],
		'options' => array(
			'admin' => array(
				'lang' => $lang['admin'],
				'active' => ''
			),
			'user' => array(
				'default' => '1',
				'lang' => $lang['user'],
				'active' => ''
			)
		)
	);
	
	$inputs[] = array(
		'type'	=> 'checkbox',
		'name'	=> 'active',
		'lang'	=> $lang['user_active'],
		'default' => '',
		'active' => ''
	);
	
}

$options = array();
$tzlist = new timezones();
$tzlist_echo = $tzlist->getlist('groups', 'name');
foreach($tzlist_echo as $continent => $citys){
	$options[$continent]['lang'] = $continent;
	foreach($citys as $city => $zone){
		$options[$continent]['options'][$zone['zone']] = array(
			'lang'		=> $city,
			'active'	=> ''
		);
	}
}
$inputs[] = array(
	'type'	=> 'select',
	'name'	=> 'timezone',
	'lang'	=> $lang['timezone'],
	'groups' => 1,
	'options' => $options
);

$options = array();
foreach($config['availablelangs'] as $optlang){
	$options[$optlang] = array(
		'lang'		=> $optlang,
		'active'	=> ''
	);
}
$inputs[] = array(
	'type'	=> 'select',
	'name'	=> 'lang',
	'lang'	=> $lang['lang'],
	'options' => $options
);

$inputs[] = array(
	'type'	=> 'checkbox',
	'name'	=> 'pms_sendemail',
	'lang'	=> $lang['pms_sendemail']
);	

$options = array();
$groups = list_groups();
if(is_array($groups))
foreach($groups as $group){
	$options[$group['id']] = array(
		'lang'		=> $group['name'],
		'active'	=> ''
	);
}

$inputs[] = array(
	'type'	=> 'select',
	'name'	=> 'groups',
	'lang'	=> $lang['groups'],
	'multiple' => '1',
	'options' => $options
);

$fields = array(
	'id' => array(
		
	),
	'name' => array(
		
	),
	'reg_time' => array(
		'renderfunction' => 'draw_time'
	),
	'perm' => array(
		'renderfunction' => 'get_label'
	),
	'active' => array(
		'renderfunction' => 'draw_bool'
	),
	'groups' => array(
		'renderfunction' => 'draw_groups'
	),
	'time' => array(
		'renderfunction' => 'draw_time'
	)
);

if(isset($installedModules) && is_array($installedModules))
foreach($installedModules as $modulename => $module)
{
	if($module['active'] == '1')
	{
		$file = $phpRootPath.'modules/'.$modulename.'/mod_additional_user_fields.php';
		if(file_exists($file))
		{
			$additionalInputs = array();
			$additionalFields = array();
			include($file);
			if(isset($additionalInputs))
			{
				$inputs = array_merge($inputs, $additionalInputs);
			}
			if(isset($additionalFields))
			{
				$fields = array_merge($fields, $additionalFields);
			}
		}
	}
}

$tab = new simpletable(
	$config['prefix'].'user',
	array(
		'link' => 'admin=user',
		'header' => get_label('user'),
		'form' => $inputs,
		'cols'	=> 'fields',
		'default_order' => 'id DESC',
		'template' => array(
			'func_draw_content' => true,
			'action_renderfunction' => 'draw_admin_user_list_actions'
		),
		'fields' => $fields
	)
);

$content .= $tab->show();

?>