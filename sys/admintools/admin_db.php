<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

if(isset($database) && is_array($database))
{
	$databaseSave = $database;
	$database = array();
	include($phpRootPath.'sys/db.php');
	$database = array_replace_recursive($database, $databaseSave);
}
else
{
	include($phpRootPath.'sys/db.php');
}



$db = new dbcheck($config, $database);
$db->load_db();
$db->check();

$dbecho['db'] = $db->current_db;
$dbecho['db_errors'] = $db->errors;

if(is_array($db->errors) AND isset($_GET['db_action']) AND $_GET['db_action'] == 'repair'){
	$db->repair();
	$db->load_db();
	$db->check();
	draw_msg($lang['db'], $lang['db_repaired'], '');
}

$dbecho['db'] = $db->current_db;
$dbecho['db_errors'] = $db->errors;

if(isset($_GET['db_del_table']) AND $_GET['db_del_table'] != ''){
	
	if(isset($_GET['db_del_table']) AND $_GET['db_del_table'] != ''){
		if(isset($_GET['ok']) AND $_GET['ok'] == 'ok'){
			$db->del('table', $_GET['db_del_table'], '');
			$content .= draw_content($lang['db'], draw_db($dbecho, 'msg'));
			$db->load_db();
			$db->check();
		}else{
			$content .= draw_content($lang['db'], draw_db($dbecho, 'msg'));
		}
		
	}
	
}

if(isset($_GET['db_action']) AND $_GET['db_action'] == 'show' AND isset($_GET['table']) AND $_GET['table'] != ''){
	
	if(isset($_GET['db_del_field']) AND $_GET['db_del_field'] != ''){
		if(isset($_GET['ok']) AND $_GET['ok'] == 'ok'){
			$db->del('field', $_GET['table'], $_GET['db_del_field']);
			$content .= draw_content($lang['db'], draw_db($dbecho, 'msg'));
			$db->load_db();
			$db->check();
		}else{
			$content .= draw_content($lang['db'], draw_db($dbecho, 'msg'));
		}
		
	}

	if(isset($_GET['db_del_var']) AND $_GET['db_del_var'] != ''){
		if(isset($_GET['ok']) AND $_GET['ok'] == 'ok'){
			$db->del('variable', $_GET['table'], $_GET['db_del_var']);
			$content .= draw_content($lang['db'], draw_db($dbecho, 'msg'));
			$db->load_db();
			$db->check();
		}else{
			$content .= draw_content($lang['db'], draw_db($dbecho, 'msg'));
		}
		
	}
	
	$dbecho['db'] = $db->current_db;
	$dbecho['db_errors'] = $db->errors;
	$content .= draw_content($lang['db'], draw_db($dbecho, 'table'));
}else if(isset($_POST['backup_send'])){

	$content .= draw_content($lang['db'], draw_db($db->backup($_POST), 'backup'));
	
}else{
	$dbecho['db'] = $db->current_db;
	$dbecho['db_errors'] = $db->errors;
	$content .= draw_content($lang['db'], draw_db($dbecho, 'db'));
}

?>