<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

if(isset($_GET['action']) && $_GET['action'] == 'sendqueue')
{
	include_once($phpRootPath.'sys/functions/func_taskqueue.php');
	Task_MailQueue(0);
}

$tab = new simpletable(
	$config['prefix'].'mailqueue',
	array(
		'infobox' => draw_admin_email_mailqueue_infobox(),
		'link' => $link,
		'header' => get_label('mailqueue'),
		'cols'	=> 'fields',
		'default_order' => 'time_created DESC',
		'truncate' => true,
		'template' => array(
			'func_draw_content' => true,
			'show_new_btns' => false,
			'show_actions_btns' => false
		),
		'fields' => array(
			'id' => array(
			),
			'recipient_email' => array(
			),
			'recipient_name' => array(
			),
			'subject' => array(
			),
			'time_created' => array(
					'renderfunction' => 'draw_time'
			)
		)
	)
);

$content .= $tab->show();