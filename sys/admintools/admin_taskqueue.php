<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

if(isset($_GET['run']))
{
	
	$function = $mysqli->real_escape_string($_GET['run']);
	$function_args = '';
	if(isset($_GET['args']))
	{
		$function_args = $mysqli->real_escape_string($_GET['args']);
	}
	
	include($phpRootPath.'sys/functions/func_taskqueue.php');

	$logfile = fopen($phpRootPath.'logs/taskqueue/'.date("Y-m-d", time()).'.log', 'a');

	foreach(array_merge($sysmoduleloader, $moduleloader) as $modulename)
	{
		if(isset($installedModules[$modulename]) && $installedModules[$modulename]['active'] == "1")
		{
			$file = $phpRootPath.'modules/'.$modulename.'/mod_tasks.php';
			if(file_exists($file))
			{
				include($file);
			}
		}
	}
	if(function_exists($function))
	{
		ob_start();
		$function($function_args);
		$page = ob_get_contents();
		ob_end_clean();
		$msg_stack .= draw_alert($page, 'info');
	}
}

$inputs[] = array(
	'name'	=> 'name',
	'type'	=> 'text',
	'lang'	=> get_label('name'),
	'required' => '1'
);

$options = array(
	'mephitis' => array(
		'lang' => get_label('mep_system'),
		'options' => array(
			'' => array(
				'lang' => get_label('none')
			),
			'Task_FixDB' => array(
				'lang' => get_label('Task_FixDB')
			),
			'Task_BackupDB' => array(
				'lang' => get_label('Task_BackupDB')
			),
			'Task_FixBackupDB' => array(
				'lang' => get_label('Task_FixBackupDB')
			),
			'Task_MailQueue' => array(
				'lang' => get_label('Task_MailQueue')
			),
			'Task_ClearCache' => array(
				'lang' => get_label('Task_ClearCache')
			)
		)
	),
);
$modulenameSave = $modulename;
foreach($modules as $key => $mod)
{
	if(isset($installedModules[$key]) && $installedModules[$key]['active'] == "1")
	{
		$mod_tasks = array();
		if(isset($mod['tasks']) && is_array($mod['tasks']))
		{
			foreach($mod['tasks'] as $task => $task_info)
			{
				
				if(is_array($task_info))
				{
					$task_name = $task;
					$task_label = $task_info['lang'];
				}
				else
				{
					$task_name = $task_info;
					$task_label = get_label($task_info);
				}
				
				$mod_tasks[$task_name] = array(
					'lang' => $task_label
				);
			}
		}
		
		if(count($mod_tasks) > 0)
		{
			if(!isset($mod['lang']))$mod['lang'] = $key;
			$options[$key] = array(
				'lang' => $mod['lang'],
				'options' => $mod_tasks
			);
		}
	}
}
$modulename = $modulenameSave;

$inputs[] = array(
	'name'	=> 'function',
	'type'	=> 'select',
	'lang'	=> get_label('function'),
	'groups' => 1,
	'options' => $options,
	'required' => '1'
);

$inputs[] = array(
	'name'	=> 'function_args',
	'type'	=> 'text',
	'lang'	=> get_label('function_args')
);

$inputs[] = array(
	'name'	=> 'exec_date',
	'type'	=> 'text',
	'datepicker' => array(
		'class' => 'exec_date_datepicker'
	),		
	'lang'	=> get_label('exec_date')
);

$inputs[] = array(
	'name'	=> 'exec_time_of_day',
	'type'	=> 'text',
	'timepicker' => '1',	
	'lang'	=> get_label('exec_time_of_day')
);

$options = array(
	'simple' => array(
		'lang' => get_label('exec_mode_simple'),
		'options' => array(
			'simple_once' => array(
				'lang' => get_label('exec_mode_simple_once')
			),
			'simple_daily' => array(
				'lang' => get_label('exec_mode_simple_daily')
			)			
		)
	),
	'weekly' => array(
		'lang' => get_label('exec_mode_weekly'),
		'options' => array(
			'weekly_mon' => array(
				'lang' => get_label('mon')
			),
			'weekly_tue' => array(
				'lang' => get_label('tue')
			),
			'weekly_wed' => array(
				'lang' => get_label('wed')
			),
			'weekly_thu' => array(
				'lang' => get_label('thu')
			),
			'weekly_fri' => array(
				'lang' => get_label('fri')
			),
			'weekly_sat' => array(
				'lang' => get_label('sat')
			),
			'weekly_sun' => array(
				'lang' => get_label('sun')
			)			
		)		
	)
);


$inputs[] = array(
	'type'	=> 'select',
	'name'	=> 'exec_mode',	
	'lang'	=> get_label('exec_mode'),
	'groups' => 1,
	'options' => $options
);

$taskqueue = new simpletable(
	$config['prefix'].'taskqueue',
	array(
		'link' => 'admin=taskqueue',
		'header' => get_label('taskqueue'),
		'form' => $inputs,
		'cols'	=> 'all',
		'fields' => array(
			'function' => array(
				'renderfunction' => 'get_label'
			),
			'exec_time_of_day' => array(
				'renderfunction' => 'draw_time_of_day'
			),			
			'last_exec' => array(
				'renderfunction' => 'draw_time'
			),
			'exec_mode' => array(
				'renderfunction' => 'get_label'
			)				
		)
	)
);

$content .= $taskqueue->show();

?>