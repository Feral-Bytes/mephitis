<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

//
$page = getvar('homepage');
if(isset($_GET['page']))
{
	$page = $mysqli->real_escape_string($_GET['page']);
}

if(isset($_GET['select_page']))
{
	$page = $mysqli->real_escape_string($_GET['select_page']);
}


$page = GetPage($page);


if(!isset($page['id']))
{
	$page['id'] = 0;
}

$pageContent = new content(
	array(
		'admin' => true
	)
);

if(isset($_GET['select_content']))
{
	$selectedContent = db_get($config['prefix'].'content', $_GET['select_content']);
	$pageContent->IncludeElement($selectedContent['fce'], get_parent_template($page['rootline']));
	$selectedContentSettingsFunc = 'fce_settings_'.$selectedContent['fce'];
	
	if(function_exists($selectedContentSettingsFunc))
	{
		$selectedContentSettingsFuncResult = $selectedContentSettingsFunc('');
		if(isset($selectedContentSettingsFuncResult['allowedParents']))
		{
			$selectedContent['allowedParents'] = $selectedContentSettingsFuncResult['allowedParents'];
		}
		if(isset($selectedContentSettingsFuncResult['allowedChildren']))
		{
			$selectedContent['allowedChildren'] = $selectedContentSettingsFuncResult['allowedChildren'];
		}
	}
	
	if(isset($_GET['move_content_in']) && isset($_GET['pos']))
	{
		$pageContent->Move($_GET['select_content'], $_GET['move_content_in'], $_GET['pos']);
		if(isset($_GET['after']))
		{
			$pageContent->MoveElement($_GET['select_content'], 'after', $_GET['after']);		
		}
	}	
}

if(isset($_GET['order_content']))
{
	$pageContent->MoveElement($_GET['itemid'], $_GET['order_content']);	
}

if(isset($_GET['content_action']))
{

	$parent = 0;
	if(isset($_GET['parent']))
	{
		$parent = $mysqli->real_escape_string($_GET['parent']);
	}

	if($_GET['content_action'] == 'new')
	{
		$elementId = 0;
	}
	else
	{
		if(isset($_GET['content_element']) && $_GET['content_element'] == 'page')
		{
			$elementId = $_GET['page'];
		}
		else
		{
			$elementId = $_GET['itemid'];
		}
	}
	
	$after = 0;
	if(isset($_GET['after']))
	{
		$after = $_GET['after'];
	}
	
	if($_GET['content_action'] == 'new' && !isset($_GET['content_element']))
	{
		$contentElements = ContentGetElementsList($pageContent->GetTemplates($page), 'merged');
		
		$parentElement = 'page';
		if($parent != 0)
		{
			$parentElement = db_get($config['prefix'].'content', $parent);
			$parentElement = $parentElement['fce'];
		}
		$pageContent->IncludeElement($parentElement, get_parent_template($page['rootline']));
		$parentSettingsFunc = 'fce_settings_'.$parentElement;
		
		$toRemove = array();
		
		foreach($contentElements as $contentElement => $template)
		{
			
			$pageContent->IncludeElement($contentElement, $template);
			$settingsFunc = 'fce_settings_'.$contentElement;
			
			$data = array(
				'page' => $page,
				'pos' => $_GET['pos'],
				'parent' => $parent,
				'after' => $after,
				'element' => $contentElement,
				'elementId' => $elementId
				
			);
			
			$settings = $pageContent->GetSettings($data);
			
			if(function_exists($parentSettingsFunc));
			{
				$parentFuncResult = $parentSettingsFunc(null);
				if(isset($parentFuncResult['allowedChildren']))
				{
					$parentFuncResult['allowedChildren'] = explode(',', $parentFuncResult['allowedChildren']);
					$allowed = false;
					foreach($parentFuncResult['allowedChildren'] as $allowedChild)
					{
						if(trim($allowedChild) == trim($contentElement))
						{
							$allowed = true;
						}
					}
					
					if(!$allowed)
					{
						$toRemove[] = $contentElement;
					}
				}
			}
			
			
			
			if(function_exists($settingsFunc))
			{
				$settingsFuncResult = $settingsFunc($settings);
				
				if($settingsFuncResult != null)
				{
					if(isset($settingsFuncResult['allowedParents']))
					{
						$settingsFuncResult['allowedParents'] = explode(',', $settingsFuncResult['allowedParents']);
						$allowed = false;
						foreach($settingsFuncResult['allowedParents'] as $allowedParent)
						{
							if(trim($allowedParent) == trim($parentElement))
							{
								$allowed = true;
							}
						}
						if(!$allowed)
						{
							$toRemove[] = $contentElement;
						}
					}
				}
				else
				{
					$toRemove[] = $contentElement;
				}
			}
			else
			{
				$toRemove[] = $contentElement;
			}
		}
		
		foreach($toRemove as $contentElement)
		{
			unset($contentElements[$contentElement]);
		}
		$content .= draw_cms_content_element_select($page, $contentElements);
		
	}
	else if(isset($_GET['content_element']))
	{

		$data = array(
			'page' => $page,
			'pos' => $_GET['pos'],
			'parent' => $parent,
			'after' => $after,
			'element' => $_GET['content_element'],
			'elementId' => $elementId
		);
		
		$settings = $pageContent->GetSettings($data);
		
		$absLink = $phproot.'?admin=content&content_action='.$_GET['content_action'].'&page='.$_GET['page'].'&itemid='.$elementId.'&pos='.$_GET['pos'].'&parent='.$parent.'&content_element='.$_GET['content_element'].'&after='.$after;
		
		switch($_GET['content_action'])
		{
			case 'new':
			case 'edit':
				
				$showForm = true;
				
				if(isset($_POST['send']))
				{
					if($_GET['content_action'] == 'new')
					{
						$newContentElementId = $pageContent->AddElement($settings, $_POST);
						if(is_numeric($newContentElementId))
						{
							$pageContent->MoveElement($newContentElementId, 'after', $_GET['after']);
							$msg_stack .= draw_alert(get_label('fce_added'), 'success');
							$showForm = false;
						}
					}
					
					if($_GET['content_action'] == 'edit')
					{
						if($pageContent->EditElement($settings, $_POST))
						{
							$msg_stack .= draw_alert(get_label('fce_edited'), 'success');
							$showForm = false;
						}
					}
				}

				if($showForm)
				{
					$formdata = array(
						'name' => 'common',
						'action' => $absLink,
						'method' => 'POST',
						'actionname' => 'content_action'
					);
					
					$content .= draw_form($settings['settings']['fields'], get_label('fce_content'), $formdata);
				}
			
				break;
			case 'del':
				
				if(isset($_GET['sure']) && $_GET['sure'] == 'yes')
				{
					$pageContent->DeleteElement($_GET['itemid']);
					
					$msg_stack .= draw_alert(get_label('fce_del_success'), 'success');
				}
				else
				{
					$msg = draw_confirm_dialog(get_label('fce_del_confirm'), $absLink.'&sure=yes', $phproot.'?admin=content&page='.$page['id']);
					$msg_stack .= draw_alert($msg, 'warning', false);
				}
				
				break;
		}
		
	}
	
	
}

?>