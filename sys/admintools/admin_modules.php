<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */


foreach(array_merge($sysmoduleloader, $moduleloader) as $modulename){
	
	if(!isset($installedModules[$modulename]))
	{
		$new = '
			INSERT INTO '.$config['prefix'].'modules
			(name, active)
			VALUES
			("'.$mysqli->real_escape_string($modulename).'", "0")
		';
		$mysqli->query($new);
	}
	
}

if(isset($_GET['itemid']))
{
	$query = 'SELECT * FROM '.$config['prefix'].'modules WHERE id = "'.$mysqli->real_escape_string($_GET['itemid']).'"';
	$result = $mysqli->query($query);
	$row = $result->fetch_assoc();
	$result->close();
	
	$lang['modules_manager'] = $lang['modules_manager'].': '.$row['name'];

	$modulename = $row['name'];

	if(isset($_GET['action']))
	{
		if($_GET['action'] == 'fix_db' || $_GET['action'] == 'update')
		{
			if(isset($installedModules[$modulename]) && $installedModules[$modulename]['active'] == "1")
			{
				$file = $phpRootPath.'modules/'.$modulename.'/mod_db.php';
				if(file_exists($file))
				{
					include($file);
					
					$databaseSave = $database;
					$database = array();
					include($phpRootPath.'sys/db.php');
					$database = array_replace_recursive($database, $databaseSave);
					
					$db = new dbcheck($config, $database);
					$db->load_db();
					$db->check();
					if(is_array($db->errors))
					{
						$db->repair();
						$msg_stack .= draw_alert(get_label('mod_db_fixed'), 'info');
					}
				}
				
				if($_GET['action'] == 'update' && isset($modules[$modulename]['update_func']))
				{
					$file = $phpRootPath.'modules/'.$modulename.'/mod_update.php';
					
					if(file_exists($file))
					{
						include($file);
					}
					
					if(function_exists($modules[$modulename]['update_func']))
					{
						if($modules[$modulename]['update_func']())
						{
							$msg_stack .= draw_alert(get_label('mod_updated'), 'info');
						}
						else
						{
							$msg_stack .= draw_alert(get_label('mod_updated_fail'), 'danger');
						}
					}
					else
					{
						$msg_stack .= draw_alert(get_label('mod_updater_notfound'), 'danger');
					}
				}
			}
		}
	}
}

$inputs[] = array(
	'name'	=> 'active',
	'type'	=> 'checkbox',
	'lang'	=> $lang['active']
);

$groups = new simpletable(
	$config['prefix'].'modules',
	array(
		'link' => 'admin=modules',
		'header' => $lang['modules_manager'],
		'template' => array(
			'func_draw_content' => true,
			'show_new_btns' => false,
			'action_renderfunction' => 'draw_admin_modules_actions'
		),		
		'form' => $inputs,
			'fields' => array(
				'id' => array(
					'display' => 'no'
				),
				'active' => array(
					'renderfunction' => 'draw_bool'
				)
			)	
	)
);

$content .= $groups->show();


if(isset($_POST['send']))
{
	$installedModules = get_installed_modules();
	
	foreach(array_merge($sysmoduleloader, $moduleloader) as $modulename)
	{
		if(isset($installedModules[$modulename]) && $installedModules[$modulename]['active'] == "1")
		{
			$file = $phpRootPath.'modules/'.$modulename.'/mod_db.php';
			if(file_exists($file))
			{
				include($file);
			}
		}
	}

	$databaseSave = $database;
	$database = array();
	include($phpRootPath.'sys/db.php');
	$database = array_replace_recursive($database, $databaseSave);
	
	$db = new dbcheck($config, $database);
	$db->load_db();
	$db->check();
	if(is_array($db->errors))
	{
		$db->repair();
	}	
}

?>