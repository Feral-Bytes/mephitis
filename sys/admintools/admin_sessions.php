<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

$tab = new simpletable(
	$config['prefix'].'sessions',
	array(
		'link' => 'admin=sessions',
		'header' => get_label('sessions'),
		'cols'	=> 'all',
		'default_order' => 'time DESC',
		'truncate' => true,
		'template' => array(
			'func_draw_content' => true,
			'show_new_btns' => false,
			'show_actions_btns' => false
		),
		'fields' => array(
			'user' => array(
				'renderfunction' => 'user_name'
			),
			'time' => array(
					'renderfunction' => 'draw_time'
			)
		)
	)
);

$content .= $tab->show();

?>