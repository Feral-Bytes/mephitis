<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

$link .= 'admin=email';

if(isset($_GET['subadmin']))
{
	$link .= '&subadmin='.$_GET['subadmin'];	
	
	
	if($_GET['subadmin'] == 'settings')
	{
		include($phpRootPath.'sys/admintools/admin_email_settings.php');
	}
	
	if($_GET['subadmin'] == 'queue')
	{
		include($phpRootPath.'sys/admintools/admin_email_queue.php');
	}	
	
}


?>