<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

//

$link .= 'admin=content';

include($phpRootPath.'sys/admintools/admin_content_page_tree.php');
include($phpRootPath.'sys/admintools/admin_content_page.php');

$content .= draw_cms_content($pageTree, $pageContent, $page);


?>