<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

$dir = 0;
if(isset($_GET['dir']) && is_numeric($_GET['dir']))$dir = $_GET['dir'];
$dir = $mysqli->real_escape_string($dir);
 
if(isset($_GET['dir_action']) && ($_GET['dir_action'] == 'edit' || ($_GET['dir_action'] == 'del' && !isset($_GET['sure']))))
$dir = $mysqli->real_escape_string($_GET['itemid']);
 
$info = array(
	'path'	=> 'files/'.$modulename.'/',
	'name'	=> 'file',
	'dir'	=> $dir,
	'module' => $modulename
);

if(isset($_GET['ajax']))
{
	if($_GET['ajax'] == 'upload')
	{
		mediaselect_uploader($info);
	}
} 

$inputs = array();

if(isset($_GET['action'])){

	$inputs[] = array(
		'type'	=> 'var',
		'name'	=> 'time',
		'value'	=> time()
	);

	$inputs[] = array(
		'type'	=> 'fix',
		'name'	=> 'user',
		'value'	=> $user['id']
	);
	
	$inputs[] = array(
		'name'	=> 'name',
		'type'	=> 'text',
		'lang'	=> $lang['name'],
		'required' => '1',
		'required_msg' => $lang['required_msg_name'],
		'value'	=> ''
	);

	$inputs[] = array(
		'name'	=> 'file',
		'type'	=> 'file',
		'path'	=> 'files/',
		'lang'	=> $lang['file'],
		'required' => '1',
		'required_msg' => $lang['required_msg_file'],
		'value'	=> ''
	);
	
	$inputs[] = array(
		'name'	=> 'thumb',
		'type'	=> 'media',
		'files'	=> mediaselect_filelist(array(
			'path'	=> 'files/',	
			'name'	=> 'thumb'
		)),
		'lang'	=> $lang['thumb_admin'],
	);
	
	$root_dir[0] = array(
		'lang' => $lang['root_dir'],
		'active' => ''
	);

	$recursion = new simple_recursion($config['prefix'].'file_dirs', 'dir', 'id', 0, 'ORDER BY id');
	$inputs[] = array(
		'type'	=> 'select',
		'name'	=> 'dir',
		'lang'	=> $lang['dir'],
		'options' => $recursion->get_list($root_dir)
	);
	
	$inputs[] = array(
		'type'	=> 'select',
		'name'	=> 'groups_mode',
		'lang'	=> $lang['groups_mode'],
		'options' => array(
			'std' => array(
				'lang' => $lang['std'],
				'active' => ''
			),
			'whitelist' => array(
				'lang' => $lang['whitelist'],
				'active' => ''
			),
			'blacklist' => array(
				'lang' => $lang['blacklist'],
				'active' => ''
			)
		)
	);
	
	$options = array();
	$groups = list_groups();
	if(is_array($groups))
	foreach($groups as $group){
		$options[$group['id']] = array(
			'lang'		=> $group['name'],
			'active'	=> ''
		);
	}
	
	$inputs[] = array(
		'type'	=> 'select',
		'name'	=> 'groups',
		'lang'	=> $lang['groups'],
		'multiple' => '1',
		'options' => $options
	);	
	
	if($_GET['action'] == 'edit' OR $_GET['action'] == 'del'){
		$info = file_info($_GET['itemid']);
		$header = $lang['file'].': '.$info['name'];
	}else{
		$header = $lang['file'];
	}

}

$files = new simpletable(
	$config['prefix'].'files',
	array(
		'link' => 'admin=files&dir='.$dir.'',
		'header' => $lang['files'],
		'form' => $inputs,
		'cols'	=> 'all',
		'search' => 'dir = "'.$dir.'"',
		'default_order' => 'time DESC',
		'template' => array(
			'func_draw_content' => false
		),
		'fields' => array(
			'user' => array(
				'renderfunction' => 'user_name'
			),
			'dir' => array(
				'display' => false
			),			
			'module' => array(
				'renderfunction' => 'get_label'
			),				
			'module_options' => array(
				'display' => false,
			),
			'thumb_display' => array(
				'after' => 'file_name',
				'name' => get_label('thumb'),
				'renderfunction' => 'draw_media'
			),			
			'thumb' => array(
				'display' => false,
			),
			'file' => array(
				'display' => false
			),
			'path' => array(
				'display' => false
			),
			'groups' => array(
				'renderfunction' => 'draw_groups'
			),
			'time' => array(
				'label' => $lang['lastchange'],
				'renderfunction' => 'draw_time'
			)
		)
	)
);

if(isset($_GET['action']) && !isset($_POST['send']) && ($_GET['action'] == 'new' || $_GET['action'] == 'edit'))
{
	$content .= $files->show();	
}
else
{
	
	$current = 0;
	if(isset($_GET['select_dir']))
	{
		$current = $_GET['select_dir'];
	}

	if(isset($_GET['dir']))
	{
		$current = $_GET['dir'];
	}
	
	$fileTree = new tree(
		array(
			'tab' => 'file_dirs',
			'fieldParent' => 'sub_of',
			'noRank' => true,
			'checkAccess' => true,
			'fields' => 'name',
			'current' => $current,
			'orderBy' => 'name ASC'
		)
	);
	
	if(isset($_GET['dir']) && isset($_GET['move_dir_in']))
	{
		$fileTree->Move($_GET['dir'], $_GET['move_dir_in']);
	}

	
	if(isset($_GET['dir_action']))
	{
		$ok = true;
		if($_GET['dir_action'] == 'del'){
			$result = $mysqli->query('SELECT * FROM '.$config['prefix'].'file_dirs WHERE sub_of = "'.$mysqli->real_escape_string($_GET['itemid']).'"') or die(draw_error($lang['mysql_error'], $mysqli->error));
			$row_dirs = $result->num_rows;
			$result = $mysqli->query('SELECT * FROM '.$config['prefix'].'files WHERE dir = "'.$mysqli->real_escape_string($_GET['itemid']).'"') or die(draw_error($lang['mysql_error'], $mysqli->error));
			$row_sites = $result->num_rows;
			if($row_sites != 0 OR $row_dirs != 0){
				$ok = false;
			}
		}
		
		if(isset($_GET['parent'])){$parent = $_GET['parent'];}else{$parent = 0;}
		$inputs[] = array(
			'type'	=> 'fix',
			'name'	=> 'sub_of',
			'value'	=> $parent
		);

		$inputs[] = array(
			'type'	=> 'text',
			'name'	=> 'name',
			'value'	=> '',
			'required' => '1',
			'required_msg' => $lang['required_msg_name'],
			'lang'	=> $lang['name']
		);

		$inputs[] = array(
			'type'	=> 'select',
			'name'	=> 'groups_mode',
			'lang'	=> $lang['groups_mode'],
			'options' => array(
				'std' => array(
					'lang' => $lang['std'],
					'active' => ''
				),
				'whitelist' => array(
					'lang' => $lang['whitelist'],
					'active' => ''
				),
				'blacklist' => array(
					'lang' => $lang['blacklist'],
					'active' => ''
				)
			)
		);
		
		$options = array();
		$groups = list_groups();
		if(is_array($groups))
		foreach($groups as $group){
			$options[$group['id']] = array(
				'lang'		=> $group['name'],
				'active'	=> ''
			);
		}
		
		$inputs[] = array(
			'type'	=> 'select',
			'name'	=> 'groups',
			'lang'	=> $lang['groups'],
			'multiple' => '1',
			'options' => $options
		);	

		if($_GET['dir_action'] == 'edit' OR $_GET['dir_action'] == 'del'){
			//$info = dir_info($_GET['dir']);
			$header = $lang['dirs'];
		}else{
			$header = $lang['dirs'];
		}
		if($ok)
		{
			$content .= autoform($config['prefix'].'file_dirs', $inputs, $phproot, 'admin=files&parent='.$parent.'&', $header, NULL, NULL, 'dir_action', '', '', true);	
		}
		else
		{
			$msg_stack .= draw_alert($lang['notempty'], 'danger');
		}
	}	
	
	$query = '
		SELECT
			id
		FROM
			'.$config['prefix'].'files
		WHERE
			dir = "0"
	';
	$root_result = $mysqli->query($query);
	


	$content .= draw_content($lang['files'], draw_files($info, $files->show(), $fileTree, $root_result->num_rows));	
}


?>