<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */


$inputs[] = array(
	'type'	=> 'var',
	'name'	=> 'time',
	'value'	=> time()
);

$inputs[] = array(
	'name'	=> 'name',
	'type'	=> 'text',
	'lang'	=> $lang['name'],
	'required' => '1',
	'required_msg' => $lang['required_msg_name'],
	'value'	=> ''
);

$inputs[] = array(
	'name'	=> 'code',
	'type'	=> 'text',
	'lang'	=> $lang['code'],
	'required' => '1',
	'required_msg' => $lang['required_msg_name'],
	'value'	=> ''
);

$inputs[] = array(
	'name'	=> 'file',
	'type'	=> 'emoji',
	'path'	=> 'files/emojis/',
	'lang'	=> $lang['file'],
	'no_thumb' => '1',
	'required' => '1',
	'required_msg' => $lang['required_msg_file'],
	'value'	=> ''
);

$fields = array(
	'id' => array(
		
	),
	'name' => array(
		
	),
	'thumbnail' => array(
		'name' => get_label('thumbnail'),
		'after' => 'name',
		'renderfunction' => 'darw_admin_emoji_img'
	),
	'code' => array(
		
	)
);

$tab = new simpletable(
	$config['prefix'].'emojis',
	array(
		'link' => 'admin=emojis',
		'header' => get_label('emojis'),
		'form' => $inputs,
		'cols'	=> 'fields',
		'rank' => true,
		'default_order' => 'rank ASC',
		'template' => array(
			'show_order_btns' => false,
			'func_draw_content' => true
		),
		'fields' => $fields
	)
);

$content .= $tab->show();



?>