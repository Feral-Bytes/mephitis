<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */


if(!($user['id'] != 0 && check_group_access(getvar('comment_mod_groups'), 'whitelist')))
{
	die(draw_error($lang['admin'], $lang['access_denied']));
}

include(include_file('template', 'admin_comments.php'));
$link .= 'frontend_admin=comments';

$inputs[] = array(
	'name'	=> 'subject',
	'type'	=> 'text',
	'lang'	=> $lang['subject'],
	'value'	=> ''
);

$inputs[] = array(
	'type'	=> 'textarea',
	'name'	=> 'content',
	'wysiwyg' => getvar('wysiwyg'),
	'required' => '1',
	'required_msg' => $lang['required_msg_content'],
	'value'	=> '',
	'lang'	=> $lang['content']
);	

$inputs[] = array(
	'type'	=> 'checkbox',
	'name'	=> 'online',
	'lang' => get_label('public')
);

if(isset($_GET['module']) && isset($_GET['get']))
{
	$link .= '&module='.$_GET['module'].'&get='.$_GET['get'];
	
	$wheres = array();

	$wheres[] = 'object = "'.$mysqli->real_escape_string($_GET['module']).'"';

	if($_GET['get'] == 'online')
	{
		$wheres[] = 'online = "1"';
	}	
	if($_GET['get'] == 'offline')
	{
		$wheres[] = 'online = "0"';
	}
	
	if(isset($_GET['comment']) AND ($_GET['comment'] == 'setonline' OR $_GET['comment'] == 'unsetonline')){
		$online = 0;
		if($_GET['comment'] == 'setonline')
		{
			$online = 1;
		}
		if($_GET['comment'] == 'unsetonline')
		{
			$online = 0;
		}
		
		$mysqli->query('
			UPDATE '.$config['prefix'].'comments
			SET
				online = "'.$online.'"
			WHERE
				id = "'.$mysqli->real_escape_string($_GET['itemid']).'"
		');
	}
	
	$comments = new simpletable(
		$config['prefix'].'comments',
		array(
			'link' => $link,
			'backlink' => 'frontend_admin=comments',
			'header' => get_label('comments').': '.get_label($mysqli->real_escape_string($_GET['module'])),
			'form' => $inputs,
			'search' => implode(' AND ', $wheres),
			'cols'	=> 'all',
			'default_order' => 'id DESC',
			'template' => array(
				'action_renderfunction' => 'draw_admin_comments_actions'
			),			
			'fields' => array(
				'object' => array(
					'renderfunction' => 'get_label'
				),
				'user_id' => array(
					'display' => false
				),
				'user_name' => array(
					'display' => false
				),					
				'user' => array(
					'after' => 'object_id',
					'name' => get_label('user'),
					'query' => '
						SELECT
							name
						FROM
							'.$config['prefix'].'user
						WHERE
							id = '.$config['prefix'].'comments.user_id
					'
				),
				'content' => array(
					
				),
				'time' => array(
					'renderfunction' => 'draw_time'
				),				
				'online' => array(
					'display' => false
				)				
			)
		)
	);
	$content .= $comments->show();
	
}
else
{
	$result_comment_containers = $mysqli->query('
		SELECT
			object, count(id) as count, sum(online = "1") as count_online, sum(online = "0") as count_offline
		FROM
			'.$config['prefix'].'comments
		GROUP BY
			object
	');

	$data = array();
	while($row_comment_containers = $result_comment_containers->fetch_assoc())
	{
		$data[$row_comment_containers['object']] = $row_comment_containers;
	}

	$content .= draw_content(get_label('comments'), draw_admin_comments($data));

}

$buildContent = false;
?>