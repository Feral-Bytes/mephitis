<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

$path = 'files/updates/';

function get_updates($path)
{
	$updates = '';

	if(file_exists($path))
	{
		$i = 0;
		$dir = opendir($path); 
		while($file = readdir($dir)) {
			if($file != '.' && $file != '..' && (strpos($file, '.zip') !== false)) {
				if(isset($_GET['del_update']) && is_numeric($_GET['del_update']) && $i == $_GET['del_update'])
				{
					unlink($path.$file);
				}
				else
				{
					$updates[] = array(
						'key' => $i,
						'name' => $file,
						'md5' => md5_file($path.$file)
					);
					
				}
				$i++;
			}
		}
		
		closedir($dir);
	}
	return $updates;
}


if(isset($_GET['update']) && is_numeric($_GET['update']))
{
	$updates = get_updates($path);
	$update = $updates[$_GET['update']];	
	
	$ok = false;
	
	if(isset($_GET['action']) && $_GET['action'] == 'install')
	{
		$ok = true;
		$run = false;	
		
		
		$runkey = md5($update['md5'].$update['name'].$user['pass']);
		if(isset($_GET['runkey']) && $_GET['runkey'] == $runkey)
		{
			$ok = true;
			$run = true;
		}
		else
		{
			if(!isset($_POST['md5_checksum']) || trim($_POST['md5_checksum']) != $update['md5'])
			{
				$ok = false;
				$msg_stack .= draw_alert(get_label('wrong_md5_checksum'), 'danger');
			}
			
			if(!isset($_POST['file_name']) || $_POST['file_name'] != $update['name'])
			{
				$ok = false;
				$msg_stack .= draw_alert(get_label('error_file'), 'danger');
			}
			
			if(!isset($_POST['admin_pw']) || !password_verify($_POST['admin_pw'], $user['pass']))
			{
				$ok = false;
				$msg_stack .= draw_alert(get_label('wrong_admin_pw'), 'danger');
			}
		}
		
		if($ok)
		{
			if($run)
			{
				$zip = new ZipArchive;
				
				if ($zip->open($path.$update['name']) === TRUE)
				{
					$zip->extractTo($phpRootPath);
					$zip->close();
					
					$databaseSave = $database;
					$database = array();
					include($phpRootPath.'sys/db.php');
					$database = array_replace_recursive($database, $databaseSave);
					
					$db = new dbcheck($config, $database);
					$db->load_db();
					$db->check();
					
					if(is_array($db->errors))
					{
						$db->repair();
					}
					
					$content .= draw_content($lang['install_update'].': '.$update['name'], draw_update_install_success($update, $runkey));
				}
				else
				{
					$content .= draw_content($lang['install_update'].': '.$update['name'], draw_update_install_fail($update, $runkey));
				}
			}
			else
			{
				$content .= draw_content($lang['install_update'].': '.$update['name'], draw_update_install($update, $runkey));
			}
		}
	}
	
	if(!$ok)
	{
		$content .= draw_content($lang['install_update'].': '.$update['name'], draw_update_install_check($update));
	}
}
else
{
	if(isset($_GET['ajax']))
	{
		if($_GET['ajax'] == 'upload')
		{
			if(isset($_FILES['file']['tmp_name']) AND $_FILES['file']['tmp_name'] != '')
			{
				if(!is_dir($phpRootPath.$path))
				{
					mkdir($phpRootPath.$path);
				}
				
				if($_FILES['file']['type'] == 'application/zip' && (strpos($_FILES['file']['name'], '.zip') !== false))
				{
					move_uploaded_file($_FILES['file']['tmp_name'], $phpRootPath.$path.$_FILES['file']['name']);
				}
			}
		}
	}

	$content .= draw_content($lang['updater'], draw_updater(get_updates($path)));
}
?>