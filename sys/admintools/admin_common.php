<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function get_form_admin_common()
{
	
	global $config, $mysqli, $lang, $phpRootPath, $httpRootPath;
	
	$settings = array();
	unset($config['settings']);
	
	if (isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS']))
	{
		$http = 'https://';
	}
	else
	{
		$http = 'http://';
	}
	$domain = $_SERVER['HTTP_HOST'];
	
	$settings[] = array(
		'name'	=> 'http',
		'type'	=> 'var',
		'value'	=> $http
	);
	
	$settings[] = array(
		'name'	=> 'domain',
		'type'	=> 'var',
		'value'	=> $domain
	);
	
	$settings[] = array(
		'name'	=> 'httpRootPath',
		'type'	=> 'var',
		'value'	=> $httpRootPath
	);
	
	$settings[] = array(
		'name'	=> 'title',
		'type'	=> 'text',
		'category' => 'settings_cat_common',
		'lang'	=> $lang['cms_title'],
		'value'	=> getvar('title')
	);
	
	$settings[] = array(
		'name'	=> 'footer',
		'type'	=> 'textarea',
		'category' => 'settings_cat_common',
		'lang'	=> $lang['cms_footer'],
		'value'	=> htmlspecialchars(getvar('footer'))
	);	

	$settings[] = array(
		'name'	=> 'cookie_name',
		'type'	=> 'text',
		'category' => 'settings_cat_common',
		'lang'	=> $lang['coookie_name'],
		'value'	=> getvar('cookie_name')
	);

	$settings[] = array(
		'name'	=> 'meta_robots',
		'type'	=> 'text',
		'category' => 'settings_cat_common',
		'lang'	=> $lang['meta_robots'],
		'value'	=> getvar('meta_robots')
	);

	$settings[] = array(
		'name'	=> 'meta_author',
		'type'	=> 'text',
		'category' => 'settings_cat_common',
		'lang'	=> $lang['meta_author'],
		'value'	=> getvar('meta_author')
	);

	$settings[] = array(
		'name'	=> 'meta_description',
		'type'	=> 'text',
		'category' => 'settings_cat_common',
		'lang'	=> $lang['meta_description'],
		'value'	=> getvar('meta_description')
	);

	$settings[] = array(
		'name'	=> 'meta_keywords',
		'type'	=> 'text',
		'category' => 'settings_cat_common',
		'lang'	=> $lang['meta_keywords'],
		'value'	=> getvar('meta_keywords')
	);

	$settings[] = array(
		'name'	=> 'shortcut_icon',
		'type'	=> 'text',
		'category' => 'settings_cat_common',
		'lang'	=> $lang['shortcut_icon'],
		'value'	=> getvar('shortcut_icon')
	);
	

	$options = array();
	$options[0] = array(
		'lang' => $lang['no_site'],
		'active' => ''
	);
	$result = $mysqli->query('SELECT * FROM '.$config['prefix'].'pages') or die(draw_error($lang['mysql_error'], $mysqli->error));
	while($row = $result->fetch_assoc()){
		$array['lang'] = $row['name'];
		if($row['id'] == getvar('homepage')){
			$array['active'] = ' selected';
		}else{
			$array['active'] = '';
		}
		$options[$row['id']] = $array;
	}
	$settings[] = array(
		'type'	=> 'select',
		'name'	=> 'homepage',
		'category' => 'settings_cat_common',
		'lang'	=> $lang['cms_homepage'],
		'options' => $options
	);

	$options = array();

	$path = 'templates/';
	$dir = opendir($phpRootPath.$path); 
	while($file = readdir($dir)) {
		if($file != '.' && $file != '..' && substr_count($file, '.') == 0) {
			if($file == getvar('template_dir')){
				$array['active'] = ' selected';
			}else{
				$array['active'] = '';
			}
			$array['lang'] = $file;
			$options[$file] = $array; 
		}
	}
	closedir($dir);

	$settings[] = array(
		'type'	=> 'select',
		'name'	=> 'template_dir',
		'category' => 'settings_cat_common',
		'lang'	=> $lang['template_dir'],
		'options' => $options
	);

	$settings[] = array(
		'name'	=> 'admin_name',
		'type'	=> 'text',
		'category' => 'settings_cat_admin',
		'lang'	=> $lang['admin_name'],
		'value'	=> getvar('admin_name')
	);

	$settings[] = array(
		'name'	=> 'admin_email',
		'type'	=> 'text',
		'category' => 'settings_cat_admin',
		'lang'	=> $lang['admin_email'],
		'value'	=> getvar('admin_email')
	);

	$settings[] = array(
		'name'	=> 'thumbnail_size',
		'type'	=> 'text',
		'category' => 'settings_cat_content',
		'lang'	=> $lang['thumbnail_size'],
		'value'	=> getvar('thumbnail_size')
	);

	$settings[] = array(
		'name'	=> 'norm_time',
		'type'	=> 'text',
		'category' => 'settings_cat_content',
		'lang'	=> $lang['norm_time'],
		'value'	=> getvar('norm_time')
	);
	
	$settings[] = array(
		'name'	=> 'norm_date',
		'type'	=> 'text',
		'category' => 'settings_cat_content',
		'lang'	=> $lang['norm_date'],
		'value'	=> getvar('norm_date')
	);	
	
	$settings[] = array(
		'name'	=> 'norm_date_notime',
		'type'	=> 'text',
		'category' => 'settings_cat_content',
		'lang'	=> $lang['norm_date_notime'],
		'value'	=> getvar('norm_date_notime')
	);	

	$settings[] = array(
		'name'	=> 'per_page',
		'type'	=> 'text',
		'category' => 'settings_cat_content',
		'lang'	=> $lang['per_page'],
		'value'	=> getvar('per_page')
	);
	if(isset($_POST['send']) AND (!is_numeric($_POST['per_page']) OR $_POST['per_page'] < 1))$_POST['per_page'] = 1;

	$settings[] = array(
		'name'	=> 'useronline_timeout',
		'type'	=> 'text',
		'category' => 'settings_cat_content',		
		'lang'	=> $lang['useronline_timeout'],
		'value'	=> getvar('useronline_timeout')
	);
	if(isset($_POST['send']) AND (!is_numeric($_POST['useronline_timeout']) OR $_POST['useronline_timeout'] < 1))$_POST['useronline_timeout'] = 1;
	
	$settings[] = array(
		'name'	=> 'msg_box_fadeout_timer',
		'type'	=> 'text',
		'category' => 'settings_cat_content',		
		'lang'	=> $lang['msg_box_fadeout_timer'],
		'value'	=> getvar('msg_box_fadeout_timer')
	);
	if(isset($_POST['send']) AND (!is_numeric($_POST['msg_box_fadeout_timer'])))$_POST['msg_box_fadeout_timer'] = 0;	
	
	$settings[] = array(
		'name'	=> 'num_emojis',
		'type'	=> 'text',
		'category' => 'settings_cat_user_content',
		'lang'	=> $lang['num_emojis'],
		'value'	=> getvar('num_emojis')
	);
	if(isset($_POST['send']) AND (!is_numeric($_POST['num_emojis']) OR $_POST['num_emojis'] < 1))$_POST['num_emojis'] = 1;
	
	$options = array(
		'bbcode' => array(
			'lang' => $lang['wysiwyg_bbcode']
		),
		'classic' => array(
			'lang' => $lang['wysiwyg_classic']
		)
	);
	$options[getvar('wysiwyg')]['active'] = ' selected';
	$settings[] = array(
		'type'	=> 'select',
		'name'	=> 'wysiwyg',
		'category' => 'settings_cat_user_content',
		'lang'	=> $lang['wysiwyg_user'],
		'options' => $options
	);	
	
	
	if(getvar('bbcode') == '1'){
		$active = ' checked';
	}else{
		$active = '';
	}
	$settings[] = array(
		'type'	=> 'checkbox',
		'name'	=> 'bbcode',
		'category' => 'settings_cat_user_content',
		'lang'	=> $lang['allow_bbcode'],
		'active' => $active
	);

	if(getvar('emojis') == '1'){
		$active = ' checked';
	}else{
		$active = '';
	}
	$settings[] = array(
		'type'	=> 'checkbox',
		'name'	=> 'emojis',
		'category' => 'settings_cat_user_content',
		'lang'	=> $lang['allow_emojis'],
		'active' => $active
	);

	$options = array();
	$groups = list_groups();
	$groups_selected = getvar('file_upload_groups');
	$groups_selected = explode(';', $groups_selected);
	if(is_array($groups))
	foreach($groups as $group){
		$active = '';
		for($i = 0; $i<count($groups_selected); $i++)
		{
			if($groups_selected[$i] == $group['id'])
			{
				$active = ' selected';
				break;
			}
		}
		
		$options[$group['id']] = array(
			'lang'		=> $group['name'],
			'active'	=> $active
		);
	}
	$settings[] = array(
		'type'	=> 'select',
		'name'	=> 'file_upload_groups',
		'category' => 'settings_cat_user_content',
		'lang'	=> $lang['file_upload_groups'],
		'multiple' => '1',
		'options' => $options
	);	
	
	$options = array();
	$groups = list_groups();
	$groups_selected = getvar('comment_mod_groups');
	$groups_selected = explode(';', $groups_selected);
	if(is_array($groups))
	foreach($groups as $group){
		$active = '';
		for($i = 0; $i<count($groups_selected); $i++)
		{
			if($groups_selected[$i] == $group['id'])
			{
				$active = ' selected';
				break;
			}
		}
		
		$options[$group['id']] = array(
			'lang'		=> $group['name'],
			'active'	=> $active
		);
	}
	$settings[] = array(
		'type'	=> 'select',
		'name'	=> 'comment_mod_groups',
		'category' => 'settings_cat_user_content',
		'lang'	=> $lang['comment_mod_groups'],
		'multiple' => '1',
		'options' => $options
	);
	
	if(getvar('comment_guests_can_write') == '1'){
		$active = ' checked';
	}else{
		$active = '';
	}
	$settings[] = array(
		'type'	=> 'checkbox',
		'name'	=> 'comment_guests_can_write',
		'category' => 'settings_cat_user_content',
		'lang'	=> $lang['comment_guests_can_write'],
		'active' => $active
	);

	if(getvar('comment_members_can_write') == '1'){
		$active = ' checked';
	}else{
		$active = '';
	}
	$settings[] = array(
		'type'	=> 'checkbox',
		'name'	=> 'comment_members_can_write',
		'category' => 'settings_cat_user_content',
		'lang'	=> $lang['comment_members_can_write'],
		'active' => $active
	);
	
	if(getvar('comment_requires_mod_check') == '1'){
		$active = ' checked';
	}else{
		$active = '';
	}
	$settings[] = array(
		'type'	=> 'checkbox',
		'name'	=> 'comment_requires_mod_check',
		'category' => 'settings_cat_user_content',
		'lang'	=> $lang['comment_requires_mod_check'],
		'active' => $active
	);
	
	if(getvar('reCAPTCHA') == '1'){
		$active = ' checked';
	}else{
		$active = '';
	}
	$settings[] = array(
		'type'	=> 'checkbox',
		'name'	=> 'reCAPTCHA',
		'category' => 'settings_cat_user_content',
		'lang'	=> $lang['reCAPTCHA'],
		'active' => $active
	);

	$settings[] = array(
		'name'	=> 'reCAPTCHA_key_private',
		'type'	=> 'text',
		'category' => 'settings_cat_user_content',
		'lang'	=> $lang['reCAPTCHA_key_private'],
		'value'	=> getvar('reCAPTCHA_key_private')
	);

	$settings[] = array(
		'name'	=> 'reCAPTCHA_key_public',
		'type'	=> 'text',
		'category' => 'settings_cat_user_content',
		'lang'	=> $lang['reCAPTCHA_key_public'],
		'value'	=> getvar('reCAPTCHA_key_public')
	);

	if(getvar('save_accesses') == '1'){
		$active = ' checked';
	}else{
		$active = '';
	}
	$settings[] = array(
		'type'	=> 'checkbox',
		'name'	=> 'save_accesses',
		'category' => 'settings_cat_user',
		'lang'	=> $lang['save_accesses'],
		'active' => $active
	);

	if(getvar('public_show_login') == '1'){
		$active = ' checked';
	}else{
		$active = '';
	}
	$settings[] = array(
		'type'	=> 'checkbox',
		'name'	=> 'public_show_login',
		'category' => 'settings_cat_user',
		'lang'	=> $lang['public_show_login'],
		'active' => $active
	);

	if(getvar('public_register') == '1'){
		$active = ' checked';
	}else{
		$active = '';
	}
	$settings[] = array(
		'type'	=> 'checkbox',
		'name'	=> 'public_register',
		'category' => 'settings_cat_user',
		'lang'	=> $lang['public_register'],
		'active' => $active
	);

	if(getvar('public_login') == '1'){
		$active = ' checked';
	}else{
		$active = '';
	}
	$settings[] = array(
		'type'	=> 'checkbox',
		'name'	=> 'public_login',
		'category' => 'settings_cat_user',
		'lang'	=> $lang['public_login'],
		'active' => $active
	);

	$options = array();
	$groups = list_groups();
	$groups_selected = getvar('new_user_groups');
	$groups_selected = explode(';', $groups_selected);
	if(is_array($groups))
	foreach($groups as $group){
		$active = '';
		for($i = 0; $i<count($groups_selected); $i++)
		{
			if($groups_selected[$i] == $group['id'])
			{
				$active = ' selected';
				break;
			}
		}
		
		$options[$group['id']] = array(
			'lang'		=> $group['name'],
			'active'	=> $active
		);
	}
	$settings[] = array(
		'type'	=> 'select',
		'name'	=> 'new_user_groups',
		'category' => 'settings_cat_user',
		'lang'	=> $lang['new_user_groups'],
		'multiple' => '1',
		'options' => $options
	);
	
	$options = array();
	$groups = list_groups();
	$groups_selected = getvar('guest_user_groups');
	$groups_selected = explode(';', $groups_selected);
	if(is_array($groups))
	foreach($groups as $group){
		$active = '';
		for($i = 0; $i<count($groups_selected); $i++)
		{
			if($groups_selected[$i] == $group['id'])
			{
				$active = ' selected';
				break;
			}
		}
		
		$options[$group['id']] = array(
			'lang'		=> $group['name'],
			'active'	=> $active
		);
	}
	$settings[] = array(
		'type'	=> 'select',
		'name'	=> 'guest_user_groups',
		'category' => 'settings_cat_user',
		'lang'	=> $lang['guest_user_groups'],
		'multiple' => '1',
		'options' => $options
	);
	
	$settings[] = array(
		'name'	=> 'register_text',
		'type'	=> 'textarea',
		'category' => 'settings_cat_user',
		'lang'	=> $lang['register_text'],
		'value'	=> getvar('register_text'),
		'wysiwyg' => 'html'
	);
	
	$settings[] = array(
		'name'	=> 'forgotpassword_max_age',
		'type'	=> 'text',
		'category' => 'settings_cat_user',
		'lang'	=> $lang['forgotpassword_max_age'],
		'value'	=> getvar('forgotpassword_max_age')
	);
	
	if(getvar('global_tracking_enable') == '1'){
		$active = ' checked';
	}else{
		$active = '';
	}
	$settings[] = array(
		'type'	=> 'checkbox',
		'name'	=> 'global_tracking_enable',
		'category' => 'settings_cat_tracking',
		'lang'	=> $lang['global_tracking_enable'],
		'active' => $active
	);
	
	$settings[] = array(
		'name'	=> 'global_tracking_code',
		'type'	=> 'textarea',
		'category' => 'settings_cat_tracking',
		'lang'	=> $lang['global_tracking_code'],
		'value'	=> getvar('global_tracking_code')
	);	
	
	return $settings;
}

$settings = get_form_admin_common();

if(isset($_POST['send'])){
	
	update_settings($settings);
	$msg_stack .= draw_alert($lang['common_settings_edited'], 'success');	
	$settings = get_form_admin_common();
	
}
	
$formdata = array(
	'name' => 'common',
	'action' => $phproot.'?admin=common',
	'method' => 'POST',
	'fileadd' => ''
);

$content .= draw_form(get_form_admin_common(), $lang['common_settings'], $formdata);


?>