<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

$current = 0;
if(isset($_GET['select_page']))
{
	$current = $_GET['select_page'];
}

if(isset($_GET['page']))
{
	$current = $_GET['page'];
}

$pageTree = new tree(
	array(
		'tab' => 'pages',
		'fields' => 'name,template',
		'checkAccess' => true,
		'checkTime' => true,
		'checkHidden' => true,
		'checkOnline' => true,
		'current' => $current,
		'orderBy' => 'rank ASC'
	)
);

if(isset($_GET['select_page']) && isset($_GET['move_page_in']))
{
	$pageTree->Move($_GET['select_page'], $_GET['move_page_in']);
	if(isset($_GET['after']))
	{
		$pageTree->MoveElement($_GET['select_page'], 'after', $_GET['after']);		
	}
}

if(isset($_GET['select_page']) && isset($_GET['page_copy_in']))
{
	$pageTree->CopyElement($_GET['select_page'], $_GET['page_copy_in']);
}

if(isset($_GET['order_page']))
{
	$pageTree->MoveElement($_GET['itemid'], $_GET['order_page']);	
}

if(isset($_GET['page_action']))
{

	$parent = 0;
	if(isset($_GET['parent']))
	{
		$parent = $mysqli->real_escape_string($_GET['parent']);
	}
	
	$result = $mysqli->query('SELECT MAX( rank ) AS rank FROM '.$config['prefix'].'pages WHERE parent = "'.$parent.'"') or die(draw_error(get_label('mysql_error'), $mysqli->error));
	$row = $result->fetch_assoc();
	$rank = $row['rank']+1; 
	
	$inputs[] = array(
		'type'	=> 'fix',
		'name'	=> 'rank',
		'value'	=> $rank
	);
	
	
	$inputs[] = array(
		'type'	=> 'fix',
		'name'	=> 'parent',
		'value'	=> $parent
	);
	
	$inputs[] = array(
		'type'	=> 'text',
		'name'	=> 'name',
		'required' => 1,
		'lang'	=> get_label('name')
	);

	$inputs[] = array(
		'type'	=> 'text',
		'name'	=> 'icon',
		'lang'	=> get_label('icon')
	);

	$options = array();
	$options['0'] = array(
		'lang' => '---'.get_label('template_parent').'---',
		'active' => ''
	);

	$path = $phpRootPath.'templates/';
	$dir = opendir($path); 
	while($file = readdir($dir)) {
		if($file != '.' && $file != '..' && substr_count($file, '.') == 0) {
			$array['active'] = '';
			$array['lang'] = $file;
			$options[$file] = $array; 
		}
	}
	closedir($dir);

	$inputs[] = array(
		'type'	=> 'select',
		'name'	=> 'template',
		'lang'	=> get_label('template'),
		'options' => $options
	);

	$inputs[] = array(
		'type'	=> 'select',
		'name'	=> 'content_kind',
		'lang'	=> get_label('content_kind'),
		'options' => array(
			'fce' => array(
				'lang' => get_label('content_kind_fce'),
				'active' => ''
			),
			'link' => array(
				'lang' => get_label('content_kind_link'),
				'active' => ''
			),
			'link_blank' => array(
				'lang' => get_label('content_kind_link_blank'),
				'active' => ''
			)
			
		)
	);

	$inputs[] = array(
		'name'	=> 'content_link_target',
		'type'	=> 'text',
		'lang'	=> get_label('content_link_target')
	);	

	$inputs[] = array(
		'type'	=> 'checkbox',
		'name'	=> 'online',
		'lang'	=> get_label('publish'),
		'default' => '1',
		'active' => ''
	);

	$inputs[] = array(
		'type'	=> 'checkbox',
		'name'	=> 'hidden',
		'lang'	=> get_label('hidden_nav'),
		'default' => '0',
		'active' => ''
	);

	$inputs[] = array(
		'type'	=> 'select',
		'name'	=> 'groups_mode',
		'lang'	=> get_label('groups_mode'),
		'options' => array(
			'std' => array(
				'lang' => get_label('std'),
				'active' => ''
			),
			'whitelist' => array(
				'lang' => get_label('whitelist'),
				'active' => ''
			),
			'blacklist' => array(
				'lang' => get_label('blacklist'),
				'active' => ''
			)
		)
	);

	$options = array();
	$groups = list_groups();
	if(is_array($groups))
	foreach($groups as $group){
		$options[$group['id']] = array(
			'lang'		=> $group['name'],
			'active'	=> ''
		);
	}

	$inputs[] = array(
		'type'	=> 'select',
		'name'	=> 'groups',
		'lang'	=> get_label('groups'),
		'multiple' => '1',
		'options' => $options
	);

	$inputs = array_merge($inputs, online_time_inputs());
	
	$content .= autoform($config['prefix'].'pages', $inputs, $phproot, 'admin=content&parent='.$parent.'&', get_label('page').': '.get_label($_GET['page_action']), NULL, NULL, 'page_action', $phproot.'?admin=content', '', true);
	
	if(isset($_GET['page_action']) && $_GET['page_action'] == 'del' && isset($autoform_success) && $autoform_success == 1)
	{
		$pageTree->DeleteChildren($_GET['itemid']);
		$query = '
			DELETE FROM '.$config['prefix'].'content'.'
			WHERE
				page = "'.$mysqli->real_escape_string($_GET['itemid']).'"
		';
		$del = $mysqli->query($query) or die(draw_error($lang['mysql_error'], $mysqli->error));
	}
}


?>