<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function write_to_lang($key, $subLang)
{
	global $lang;
	$lang[$key] = $subLang;
}
 
function load_templates()
{
	global $mysqli, $config, $user, $phpRootPath;
	
	$templates = array();
	 
	$path = $phpRootPath.'templates/';
	$dir = opendir($path); 
	while($file = readdir($dir))
	{
		if($file != '.' && $file != '..' && substr_count($file, '.') == 0)
		{ 
			$templates[$file] = array();
			
			$lang_file				= $phpRootPath.'templates/'.$file.'/lang.'.$user['lang'].'.php';
			$lang_file_fallback1	= $phpRootPath.'templates/'.$file.'/lang.english.php';
			$lang_file_fallback2	= $phpRootPath.'templates/'.$file.'/lang.php';
			
			if(file_exists($lang_file)){
				include($lang_file);
			}else if(file_exists($lang_file_fallback1)){
				include($lang_file_fallback1);
			}else if(file_exists($lang_file_fallback2)){
				include($lang_file_fallback2);
			}			
			
			if(isset($lang))
			{
				write_to_lang($file, $lang);
			}
			
			if(file_exists($phpRootPath.'templates/'.$file.'/variables.php'))
			{	
				include($phpRootPath.'templates/'.$file.'/variables.php');
				$templates[$file]['vars'] = $tpl_vars;
				$query_tpl = '
					SELECT
						*
					FROM
						'.$config['prefix'].'templates
					WHERE
						name = "'.$mysqli->real_escape_string($file).'"
				';
				
				$result_tpl = $mysqli->query($query_tpl);
				$db_tpl = $result_tpl->fetch_assoc();	
				
				if(isset($db_tpl))
				{
					$db_tpl['vars'] = json_decode($db_tpl['vars'], true);
					$templates[$file]['db'] = $db_tpl;
				}	
			}
		}
	}
	
	closedir($dir);
	
	return $templates;
}
 
$templates = load_templates();
if(isset($_GET['template']))
{
	$tpl = $mysqli->real_escape_string($_GET['template']);
}

if(isset($_GET['action']) && $_GET['action'] == 'edit_db_vars' && isset($tpl) && !isset($_POST['send']))
{
	$header = get_label('tpl_edit_db_vars').': '.get_label('name', $tpl);
	$content .= draw_content($header, draw_template($templates[$tpl]));
}
else
{
	if(isset($_GET['action']) && isset($tpl))
	{
		if($_GET['action'] == 'create_db_vars' && !isset($templates[$tpl]['db']) && isset($templates[$tpl]['vars']))
		{
			$vars = json_encode($templates[$tpl]['vars']);
			
			$post = '
				INSERT INTO '.$config['prefix'].'templates
					(name, vars) 
				VALUES
					("'.$tpl.'", "'.$mysqli->real_escape_string($vars).'")
			';
			$do_post = $mysqli->query($post);
			
			$msg_stack .= draw_alert(get_label('tpl_vars_created'), 'success');
			
		}
		
		if($_GET['action'] == 'edit_db_vars' && isset($templates[$tpl]['db']) && isset($_POST['send']))
		{
			$add = array();
			
			foreach($templates[$tpl]['vars'] as $cat_key => $cat_data)
			{
				
				foreach($cat_data as $var_key => $value)
				{
					if(isset($_POST[$cat_key.'_'.$var_key]))
					{
						$add[$cat_key][$var_key] = $_POST[$cat_key.'_'.$var_key];
						unset($_POST[$cat_key.'_'.$var_key]);
					}
				}
				
			}
			
			$vars = array_merge($_POST['vars'], $add);
			$vars = json_encode($vars);
			
			
			
			$update = '
				UPDATE
					'.$config['prefix'].'templates
				SET
					vars = "'.$mysqli->real_escape_string($vars).'"
				WHERE
					name = "'.$tpl.'"
			';
			$do_update = $mysqli->query($update);
			
			$msg_stack .= draw_alert(get_label('tpl_vars_updated'), 'success');
			
		}
		
		if($_GET['action'] == 'del_db_vars' && isset($templates[$tpl]['db']))
		{
			if(isset($_GET['sure'])){
				$query = 'DELETE FROM '.$config['prefix'].'templates WHERE name = "'.$tpl.'"';
				$del = $mysqli->query($query) or die(draw_error($lang['mysql_error'], $mysqli->error));
				$msg_stack .= draw_alert($lang['deleted'], 'success');
			}else{
				$msg = draw_confirm_dialog($lang['del_confirm'], $phproot.'?admin=templates&template='.$tpl.'&action=del_db_vars&sure=yes', $phproot.'?admin=templates');
				$msg_stack .= draw_alert($msg, 'warning');
			}			
		}		
	}
	$content .= draw_content(get_label('templates'), draw_templates_list(load_templates()));
}
 ?>