<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

if($user['id'] == '0')
{
	die(draw_error($lang['error'], $lang['access_denied']));
}

if(isset($_GET['pm_action']))
{
	if($_GET['pm_action'] == 'del' && isset($_GET['pm']))
	{
		if(!isset($_GET['sure']) || $_GET['sure'] != 'yes')
		{
			$msg_stack .= draw_alert($pms->draw->DelDialogue($_GET['pm'], $_GET['pms']), 'warning');
		}
		if(isset($_GET['sure']) && $_GET['sure'] == 'yes')
		{
			$msg_stack .= $pms->Del($_GET['pm'], $_GET['pms']);
		}		
		
	}
}

if($_GET['pms'] == 'show' && isset($_GET['pm']))
{
	$content .= $pms->ShowPm($_GET['pm']);
}
else if($_GET['pms'] == 'inbox')
{
	$content .= $pms->Inbox();
}
else if($_GET['pms'] == 'outbox')
{
	$content .= $pms->Outbox();
}
else if($_GET['pms'] == 'action' && isset($_GET['pm_action']) && $_GET['pm_action'] == 'new')
{
	if(isset($_GET['ajax']) && $_GET['ajax'] == 'getform')
	{
		echo $pms->NewPm(null, true);
		exit;
	}
	
	$content .= $pms->NewPm();

}
else
{
	$content .= $pms->Inbox();
}
?>