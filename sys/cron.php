<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

define('MephitisCMS', 'cron');

$rootDir = dirname(dirname($_SERVER['PHP_SELF']));
if(strlen($rootDir) <= 1)
{
	$rootDir = '';
}
$phpRootPath = dirname(__DIR__).'/';
$httpRootPath = $rootDir.'/';
$phproot = $rootDir.'/sys/cron.php';

$stylefile = 'style.php';

include($phpRootPath.'sys/init.php');

$httpRootPath = getvar('httpRootPath');
$phproot = $httpRootPath.'/sys/cron.php';

include($phpRootPath.'sys/functions/func_taskqueue.php');

$logfile = fopen($phpRootPath.'logs/taskqueue/'.date("Y-m-d", time()).'.log', 'a');

foreach(array_merge($sysmoduleloader, $moduleloader) as $modulename)
{
	if(isset($installedModules[$modulename]) && $installedModules[$modulename]['active'] == "1")
	{
		$file = $phpRootPath.'modules/'.$modulename.'/mod_tasks.php';
		if(file_exists($file))
		{
			include($file);
		}
	}
}

include($phpRootPath.'sys/taskqueue.php');

fclose($logfile);

//debug mode
if($config['debugmode'] == 1){

	$query = "SHOW SESSION STATUS LIKE 'Questions'";
	$result = $mysqli->query($query);
	$row = $result->fetch_assoc();
	
	debug ($row);

	$endtime = microtime(TRUE);
	debug(round($endtime-$starttime, 3));

	debug($_SESSION);
	debug($_COOKIE);
	debug($_SERVER);
	debug($_GET);
	debug($_POST);
	debug($user);
}

?>