<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

$day = 3600*24;
$week = $day*7;

TaskLog('init tasks');


$query = '
	SELECT
		*
	FROM
		'.$config['prefix'].'taskqueue
	ORDER BY
		exec_time_of_day ASC
';

$result = $mysqli->query($query);
while($task = $result->fetch_assoc())
{
		TaskLog('check for ID: '.$task['id'].', name '.$task['name'].', function '.$task['function'].'');
		
		$mode = $task['exec_mode'];
		
		$exec_time_of_day = $task['exec_time_of_day'];
		$time_of_day = time()-strtotime('today');
		$day_of_week = strtolower(date('D', time()));
		$last_exec =  $task['last_exec'];

		$run = false;
		
		if($mode == '')$run = false;
		
		if($mode == 'simple_once' && $last_exec = '0' && strtotime($task['exec_date']))
		{
			$exec_time = strtotime($task['exec_date'])+$task['exec_time_of_day'];
			if(time() >= $exec_time)
			{
				$run = true;
			}
		}
		
		if($mode == 'simple_daily' && $time_of_day >= $exec_time_of_day && (time()-$last_exec) >= $day)
		{
			$run = true;
		}
		
		if($mode == 'weekly_mon' && $day_of_week == 'mon' && $time_of_day >= $exec_time_of_day && (time()-$last_exec) >= $day)
		{
			$run = true;
		}
		if($mode == 'weekly_tue' && $day_of_week == 'tue' && $time_of_day >= $exec_time_of_day && (time()-$last_exec) >= $day)
		{
			$run = true;
		}
		if($mode == 'weekly_wed' && $day_of_week == 'wed' && $time_of_day >= $exec_time_of_day && (time()-$last_exec) >= $day)
		{
			$run = true;
		}
		if($mode == 'weekly_thu' && $day_of_week == 'thu' && $time_of_day >= $exec_time_of_day && (time()-$last_exec) >= $day)
		{
			$run = true;
		}
		if($mode == 'weekly_fri' && $day_of_week == 'fri' && $time_of_day >= $exec_time_of_day && (time()-$last_exec) >= $day)
		{
			$run = true;
		}
		if($mode == 'weekly_sat' && $day_of_week == 'sat' && $time_of_day >= $exec_time_of_day && (time()-$last_exec) >= $day)
		{
			$run = true;
		}
		if($mode == 'weekly_sun' && $day_of_week == 'sun' && $time_of_day >= $exec_time_of_day && (time()-$last_exec) >= $day)
		{
			$run = true;
		}		
		
		if($run)
		{
			if(function_exists($task['function']))
			{
				TaskLog('execute function for ID: '.$task['id'].' ...');	
				
				$task['function']($task['function_args']);
				
				$update = '
					UPDATE
						'.$config['prefix'].'taskqueue
					SET
						last_exec = "'.time().'"
					WHERE
						id = "'.$task['id'].'"
				';
				$mysqli->query($update);
				
				TaskLog('done for ID: '.$task['id'].'');
			}
			else
			{
				TaskLog('ERROR: function not found for ID: '.$task['id'].'');
			}
		}
		else
		{
			TaskLog('nothing for ID: '.$task['id'].'');
		}
}

TaskLog('end of queue', 0, true);

?>