<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

$sql .= "
INSERT INTO `".$config['prefix']."emojis` (`id`, `rank`, `name`, `code`, `file`, `file_name`, `path`, `type`, `size`, `crypt`, `crypt_key`, `time`) VALUES
(1, 1, 'saiyajin', ':saiyajin:', '1_d90a8363ee67fdbd00be53d69d26efd0', 'afro.gif', 'files/emojis/', 'image/gif', '21720', '', '', 1429435138),
(2, 2, 'angel', ':angel:', '1_4971bcd302ff9e1cc63dc7751f026cdb', 'angel.gif', 'files/emojis/', 'image/gif', '6407', '', '', 1429435232),
(3, 3, 'angry', ':angry:', '1_eb5887b21fca1e5d83e04dd0d219bc84', 'angry.gif', 'files/emojis/', 'image/gif', '3171', '', '', 1429435244),
(4, 4, 'anz', ':anz:', '1_0ba55c16a2326f192b30f420fba20be1', 'azn.gif', 'files/emojis/', 'image/gif', '1962', '', '', 1429435254),
(5, 5, 'bag', ':bag:', '1_c0265b387715519d66bce54745b1db96', 'bag.gif', 'files/emojis/', 'image/gif', '8812', '', '', 1429435267),
(6, 6, 'cheesy', ':cheesy:', '1_8935b6584221024ad5821e78dfd1a5fd', 'cheesy.gif', 'files/emojis/', 'image/gif', '3846', '', '', 1429435280),
(7, 7, 'cool', ':cool:', '1_62ff8b08bd8e5ccc5a5ea9931bf9c415', 'cool.gif', 'files/emojis/', 'image/gif', '4108', '', '', 1429435292),
(8, 8, 'cry', ':cry:', '1_9cec5969304bc6c946d4bdd504450a7d', 'cry.gif', 'files/emojis/', 'image/gif', '2732', '', '', 1429435303),
(9, 9, 'drink', ':drink:', '1_66da8560dc964ed451611915b2b8199f', 'drink.gif', 'files/emojis/', 'image/gif', '9301', '', '', 1429435317),
(10, 10, 'drool', ':drool:', '1_07c79cc9de09840c79cec839ea5ab70c', 'drool.gif', 'files/emojis/', 'image/gif', '2099', '', '', 1429435329),
(11, 11, 'evil', ':evil:', '1_08940db793e89c164ba7d16f6e35c1c3', 'evil.gif', 'files/emojis/', 'image/gif', '14369', '', '', 1429435340),
(12, 12, 'feed', ':feed:', '1_72a9b0086fbaf636ecb678903c799970', 'feed.gif', 'files/emojis/', 'image/gif', '2767', '', '', 1429435351),
(13, 13, 'fool', ':fool:', '1_391ea4b06af8a01f62b1d2680043eb3b', 'fool.gif', 'files/emojis/', 'image/gif', '19435', '', '', 1429435365),
(14, 14, 'ghost', ':ghost:', '1_4d18c243a335f9db5953a7b11b8655ae', 'ghost.gif', 'files/emojis/', 'image/gif', '3571', '', '', 1429435375),
(15, 15, 'grin', ':grin:', '1_a0609416b3593616989e1d16b1f4d487', 'grin.gif', 'files/emojis/', 'image/gif', '9910', '', '', 1429435386),
(16, 16, 'happy', ':happy:', '1_4a41fca4b11db6232ea5f9c4bf5af488', 'happy.gif', 'files/emojis/', 'image/gif', '1574', '', '', 1429435397),
(17, 17, 'huh', ':huh:', '1_8b54e2a28caafce444ab9df98b924f69', 'huh.gif', 'files/emojis/', 'image/gif', '12898', '', '', 1429435406),
(18, 18, 'humm', ':humm:', '1_fb286f7642611712494963efcad80c6a', 'humm.gif', 'files/emojis/', 'image/gif', '1033', '', '', 1429435417),
(19, 19, 'idea', ':idea:', '1_6d15b7c94114b3be1d545c3e52612d41', 'idea.gif', 'files/emojis/', 'image/gif', '3979', '', '', 1429435430),
(20, 20, 'irony', ':irony:', '1_5adc37c736820eda45419d8265cbf41e', 'irony.gif', 'files/emojis/', 'image/gif', '1567', '', '', 1429435442),
(21, 21, 'kiss', ':kiss:', '1_aabf14e8e05a0430cea5fc9b9ee0117b', 'kiss.gif', 'files/emojis/', 'image/gif', '4496', '', '', 1429435453),
(22, 22, 'laugh', ':laugh:', '1_610bfdec546c1f1673fbaac4b688e02c', 'laugh.gif', 'files/emojis/', 'image/gif', '2551', '', '', 1429435466),
(23, 23, 'lipsrsealed', ':libsrsealed:', '1_83379bde0c85232a71fd859dc64fbd97', 'lipsrsealed.gif', 'files/emojis/', 'image/gif', '976', '', '', 1429435503),
(24, 24, 'love', ':love:', '1_2fe6e7474ef8322bf55c6ed524389ca4', 'love.gif', 'files/emojis/', 'image/gif', '1903', '', '', 1429435518),
(25, 25, 'love2', ':love2:', '1_9ad568a4116feb2d11f050ffa8e58467', 'love2.gif', 'files/emojis/', 'image/gif', '6150', '', '', 1429435531),
(26, 26, 'love3', ':love3:', '1_f2ccda9035b368e83cf1ec264e3d3fa9', 'love3.gif', 'files/emojis/', 'image/gif', '4500', '', '', 1429435543),
(27, 27, 'no', ':no:', '1_b96e36b9710fafdf63508ff8c963f184', 'no.gif', 'files/emojis/', 'image/gif', '2244', '', '', 1429435554),
(28, 28, 'nopes', ':nopes:', '1_41f8453f9f74576dbc6081a7f1773b00', 'nopes.gif', 'files/emojis/', 'image/gif', '3287', '', '', 1429435566),
(29, 29, 'ok', ':ok:', '1_b476b039b2f112e160e1c51cbdf23689', 'ok.gif', 'files/emojis/', 'image/gif', '6407', '', '', 1429435577),
(30, 30, 'police', ':police:', '1_54c3589cc1221e3e10a6680517f039c1', 'police.gif', 'files/emojis/', 'image/gif', '6344', '', '', 1429435590),
(31, 31, 'puagh', ':puagh:', '1_d96172f56c116b4a4aec74c3428ea976', 'puagh.gif', 'files/emojis/', 'image/gif', '11929', '', '', 1429435604),
(32, 32, 'rolleyes', ':rolleyes:', '1_397abdff1424b10ac87664f431399138', 'rolleyes.gif', 'files/emojis/', 'image/gif', '2484', '', '', 1429435620),
(33, 33, 'sad', ':sad:', '1_4c5c2d247674031303824258a4ae3b39', 'sad.gif', 'files/emojis/', 'image/gif', '2438', '', '', 1429435634),
(34, 34, 'shocked', ':shocked:', '1_0206c4935b287097a9f5f34d27f0db7d', 'shocked.gif', 'files/emojis/', 'image/gif', '1174', '', '', 1429435647),
(35, 35, 'sip', ':sip:', '1_b6e99aa5d9d6d68a872f8ecb05929a01', 'sip.gif', 'files/emojis/', 'image/gif', '2085', '', '', 1429435659),
(36, 36, 'smiley', ':smiley:', '1_85b166a1f2e413ebca47e6c91313b443', 'smiley.gif', 'files/emojis/', 'image/gif', '2374', '', '', 1429435672),
(37, 37, 'tongue', ':tongue:', '1_a953faf43269114ef5b0024b822f5908', 'tongue.gif', 'files/emojis/', 'image/gif', '3628', '', '', 1429435685),
(38, 38, 'tss', ':tss:', '1_11b9658b9cddc41bc03ab8ebc2828a93', 'tss.gif', 'files/emojis/', 'image/gif', '2862', '', '', 1429435695),
(39, 39, 'undecided', ':undecided:', '1_d33d97b84ae3f3be1e5632522549f2ab', 'undecided.gif', 'files/emojis/', 'image/gif', '16353', '', '', 1429435718),
(40, 40, 'woa', ':woa:', '1_23b6da8e7ad1a74ea71cff6824d39d2a', 'woa.gif', 'files/emojis/', 'image/gif', '10764', '', '', 1429435741),
(41, 41, 'wtf', ':wtf:', '1_0fe63f9509b1d8e26a631d9aa01e59b5', 'wtf.gif', 'files/emojis/', 'image/gif', '9071', '', '', 1429435751),
(42, 42, 'yawn', ':yawn:', '1_6fe97c507f1c877864d1040c2a43bbf1', 'yawn.gif', 'files/emojis/', 'image/gif', '22756', '', '', 1429435763),
(43, 43, 'zoom', ':zoom:', '1_790dfbcdc5e70379e67b3988d0e63855', 'zoom.gif', 'files/emojis/', 'image/gif', '12517', '', '', 1429435774);
";

?>