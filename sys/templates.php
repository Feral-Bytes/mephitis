<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

include(include_file('template', 'style_html.php'));
include(include_file('template', $stylefile));
include(include_file('template', 'template_main.php'));
include(include_file('template', 'template_fce.php'));
include(include_file('template', 'template_content.php'));
include(include_file('template', 'template_user.php'));
include(include_file('template', 'template_comments.php'));
include(include_file('template', 'template_pagination.php'));
include(include_file('template', 'template_actions.php'));
include(include_file('template', 'template_time.php'));
include(include_file('template', 'template_nav.php'));
include(include_file('template', 'template_autoform.php'));
include(include_file('template', 'template_mediaselect.php'));
include(include_file('template', 'template_bootstrap_elements.php'));
include(include_file('template', 'template_pms.php'));

if(isset($mysqli) && file_exists('templates/'.$config['template'].'/variables.php'))
{
	include('templates/'.$config['template'].'/variables.php');
	load_tpl_vars();
}

if(isset($mysqli))
{
	load_renderfunctions();
}

?>