<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function MepAutoloader($class) {
	
	global $phpRootPath;
	
	$classPath = $phpRootPath.'sys/classes/'.$class.'.php';
	if(file_exists($classPath))
	{
		include($classPath);
	}
}

spl_autoload_register('MepAutoloader');

?>