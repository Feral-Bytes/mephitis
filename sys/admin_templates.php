<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

include(include_file('template', 'admin_nav.php'));
include(include_file('template', 'admin_templates.php'));
include(include_file('template', 'admin_content.php'));
include(include_file('template', 'admin_emojis.php'));
include(include_file('template', 'admin_ips.php'));
include(include_file('template', 'admin_sessions.php'));
include(include_file('template', 'admin_email.php'));
include(include_file('template', 'admin_user.php'));
include(include_file('template', 'admin_files.php'));
include(include_file('template', 'admin_fce.php'));
include(include_file('template', 'admin_updater.php'));
include(include_file('template', 'admin_modules.php'));
include(include_file('template', 'admin_db.php'));


?>