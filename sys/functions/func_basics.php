<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function simple_post($var){
	
	$var = nl2br($var);
	
	$var = str_replace('@', '&#64;', $var); // email crypt
	return ($var);
}

function simple_post_form($var){

	$var = str_replace('<br />', '', $var);
	return ($var);
}

function autolink($var){

	$var = '<a target="_blank" href="http://'.$var.'">'.$var.'</a>';

	return ($var);
}

function automaillink($var){

	$var = '<a href="mailto:'.$var.'">'.$var.'</a>';

	return ($var);
}

function mailcheck($var){
	
	// Remove all illegal characters from email
	//$var = filter_var($var, FILTER_SANITIZE_EMAIL);

	// Validate e-mail
	if (!filter_var($var, FILTER_VALIDATE_EMAIL) === false) {
		return 1;
	} else {
		return 0;
	}

}

// got from http://php.net/manual/de/function.bbcode-create.php by user comment (but with some changes)
function bb_parse($string)
{
	$tags = 'b|u|i|size|color|left|right|justify|center|quote|url|img|youtube|vimeo|soundcloud';
	while (preg_match_all('`\[('.$tags.')=?(.*?)\](.+?)\[/\1\]`', $string, $matches))
	foreach ($matches[0] as $key => $match)
	{	
		$tag		= $matches[1][$key];
		$param		= $matches[2][$key];
		$innertext	= $matches[3][$key];
		
		switch ($tag)
		{
			case 'b':
				$replacement = '<b>'.$innertext.'</b>';
				break;
			case 'i':
				$replacement = '<i>'.$innertext.'</i>';
				break;
			case 'u':
				$replacement = '<u>'.$innertext.'</u>';
				break;				
			case 'size':
				$replacement = '<span style="font-size: '.$param.';">'.$innertext.'</span>';
				break;
			case 'color':
				$replacement = '<span style="color: '.$param.';">'.$innertext.'</span>';
				break;
			case 'center':
				$replacement = '<p style="text-align: center;">'.$innertext.'</p>';
				break;
			case 'left':
				$replacement = '<p style="text-align: left;">'.$innertext.'</p>';
				break;
			case 'right':
				$replacement = '<p style="text-align: right;">'.$innertext.'</p>';
				break;
			case 'justify':
				$replacement = '<p style="text-align: justify;">'.$innertext.'</p>';
				break;				
			case 'quote':
				$replacement = '<blockquote>'.$innertext.'</blockquote>' . $param? '<cite>'.$param.'</cite>' : '';
				break;
			case 'url':
				$replacement = '<a href="' . ($param? $param : $innertext) .'" target="_BLANK">'.$innertext.'</a>';
				break;
			case 'img':
				$replacement = '<img src="'.$innertext.'" />';
				break;
			case 'youtube':
				
				$videourl = parse_url($innertext);
				$videoid = '';
				if (isset($videourl['host']) && strpos($videourl['host'], 'youtube.com') !== FALSE)
				{
					parse_str($videourl['query'], $videoquery);
					$videoid = $videoquery['v'];
				}
				else
				{
					if(strpos($innertext, '/') === FALSE)
					{
						$videoid = explode('&', $innertext);
						$videoid = $videoid[0];
					}
				}
				
				if(isset($videoid) && $videoid != '')
				{
					$replacement = '
							<iframe class="embed-responsive-item" id="ytplayer" width="425" height="344" frameborder="0" type="text/html" src="https://www.youtube.com/embed/' . $videoid . '"></iframe>
					';					
				}
				
				break;
			case 'vimeo':
				
				$videourl = parse_url($innertext);
				
				$videoid = '';
				if (isset($videourl['host']) && strpos($videourl['host'], 'vimeo.com') !== FALSE)
				{
					$videourl = explode('/', $videourl['path']);
					if(isset($videourl[1]))
					{
						$videoid = $videourl[1];
					}
				}
				else
				{
					if(strpos($innertext, '/') === FALSE)
					{
						$videoid = explode('&', $innertext);
						$videoid = $videoid[0];
					}
				}
				
				if(isset($videoid) && $videoid != '')
				{
					$replacement = '
						<iframe class="embed-responsive-item" src="//player.vimeo.com/video/'.$videoid.'" width="425" height="344" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					';					
				}
				
				break;
			case 'soundcloud':
				
				$replacement = '
					<iframe class="embed-responsive-item" width="400" height="150" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url='.str_replace('&', '&amp;', $innertext).'&amp;auto_play=false&amp;visual=true"></iframe>
				';
				
				break;				
		}
		$string = str_replace($match, $replacement, $string);
	}
	return $string;
}

function mk_post($text, $bbcode_on = 1, $emojis_on = 1){

	global $config, $mysqli;

	// bbcode
	
	if($bbcode_on == '1'){
		/*
		$bbcode = array(
			'[b]' => '<b>',
			'[/b]' => '</b>',

			'[u]' => '<u>',
			'[/u]' => '</u>',

			'[i]' => '<i>',
			'[/i]' => '</i>'
		);
		
		$text = strtr($text, $bbcode);
		*/
		$text = str_replace("\r\n", '', $text);
		$text = str_replace("\n", '', $text);
		
		$text = bb_parse($text);
		
	}

	//emojis

	if($emojis_on == '1'){
		$emojis = array();
		$query = 'SELECT * FROM '.$config['prefix'].'emojis';
		$result = $mysqli->query($query);
		while($row = $result->fetch_assoc()){
			$emojis[$row['code']] = show_emoji($row['id'], 'mk_emoji');
		}

		$text = strtr($text, $emojis);
	}

	return $text;
	
}

function cms_link(){
	
	$link = '';

	if(isset($_GET['site'])){
		$link .= 'site='.$_GET['site'];
	}

	if(isset($_GET['show']) AND $_GET['show'] != ''){
		$link .= 'show='.$_GET['show'];
	}
	
	return $link;

}

function emoji_list($num){

	global $config, $mysqli;
	
	if($num != 'all' AND is_numeric($num)){
		$search = ' LIMIT '.$num.'';
	}else if($num == 'all'){
		$search = '';
	}else{
		$search = ' LIMIT '.getvar('num_emojis').'';
	}
	
	$emojis = array();
	$result = $mysqli->query('SELECT * FROM '.$config['prefix'].'emojis ORDER BY rank'.$search.'');
	while($row = $result->fetch_assoc()){
		$emojis[$row['code']] = show_emoji($row['id'], 'mk_emoji');
	}
	
	return $emojis;

}

function emoji_list_raw($num){

	global $config, $mysqli;
	
	if($num != 'all' AND is_numeric($num)){
		$search = ' LIMIT '.$num.'';
	}else if($num == 'all'){
		$search = '';
	}else{
		$search = ' LIMIT '.getvar('num_emojis').'';
	}
	
	$emojis = array();
	$result = $mysqli->query('SELECT * FROM '.$config['prefix'].'emojis ORDER BY rank'.$search.'');
	while($row = $result->fetch_assoc()){
		$emojis[] = $row;
	}
	
	return $emojis;

}

function getvar($var){

	global $config, $mysqli;
	
	if(!isset($config['settings']))
	{
		$config['settings'] = array();
	}
	
	if(!isset($config['settings'][$var]))
	{	
		$query = 'SELECT * FROM '.$config['prefix'].'settings WHERE variable = "'.$var.'"';
		$result = $mysqli->query($query);
		$row = $result->fetch_assoc();
		
		$config['settings'][$var] = $row['value'];
	}

	return $config['settings'][$var];
}

function StrPlaceholder($txt, $array)
{
	$text = $txt;
	
	foreach($array as $key => $value)
	{
		$text = str_replace('{'.$key.'}', $value, $text);
	}
	
	return $text;
}

function mysql_perm_search($mode = 'nav'){
	
	global $config, $user;
	
	if($mode == 'nav'){
		$serachadd = ' AND hidden = "0"';
	}else{
		$serachadd = '';
	}
	
	if($user['id'] == '0'){
		$mysql_search = ' AND online = "1"'; 
	}else{
		$mysql_search = ' AND online = "1"';
		
		// normal user or mods
		if($user['perm'] != 'admin'){
			$mysql_search = ' AND online = "1"'.$serachadd.'';
		}
		
		//admins
		if($user['perm'] == 'admin'){
			$mysql_search = '';
		}
	}
	
	return $mysql_search;

}

function random_string($len = 10, $pool_user = ''){

	if($pool_user == ''){
		$pool = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
	}else{
		$pool = $pool_user;
	}

	$pool_size = strlen($pool);
	$string = '';
	for($i=1;$i<=$len;$i++){
		$string .= $pool[mt_rand(0,($pool_size-1))];
	}
	return $string;
}

function site_info($id){

	global $config, $mysqli;
	
	$query = 'SELECT id, name, dir, shortlink FROM '.$config['prefix'].'sites WHERE id = "'.$mysqli->real_escape_string($id).'"';
	$result = $mysqli->query($query);
	$row = $result->fetch_assoc();	
	
	return $row;

}

function dir_info($id){

	global $config, $mysqli;
	$result = $mysqli->query('SELECT * FROM '.$config['prefix'].'dirs WHERE id = "'.$mysqli->real_escape_string($id).'"');
	$row = $result->fetch_assoc();
	return $row;

}

function include_file($type, $file, $module = ''){

	global $config, $lang, $user, $modulename, $phpRootPath;

	if($type == 'template'){
		if(isset($config['selected_template']) AND $config['selected_template'] != '' AND file_exists($phpRootPath.'templates/'.$config['selected_template'].'/'.$file)){
			return $phpRootPath.'templates/'.$config['selected_template'].'/'.$file;
		}else if(file_exists($phpRootPath.'templates/'.$config['template'].'/'.$file)){
			return $phpRootPath.'templates/'.$config['template'].'/'.$file;
		}else if(file_exists($phpRootPath.'templates/'.$config['default_template'].'/'.$file)){
			return $phpRootPath.'templates/'.$config['default_template'].'/'.$file;
		}else if(file_exists($phpRootPath.'modules/'.$modulename.'/templates/'.$file)){
			return $phpRootPath.'modules/'.$modulename.'/templates/'.$file;
		}else if(file_exists($phpRootPath.'modules/'.$module.'/templates/'.$file)){
			return $phpRootPath.'modules/'.$module.'/templates/'.$file;
		}else{
			die(draw_error($lang['error'], $lang['template_error']));
		}
	}else if($type == 'lang'){
		if(file_exists($phpRootPath.'lang/'.$file.'.'.$user['lang'].'.php')){
			return $phpRootPath.'lang/'.$file.'.'.$user['lang'].'.php';
		}else{
			die($lang['error'].'<br>'.$lang['lang_error']);
		}
	}else if($type == 'module_main'){
		if(file_exists($phpRootPath.'modules/'.$file.'/mod_main.php')){
			return $phpRootPath.'modules/'.$file.'/mod_main.php';
		}else{
			$msg = $lang['module_error'];
			die(draw_error($lang['error'], $msg));
		}	
	}
}

function module_link($modulename, $instance = '', $link = false){
	
	global $config, $mysqli, $user;

	$queryadd = '';

	$query = 'SELECT * FROM '.$config['prefix'].'content WHERE fce = "module_'.$mysqli->real_escape_string($modulename).'_main" OR content LIKE "%<module>'.$modulename.'</module>%" ORDER BY rank DESC';
	$result = $mysqli->query($query);
	
	while($content = $result->fetch_assoc())
	{
		$row = $content;
		
		if(trim($content['content']) != '')
		{
			if($instance != '')
			{
				@$content['content'] = simplexml_load_string($content['content']);
				if(isset($content['content']->instance) && (string)$instance == (string)$content['content']->instance)
				{
					break;
				}
			}
		}
	}
	
		
	

	
	if(
		is_array($row) 
		&& 
		(
			(
				$row['online'] == '1'
				&&
				check_group_access($row['groups'], $row['groups_mode'])
			)
			||
			(
				$user['perm'] == 'admin'
			)
		)
	)
	{
		
		
		$page = GetPage($row['page']);
		$page['kind'] = 'site';
		$page['contentElement'] = $row;
		if($link)
		{
			
			$link = '';
			if(trim($page['shortlink']) != '')
			{
				$link = 'show='.trim($row['shortlink']).'';
			}
			else
			{
				$link_id = $page['contentElement']['page'];
				$link_kind = $page['kind'];
				$link = ''.$link_kind.'='.$link_id .'';
			}
			return $link;
		}
		
		return $page;
	}
	else
	{
		return null;
	}
}

function get_parent_template($rootline){

	if(is_array($rootline))
	{
		$path = array_reverse($rootline);
		
		foreach($path as $path_element){
			if($path_element['template'] != '' AND $path_element['template'] != '0'){
				return $path_element['template'];
			}
		}
	}
	return getvar('template_dir');
}

function availablelangs($path = 'lang/lang_main'){

	global $phpRootPath;
	
	if($path == 'lang/lang_main')
	{
		$path = $phpRootPath.$path;
	}
	
	$mainlangs = glob($path.'.*');
	
	$langsarray = array();
	
	foreach($mainlangs as $mainlang){
		$mainlang		= explode($path.'.', $mainlang);
		$mainlang		= explode('.', $mainlang[1]);
		$langsarray[]	= $mainlang[0];
	}

	return $langsarray;

}

function order_items($tab, $rank, $dir, $dir_id, $dir_meta_tab, $dir_meta_info, $mode, $addwhere = '', $addfields = ''){

	global $config, $mysqli, $lang;
	
	if($mode == 'do_order'){

		if(isset($_GET['itemid']) AND isset($_GET['order'])){
		
			$result = $mysqli->query('SELECT id, '.$dir.', '.$rank.' FROM '.$config['prefix'].''.$tab.' WHERE id = "'.$mysqli->real_escape_string($_GET['itemid']).'"');
			$row = $result->fetch_assoc()or die(draw_error($lang['mysql_error'], $mysqli->error));;
			
			if($_GET['order'] == 'up'){
				$result_up = $mysqli->query('SELECT id, '.$dir.', '.$rank.' FROM '.$config['prefix'].''.$tab.' WHERE '.$dir.' = "'.$row[$dir].'" '.$addwhere.' AND '.$rank.' < "'.$row[$rank].'" ORDER BY '.$dir.', '.$rank.' DESC LIMIT 1');
				$row_up = $result_up->fetch_assoc();

				if(is_array($row_up)){
					
					// down lvl
					$new = 'UPDATE '.$config['prefix'].''.$tab.' Set
					'.$rank.' = "'.$row[$rank].'"
					WHERE id = "'.$row_up['id'].'"';
					$update = $mysqli->query($new)or die(draw_error($lang['mysql_error'], $mysqli->error));
					
					// up lvl
					$new = 'UPDATE '.$config['prefix'].''.$tab.' Set
					'.$rank.' = "'.$row_up[$rank].'"
					WHERE id = "'.$row['id'].'"';
					$update = $mysqli->query($new)or die(draw_error($lang['mysql_error'], $mysqli->error));

				}else{
					//die(draw_error($lang['error'], $lang['logic_error']));
				}
				
			
			}else if($_GET['order'] == 'down'){
				$result_down = $mysqli->query('SELECT id, '.$dir.', '.$rank.' FROM '.$config['prefix'].''.$tab.' WHERE '.$dir.' = "'.$row[$dir].'" '.$addwhere.' AND '.$rank.' > "'.$row[$rank].'" ORDER BY '.$dir.', '.$rank.' LIMIT 1');
				$row_down = $result_down->fetch_assoc();;
				if(is_array($row_down)){
					
					// down lvl
					$new = 'UPDATE '.$config['prefix'].''.$tab.' Set
					'.$rank.' = "'.$row[$rank].'"
					WHERE id = "'.$row_down['id'].'"';
					$update = $mysqli->query($new)or die(draw_error($lang['mysql_error'], $mysqli->error));
					
					// up lvl
					$new = 'UPDATE '.$config['prefix'].''.$tab.' Set
					'.$rank.' = "'.$row_down[$rank].'"
					WHERE id = "'.$row['id'].'"';
					$update = $mysqli->query($new)or die(draw_error($lang['mysql_error'], $mysqli->error));

				}else{
					//die(draw_error($lang['error'], $lang['logic_error']));
				}
			}
			
		}
	}else if($mode == 'mk_list'){

		$list = '';
		$result = $mysqli->query('SELECT id, '.$dir.', '.$rank.', name '.$addfields.' FROM '.$config['prefix'].''.$tab.' WHERE id != 0 '.$addwhere.' ORDER BY '.$dir.', '.$rank.'')or die(draw_error($lang['mysql_error'], $mysqli->error));
		while($row = $result->fetch_assoc()){

			$result_up = $mysqli->query('SELECT id, '.$dir.', '.$rank.' FROM '.$config['prefix'].''.$tab.' WHERE '.$dir.' = "'.$row[$dir].'" '.$addwhere.' AND rank < "'.$row['rank'].'" ORDER BY '.$dir.', '.$rank.' DESC LIMIT 1')or die(draw_error($lang['mysql_error'], $mysqli->error));
			$row_up = $result_up->fetch_assoc();
			if(is_array($row_up)){
				$row['uplink'] = '1';
			}else{
				$row['uplink'] = '';
			}

			$result_down = $mysqli->query('SELECT id, '.$dir.', '.$rank.' FROM '.$config['prefix'].''.$tab.' WHERE '.$dir.' = "'.$row[$dir].'" '.$addwhere.' AND rank > "'.$row['rank'].'" ORDER BY '.$dir.', '.$rank.' LIMIT 1')or die(draw_error($lang['mysql_error'], $mysqli->error));
			$row_down = $result_down->fetch_assoc();
			if(is_array($row_down)){
				$row['downlink'] = '1';
			}else{
				$row['downlink'] = '';
			}

			$result_dir = $mysqli->query('SELECT id, '.$dir_meta_info.' FROM '.$config['prefix'].''.$dir_meta_tab.' WHERE id = "'.$row[$dir].'"')or die(draw_error($lang['mysql_error'], $mysqli->error));
			$row_dir = $result_dir->fetch_assoc();
			$row[$dir] = $row_dir;
			$list[] = $row;

		}
		return $list;
		//return draw_orderlist($list, $link, $header);

	}else if($mode == 'count'){
		$result = $mysqli->query('SELECT id, '.$dir.', '.$rank.' FROM '.$config['prefix'].''.$tab.' WHERE '.$dir.' = "'.$mysqli->real_escape_string($dir_id).'" '.$addwhere.' ')or die(draw_error($lang['mysql_error'], $mysqli->error));
		$row = $result->num_rows;
		return $row;
	}

}

function order_item_after($tab, $rank, $parent_id_field, $parent_id, $new_entry_id, $after_id, $parent_kind_field = '', $parent_kind = ''){

	global $config, $mysqli, $lang;
	
	if($parent_kind_field != '' AND $parent_kind != ''){
		$result = $mysqli->query('SELECT id, '.$rank.', '.$parent_kind_field.', '.$parent_id_field.' FROM '.$config['prefix'].''.$tab.' WHERE '.$parent_kind_field.' = "'.$parent_kind.'" AND '.$parent_id_field.' = "'.$parent_id.'" ORDER BY '.$rank.' ASC') or die(draw_error($lang['mysql_error'], $mysqli->error));
	}else{
		$result = $mysqli->query('SELECT id, '.$rank.', '.$parent_id_field.' FROM '.$config['prefix'].''.$tab.' WHERE '.$parent_id_field.' = "'.$parent_id.'" ORDER BY '.$rank.' ASC') or die(draw_error($lang['mysql_error'], $mysqli->error));
	}
	
	
	$saverank = 0;	
	$rank_new = 0;
	
	while($row = $result->fetch_assoc()){
		
		$rank_new++;
		$row[$rank] = $rank_new;

		if($after_id == 0)
		{
			$row[$rank] += 1;
			if($new_entry_id == $row['id'])
			{
				$row[$rank] = 1;
			}
		}
		else
		{
			if($after_id == $row['id'])
			{
				$saverank = $row[$rank]+1;
				$rank_new++;
			}
			else if($new_entry_id == $row['id'])
			{
				$row[$rank] = $saverank;
			}
			else
			{
				
			}
		}
		
		$new = 'UPDATE '.$config['prefix'].''.$tab.' SET 
		'.$rank.' = "'.$row[$rank].'"
		WHERE id = "'.$row['id'].'"';
		$update = $mysqli->query($new) or die(draw_error($lang['mysql_error'], $mysqli->error));
		
	}

}


function pages($mode, $table, $search, $link, $varname, $per_page = 0, $prefix = TRUE){

	global $config, $mysqli;

	if($per_page == 0)
	$per_page = getvar('per_page');

	if($mode == 'draw'){
		
		if($search != ''){
			$search = ' WHERE '.$search;
		}
		
		if($prefix != TRUE){
			$result = $mysqli->query('SELECT id FROM '.$table.''.$search.''); 
			
		}else{
			$result = $mysqli->query('SELECT id FROM '.$config['prefix'].''.$table.''.$search.''); 
		}

		$num_rows = $result->num_rows;
		
		$i = 0;
		$pages = '';
		$count2 = $num_rows-$per_page;
		
		if($count2 <= 0){
			$pages = '';
		}else{
			$count = 0;
			while($count < $count2){
				$count = $per_page*$i++;
				$pages[] = $i;
			}
		}
		
		$echo = draw_pages($pages, $num_rows, $i, $per_page, $link, $varname);
		return $echo;
	}
	
	if($mode == 'limits'){
		if((isset($_GET[$varname]) AND is_numeric($_GET[$varname]) AND $_GET[$varname] > 0) OR (isset($_POST[$varname]) AND is_numeric($_POST[$varname]) AND $_POST[$varname] > 0)){
			if(isset($_GET[$varname])){
				$var = $_GET[$varname];
			}else if(isset($_POST[$varname])){
				$var = $_POST[$varname];
			}
			$limits = ' LIMIT '.$mysqli->real_escape_string((($var-1)*$per_page)).','.$per_page.'';
		}else{
			$limits = ' LIMIT '.$per_page.'';
		}
		return $limits;
	}
}

function lastget($get){
	
	if(isset($_SERVER['HTTP_REFERER'])){
		$last_get = explode('&'.$get.'=', $_SERVER['HTTP_REFERER']);
		if(isset($last_get['1'])){
			$last_get = explode('&', $last_get['1']);
			$last_get = $last_get['0'];
		}else{
			$last_get = '0';
		}
	}else{
		$last_get = '0';
	}
	return $mysqli->real_escape_string($last_get);

}

function mailer($to, $subject, $message, $from_name, $from_email, $mode = 'text'){
	
	//use:  mailer('foo@afoo.de', 'foo', $message, 'foo2', 'foo2@foo2.de', 'html');
	
	if(trim($to) == '')
	{
		return 0;
	}
	
	if(getvar('email_smtp') == '1')
	{
		
		$mail = new PHPMailer();

		$mail->isSMTP();
		
		$mail->Host = getvar('email_smtp_host');
		$mail->SMTPAuth = getvar('email_smtp_auth');
		$mail->Username = getvar('email_smtp_user');
		$mail->Password = getvar('email_smtp_pass');
		$mail->SMTPSecure = getvar('email_smtp_secure');
		$mail->Port = getvar('email_smtp_port');
		
		$mail->CharSet = 'utf-8';
		$mail->setFrom($from_email, $from_name);
		$mail->addAddress($to);
		$mail->Subject = $subject;
		
		if(is_array($message))
		{
			$mail->Body    = $message['body'];
			$mail->AltBody = $message['altbody'];
		}
		else
		{
			$mail->Body    = $message;
		}
		
		if($mode == 'html')
		{
			$mail->isHTML(true);	
		}
		

		if(!$mail->send()) {
			return 0;
		} else {
			return 1;
		}
		
	}
	else
	{
		if(is_array($message))
		{
			$message = $message['body'];
		}

		$header = 'from: '.$from_name.' <'.$from_email.'>'."\n";
		if($mode == 'html')
		{
			$header .= 'content-type: text/html; charset=utf-8'."\n";
		}
		else
		{
			$header .= 'content-type: text/plain; charset=utf-8'."\n";
		}
		
		if(mail($to, $subject, $message, $header)){
			return 1;
		}else{
			return 0;
		}
	}
}

function tag_attribute_rewrite($tag, $attribute, $content){
	
	$echo = '';
	
	$content_edit = explode('<'.$tag, $content);

	if(isset($content_edit['1'])){
		$echo .= $content_edit['0'];
		foreach($content_edit as $key => $value){
			if($key != '0'){
				$value = explode('>', $value, 2);
				$data = $value['0'];
				$tail = $value['1'];
				
				$data_edit 	= explode($attribute.'="', $data, 2); 
				if(isset($data_edit['1'])){
					$attribute_head 	= $data_edit['0'];
					$data_edit 		= explode('"', $data_edit['1'], 2); 
					$attribute_tail 	= $data_edit['1'];
					$attribute_data		= $data_edit['0'];
					
					$attribute_data	= rewrite_rule($attribute_data); //to rewrite
					
					$link 			= '<'.$tag.$attribute_head.$attribute.'="'.$attribute_data.'"'.$attribute_tail.'>';
				}else{
					$link = '<'.$tag.$data.'>';
				}
				$echo .= $link.$tail;
			}
		
		}
		
	}else{
		$echo = $content;
	}
	
	return $echo;
}

function mk_ref($maxlen = 6){

	$crypt = md5($_SERVER['REMOTE_ADDR'].time().microtime());
	$strlen = strlen($crypt);
	
	$min = 1;
	$max = $strlen-$maxlen;
	
	$start = rand($min, $max);
	
	$ref = substr($crypt, $start, $maxlen);
	
	return $ref;
	
}

function rewrite($content){
	
	$content = tag_attribute_rewrite('a', 'href', $content);
	$content = tag_attribute_rewrite('form', 'action', $content);
	return $content;

}

function get_label($node, $parent = null){

	global $lang, $modulename;
	
	if(is_object($node)){
		$attr = $node->attributes();
		
		if(is_object($attr['label'])){
			if(isset($lang[(string)$attr['label']])){
				$echo = $lang[(string)$attr['label']];
			}else{
				$echo = (string)$node;
			}
		}else{
			$echo = (string)$node;
		}
		
	}else if($parent != null && !is_numeric($parent)){
		if(isset($lang[$parent][$node])){
			$echo = $lang[$parent][$node];
		}else if(isset($lang[$node])){
			$echo = $lang[$node];
		}else{
			$echo = $parent.'->'.$node;
		}		
	}else{
		if(isset($lang[$node][$node])){
			$echo = $lang[$node][$node];
		}else if(isset($lang[$modulename][$node])){
			$echo = $lang[$modulename][$node];
		}else if(isset($lang[$node])){
			$echo = $lang[$node];
		}else{
			$echo = $node;
		}
	}
	if(is_array($echo))
	{
		$echo = $node;
	}
	
	return $echo;

}

function get_installed_modules()
{
	global $config, $mysqli;
	
	$installedModules = array();
	
	$result = $mysqli->query('SELECT * FROM '.$config['prefix'].'modules');
	while($row = $result->fetch_assoc())
	{
		$installedModules[$row['name']] = $row;
	}
	$result->close();
	
	return $installedModules;
}

function str2array($str, $cut_l1, $cut_r1, $cut_l2, $cut_r2){

	// a(1:test1;test2;)b(2:test1;test2;)   ->>>>> array
	
	$echo = '';
	$sub_posts = explode($cut_r1, $str);

	if(isset($sub_posts['1'])){
		foreach($sub_posts as $sub){
			
			$sub = explode($cut_l1, $sub);
			
			if(isset($sub[0]) AND $sub[0] != ''){
				$data = $sub['1'];
				
				$sub_posts2 = explode($cut_r2, $data);
				
				if(isset($sub_posts['1'])){
					$echo[$mysqli->real_escape_string($sub[0])] =  str2array($data, $cut_l2, $cut_r2, $cut_l1, $cut_r1);
				}else{
					$echo[$mysqli->real_escape_string($sub[0])] = $mysqli->real_escape_string($data);
				}
			}
			
			
		}
		
	}else{
		$echo = $mysqli->real_escape_string($str);
	}


	return $echo;

}

function build_lib($lib)
{
	$echo = '';
	
	if(is_array($lib))
	{
		foreach($lib as $item)
		{
			if(is_array($item))
			{
				build_lib($item);
			}
			else
			{
				$echo .= $item;
			}
		}
	}
	else
	{
		$echo .= $lib;
	}
	
	
	
	return $echo;
}

function load_tpl_vars()
{
	global $mysqli, $config, $tpl_vars, $default_tpl_vars;
	
	$query = '
		SELECT
			*
		FROM
			'.$config['prefix'].'templates
		WHERE
			name = "'.$config['template'].'"
	';
	
	$result = $mysqli->query($query);
	$row = $result->fetch_assoc();
	if(is_array($row))
	{
		$default_tpl_vars = $tpl_vars;
	}
	$tpl_vars = json_decode($row['vars'], true);
	
}

function load_renderfunctions()
{
	global $renderfunctions, $phpRootPath;
	
	if(!isset($renderfunctions))
	{
		$renderfunctions = array();
	}
	
	$path = $phpRootPath.'templates/';
	$dir = opendir($path); 
	while($file = readdir($dir)) {
		if($file != '.' && $file != '..' && substr_count($file, '.') == 0) {
			if(file_exists($path.$file.'/renderfunctions.php'))
			{
				include($path.$file.'/renderfunctions.php');
				$renderfunctions[$file] = $tpl_renderfunctions;
			}
		}
	}
	closedir($dir);	
	
}

function siteLink($mode = '')
{
	
	$link = '';
	
	$link = getvar('domain').getvar('httpRootPath').'index.php';
	
	$uri = '';
	
	if($mode == 'path')
	{
		$link = getvar('domain').getvar('httpRootPath');
	}
	
	if($mode == 'domain')
	{
		$link = getvar('domain');
	}
	
	$uri .= getvar('http').$link;
	
	return $uri;
}

function CacheClear()
{
	global $phpRootPath;
	$files = glob($phpRootPath.'cache/*');
	foreach($files as $file)
	{
		if(is_file($file))
		{
			unlink($file);
		}
	}
	
	file_put_contents($phpRootPath.'cache/index.html', '');
}

function MkAbsPaths($html)
{
	$doc = new DOMDocument();
	@$doc->loadHTML($html);
	$selector = new DOMXPath($doc);

	$result = $selector->query('//a/@href | //@src');
	
	foreach($result as $link)
	{
		if(!(strpos($link->value, '://') !== false))
		{
			$link->value = str_replace('&' ,'&amp;' ,siteLink('domain').$link->value);
		}
	}
	$body = $doc->getElementsByTagName('body');
	$body = $body->item(0);
	
	$body = $doc->savehtml($body);
	
	$body = str_replace(array('<body>', '</body>'), '', $body);
	
	
	return $body;
}

function user_function($func, $data = '', $args = '')
{
	$func = trim($func);
	$args = trim($args);
	
	if($func != '')
	{	
		if(function_exists($func))
		{
			if($args != '')
			{
				return $func($data, $args);
			}
			else
			{
				return $func($data);
			}
		}
	}
	else
	{
		return false;
	}
}

function array2xml($array, $xml = false, $root = 'root'){
    if($xml === false){
        $xml = new SimpleXMLElement('<'.$root.'/>');
    }
    foreach($array as $key => $value){
		
        if(is_array($value)){
			if($key == '@attributes'){
				foreach($value as $attr_key => $attr_value){
					$xml->addAttribute($attr_key,  $attr_value);
				}
			}else{
				array2xml($value, $xml->addChild($key));
			}
        }else{
            $xml->addChild($key, $value);
        }
    }
    return $xml->asXML();
}

function ajax2php($post){
	
	$echo = array();
	
	if(is_array($post))foreach($post as $key => $data){
		$echo[$key] = str2array($data, '(', ')', ':', ';');
	}
	
	return $echo;
	
}

function CheckCString($string, $needle)
{
	$strings = explode(',', $string);
	for($i = 0; $i < count($strings); $i++)
	{
		if(trim($strings[$i]) == trim($needle))
		{
			return true;
		}
	}
	return false;
}

function StartsWith($haystack, $needle) {
	return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
}

function debug($debug){

	echo '<div style="text-align: left;"><pre>';
	var_dump($debug);
	echo '</pre></div>';

}

?>