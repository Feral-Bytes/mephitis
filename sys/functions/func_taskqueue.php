<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function TaskLog($msg, $depth = 0, $newLine = false)
{
	global $logfile, $user;
	
	if(!isset($logfile) || $logfile == '')
	{
		return;
	}
	
	$lineEnding = "\r\n";
	
	if($newLine)
	{
		$lineEnding = "\r\n\r\n";
	}
	
	$tabs = '';
	for($i = 0; $i < $depth; $i++)
	{
		$tabs .= "\t";
	}
	
	$msg = date("Y-m-d, H:i:s", time())."\t ".$tabs.$msg.$lineEnding;
	
	if(isset($user['perm']) && $user['perm'] == 'admin')
	{
		echo '<pre>';
		echo $msg.'<br>';
		echo '</pre>';
	}
	
	fputs($logfile, $msg);
}

function Task_FixDB($arg, $tab = 1)
{
	global $mysqli, $config, $database, $phpRootPath;
	
	TaskLog('Cecking Database ...', $tab);
	
	$databaseSave = $database;
	$database = array();
	include($phpRootPath.'sys/db.php');
	$database = array_replace_recursive($database, $databaseSave);
	
	$db = new dbcheck($config, $database);
	$db->load_db();
	$db->check();
	
	if(is_array($db->errors))
	{
		TaskLog('found Erros, trying to fix ...', $tab);
		$db->repair();
		$db->load_db();
		$db->check();
		TaskLog('Database fixing done', $tab);
	}
	else
	{
		TaskLog('Database OK', $tab);
	}
	
}

function Task_BackupDB($arg, $tab = 1)
{
	global $mysqli, $config, $database, $phpRootPath;
	
	TaskLog('Loading Database for Backup ...', $tab);
	
	$databaseSave = $database;
	$database = array();
	include($phpRootPath.'sys/db.php');
	$database = array_replace_recursive($database, $databaseSave);
	
	$db = new dbcheck($config, $database);
	$db->load_db();
	
	$sql = $db->backup(array(
		'breakat' => '100',
		'drop_table_if_exists' => '1'
	));
	
	TaskLog('Writing Backupfile ...', $tab);
	
	$filename = 'db-'.date('Y-m-d-H-i-s').'.sql';
	$path = 'files/backups/';
	
	if(!is_dir($phpRootPath.$path))
	{
		mkdir($phpRootPath.$path);
	}
	
	$file = fopen($phpRootPath.$path.$filename, 'w');
	fwrite($file, $sql);
	fclose($file);	
	
	TaskLog('Backupfile: "'.$phpRootPath.$path.$filename.'" done', $tab);
	
}

function Task_FixBackupDB($arg, $tab = 1)
{
	TaskLog('Fix Database ...', $tab);
	Task_FixDB($arg, $tab+1);
	
	TaskLog('Backup Database ...', $tab);
	Task_BackupDB($arg, $tab+1);
}

function Task_MailQueue($arg, $tab = 1)
{
	global $mysqli, $config;
	
	if(getvar('email_smtp') == '1')
	{
		$mail = new PHPMailer();
		$mail->isSMTP();
		$mail->Host = getvar('email_smtp_host');
		$mail->SMTPAuth = getvar('email_smtp_auth');
		$mail->Username = getvar('email_smtp_user');
		$mail->Password = getvar('email_smtp_pass');
		$mail->SMTPSecure = getvar('email_smtp_secure');
		$mail->SMTPKeepAlive = true; 
		$mail->Port = getvar('email_smtp_port');
	}
	
	TaskLog('Sarting Mailqueue ...', $tab);
	
	$query = '
		SELECT
			*
		FROM
			'.$config['prefix'].'mailqueue
		WHERE
			issent = "0"
		ORDER BY
			id ASC
	';
	$result = $mysqli->query($query);
	
	$i = 0;
	while($row = $result->fetch_assoc())
	{
		
		if(getvar('email_smtp') == '1')
		{
			$mail->CharSet = 'utf-8';
			$mail->setFrom($row['from_email'], $row['from_name']);
			$mail->addAddress($row['recipient_email'], $row['recipient_name']);
			$mail->Subject = $row['subject'];
			$mail->Body    = $row['body'];

			if($row['ishtml'])
			{
				$mail->isHTML(true);
				$mail->AltBody = $row['altbody'];
			}
			
			if($mail->send())
			{
				$i++;
				db_update($config['prefix'].'mailqueue', array('issent' => '1'), $row['id']);
			}
			
			$mail->clearAddresses();
			$mail->clearAttachments();
		}
		else
		{
			$mode = 'text';
			if($row['ishtml'])
			{
				$mode = 'html';
			}
			
			if(mailer($row['recipient_email'], $row['subject'], $row['body'], $row['from_name'], $row['from_email'], $mode))
			{
				$i++;
				db_update($config['prefix'].'mailqueue', array('issent' => '1'), $row['id']);
			}
		}
	}
	TaskLog(''.$i.' mails has been sent ...', $tab);
	TaskLog('Cleaning Mailqueue ...', $tab);
	
	$query = '
		DELETE FROM '.$config['prefix'].'mailqueue
		WHERE
			issent = "1"
	';
	$mysqli->query($query);
	
	$query = '
		SELECT
			id
		FROM
			'.$config['prefix'].'mailqueue
		WHERE
			issent = "0"
		ORDER BY
			id ASC
	';
	$result = $mysqli->query($query);
	
	if($result->num_rows < 1)
	{
		$mysqli->query('TRUNCATE '.$config['prefix'].'mailqueue');
	}
	
}

function Task_ClearCache($arg, $tab = 1)
{
	global $mysqli, $config, $database, $phpRootPath;
	
	TaskLog('Clearing Cache ...', $tab);
	
	CacheClear();
	
}

?>