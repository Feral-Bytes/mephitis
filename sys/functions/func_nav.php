<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function nav_conf($root, $deep, $mode){
	
	global $config;
	
	$nav_conf['dir_table'] 		= $config['prefix'].'dirs';
	$nav_conf['sites_table']	= $config['prefix'].'sites';
	$nav_conf['rank']		= 'rank';
	$nav_conf['root']		= $root;
	$nav_conf['deep']		= $deep;
	$nav_conf['mode']		= $mode;
	$nav_conf['search']		= mysql_perm_search('nav');
	$nav_conf['search_site']	= mysql_perm_search('site');
	
	return $nav_conf;
	
}

function user_tools(){
	
	global $links, $user, $lang, $link, $phproot, $pms, $installedModules, $modules, $phpRootPath, $httpRootPath;

	
	if(is_array($user) AND isset($user['id']) AND $user['id'] != '0'){

		$links['usercp_nav'] = array(
			'logout' => array(
				'icon'		=> 'fa fa-sign-out',
				'lang'		=> $lang['logout'],
				'module'	=> 'usercp',
				'phproot'	=> $phproot
			),
			'user' => array(
				'icon'		=> 'fa fa-user',
				'lang'		=> $lang['user_account'],
				'module'	=> 'usercp',
				'phproot'	=> $phproot
			),
			'inbox' => array(
				'icon'		=> 'fa fa-envelope-o',
				'lang'		=> $pms->DrawPmsLinkLabel(),
				'module'	=> 'pms',
				'phproot'	=> $phproot
			)
		);
		
		if($user['id'] != 0 && check_group_access(getvar('comment_mod_groups'), 'whitelist'))
		{
			$links['usercp_nav']['comments'] = array(
				'icon'		=> 'fa fa-pencil-square-o',
				'lang'		=> $lang['comments'],
				'module'	=> 'frontend_admin',
				'phproot'	=> $phproot			
			);
		}
		
		if(is_array($installedModules))
		foreach($installedModules as $modulename => $module)
		{
			if($module['active'] == '1')
			{
				if(isset($modules[$modulename]['frontend_admin']) && ((isset($modules[$modulename]['frontend_admin_access']) && $modules[$modulename]['frontend_admin_access']) || !isset($modules[$modulename]['frontend_admin_access'])))
				{
					$icon = '';
					if(isset($modules[$modulename]['frontend_admin_icon']))
					{
						$icon = $modules[$modulename]['frontend_admin_icon'];
					}
					else if(isset($modules[$modulename]['icon']))
					{
						$icon = $modules[$modulename]['icon'];
					}
					else
					{
						
					}
					
					$links['usercp_nav'][$modulename] = array(
						'icon'		=> $icon,
						'lang'		=> get_label($modulename),
						'module'	=> 'frontend_admin',
						'phproot'	=> $phproot
					);
				}
			}
		}
		
		if($user['perm'] == 'admin' && MephitisCMS == 'frontend')
		{
			$links['usercp_nav']['admin'] = array(
				'icon'		=> 'fa fa-server',
				'lang'		=> $lang['admincp'],
				'phproot'	=> $httpRootPath.'sys/index.php',
			);
		}
		else if($user['perm'] == 'admin' && MephitisCMS == 'backend')
		{
			$links['usercp_nav']['homepage'] = array(
				'icon'		=> 'fa fa-globe',
				'lang'		=> $lang['cms_homepage'],
				'phproot'	=> $httpRootPath.'index.php',
			);

			$links['usercp_nav']['install'] = array(
				'icon'		=> 'fa fa-database',
				'lang'		=> $lang['install_tool'],
				'phproot'	=> $httpRootPath.'sys/install.php',
			);			
		}
		
	}else{
		$links['usercp_nav'] = array(
			'login' => array(
				'icon'		=> 'fa fa-sign-in',
				'lang'		=> $lang['login'],
				'module'	=> 'usercp',
				'phproot'	=> $phproot
			)
		);
	}

}

function admin_tools(){
	
	global $links, $user, $lang, $link, $config, $modules, $sysmoduleloader, $moduleloader, $installedModules, $phpRootPath;
	
	if(isset($user['perm']) AND $user['perm'] == 'admin'){
	$links['admin_nav']['cms'] = array(
			'icon' => 'fa fa-paw',
			'lang' => $lang['cms_tools_dir'],
			'modules' => array(
				'dashboard'	=> array(
					'icon'		=> 'fa fa-tachometer',
					'lang'		=> $lang['dashboard'],
					'module'	=> ''
				),
				'common'	=> array(
					'icon'		=> 'fa fa-check-square-o',
					'lang'		=> $lang['common_settings'],
					'module'	=> 'admin'
				),
				'email'	=> array(
					'icon'		=> 'fa fa-envelope-o',
					'lang'		=> $lang['email_system'],
					'module'	=> 'admin',
					'submodules' => array(
						'settings' => array(
							'icon'		=> 'fa fa-check-square-o',
							'lang'		=> $lang['email_settings']
						),
						'queue' => array(
							'icon'		=> 'fa fa-database',
							'lang'		=> $lang['email_queue']
						),
					)
				),
				'templates'	=> array(
					'icon'		=> 'fa fa-picture-o',
					'lang'		=> $lang['templates'],
					'module'	=> 'admin'
				),				
				'content'	=> array(
					'icon'		=> 'fa fa-folder-open-o',
					'lang'		=> $lang['content'],
					'module'	=> 'admin'
				),
				'emojis'	=>  array(
					'icon'		=> 'fa fa-smile-o',
					'lang'		=> $lang['emojis'],
					'module'	=> 'admin'
				),
				'ips'		=>  array(
					'icon'		=> 'fa fa-laptop',
					'lang'		=> $lang['accesses'],
					'module'	=> 'admin'
				),
				'sessions'		=>  array(
					'icon'		=> 'fa fa-paper-plane-o',
					'lang'		=> $lang['sessions'],
					'module'	=> 'admin'
				),
				'user'		=>  array(
					'icon'		=> 'fa fa-user',
					'lang'		=> $lang['user'],
					'module'	=> 'admin'
				),
				'groups'	=>  array(
					'icon'		=> 'fa fa-users',
					'lang'		=> $lang['groups'],
					'module'	=> 'admin'
				),						
				'files'		=>  array(
					'icon'		=> 'fa fa-files-o',
					'lang'		=> $lang['files'],
					'module'	=> 'admin'
				),
				'modules'	=>  array(
					'icon'		=> 'fa fa-cubes',
					'lang'		=> $lang['modules_manager'],
					'module'	=> 'admin'
				),
				'updater'	=>  array(
					'icon'		=> 'fa fa-refresh',
					'lang'		=> $lang['updater'],
					'module'	=> 'admin'
				),				
				'taskqueue'		=>  array(
					'icon'		=> 'fa fa-tasks',
					'lang'		=> $lang['taskqueue'],
					'module'	=> 'admin'
				),
				'db'		=>  array(
					'icon'		=> 'fa fa-database',
					'lang'		=> $lang['db'],
					'module'	=> 'admin'
				),
				'credits'		=>  array(
					'icon'		=> 'fa fa-puzzle-piece',
					'lang'		=> $lang['credits'],
					'module'	=> 'admin'
				)
				
			)
		);
		
		$links['admin_nav']['modules_dir'] = array(
			'icon' => 'fa fa-cubes',
			'lang' => $lang['cms_modules_dir']
		);
		
		foreach(array_merge($sysmoduleloader, $moduleloader) as $modulename){
			$file = $phpRootPath.'modules/'.$modulename.'/mod_admin.php';
				
			if(isset($installedModules[$modulename]) && $installedModules[$modulename]['active'] == "1" && file_exists($file) && (!isset($modules[$modulename]) || $modules[$modulename] != "")){
				
				if(isset($modules[$modulename])){
					
					if(isset($modules[$modulename]['lang_admin'])){
						$modulelang = $modules[$modulename]['lang_admin'];
					}else if(isset($modules[$modulename]['lang'])){
						$modulelang = $modules[$modulename]['lang'];
					}else{
						$modulelang = $modulename;
					}
					
				}else{
					$modulelang = $modulename;
				}
				
				$icon = 'fa fa-cube';
				if(isset($modules[$modulename]['icon']) AND $modules[$modulename]['icon'] != '')$icon = $modules[$modulename]['icon'];
				
				$submodules = '';
				if(isset($modules[$modulename]['modules'])){
					$submodules = $modules[$modulename]['modules'];
				}
				
				$modulesubmodules = '';
				if(isset($modules[$modulename]['submodules'])){
					$modulesubmodules = $modules[$modulename]['submodules'];
				}				
				
				$links['admin_nav']['modules_dir']['modules'][$modulename] = array(
					'icon' => $icon,
					'lang' => $modulelang,
					'usermodule' => $modulename,
					'modules' => $submodules,
					'submodules' => $modulesubmodules,
					'module' => 'admin'
				);
				
			}
			
		}
		
		if(isset($_GET['directory'])){
			$dir_id = $_GET['directory'];
		}else{
			$dir_id = 0;
		}

		if(isset($_GET['site'])){
			$site_id = $_GET['site'];
		}else{
			$site_id = 0;
		}
		
		if(isset($_GET['show']) OR $site_id != 0){
		
			if(isset($_GET['show'])){
				$search = 'shortlink = "'.$mysqli->real_escape_string($_GET['show']).'"';
			}else{
				$search = 'id = "'.$mysqli->real_escape_string($site_id).'"';
			}

			$query = 'SELECT id, name, dir, shortlink, content_kind FROM '.$config['prefix'].'sites WHERE '.$search .'';
			$result = $mysqli->query($query);
			$row = $result->fetch_assoc();
			
			$site_id = $row['id'];
			$dir_id = $row['dir'];
		}

		$links['admin_tools']['site_new'] = 
			array(
				'module'	=> 'sites',
				'action'	=> 'new',
				'itemid'	=> '',
				'lang'		=> $lang['site_new'],
				'link'		=> 'directory='.$dir_id
			)
		;

		if(isset($_GET['site']) OR isset($_GET['show'])){
			if(isset($_GET['site'])){
				$site_id = $_GET['site'];
			}
			if($site_id != 0){
				if($row['content_kind'] == 'fce'){
					$links['admin_tools']['fce'] = 
						array(
							'module'	=> 'fce',
							'action'	=> 'show',
							'siteid'	=> $site_id,
							'lang'		=> $lang['fces'],
							'link'		=> ''
						)
					;
				}
				
				$links['admin_tools']['site_edit'] = 
					array(
						'module'	=> 'sites',
						'action'	=> 'edit',
						'itemid'	=> $site_id,
						'lang'		=> $lang['site_edit'],
						'link'		=> ''
					)
				;
				$links['admin_tools']['site_del'] = 
					array(
						'module'	=> 'sites',
						'action'	=> 'del',
						'itemid'	=> $site_id,
						'lang'		=> $lang['site_del'],
						'link'		=> ''
					)
				;
			}
		}

		$links['admin_tools']['dir_new'] = 
			array(
				'module'	=> 'dirs',
				'action'	=> 'new',
				'itemid'	=> '',
				'lang'		=> $lang['dir_new'],
				'link'		=> 'directory='.$dir_id
			)
		;

		if(isset($_GET['directory']) OR isset($_GET['show'])){
			if(isset($_GET['directory'])){
				$dir_id = $_GET['directory'];
			}
			if($dir_id != 0){
				$links['admin_tools']['dir_edit'] = 
					array(
						'module'	=> 'dirs',
						'action'	=> 'edit',
						'itemid'	=> $dir_id,
						'lang'		=> $lang['dir_edit'],
						'link'		=> ''
					)
				;
				$links['admin_tools']['dir_del'] = 
					array(
						'module'	=> 'dirs',
						'action'	=> 'del',
						'itemid'	=> $dir_id,
						'lang'		=> $lang['dir_del'],
						'link'		=> ''
					)
				;
			}
		}

	}
	else
	{
		$links['admin_nav'] = '';
	}
}

?>