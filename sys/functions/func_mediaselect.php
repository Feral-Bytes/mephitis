<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

 
function mediaselect_filelist($info, $dir = 0, $search = array('id != "0"'), $orderby = 'id DESC')
{

	global $mysqli, $config, $user, $phproot, $last_upload;
	
	$dir = $mysqli->real_escape_string($dir);
	
	if(isset($info['dir']))
	{
		$dir = $info['dir'];
	}
	else
	{
		$info['dir'] = $dir;
	}
	
	$root = $dir;
	
	if(isset($_GET['file_dir']) && is_numeric($_GET['file_dir']))
	{	
		$dir = $mysqli->real_escape_string($_GET['file_dir']);
		$info['dir'] = $dir;
	}	
	
	if(check_group_access(getvar('file_upload_groups'), 'whitelist'))
	{
		mediaselect_uploader($info);
	}
	
	$search[] = 'dir = "'.$dir.'"';
	
	$files = array();
	$query = '
		SELECT
			*
		FROM
			'.$config['prefix'].'files
		WHERE
			'.implode(' AND ' ,$search).'
		ORDER BY
			'.$orderby.'
			
	';
	$result = $mysqli->query($query);
	while($row = $result->fetch_assoc())
	{
		if(check_group_access($row['groups'], $row['groups_mode']))
		{
			$files[$row['id']] = $row;
		}
	}
	
	$fileTree = new tree(
		array(
			'tab' => 'file_dirs',
			'fieldParent' => 'sub_of',
			'noRank' => true,
			'checkAccess' => true,
			'fields' => 'name',
			'orderBy' => 'name ASC',
			'root' => $root
		)
	);
	
	$query = '
		SELECT
			id
		FROM
			'.$config['prefix'].'files
		WHERE
			dir = "'.$root.'"
	';
	$root_result = $mysqli->query($query);
	
	
	$query = '
		SELECT
			id, name
		FROM
			'.$config['prefix'].'file_dirs 
		WHERE
			id = "'.$root.'"
	';	
	$rootdir_result = $mysqli->query($query);
	$root_row = $rootdir_result->fetch_assoc();
	
	if(!is_array($root_row))
	{
		$root_row['name'] = get_label('root_dir');
	}
	
	$data = array(
		'files' => $files,
		'dir' => $dir,
		'root' => $root,
		'root_name' => $root_row['name'],
		'tree' => $fileTree,
		'rootcount' => $root_result->num_rows
	);	
	
	return $data;
}

function mediaselect_uploader($info)
{
	global $mysqli, $config, $user, $phproot, $last_upload;
	
	if(isset($_GET['ajax']))
	{
		if($_GET['ajax'] == 'upload' && check_group_access(getvar('file_upload_groups'), 'whitelist'))
		{
			$dir = 0;
			
			if(isset($info['dir']))
			{
				$dir = $info['dir'];
			}
			
			if(isset($_GET['file_dir']) && is_numeric($_GET['file_dir']))
			{	
				$dir = $mysqli->real_escape_string($_GET['file_dir']);
				$info['dir'] = $dir;
			}
			
			$fileTree = new tree(
				array(
					'tab' => 'file_dirs',
					'fieldParent' => 'sub_of',
					'noRank' => true,
					'checkAccess' => true,
					'fields' => 'name',
					'orderBy' => 'name ASC',
					'current' => $dir
				)
			);
			
			if(check_rootline_groups($fileTree->GetRootline()))
			{
				$info['id'] = '';
				$last_upload_id = upload_file($info, 'new');
				
				if(isset($last_upload_id) && is_array($last_upload_id))
				{
					$last_upload = $last_upload_id;
				}
			}
			
		}
	}	
	
}

function mediainfo($id)
{
	global $mysqli, $config, $user;
	
	if($id != 0 || $id != '')
	{	
		
		if(is_numeric($id))
		{
			$where = 'id = "'.$mysqli->real_escape_string($id).'"';
		}
		else
		{
			$where = 'file = "'.$mysqli->real_escape_string($id).'"';
		}

		$query = '
			SELECT
				*
			FROM
				'.$config['prefix'].'files
			WHERE
				'.$where.'
		';
		$result = $mysqli->query($query);
		$row = $result->fetch_assoc();
		if(is_array($row))
		{
			return $row;
		}
	}
}

?>