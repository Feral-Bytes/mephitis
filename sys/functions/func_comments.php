<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function comments($conf){
	
	global $config, $mysqli, $lang, $link, $user, $libs, $autoform_success, $phproot;

	if(!isset($conf['header']))
	{
		$conf['header'] = get_label('comments');
	}	
	
	if(!isset($conf['draw_mode']))
	{
		$conf['draw_mode'] = 'content';
	}
	
	if(!isset($conf['guests_can_write']))
	{
		if(getvar('comment_guests_can_write') == '1')
		{
			$conf['guests_can_write'] = true;
		}
		else
		{
			$conf['guests_can_write'] = false;
		}
	}

	if(!isset($conf['members_can_write']))
	{
		if(getvar('comment_members_can_write') == '1')
		{
			$conf['members_can_write'] = true;
		}
		else
		{
			$conf['members_can_write'] = false;
		}
	}
	
	if(!isset($conf['requires_mod_check']))
	{
		if(getvar('comment_requires_mod_check') == '1')
		{
			$conf['requires_mod_check'] = true;
		}
		else
		{
			$conf['requires_mod_check'] = false;
		}
	}		
	
	$conf['can_write'] = false;
	if($user['id'] != 0 && ($conf['members_can_write'] || $conf['guests_can_write']))
	{
		$conf['can_write'] = true;
	}
	
	if($user['id'] == 0 && $conf['guests_can_write'])
	{
		$conf['can_write'] = true;
	}	
	
	$inputs[] = array(
		'type'	=> 'fix',
		'name'	=> 'object',
		'value'	=> $conf['object']
	);

	$inputs[] = array(
		'type'	=> 'fix',
		'name'	=> 'object_id',
		'value'	=> $conf['object_id']
	);

	if($user['id'] != 0)
	{
		$inputs[] = array(
			'type'	=> 'fix',
			'name'	=> 'user_id',
			'value'	=> $user['id']
		);
	}
	else
	{
		$inputs[] = array(
			'type'	=> 'text',
			'name'	=> 'user_name',
			'lang' => $lang['name'],
			'required' => 1
		);		
	}
	
	$inputs[] = array(
		'type'	=> 'fix',
		'name'	=> 'time',
		'value'	=> time()
	);

	$inputs[] = array(
		'type'	=> 'fix',
		'name'	=> 'ip',
		'value'	=> $mysqli->real_escape_string($_SERVER['REMOTE_ADDR'])
	);

	$inputs[] = array(
		'name'	=> 'subject',
		'type'	=> 'text',
		'lang'	=> $lang['subject'],
		'value'	=> ''
	);
	
	$inputs[] = array(
		'type'	=> 'textarea',
		'name'	=> 'content',
		'wysiwyg' => getvar('wysiwyg'),
		'required' => '1',
		'required_msg' => $lang['required_msg_content'],
		'value'	=> '',
		'lang'	=> $lang['content']
	);	
	
	$form = '';
	
	if(isset($_GET['comment']) AND $_GET['comment'] == 'new' AND ($conf['can_write'] OR check_group_access(getvar('comment_mod_groups'), 'whitelist'))){
		
		$lang_save = $lang['posted'];
		if($conf['requires_mod_check'] AND !check_group_access(getvar('comment_mod_groups'), 'whitelist'))
		{
			$inputs[] = array(
				'type'	=> 'fix',
				'name'	=> 'online',
				'value'	=> 0
			);
			$lang['posted'] = $lang['new_comment_posted_mc'];
		}
		else
		{
			$inputs[] = array(
				'type'	=> 'fix',
				'name'	=> 'online',
				'value'	=> 1
			);
			$lang['posted'] = $lang['new_comment_posted'];
		}
		
		$form = autoform($config['prefix'].'comments', $inputs, $phproot, $link.'&comment=new&', $lang['new_comment'], 'new', 'noframe', 'comment', '', '', true);
		$lang['posted'] = $lang_save;
		
	}else if(isset($_GET['comment']) AND $_GET['comment'] == 'edit' AND check_group_access(getvar('comment_mod_groups'), 'whitelist')){
		$inputs[] = array(
			'type'	=> 'checkbox',
			'name'	=> 'online',
			'lang' => get_label('public')
		);		
		$form = autoform($config['prefix'].'comments', $inputs, 'index.php', $link.'&comment=edit&itemid='.$_GET['itemid'].'', $lang['edit_comment'], 'edit', 'noframe', 'comment', '', '', true);
	}else if(isset($_GET['comment']) AND $_GET['comment'] == 'del' AND check_group_access(getvar('comment_mod_groups'), 'whitelist')){
		$form = autoform($config['prefix'].'comments', $inputs, 'index.php', $link.'&comment=del&itemid='.$_GET['itemid'].'', $lang['del_comment'], 'del', 'noframe', 'comment', '', '', true);
	}else if(isset($_GET['comment']) AND ($_GET['comment'] == 'setonline' OR $_GET['comment'] == 'unsetonline') AND check_group_access(getvar('comment_mod_groups'), 'whitelist')){
		$online = 0;
		if($_GET['comment'] == 'setonline')
		{
			$online = 1;
		}
		if($_GET['comment'] == 'unsetonline')
		{
			$online = 0;
		}
		
		$mysqli->query('
			UPDATE '.$config['prefix'].'comments
			SET
				online = "'.$online.'"
			WHERE
				id = "'.$mysqli->real_escape_string($_GET['itemid']).'"
		');
	}
	
	if(isset($autoform_success) && $autoform_success == 1)
	{
		$form = '';
	}
	
	$comments = array();
	$i = 0;
	
	$wheres = array();
	$wheres[] = 'object = "'.$conf['object'].'"';
	$wheres[] = 'object_id = "'.$conf['object_id'].'"';
	
	if(!check_group_access(getvar('comment_mod_groups'), 'whitelist'))
	{
		$wheres[] = 'online = "1"';
	}
	$where = implode(' AND ', $wheres);
	
	$query = '
		SELECT
			*
		FROM
			'.$config['prefix'].'comments
		WHERE
			'.$where.'
		ORDER BY 
			id DESC
		'.pages('limits', 'comments', $where, $link, 'comment_page').'
	';
	$result = $mysqli->query($query);
	
	while($row = $result->fetch_assoc()){
		if($row['user_id'] != 0)
		{
			$row['user'] = user_info($row['user_id']);
		}
		$comments[] = $row;
	}
	
	$result = $mysqli->query('SELECT id FROM '.$config['prefix'].'comments WHERE '.$where.'');
	
	$data = array(
		'comments' => $comments,
		'count' => $result->num_rows,
		'form' => $form,
		'conf' => $conf
	);
	
	$echo = draw_comments($data);
	
	return $echo;
	
	
}

?>