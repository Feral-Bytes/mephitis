<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function user_info($id){

	global $config, $mysqli, $lang;

	if($id == '0'){
		
		return user_guest();
		
	}else{
		
		$query = 'SELECT * FROM '.$config['prefix'].'user WHERE id = "'.$mysqli->real_escape_string($id).'"';
		$result = $mysqli->query($query);
		$row = $result->fetch_assoc();	
		
		return $row;
	}

}

function user_guest()
{
	global $config;
	
	return array(
		'id' => 0,
		'name' => get_label('guest'),
		'lang' => browser_lang($config['availablelangs']),
		'perm' => 'guest',
		'groups' => getvar('guest_user_groups')
	);
}

function user_name($id)
{
	global $config, $mysqli, $lang;

	if($id == '0'){

		return $lang['guest'];
		
	}else{
		
		$query = 'SELECT * FROM '.$config['prefix'].'user WHERE id = "'.$mysqli->real_escape_string($id).'"';
		$result = $mysqli->query($query);
		$row = $result->fetch_assoc();	
		
		return $row['name'];
	}	
}

function user_list($order = 'name', $mode = 'ASC')
{
	global $config, $mysqli;

	$list = array();
		
	$query = 'SELECT * FROM '.$config['prefix'].'user ORDER BY '.$order.' '.$mode.'';
	$result = $mysqli->query($query);
	
	while($row = $result->fetch_assoc())
	{
		$list[$row['id']] = $row;
	}

	return $list;
}

function user(){
	
	global $config, $mysqli, $lang, $msg_stack, $phproot;
	
	$user = array();
	$user['browser_lang']	= browser_lang($config['availablelangs']);
	$user['lang']			= $user['browser_lang'];
	$user['timezone']		= $config['timezone'];
	$user['groups']			= '';
	
	if(isset($_GET['usercp']) AND $_GET['usercp'] == "login"){
	
		$ok = true;
		
		if(isset($_POST['login'])){
			$name  = $mysqli->real_escape_string(trim($_POST['name']));
			$pass  = $_POST["pass"];
			
			$field = '';
			
			if(mailcheck($name))
			{
				$field = 'email';
			}
			else
			{
				$field = 'name';
			}
			
			$query = 'SELECT * FROM '.$config['prefix'].'user WHERE '.$field.' = "'.$name.'"';
			$result = $mysqli->query($query);
			$row = $result->fetch_assoc();				
			
			if($row[$field] == $name AND password_verify($pass, $row['pass']) AND $row['active'] == 1){
				if(getvar('public_login') == '0' AND $row['perm'] != 'admin'){
					die(draw_error($lang['error'], $lang['access_denied']));
				}
				
				$_SESSION[getvar('cookie_name')]['user']	= $row;
				if(isset($_POST['login_remember'])){
					$_SESSION[getvar('cookie_name')]['user']['login_remember'] = "1";
				}
				
				$_SESSION[getvar('cookie_name')]['user']['session_id'] = mk_session($row);
				
				$ok = true;
				$msg_stack .= draw_alert($lang['login_msg'], 'success');
			}else{
				$ok = false;
				$msg_stack .= draw_alert($lang['login_error'], 'danger');
			}
		}
	
	}	
	
	//user is loggedin
	if(isset($_COOKIE[getvar('cookie_name')])){
		$cookie = $_COOKIE[getvar('cookie_name')];
		
		list($id, $pass) = explode(':', $cookie);
		
		$query = 'SELECT * FROM '.$config['prefix'].'user WHERE id = "'.$mysqli->real_escape_string($id).'"';
		$result = $mysqli->query($query);
		$row = $result->fetch_assoc();
		
		
		$new = 'UPDATE '.$config['prefix'].'user SET 
		time = "'.time().'"
		WHERE id = "'.$row['id'].'"';
		$update = $mysqli->query($new) or die(draw_error($lang['error'], $mysqli->error));
		
		$user_session = get_session($row);
		
		if(md5($row['pass'].$user_session) == $pass AND $user_session != '0' AND $row['active'] == 1 AND (getvar('public_login') == '1' OR $row['perm'] == 'admin')){ 
			$_SESSION[getvar('cookie_name')]['user']	= $row;
			$cookie = $row['id'].':'.md5($row['pass'].$user_session);
			setcookie(getvar('cookie_name'), $cookie, time()+2592000);
			$user	= $row;
		}else{
			user_logout($row);
			
			$user['id']		= '0';
			$user['perm']	= 'guest';
			$user['groups']	= getvar('guest_user_groups');
		
		}
	}else if(isset($_SESSION[getvar('cookie_name')])){

		$id = $_SESSION[getvar('cookie_name')]['user']['id'];
		
		$query = 'SELECT * FROM '.$config['prefix'].'user WHERE id = "'.$mysqli->real_escape_string($id).'"';
		$result = $mysqli->query($query);
		$row = $result->fetch_assoc();		
		
		$new = 'UPDATE '.$config['prefix'].'user SET 
		time = "'.time().'"
		WHERE id = "'.$row['id'].'"';
		$update = $mysqli->query($new) or die(draw_error($lang['error'], $mysqli->error));
		
		$user_session = get_session($row);
		
		if(md5($row['pass'].$user_session) == md5($_SESSION[getvar('cookie_name')]['user']['pass'].$_SESSION[getvar('cookie_name')]['user']['session_id']) AND $user_session != '0' AND $row['active'] == 1  AND (getvar('public_login') == '1' OR $row['perm'] == 'admin')){
			$user	= $row;
			
			//user wants login cookie
			if(isset($_SESSION[getvar('cookie_name')]['user']['login_remember'])){
				$cookie = $row['id'].':'.md5($row['pass'].$user_session);
				setcookie(getvar('cookie_name'), $cookie, time()+2592000);
			}
		}else{
			user_logout($row);
			
			$user['id']		= '0';
			$user['perm']	= 'guest';
			$user['groups']	= getvar('guest_user_groups');
		}
	}else{
		$user['id']		= '0';
		$user['perm']	= 'guest';
	}
	
	if(isset($_GET['usercp']) AND $_GET['usercp'] == "logout"){
	
		user_logout($user);
		$user['id']		= '0';
		$user['perm']	= 'guest';
		$user['groups']	= getvar('guest_user_groups');
		
		$msg_stack .= draw_alert($lang['logout_msg'], 'success');
	
	}
	
	if($user['lang'] == "")
	{
		$user['browser_lang']	= browser_lang($config['availablelangs']);
		$user['lang']			= $user['browser_lang'];		
	}
	
	return $user;
	
}

function user_logout($user){

	global $config;
	
	del_session($user);
	//session_destroy ();
	$_SESSION[getvar('cookie_name')] = '';
	unset($_SESSION[getvar('cookie_name')]);
	
	setcookie(getvar('cookie_name'), '', -3600);
	
}

function browser_lang($availablelangs, $fallback = 'english'){
	
	if(isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
	{
		$langs = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
	}
	else
	{
		$langs = 'english';
	}
	$langs = explode(',', $langs);
	
	$langarray = array();
	foreach($langs as $lang){
		$lang = explode(';', $lang);
		
		$lang_code = explode('-', $lang[0]);
		
		if(!isset($lang[1])){
			$langarray[$lang_code[0]] = 1;
		}else{
			$priority = explode('=', $lang[1]);
			$langarray[$lang_code[0]] = $priority[1];
		}
	}
	
	arsort($langarray, SORT_NATURAL);
	
	$langidentifiers = array(
    "en" => "english",
    "de" => "german",
	);
	
	foreach($langarray as $key => $priority){
		$identifier = $langidentifiers[$key];
		foreach($availablelangs as $availablelang){
			if($availablelang == $identifier){
				return $identifier;
			}
		}
	}
	
	return $fallback;
}

function ipsave(){

	global $config, $mysqli, $user, $lang;
	
	$ip					= @$mysqli->real_escape_string($_SERVER['REMOTE_ADDR']);
	$host				= @$mysqli->real_escape_string(gethostbyaddr($ip)); 
	$request_method		= @$mysqli->real_escape_string($_SERVER['REQUEST_METHOD']);
	$php_self			= @$mysqli->real_escape_string($_SERVER['PHP_SELF']);
	$http_user_agent	= @$mysqli->real_escape_string($_SERVER['HTTP_USER_AGENT']);
	
	if(isset($_SERVER['HTTP_REFERER'])){
		$http_referer = $mysqli->real_escape_string($_SERVER['HTTP_REFERER']);
	}else{
		$http_referer = '';
	}
	$time    = time();
	
	$query = 'SELECT * FROM '.$config['prefix'].'ips WHERE ip = "'.$ip.'"';
	$result = $mysqli->query($query);
	$row = $result->fetch_assoc();	
	
	if(is_array($row)){

		if(($time-$row['time']) < 3600){
			$http_referer = $row['http_referer'];
		}

		$new = 'UPDATE '.$config['prefix'].'ips SET 
		host = "'.$host.'",
		time = "'.$time.'",
		user = "'.$user['id'].'",
		request_method = "'.$request_method.'",
		php_self = "'.$php_self.'",
		http_user_agent = "'.$http_user_agent.'",
		http_referer = "'.$http_referer.'"
		WHERE ip = "'.$ip.'"';
		$update = $mysqli->query($new) or die(draw_error($lang['error'], $mysqli->error));
	}else{
		$new = 'INSERT INTO '.$config['prefix'].'ips 
			(ip, host, time, user, request_method, php_self, http_user_agent, http_referer) 
			VALUES ("'.$ip.'", "'.$host.'", "'.$time.'", "'.$user['id'].'", "'.$request_method.'", "'.$php_self.'", "'.$http_user_agent.'", "'.$http_referer.'")
		';
		$post = $mysqli->query($new) or die(draw_error($lang['error'], $mysqli->error));
	}

}

function get_session($user, $update = true){

	global $config, $mysqli;
	
	$query = 'SELECT * FROM '.$config['prefix'].'sessions WHERE user = "'.$user['id'].'"';
	$result = $mysqli->query($query);
	$row = $result->fetch_assoc();	
	
	if(is_array($row)){
	
		if($update)
		{
			$new = 'UPDATE '.$config['prefix'].'sessions SET 
			time = "'.time().'"
			WHERE user = "'.$user['id'].'" AND id = "'.$row['id'].'"';
			$updateSession = $mysqli->query($new) or die(draw_error($lang['error'], $mysql->error));
		}
		
		return $row['session_id'];
		
	}else{
		
		return 0;
		
	}
	
}

function mk_session($user){

	global $config, $mysqli;
	
	del_session($user);
	
	$session_id = $mysqli->real_escape_string(session_id());
	
	$new = 'INSERT INTO '.$config['prefix'].'sessions 
		(session_id, user, time) 
		VALUES ("'.$session_id.'", "'.$user['id'].'", "'.time().'")
	';
	$post = $mysqli->query($new) or die(draw_error($lang['error'], $mysqli->error));
	
	return $session_id;
	

}

function del_session($user){

	global $config, $mysqli;

	$query = 'DELETE FROM '.$config['prefix'].'sessions WHERE user = "'.$user['id'].'"';
	$del = $mysqli->query($query) or die(draw_error($lang['error'], $mysqli->error));

}

function check_group_access($groups, $groups_mode = 'whitelist', $cUser = '')
{
	global $user;
	
	if($cUser == '')
	{
		$cUser = $user;
	}
	
	if($cUser['perm'] == 'admin')
	{
		return true;
	}
	
	if($groups_mode != 'std' && $groups_mode != '')
	{
		$check = check_groups($groups, $cUser);
		
		if($groups_mode == 'whitelist')
		{
			if($check)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		if($groups_mode == 'blacklist')
		{
			if(!$check)
			{
				return true;
			}
			else
			{
				return false;
			}
		}		
	}
	else
	{
		return true;
	}

}

function check_rootline_groups($rootline, $cUser = ''){

	if(is_array($rootline))
	{
		$path = array_reverse($rootline);
		foreach($path as $path_element){

			if(!check_group_access($path_element['groups'], $path_element['groups_mode'], $cUser))
			{
				return false;
			}
		}
	}
	
	return true;
}

function check_group($groups, $cUser = '')
{
	global $user;
	
	if($cUser == '')
	{
		$cUser = $user;
	}
	
	if($cUser['perm'] == 'admin')
	{
		return true;
	}
	else
	{
		return check_groups($groups, $cUser);
	}
}

function check_groups($groups, $cUser = '')
{
	global $user;
	
	if($cUser == '')
	{
		$cUser = $user;
	}
	
	$groups = explode(';', $groups);
	$groups_user = explode(';', $cUser['groups']);
	
	foreach($groups_user as $group_user)
	{
		foreach($groups as $group)
		{
			if($group_user == $group && is_array(group_info($group)))
			{
				return true;
			}
		}		
	}
	
	return false;
	
}

function list_groups()
{
	global $config, $mysqli, $lang;
	
	$groups = array();
	
	$query = 'SELECT * FROM '.$config['prefix'].'groups';
	$result = $mysqli->query($query);
	while($row = $result->fetch_assoc())
	{
		$groups[$row['id']] = $row;
	}
	
	return $groups;
	
}

function group_info($group_id)
{
	global $config, $mysqli;

	if(!isset($config['groups'][$group_id]) || !is_array($config['groups'][$group_id]))
	{
		
		$query = 'SELECT * FROM '.$config['prefix'].'groups WHERE id = "'.$mysqli->real_escape_string($group_id).'"';
		$result = $mysqli->query($query);
		$row = $result->fetch_assoc();
		
		$config['groups'][$row['id']] = $row;
	}
	
	if(isset($config['groups'][$group_id]) && is_array($config['groups'][$group_id]))
	{
		return $config['groups'][$group_id];
	}
	else
	{
		return '';
	}
}

function check_frontend_admin($row, $cUser = '')
{
	global $user, $pageTree;
	
	if($cUser == '')
	{
		$cUser = $user;
	}
	
	if($user['id'] == 0)
	{
		return false;
	}
	
	if(
		!check_group_access($row['groups'], $row['groups_mode'], $cUser)
		|| 
		!check_group_access($row['edit_groups'], 'whitelist', $cUser)
		||
		!check_rootline_groups($pageTree->GetRootline(), $cUser)
	)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function update_user_settings($settings, $msg = '')
{
	global $config, $mysqli, $user, $lang, $msg_stack, $ok;
	
	$data = '';
	
	if(!isset($ok))
	{
		$ok = true;
	}
	
	foreach($settings as $setting){

	
		if(isset($setting['required']) AND $setting['required'] == '1' AND !isset($setting['required_msg']))
		{
			$setting['required_msg'] = $setting['lang'].' '.get_label('required_msg');
			if(isset($setting['category']))
			{
				$setting['required_msg'] = get_label($setting['category']).':&nbsp;'.$setting['required_msg'];
			}
		}
	
		if(isset($setting['required']) AND $setting['required'] == '1')
		{
			if(!isset($_POST[$setting['name']]) || $_POST[$setting['name']] == '')
			{
				$ok = false;
				$msg_stack .= draw_alert($setting['required_msg'], 'danger');	
			}
		}
	
		if(isset($setting['validation_func']) && trim($setting['validation_func'] != '') && isset($_POST[$setting['name']]) && $_POST[$setting['name']] != '')
		{
			$validation = $setting['validation_func']($_POST[$setting['name']]);
			if(!$validation)
			{
				$ok = false;
				if(!isset($setting['not_valid_msg']))
				{
					$setting['not_valid_msg'] = $setting['lang'].' '.get_label('not_valid_msg');
					if(isset($setting['category']))
					{
						$setting['not_valid_msg'] = get_label($setting['category']).':&nbsp;'.$setting['not_valid_msg'];
					}
				}
				$msg_stack .= draw_alert($setting['not_valid_msg'], 'danger');
			}
		}
	
		if($setting['type'] == 'custom')
		{

		}
		else if($setting['type'] == 'password')
		{
			if(isset($_POST[$setting['name']]) AND $_POST[$setting['name']] != '' OR isset($_POST[$setting['name'].'_v']) AND $_POST[$setting['name'].'_v'] != ''){
				
				if(!password_verify($_POST['passcheck_crit'], $user['pass'])){
					$ok = false;
					$msg_stack .= draw_alert($lang['usercp_passcheck_fail'], 'danger');
				}
				
				if($_POST[$setting['name']] != $_POST[$setting['name'].'_v']){
					$ok = false;
					$msg_stack .= draw_alert($lang['pass_equal_error'], 'danger');
				}
				$data .= $setting['name'].' = "'.$mysqli->real_escape_string(password_crypt($_POST[$setting['name']])).'",';
			}
		}
		else if($setting['type'] == 'checkbox')
		{
			if(isset($_POST[$setting['name']]))
			{
				$value = '1';
			}
			else
			{
				$value = '0';
			}
			$data .= $setting['name'].' = "'.$value.'",';
		}
		else if($setting['type'] == 'file')
		{
			//
		}
		else if($setting['type'] == 'var')
		{
			$data .= $setting['name'].' = "'.$setting['value'].'",';
		}
		else
		{
			
			if(isset($_POST[$setting['name']]))
			{			
			
				if($setting['name'] == 'email' && $_POST[$setting['name']] != $user[$setting['name']] && !password_verify($_POST['passcheck_crit'], $user['pass']))
				{
					$ok = false;
					$msg_stack .= draw_alert($lang['usercp_passcheck_fail'], 'danger');
				}

				$postData = $_POST[$setting['name']];
				
				if(isset($setting['datepicker']))
				{
					$postData = date('Y-m-d', strtotime($postData));
				}
				
				if(isset($_POST[$setting['name']]) && is_array($_POST[$setting['name']]))
				{
					$postData = implode(';', $postData);
				}
			}
			else
			{
				$postData = '';
			}
			
			$data .= $setting['name'].' = "'.$mysqli->real_escape_string($postData).'",';
		}
	}

	if($ok)
	{
		
		foreach($settings as $setting)
		{
			if($setting['type'] == 'file')
			{
				
				if(isset($_POST[$setting['name'].'_delbtn']))
				{
					del_file($setting['value']);
					$data .= $setting['name'].' = "0",';
				}
				else
				{						
					$file = upload_file($setting, 'edit');
					if(is_numeric($file)){
						$data .= $setting['name'].' = "'.$file.'",';
					}
				}
			}
		}
		
		
		$new = 'UPDATE '.$config['prefix'].'user Set
		'.substr($data, 0, -1).'
		WHERE id = "'.$user['id'].'"';
		$update = $mysqli->query($new)or die(draw_error($lang['mysql_error'], $mysqli->error));
		
		$user = user();
		user_tools();
		
		if($msg == '')
		{
			$msg = get_label('user_account_edited');
		}
		
		$msg_stack .= draw_alert($msg, 'success');
		
		return true;
	}
	return false;
}

function groups_inputs($name, $category = '')
{	
	$inputs[] = array(
		'type'	=> 'select',
		'name'	=> $name.'_mode',
		'category' => $category,
		'lang'	=> get_label($name.'_mode'),
		'options' => array(
			'whitelist' => array(
				'lang' => get_label('whitelist')
			),
			'blacklist' => array(
				'lang' => get_label('blacklist')
			)
		)
	);
	
	$options = array();
	$groups = list_groups();
	if(is_array($groups))
	foreach($groups as $group){
		$options[$group['id']] = array(
			'lang'		=> $group['name'],
			'active'	=> ''
		);
	}
	
	$inputs[] = array(
		'type'	=> 'select',
		'name'	=> $name,
		'category' => $category,
		'lang'	=> get_label($name),
		'multiple' => '1',
		'options' => $options
	);	
	
	return $inputs;
}

function password_validation($pass)
{
	
	return true;
}

function password_crypt($input, $rounds = 10)
{
	$crypt_options = array(
		'cost' => $rounds
	);
	return password_hash($input, PASSWORD_BCRYPT, $crypt_options);
}



?>