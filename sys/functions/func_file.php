<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function file_info($id){

	global $config, $mysqli;
	
	$query = 'SELECT * FROM '.$config['prefix'].'files WHERE id = "'.$mysqli->real_escape_string($id).'"';
	$result = $mysqli->query($query);
	$row = $result->fetch_assoc();	
	
	return $row;

}

function del_file($id, $keepDB = false){

	global $config, $mysqli, $lang, $phpRootPath;

	$query = 'SELECT * FROM '.$config['prefix'].'files WHERE id = "'.$mysqli->real_escape_string($id).'"';
	$result = $mysqli->query($query);
	$row = $result->fetch_assoc();
	
	if(is_array($row))
	{
		if(file_exists($phpRootPath.$row['path'].$row['file'])){
			unlink($phpRootPath.$row['path'].$row['file']);
		}
		
		if(!$keepDB)
		{
			$query = '
				DELETE FROM '.$config['prefix'].'files
				WHERE
					id = "'.$row['id'].'"
			';
			$del = $mysqli->query($query) or die(draw_error($lang['mysql_error'], $mysqli->error));
		}
	}
}

function show_emoji($file_id, $mode = 'draw'){
	
	global $lang, $config, $mysqli, $user, $httpRootPath, $phpRootPath;
	
	$query = 'SELECT * FROM '.$config['prefix'].'emojis WHERE id = "'.$mysqli->real_escape_string($file_id).'"';
	$result = $mysqli->query($query);
	$row = $result->fetch_assoc();	
	if($mode == 'draw'){

		$data = implode("",file($phpRootPath.$row['path'].$row['file']));

		header('Content-type: '.$row['type'].'');
		header('Content-Disposition: inline; filename="'.$row['file_name'].'"');
		echo $data;
		exit;
	}
	
	if($mode == 'mk_emoji'){
		$echo = '<img border="0" src="'.$httpRootPath.'index.php?emoji='.$row['id'].'" title="'.$row['name'].'" alt="'.$row['code'].'">';
		return $echo;
	}

	
}

function show_file($file_id, $mode = 'full', $filemodule = null){

	global $lang, $config, $mysqli, $user, $phpRootPath, $httpRootPath;
	
	$forbidden = array('.php', '..', 'logs', 'updates', 'htaccess', ';', '//');
	if(substr($file_id, 0, strlen('/')) === '/' || substr($file_id, 0, strlen('\\')) === '\\')
	{
		die();
	}
	
	foreach($forbidden as $value)
	{
		if((strpos($file_id, $value) !== false))
		{
			die();
		}		
	}
	
	if($mode == 'text'){
		file_text($file_id, $filemodule);
		exit;
	}	
	
	if(is_numeric($file_id))
	{	
		$query = 'SELECT * FROM '.$config['prefix'].'files WHERE id = "'.$mysqli->real_escape_string($file_id).'"';
		$result = $mysqli->query($query);
		$row = $result->fetch_assoc();
		
		$fileTree = new tree(
			array(
				'tab' => 'file_dirs',
				'fieldParent' => 'sub_of',
				'noRank' => true,
				'checkAccess' => true,
				'fields' => 'name',
				'current' => $row['dir'],
				'orderBy' => 'name ASC'
			)
		);

		if(!is_array($row) || !check_group_access($row['groups'], $row['groups_mode']) || !check_rootline_groups($fileTree->GetRootline()))
		{
			die();
		}
	}
	else
	{
		$row['path'] = 'files/'.$mysqli->real_escape_string($filemodule).'/';
		$row['id'] = $mysqli->real_escape_string($file_id);
		$row['file'] = $mysqli->real_escape_string($file_id);
		$row['file_name'] = $mysqli->real_escape_string($file_id);
		$row['type'] = filetype($row['path'].$row['file']);
	}
	
	$file = $phpRootPath.$row['path'].$row['file'];
	
	if(!file_exists($file))
	{
		die();
	}
	
	$mode = $mysqli->real_escape_string($mode);
	
	if(file_exists($file))
	{
		if($mode == 'thumb'){
			
			$data = implode("",file($file));

			header('Content-type: '.$row['type'].'');
			header('Content-Disposition: inline; filename="thumb_'.$row['file_name'].'"');

			if(isset($_GET['size']))
			{
				$size = $mysqli->real_escape_string($_GET['size']);
			}
			else
			{
				$size = getvar('thumbnail_size');	
			}
			
			$fileCache = $phpRootPath.'cache/file-'.md5($file.$row['file_name'].$size).'.temp';
			
			if(file_exists($fileCache))
			{
				echo file_get_contents($fileCache);
			}
			else
			{
				$image = new SimpleImage();
				$image->image = imagecreatefromstring($data);
				$image->type = $row['type'];
				$image->resizeToEdgelength($size);
				$image->save($fileCache);
				$image->output($image->type);
			}
			exit;
			
		}

		if($mode == 'user_thumb'){
			if($row['thumb'] != 0){
				show_file($row['thumb'], $mode = 'thumb', $filemodule);
			}
			else
			{
				show_file($file_id, $mode = 'thumb', $filemodule);
			}
		}

		if($mode == 'full'){
			$data = implode("",file($file));

			header('Content-type: '.$row['type'].'');
			header('Content-Disposition: inline; filename="'.$row['file_name'].'"');
			
			if(isset($_GET['size']))
			{
				$fileCache = $phpRootPath.'cache/file-'.md5($file.$row['file_name'].$_GET['size']).'.temp';
				if(file_exists($fileCache))
				{
					echo file_get_contents($fileCache);
				}
				else
				{
					$size = $mysqli->real_escape_string($_GET['size']);
					$image = new SimpleImage();
					$image->image = imagecreatefromstring($data);
					$image->type = $row['type'];
					$image->resizeToEdgelength($size);
					$image->save($fileCache);
					$image->output($image->type);
				}
			}
			else
			{
				echo $data;
			}
			
			exit;
		}
	}
}

function file_text($file, $filemodule)
{
	
	$data = implode("",file($file));
	
	if($filemodule == 'text/css')
	{
		global $tpl_vars, $default_tpl_vars;
		
		$search1  = array();
		$search2  = array();		
		foreach($tpl_vars as $cat_key => $cat)
		{
			foreach($cat as $key => $value)
			{	
				$search1['{'.$key.'}'] = $value;
				$search2[$default_tpl_vars[$cat_key][$key]] = $value;
			}		
		}
		
		$data = str_ireplace(array_keys($search1), $search1, $data);
		$data = str_ireplace(array_keys($search2), $search2, $data);
		
		
	}
	
	header('Content-type: '.$filemodule.'');
	echo $data;
	exit;	
}

function upload_file($file, $mode){
	
	global $lang, $mysqli, $config, $user, $msg_stack;
	
	$file_info = array();

	if(isset($_FILES[$file['name']]['tmp_name']) AND $_FILES[$file['name']]['tmp_name'] != '' AND isset($mode)){
		
		if(is_array($_FILES[$file['name']]['tmp_name']))
		{
			for($i = 0; $i < count($_FILES[$file['name']]['tmp_name']); $i++)
			{
				if($_FILES[$file['name']]['name'][$i] != '')
				{
					$file_info[] = upload_file_handler(
						$file,
						array(
							'name' => $_FILES[$file['name']]['name'][$i],
							'type' => $_FILES[$file['name']]['type'][$i],
							'tmp_name' => $_FILES[$file['name']]['tmp_name'][$i],
							'error' => $_FILES[$file['name']]['error'][$i],
							'size' => $_FILES[$file['name']]['size'][$i]
						),
						$mode
					);
				}
			}
		}
		else
		{
			$file_info = upload_file_handler($file, $_FILES[$file['name']], $mode);
		}
	}
	
	return $file_info;
	
}

function upload_file_handler($file, $file_info, $mode)
{
	global $lang, $mysqli, $config, $user, $msg_stack, $phpRootPath;
	
	$file_name = $user['id'].'_'.md5(time().microtime().$mysqli->real_escape_string($file_info['name']).random_string(20));
	
	if($file_info['error'] != 0)
	{
		return '';
	}
	
	list($type1, $type2) = explode('/', $file_info['type']);

	$error = '';
	
	
	if($type1 != 'image' AND ($file['name'] == 'thumb' || (isset($file['opt']['onlyimage']) AND $file['opt']['onlyimage'] == true))){
		$msg_stack .= draw_alert($file['lang'].' '.$lang['isnopic'], 'danger');
		$error .= '1';
	}
	
	
	if($error == ''){
		if(!is_dir($phpRootPath.$file['path']))
		{
			mkdir($phpRootPath.$file['path']);
		}				
		move_uploaded_file($file_info['tmp_name'], $phpRootPath.$file['path'].$file_name);
	}
	
	if($mode == 'edit' AND $file['value'] != '' AND $file['value'] != 0 AND $error == ''){
		
		if(isset($file['id']))
		{
			del_file($file['value'], true);
		}
		else
		{
			if(file_exists($file['path'].$file['value']))
			{
				unlink($phpRootPath.$file['path'].$file['value']);
			}
		}
	}

	if($error == ''){
		
		$file_input = $file;
		
		$file = array();
		
		if(!isset($file_input['dir']))
		{
			$file_input['dir'] = 0;
		}
		
		$file['user']		= $user['id'];
		$file['name']		= $file_input['name'];
		$file['path']		= $file_input['path'];
		$file['dir']		= $file_input['dir'];
		$file['file']		= $file_name;
		$file['file_name']	= $file_info['name'];
		$file['type']		= $file_info['type'];
		$file['size']		= $file_info['size'];
		$file['time']		= time();
		
		if(isset($file_input['module']))
		{
			$file['module'] = $file_input['module'];
		}
		
		if(isset($file_input['id']))
		{
			if($mode == 'new' || ($file_input['value'] == 0 || !is_array(file_info($file_input['value']))))
			{		
				$cols = '';
				$data = '';
				
				foreach($file as $key => $value)
				{
					$cols .= $key.', ';
					$data .= '"'.$mysqli->real_escape_string($value).'", ';
				}
			
				$new = '
					INSERT INTO '.$config['prefix'].'files 
						('.substr($cols, 0, -2).') 
					VALUES
						('.substr($data, 0, -2).')
				';
				$post = $mysqli->query($new) or die(draw_error($lang['mysql_error'], $mysqli->error));		
				
				$file['id'] = $mysqli->insert_id;
				
				return $file['id'];
			}
			else if($mode == 'edit')
			{
				$data = '';
				foreach($file as $key => $value)
				{
					$data .= $key.' = "'.$mysqli->real_escape_string($value).'",';
				}					
				$new = '
					UPDATE '.$config['prefix'].'files Set
						'.substr($data, 0, -1).'
					WHERE
						id = "'.$mysqli->real_escape_string($file_input['value']).'"
				';
				$update = $mysqli->query($new)or die(draw_error($lang['mysql_error'], $mysqli->error));
				
				$file['id'] = $file_input['value'];
				
				return $file['id'];
				
			}
		}
		else
		{
			return $file;
		}
		
	}else{
		
		return '';
	}	
}

function GetDirList($path)
{
	if (file_exists($path) AND $handle = opendir($path))
	{
		$list = array();
		while (false !== ($file = readdir($handle)))
		{
			if ($file != "." && $file != ".." && is_dir($path))
			{
				$list[]	= $file;
			}
		}
		closedir($handle);
		
		return $list;
	}
	return null;
}

// http://stackoverflow.com/questions/13076480/php-get-actual-maximum-upload-size
// Returns a file size limit in bytes based on the PHP upload_max_filesize
// and post_max_size
function file_upload_max_size()
{
	static $max_size = -1;

	if ($max_size < 0)
	{
		// Start with post_max_size.
		$max_size = parse_size(ini_get('post_max_size'));

		// If upload_max_size is less, then reduce. Except if upload_max_size is
		// zero, which indicates no limit.
		$upload_max = parse_size(ini_get('upload_max_filesize'));
		if ($upload_max > 0 && $upload_max < $max_size)
		{
			$max_size = $upload_max;
		}
	}
	return $max_size;
}

function CountDirFiles($dir)
{
	global $mysqli, $config;
	
	$query = '
		SELECT
			id
		FROM
			'.$config['prefix'].'files
		WHERE
			dir = "'.$mysqli->real_escape_string($dir).'"
	';
	$result = $mysqli->query($query);
	
	return $result->num_rows;
}

function parse_size($size)
{
	$unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
	$size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
	if ($unit)
	{
		// Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
		return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
	}
	else
	{
		return round($size);
	}
}

?>