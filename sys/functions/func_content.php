<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */
//

function ContentGetElementsList($templates, $mode = 'tree')
{
	global $phpRootPath, $installedModules;
	
	$list = array();
	
	
	foreach($installedModules as $installedModule)
	{
		if($installedModule['active'])
		{
			$path = $phpRootPath.'modules/'.$installedModule['name'].'/templates/fces/';
			
			$dirList = GetDirList($path);
			if(is_array($dirList))
			{
				foreach($dirList as $key => $value)
				{
					if($value == 'index.html')
					{
						continue;
					}
					$list['module_'.$installedModule['name']][] = 'module_'.$installedModule['name'].'_'.$value;
				}
			}
		}
	}
	
	foreach($templates as $template)
	{
		$path = $phpRootPath.'templates/'.$template.'/fces/';
		
		$dirList = GetDirList($path);
		if(is_array($dirList))
		{
			foreach($dirList as $key => $value)
			{
				if($value == 'index.html' || $value == 'page')
				{
					continue;
				}
				$list[$template][] = $value;
			}
		}
	}
	
	
	
	if($mode == 'merged')
	{
		$listSave = $list;
		$list = array();
		
		foreach($listSave as $template => $contentElements)
		{
			foreach($contentElements as $contentElement)
			{
				$list[$contentElement] = $template;
			}
		}
	}
	
	return $list;
}

function GetPage($id = 0)
{
	global $config, $pageTree;
	
	if($id == 0)
	{
		if(isset($_GET['site']))
		{
			if(is_numeric($_GET['site']))
			{
				$id = $_GET['site'];
			}
			else
			{
				$id = array('shortlink' => $_GET['site']);
			}
		}
		else if(isset($_GET['show']))
		{
			$id = array('shortlink' => $_GET['site']);
		}
		else
		{
			$id = getvar('homepage');
		}
	}
	
	$page = db_get($config['prefix'].'pages', $id, false);
	
	if(isset($page[0]))
	{
		$page = $page[0];
	}
	
	if(isset($pageTree) && method_exists($pageTree, 'GetRootline'))
	{
		$page['rootline'] = $pageTree->GetRootline();
	}
	
	return $page;
}

function CheckContentDependencies($current, $target)
{

	$targetSettingsFunc = 'fce_settings_'.$target['fce'];
	
	if(function_exists($targetSettingsFunc))
	{
		$targetSettingsFuncResult = $targetSettingsFunc('');
		
		if(isset($targetSettingsFuncResult['allowedParents']) && !CheckCString($targetSettingsFuncResult['allowedParents'], $current['fce']))
		{
			return false;
		}
			
		if(isset($targetSettingsFuncResult['allowedChildren']) && !CheckCString($targetSettingsFuncResult['allowedChildren'], $current['fce']))
		{
			return false;
		}
	}
	
	
	if(isset($current['allowedParents']) && !CheckCString($current['allowedParents'], $target['fce']))
	{
		return false;
	}
		
	if(isset($current['allowedChildren']) && !CheckCString($current['allowedChildren'], $target['fce']))
	{
	//	return false;
	}
	
	return true;
}

?>