<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */
//

function db_escape($string)
{
	global $config, $mysqli;
	
	if($string != '')
	{
		return $mysqli->real_escape_string($string);
	}
}

function db_insert($tab, $data, $keepId = false)
{
	global $config, $mysqli;
	
	$fields = array();
	$contents = array();
	
	foreach($data as $key => $value)
	{
		if(!$keepId && $key == 'id')
		{
			continue;
		}
		
		$fields[] = $mysqli->real_escape_string($key);
		$contents[] = '"'.$mysqli->real_escape_string($value).'"';
	}
	
	$query = '
		INSERT INTO '.$tab.' 
			('.implode(', ', $fields).') 
		VALUES
			('.implode(', ', $contents).')
	';
	
	$mysqli->query($query) or die(draw_error(get_label('mysql_error'), $mysqli->error));	
	
	return $mysqli->insert_id;
}

function db_get($tab, $search = '', $queryAdd = '', $idKey = true)
{
	global $config, $mysqli;
	
	if(is_numeric($search))
	{
		$query = '
			SELECT
				*
			FROM
				'.$tab.'
			WHERE
				id = '.$mysqli->real_escape_string($search).'
			'.$queryAdd.'
		';
		
		$result = $mysqli->query($query);
		$row = $result->fetch_assoc();
		
		return $row;
	}
	else if(is_array($search))
	{
		$where = array();
		$where[] = 'id != "0"';
		foreach($search as $key => $value)
		{
			$where[] = ''.$mysqli->real_escape_string($key).' = "'.$mysqli->real_escape_string($value).'"';
		}
	
		
		$query = '
			SELECT
				*
			FROM
				'.$tab.'
			WHERE
				'.implode(' AND ', $where).'
			'.$queryAdd.'
		';
		$data = array();
		
		$result = $mysqli->query($query);
		while($row = $result->fetch_assoc())
		{
			if($idKey)
			{
				$data[$row['id']] = $row;
			}
			else
			{
				$data[] = $row;
			}
		}
		
		return $data;
	}
	else if($search == '')
	{	
		
		$query = '
			SELECT
				*
			FROM
				'.$tab.'
				'.$queryAdd .'
		';
		$data = array();
		
		$result = $mysqli->query($query);
		while($row = $result->fetch_assoc())
		{
			$data[] = $row;
		}
		return $data;
	}
}

function db_update($tab, $data, $id, $keepId = false)
{
	global $config, $mysqli;
	
	$contents = array();
	
	$id = $mysqli->real_escape_string($id);
	
	foreach($data as $key => $value)
	{
		if(!$keepId && $key == 'id')
		{
			continue;
		}
		
		$contents[] = ''.$mysqli->real_escape_string($key).' = "'.$mysqli->real_escape_string($value).'"';
	}
	
	$query = '
		UPDATE '.$tab.' SET
			'.implode(', ', $contents).'
		WHERE
			id = "'.$id.'"
	';
	
	$mysqli->query($query)or die(draw_error(get_label('mysql_error'), $mysqli->error));
	
}

function db_delete($tab, $id)
{
	global $config, $mysqli;
	
	$query = '
		DELETE FROM '.$tab.'
		WHERE
			id = "'.$mysqli->real_escape_string($id).'"
	';
	
	$mysqli->query($query) or die(draw_error(get_label('mysql_error'), $mysqli->error));
}

function db_fix($modulename)
{
	global $config, $mysqli, $database, $phpRootPath;
	
	$path = $phpRootPath.'modules/'.$modulename.'/mod_db.php';
	
	if(file_exists($path))
	{
		include($path);
	}
	
	$databaseSave = $database;
	$database = array();
	include($phpRootPath.'sys/db.php');
	$database = array_replace_recursive($database, $databaseSave);

	$db = new dbcheck($config, $database);
	$db->load_db();
	$db->check();
	if(is_array($db->errors))
	{
		$db->repair();
	}
	
}

function db_save_name($str)
{	
	$forbidden = 'ACCESSIBLE,ACTION,ADD,AFTER,AGAINST,AGGREGATE,ALGORITHM,ALL,ALTER,ANALYZE,AND,ANY,AS,ASC,ASCII,ASENSITIVE,AT,AUTHORS,AUTOEXTEND_SIZE,AUTO_INCREMENT,AVG,AVG_ROW_LENGTH,BACKUP,BEFORE,BEGIN,BETWEEN,BIGINT,BINARY,BINLOG,BIT,BLOB,BLOCK,BOOL,BOOLEAN,BOTH,BTREE,BY,BYTE,CACHE,CALL,CASCADE,CASCADED,CASE,CATALOG_NAME,CHAIN,CHANGE,CHANGED,CHAR,CHARACTER,CHARSET,CHECK,CHECKSUM,CIPHER,CLASS_ORIGIN,CLIENT,CLOSE,COALESCE,CODE,COLLATE,COLLATION,COLUMN,COLUMNS,COLUMN_NAME,COMMENT,COMMIT,COMMITTED,COMPACT,COMPLETION,COMPRESSED,CONCURRENT,CONDITION,CONNECTION,CONSISTENT,CONSTRAINT,CONSTRAINT_CATALOG,CONSTRAINT_NAME,CONSTRAINT_SCHEMA,CONTAINS,CONTEXT,CONTINUE,CONTRIBUTORS,CONVERT,CPU,CREATE,CROSS,CUBE,CURRENT_DATE,CURRENT_TIME,CURRENT_TIMESTAMP,CURRENT_USER,CURSOR,CURSOR_NAME,DATA,DATABASE,DATABASES,DATAFILE,DATE,DATETIME,DAY,DAY_HOUR,DAY_MICROSECOND,DAY_MINUTE,DAY_SECOND,DEALLOCATE,DEC,DECIMAL,DECLARE,DEFAULT,DEFINER,DELAYED,DELAY_KEY_WRITE,DELETE,DESC,DESCRIBE,DES_KEY_FILE,DETERMINISTIC,DIRECTORY,DISABLE,DISCARD,DISK,DISTINCT,DISTINCTROW,DIV,DO,DOUBLE,DROP,DUAL,DUMPFILE,DUPLICATE,DYNAMIC,EACH,ELSE,ELSEIF,ENABLE,ENCLOSED,END,ENDS,ENGINE,ENGINES,ENUM,ERROR[a],ERRORS,ESCAPE,ESCAPED,EVENT,EVENTS,EVERY,EXECUTE,EXISTS,EXIT,EXPANSION,EXPLAIN,EXTENDED,EXTENT_SIZE,FALSE,FAST,FAULTS,FETCH,FIELDS,FILE,FIRST,FIXED,FLOAT,FLOAT4,FLOAT8,FLUSH,FOR,FORCE,FOREIGN,FOUND,FRAC_SECOND[b],FROM,FULL,FULLTEXT,FUNCTION,GENERAL[c],GEOMETRY,GEOMETRYCOLLECTION,GET_FORMAT,GLOBAL,GRANT,GRANTS,GROUP,HANDLER,HASH,HAVING,HELP,HIGH_PRIORITY,HOST,HOSTS,HOUR,HOUR_MICROSECOND,HOUR_MINUTE,HOUR_SECOND,IDENTIFIED,IF,IGNORE,IGNORE_SERVER_IDS[d],IMPORT,IN,INDEX,INDEXES,INFILE,INITIAL_SIZE,INNER,INNOBASE[e],INNODB[f],INOUT,INSENSITIVE,INSERT,INSERT_METHOD,INSTALL,INT,INT1,INT2,INT3,INT4,INT8,INTEGER,INTERVAL,INTO,INVOKER,IO,IO_THREAD,IPC,IS,ISOLATION,ISSUER,ITERATE,JOIN,KEY,KEYS,KEY_BLOCK_SIZE,KILL,LANGUAGE,LAST,LEADING,LEAVE,LEAVES,LEFT,LESS,LEVEL,LIKE,LIMIT,LINEAR,LINES,LINESTRING,LIST,LOAD,LOCAL,LOCALTIME,LOCALTIMESTAMP,LOCK,LOCKS,LOGFILE,LOGS,LONG,LONGBLOB,LONGTEXT,LOOP,LOW_PRIORITY,MASTER,MASTER_CONNECT_RETRY,MASTER_HEARTBEAT_PERIOD[g],MASTER_HOST,MASTER_LOG_FILE,MASTER_LOG_POS,MASTER_PASSWORD,MASTER_PORT,MASTER_SERVER_ID,MASTER_SSL,MASTER_SSL_CA,MASTER_SSL_CAPATH,MASTER_SSL_CERT,MASTER_SSL_CIPHER,MASTER_SSL_KEY,MASTER_SSL_VERIFY_SERVER_CERT,MASTER_USER,MATCH,MAXVALUE,MAX_CONNECTIONS_PER_HOUR,MAX_QUERIES_PER_HOUR,MAX_ROWS,MAX_SIZE,MAX_UPDATES_PER_HOUR,MAX_USER_CONNECTIONS,MEDIUM,MEDIUMBLOB,MEDIUMINT,MEDIUMTEXT,MEMORY,MERGE,MESSAGE_TEXT,MICROSECOND,MIDDLEINT,MIGRATE,MINUTE,MINUTE_MICROSECOND,MINUTE_SECOND,MIN_ROWS,MOD,MODE,MODIFIES,MODIFY,MONTH,MULTILINESTRING,MULTIPOINT,MULTIPOLYGON,MUTEX,MYSQL_ERRNO,NAME,NAMES,NATIONAL,NATURAL,NCHAR,NDB,NDBCLUSTER,NEW,NEXT,NO,NODEGROUP,NONE,NOT,NO_WAIT,NO_WRITE_TO_BINLOG,NULL,NUMERIC,NVARCHAR,OFFSET,OLD_PASSWORD,ON,ONE,ONE_SHOT,OPEN,OPTIMIZE,OPTION,OPTIONALLY,OPTIONS,OR,ORDER,OUT,OUTER,OUTFILE,OWNER,PACK_KEYS,PAGE,PARSER,PARTIAL,PARTITION,PARTITIONING,PARTITIONS,PASSWORD,PHASE,PLUGIN,PLUGINS,POINT,POLYGON,PORT,PRECISION,PREPARE,PRESERVE,PREV,PRIMARY,PRIVILEGES,PROCEDURE,PROCESSLIST,PROFILE,PROFILES,PROXY[h],PURGE,QUARTER,QUERY,QUICK,RANGE,READ,READS,READ_ONLY,READ_WRITE,REAL,REBUILD,RECOVER,REDOFILE,REDO_BUFFER_SIZE,REDUNDANT,REFERENCES,REGEXP,RELAY[i],RELAYLOG,RELAY_LOG_FILE,RELAY_LOG_POS,RELAY_THREAD,RELEASE,RELOAD,REMOVE,RENAME,REORGANIZE,REPAIR,REPEAT,REPEATABLE,REPLACE,REPLICATION,REQUIRE,RESET,RESIGNAL,RESTORE,RESTRICT,RESUME,RETURN,RETURNS,REVOKE,RIGHT,RLIKE,ROLLBACK,ROLLUP,ROUTINE,ROW,ROWS,ROW_FORMAT,RTREE,SAVEPOINT,SCHEDULE,SCHEMA,SCHEMAS,SCHEMA_NAME,SECOND,SECOND_MICROSECOND,SECURITY,SELECT,SENSITIVE,SEPARATOR,SERIAL,SERIALIZABLE,SERVER,SESSION,SET,SHARE,SHOW,SHUTDOWN,SIGNAL,SIGNED,SIMPLE,SLAVE,SLOW[j],SMALLINT,SNAPSHOT,SOCKET,SOME,SONAME,SOUNDS,SOURCE,SPATIAL,SPECIFIC,SQL,SQLEXCEPTION,SQLSTATE,SQLWARNING,SQL_BIG_RESULT,SQL_BUFFER_RESULT,SQL_CACHE,SQL_CALC_FOUND_ROWS,SQL_NO_CACHE,SQL_SMALL_RESULT,SQL_THREAD,SQL_TSI_DAY,SQL_TSI_FRAC_SECOND[k],SQL_TSI_HOUR,SQL_TSI_MINUTE,SQL_TSI_MONTH,SQL_TSI_QUARTER,SQL_TSI_SECOND,SQL_TSI_WEEK,SQL_TSI_YEAR,SSL,START,STARTING,STARTS,STATUS,STOP,STORAGE,STRAIGHT_JOIN,STRING,SUBCLASS_ORIGIN,SUBJECT,SUBPARTITION,SUBPARTITIONS,SUPER,SUSPEND,SWAPS,SWITCHES,TABLE,TABLES,TABLESPACE,TABLE_CHECKSUM,TABLE_NAME,TEMPORARY,TEMPTABLE,TERMINATED,TEXT,THAN,THEN,TIME,TIMESTAMP,TIMESTAMPADD,TIMESTAMPDIFF,TINYBLOB,TINYINT,TINYTEXT,TO,TRAILING,TRANSACTION,TRIGGER,TRIGGERS,TRUE,TRUNCATE,TYPE,TYPES,UNCOMMITTED,UNDEFINED,UNDO,UNDOFILE,UNDO_BUFFER_SIZE,UNICODE,UNINSTALL,UNION,UNIQUE,UNKNOWN,UNLOCK,UNSIGNED,UNTIL,UPDATE,UPGRADE,USAGE,USE,USER,USER_RESOURCES,USE_FRM,USING,UTC_DATE,UTC_TIME,UTC_TIMESTAMP,VALUE,VALUES,VARBINARY,VARCHAR,VARCHARACTER,VARIABLES,VARYING,VIEW,WAIT,WARNINGS,WEEK,WHEN,WHERE,WHILE,WITH,WORK,WRAPPER,WRITE,X509,XA,XML,XOR,YEAR,YEAR_MONTH';
	$forbidden .= ',RANK';
	$forbidden = explode(',', $forbidden);
	
	
	//if(preg_match('/^[a-zA-Z_][a-zA-Z0-9_]*$/', $str))
	if(preg_match('/^[a-z_][a-z0-9_]*$/', $str))
	{
		foreach($forbidden as $check)
		{
			if((strcasecmp($str, $check) == 0))
			{
				return false;
			}
		}
		return true;
	}
	else
	{
		return false;
	}
	
}

function db_table($table, $field, $type = 'text')
{
	global $database;
	$databaseSave = $database;
	
	if($type == 'id')
	{
		$database[$table]['fields'][$field] = array(
			'Type' => 'int(10)',
			'Key' => 'PRI',
			'Extra' => 'auto_increment'
		);
	}
	else
	{
		$database[$table]['fields'][$field]['Type']	= $type;
	}
	
	$database = array_replace_recursive($database, $databaseSave);
}

?>