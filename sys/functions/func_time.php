<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function online_time_query($access = false)
{
	global $user;
	
	$time = time();
	
	$where = '';
	
	if($user['perm'] != 'admin' && !$access)
	{
		$where .= '
			AND
			(
				(
					online_time_mode = ""
				)
				OR
				(
					online_time_mode = "start"
					AND
					online_time_start <= "'.$time.'"
				)
				OR
				(
					online_time_mode = "end"
					AND
					online_time_end > "'.$time.'"			
				)
				OR
				(
					online_time_mode = "startend"
					AND
					online_time_start <= "'.$time.'"
					AND
					online_time_end > "'.$time.'"
				)
			)
		';
	}
	return $where;
}

function online_time_check($row, $access = false, $cUser = '')
{
	global $user;
	
	if($cUser == '')
	{
		$cUser = $user;
	}
	
	if($cUser['perm'] == 'admin' || $access)
	{
		return true;
	}
	
	if(isset($row['online_time_mode']) && isset($row['online_time_start']) && isset($row['online_time_end']))
	{
		
		if($row['online_time_mode'] == '')
		{
			return true;
		}
		
		$time = time();
		
		if(
			(
				$row['online_time_mode'] == 'start'
				&&
				$row['online_time_start'] <= $time
			)
			||
			(
				$row['online_time_mode'] == 'end'
				&&
				$row['online_time_end'] > $time
			)
			||
			(
				$row['online_time_mode'] == 'startend'
				&&
				$row['online_time_start'] <= $time
				&&
				$row['online_time_end'] > $time
			)
		)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return true;
	}
}

function online_time_inputs($category = '')
{
	$inputs = array();
	
	$options = array();
	$options[''] = array(
		'lang' => get_label('online_time_mode_none')
	);
	
	$options['start'] = array(
		'lang' => get_label('online_time_mode_start')
	);

	$options['end'] = array(
		'lang' => get_label('online_time_mode_end')
	);

	$options['startend'] = array(
		'lang' => get_label('online_time_mode_startend')
	);	
	
	$inputs[] = array(
		'type'	=> 'select',
		'name'	=> 'online_time_mode',
		'category' => $category,
		'lang'	=> get_label('online_time_mode'),
		'options' => $options
	);		
	
	$inputs[] = array(
		'type'	=> 'timepicker',
		'name'	=> 'online_time_start',
		'category' => $category,
		'lang'	=> get_label('online_time_start')
	);
	
	$inputs[] = array(
		'type'	=> 'timepicker',
		'name'	=> 'online_time_end',
		'category' => $category,
		'lang'	=> get_label('online_time_end')
	);
	
	return $inputs;
}

function online_time($row)
{	
	$data = array(
		'mode' => '',
		'time' => 0
	);
	
	if(isset($row['online_time_mode']) && $row['online_time_mode'] != '' && isset($row['online_time_start']) && isset($row['online_time_end']))
	{
		$time = time();

		if($time < $row['online_time_start'] && $row['online_time_mode'] != 'end')
		{
			$data['mode'] = 'online_in';
			$data['time'] = $row['online_time_start']-$time;
		}
		
		if($time >= $row['online_time_start'] && $row['online_time_mode'] != 'end' && $row['online_time_mode'] != 'startend')
		{
			$data['mode'] = 'online_since';
			$data['time'] = $time-$row['online_time_start'];
		}		
		
		if($time <= $row['online_time_end'] && $row['online_time_mode'] != 'start')
		{
			
			$data['mode'] = 'offline_in';
			$data['time'] = $row['online_time_end']-$time;
		}
		
		if($time > $row['online_time_end'] && $row['online_time_mode'] != 'start')
		{
			$data['mode'] = 'offline_since';
			$data['time'] = $time-$row['online_time_end'];
		}	
		
	}
	
	return $data;
}

function get_year($year)
{
	return array(
		'start' => strtotime($year.'-01-01'),
		'end' => strtotime($year.'-12-31, 23:59:59')
	);
	
}

?>