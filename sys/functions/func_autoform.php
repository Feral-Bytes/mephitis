<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function autoform_autocomplete($input, $tab){

	global $mysqli;

	$ajax = '';
	if(isset($input['autocomplete']) AND isset($_GET['ajax']) AND $_GET['ajax'] == 'get'.$input['name'].'ByLetters'){
		
		if(isset($input['autocomplete']['search_tab']) AND $input['autocomplete']['search_tab'] != ''){
			$ajax_data['tab'] = $input['autocomplete']['search_tab'];
		}else{
			$ajax_data['tab'] = $tab;
		}
		if(isset($input['autocomplete']['search_field']) AND $input['autocomplete']['search_field'] != ''){
			$ajax_data['field'] = $input['autocomplete']['search_field'];
		}else{
			$ajax_data['field'] = $input['name'];
		}
		if(isset($input['autocomplete']['search_order']) AND $input['autocomplete']['search_order'] != ''){
			$ajax_data['order'] = ' ORDER BY '.$input['autocomplete']['search_order'].'';;
		}else{
			$ajax_data['order'] = ' ORDER BY '.$input['name'].' ASC';
		}
		
		if(isset($input['autocomplete']['multiple']) AND $input['autocomplete']['multiple'] == '1'){
			$ajax_data['group'] = ' GROUP BY '.$ajax_data['field'].'';
		}else{
			$ajax_data['group'] = '';
		}
		
		
		$result = $mysqli->query('SELECT * FROM '.$ajax_data['tab'].' WHERE '.$ajax_data['field'].' like "'.$mysqli->real_escape_string($_REQUEST['term']).'%" '.$ajax_data['order'].'');
		 

		$data = array();
		if($result && $result->num_rows)
		{
			while($row = $result->fetch_assoc())
			{
				$data[] = array(
					'label' => $row[$ajax_data['field']],
					'value' => $row[$ajax_data['field']]
				);
			}
		}
		
		echo json_encode($data);
		
		exit;
		
	}

}

function autoform_conditional_field($condition, $inputs){
	if(isset($_POST['send'])){
		$save = '';
		$target = $condition;
		foreach($inputs as $key => $input){
			if(isset($input['conditional_field'])){
				if($input['conditional_field'] == $target){
					$save = $input;
				}
				unset($inputs[$key]);
			}
		}
		if($save != ''){
			$inputs[] = $save;
		}
	}
	
	return $inputs;
}

function chk_captcha($mode = '', $draw = ''){

	global $user, $phpRootPath;
	
	if($mode == '' OR $mode == 'submit'){
		if($user['id'] == '0'){
			
			if(getvar('reCAPTCHA') == '1')
			{
				if(isset($_POST['g-recaptcha-response']))
				{
					require_once ($phpRootPath.'libs/recaptcha-master/src/autoload.php');
					
					$recaptcha = new \ReCaptcha\ReCaptcha(getvar('reCAPTCHA_key_private'));
					
					$resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
					
					if($resp->isSuccess())
					{
						return 1;
					}
					else
					{
						return 0;
					}
				}
				else
				{
					return 0;
				}
			}
			else
			{
			
				if(isset($_SESSION['captcha_spam']) AND isset($_POST['captcha']) AND $_POST['captcha'] == $_SESSION['captcha_spam']){
					unset($_SESSION['captcha_spam']);
					return '1';
				}else{
					return '0';
				}
			
			}
			
		}else{
			return '1';
		}
	}
	
	if($mode == 'draw'){
		
		if(getvar('reCAPTCHA') == '1')
		{
			return '
				<div class="g-recaptcha" data-sitekey="'.getvar('reCAPTCHA_key_public').'"></div>
				<script type="text/javascript"
						src="https://www.google.com/recaptcha/api.js?hl='.$user['lang'].'">
				</script>
				<noscript>
				  <div style="width: 302px; height: 422px;">
					<div style="width: 302px; height: 422px; position: relative;">
					  <div style="width: 302px; height: 422px; position: absolute;">
						<iframe src="https://www.google.com/recaptcha/api/fallback?k='.getvar('reCAPTCHA_key_public').'"
								frameborder="0" scrolling="no"
								style="width: 302px; height:422px; border-style: none;">
						</iframe>
					  </div>
					  <div style="width: 300px; height: 60px; border-style: none;
								  bottom: 12px; left: 25px; margin: 0px; padding: 0px; right: 25px;
								  background: #f9f9f9; border: 1px solid #c1c1c1; border-radius: 3px;">
						<textarea id="g-recaptcha-response" name="g-recaptcha-response"
								  class="g-recaptcha-response"
								  style="width: 250px; height: 40px; border: 1px solid #c1c1c1;
										 margin: 10px 25px; padding: 0px; resize: none;" >
						</textarea>
					  </div>
					</div>
				  </div>
				</noscript>
			';
		}
		
		if($draw == 'lang'){
			return get_label('captcha');
		}
		if($draw == 'lang_confirm'){
			return (get_label('captcha_confirm').'<font color="red">*</font>');
		}
		if($draw == 'img'){
			return '<img src="libs/captcha/captcha.php" border="0" title="Captcha">';
		}
		if($draw == 'input'){
			return '<input type="text" name="captcha" size="5">';
		}
		
	}
	
}

function autoform($tab, $inputs, $php, $link, $header, $force = NULL, $mode = NULL, $actionname = 'action', $backLink = 'back2', $backLinkName = '', $dismissibleAlert = false, $infobox = ''){

	global $config, $mysqli, $lang, $user, $autoformuse, $autoform_success, $autoform_lock, $msg_stack, $modulename, $autoform_last_insert_id, $autoform_last_edit_id, $autoform_last_del_id, $phpRootPath;
	$autoformuse = 1;	
	
	if(isset($autoform_lock))
	{
		return;
	}
	
	//new entry in database
	if(isset($_GET[$actionname]) AND $_GET[$actionname] == 'new' OR $force == 'new'){
		
		$ok = true;
		$form_msgs = '';
		$hasdata = false;
		
		$captcha = chk_captcha();
		
		if(isset($_POST['send']) AND $captcha != '1'){
			$form_msgs .= draw_alert($lang['captcha_wrong'], 'danger');
		}		
		
		if(isset($_POST['send']) AND $captcha == '1'){

			$formhasfiles = '';

			$cols = '';
			$data = '';
			$file = '';
			foreach($inputs as $input){
				
				if(!isset($input['name']) OR (isset($input['disabled']) AND  $input['disabled'] != false))continue;
				
				if(isset($input['required']) AND $input['required'] == '1' AND !isset($input['required_msg']))
				{
					$input['required_msg'] = $input['lang'].' '.get_label('required_msg');
					if(isset($input['category']))
					{
						$input['required_msg'] = get_label($input['category']).':&nbsp;'.$input['required_msg'];
					}
				}			
				
				$hasdata = true;
				
				if($input['name'] != 'crypt_user_select'){
					
					$db_field = $input['name'];
					$post_field = $input['name'];
			
					if(isset($input['conditional_field'])){
						$db_field = str_replace('_'.$input['conditional_field'], '', $input['name']);
						$post_field = $input['name'].'_'.$input['conditional_field'];
					}
					
					if(isset($input['validation_func']) && trim($input['validation_func'] != '') && isset($_POST[$post_field]) && $_POST[$post_field] != '')
					{
						$validation = $input['validation_func']($_POST[$post_field]);
						if(!$validation)
						{
							$ok = false;
							if(!isset($input['not_valid_msg']))
							{
								$input['not_valid_msg'] = $input['lang'].' '.get_label('not_valid_msg');
								if(isset($input['category']))
								{
									$input['not_valid_msg'] = get_label($input['category']).':&nbsp;'.$input['not_valid_msg'];
								}
							}
							$form_msgs .= draw_alert($input['not_valid_msg'], 'danger');
						}
					}
					
					if(isset($input['nodb']) && $input == true)
					{
						continue;
					}
					
					$cols .= $db_field.', ';
					
					if($input['type'] == 'var' OR $input['type'] == 'fix'){
						$data .= '"'.$input['value'].'", ';
					}else if($input['type'] == 'checkbox'){
						if(isset($_POST[$post_field])){
							$input['value'] = '1';
						}else{
							$input['value'] = '0';
						}
						if(isset($input['required']) AND $input['required'] == '1'){
							if($input['value'] == '0'){
								$ok = false;
								$form_msgs .= draw_alert($input['required_msg'], 'danger');	
								//die(draw_error($lang['error'], $input['required_msg']));
							}
						}
						$data .= '"'.$input['value'].'", ';
					}else if($input['type'] == 'select' AND isset($input['multiple']) AND $input['multiple'] == '1'){
						$array = '';
						if(isset($_POST[$post_field]) && is_array($_POST[$post_field]))foreach($_POST[$post_field] as $value){
							$array .= $value.';';
						}
						if(isset($input['required']) AND $input['required'] == '1'){
							if(!isset($_POST[$post_field]) OR $_POST[$post_field] == '' OR !is_array($_POST[$post_field])){
								$ok = false;
								$form_msgs .= draw_alert($input['required_msg'], 'danger');	
								//die(draw_error($lang['error'], $input['required_msg']));
							}
						}
						$data .= '"'.substr($array, 0, -1).'", ';
					}else if($input['type'] == 'password'){
						if(isset($_POST[$post_field]) AND $_POST[$post_field] != '' OR isset($_POST[$post_field.'_v']) AND $_POST[$post_field.'_v'] != ''){
							if($_POST[$post_field] != $_POST[$post_field.'_v']){
								$ok = false;
								$form_msgs .= draw_alert($lang['pass_equal_error'], 'danger');
							}
							
							$data .= '"'.$mysqli->real_escape_string(password_crypt($_POST[$post_field])).'", ';
						}else{
							$ok = false;
							die(draw_error($lang['error'], $lang['no_pass_error']));
						}
					}else if($input['type'] == 'file' OR $input['type'] == 'emoji'){
						$formhasfiles .= '1';
						
						$file = upload_file($input, 'new');
						if(is_array($file)){
							$data .= '"'.$mysqli->real_escape_string($file['file']).'", ';
							if($input['name'] != 'thumb' && $input['name'] == 'file'){
								$cols .= 'file_name, ';
								$data .= '"'.$mysqli->real_escape_string($file['file_name']).'", ';
								$cols .= 'size, ';
								$data .= '"'.$mysqli->real_escape_string($file['size']).'", ';
								$cols .= 'type, ';
								$data .= '"'.$mysqli->real_escape_string($file['type']).'", ';
								$cols .= 'path, ';
								$data .= '"'.$mysqli->real_escape_string($file['path']).'", ';
							}
						}else{
							
							if(isset($file) AND is_numeric($file))
							{
								$data .= '"'.$file.'", ';
							}
							else
							{
								$data .= '"", ';
							}
						}
						if(isset($input['required']) AND $input['required'] == '1'){
							if(!is_array($file) AND $_GET['action'] == 'new'){
								$form_msgs .= draw_alert($input['required_msg'], 'danger');	
								//die(draw_error($lang['error'], $input['required_msg']));
							}
						}
					}else{
						if((isset($input['simple']) AND $input['simple'] == '1') || (isset($input['wysiwyg']) AND ($input['wysiwyg'] == 'bbcode' OR $input['wysiwyg'] == 'classic'))){
							$_POST[$post_field] = simple_post(htmlspecialchars($_POST[$post_field]));
						}
						if($input['type'] == 'text'){
							if(isset($_POST[$post_field]))
							{
								$_POST[$post_field] = htmlspecialchars($_POST[$post_field]);
							}
							if(isset($input['entertime']) AND $input['entertime'] == '1'){
								$_POST[$post_field] = strtotime($_POST[$post_field]);
							}
							if(isset($input['timepicker']) AND $input['timepicker'] == '1'){
								$_POST[$post_field] = 0;
								if(isset($_POST[$post_field.'_d']))
								{
									$_POST[$post_field] += $_POST[$post_field.'_d']*3600*24;
								}								
								if(isset($_POST[$post_field.'_h']))
								{
									$_POST[$post_field] += $_POST[$post_field.'_h']*3600;
								}
								if(isset($_POST[$post_field.'_m']))
								{
									$_POST[$post_field] += $_POST[$post_field.'_m']*60;
								}
								if(isset($_POST[$post_field.'_s']))
								{
									$_POST[$post_field] += $_POST[$post_field.'_s'];
								}
							}

							if(isset($input['datepicker']))
							{
								$_POST[$post_field] = date('Y-m-d', strtotime($_POST[$post_field]));
							}
						}
						else if($input['type'] == 'timepicker')
						{
							$timeofday = 0;
							if(isset($_POST[$post_field.'_h']))
							{
								$timeofday += $_POST[$post_field.'_h']*3600;
							}
							if(isset($_POST[$post_field.'_m']))
							{
								$timeofday += $_POST[$post_field.'_m']*60;
							}
							if(isset($_POST[$post_field.'_s']))
							{
								$timeofday += $_POST[$post_field.'_s'];
							}
							
							$datetime = strtotime($_POST[$post_field.'_date']);
							
							$time = $datetime + $timeofday;
							
							$_POST[$post_field] = $time;
						}						
						
						if(isset($input['required']) AND $input['required'] == '1'){
							if(!isset($_POST[$post_field]) || $_POST[$post_field] == ''){
								$_POST[$post_field] = '';
								$ok = false;
								$form_msgs .= draw_alert($input['required_msg'], 'danger');								
								//die(draw_error($lang['error'], $input['required_msg']));
							}
						}
						if(isset($_POST[$post_field]))
						{
							$data .= '"'.$mysqli->real_escape_string($_POST[$post_field]).'", ';
						}
						else
						{
							$data .= '"", ';
						}
					}
				}
			}
			
			if($ok && $hasdata)
			{
				$post = '
					INSERT INTO '.$tab.' 
						('.substr($cols, 0, -2).') 
					VALUES
						('.substr($data, 0, -2).')
				';
				
				$autoform_success = 1;
				
				$do_post = $mysqli->query($post) or die(draw_error($lang['mysql_error'], $mysqli->error));
				
				$autoform_last_insert_id = $mysqli->insert_id;
				
				$msg_stack .= draw_alert($lang['posted'], 'success', $dismissibleAlert, $backLink, $backLinkName);	
				//draw_msg($header, $lang['posted'].'<br><a href="#" onclick="history.go(-2)">'.$lang['back'].'</a>', '');
			}
		}
		
		if(!isset($_POST['send']) || !$ok || chk_captcha() == '0')
		{
			$formhasfiles = '';
			foreach($inputs as $key => $input){
				
				if(!isset($input['name']))continue;
				
				$db_field = $input['name'];
				$post_field = $input['name'];
				if(isset($input['conditional_field'])){
					$db_field = str_replace('_'.$input['conditional_field'], '', $input['name']);
					$post_field = $input['name'].'_'.$input['conditional_field'];
				}
				
				$inputs[$key]['name'] = $post_field;

				if($input['type'] == 'file' OR $input['type'] == 'emoji' OR $input['type'] == 'media'){
					$formhasfiles .= '1';
				}
				if(isset($_POST[$input['name']]) AND $input['type'] != 'file'){
					$inputs[$key]['value'] = $_POST[$input['name']];
				}
				if($input['type'] == 'text'){
					autoform_autocomplete($input, $tab);
				}
			}
			
			if($force == ''){
				$action = $actionname.'=new';
			}else{
				$action = '';
			}
			if($formhasfiles == ''){
				$fileadd = '';
			}else{
				$fileadd  = ' enctype="multipart/form-data"';
			}

			chk_captcha('submit');
			

			$formdata = array(
				'name' => 'post',
				'action' => ''.$php.'?'.$link.''.$action.'',
				'method' => 'POST',
				'fileadd' => $fileadd,
				'actionname' => $actionname,
				'mode' => $mode,
				'form_msgs' => $form_msgs,
				'infobox' => $infobox
			);
			
			if(!$ok && $mode == 'modal')
			{
				die(draw_error($lang['error'], draw_form($inputs, $header, $formdata)));
			}
			
			return draw_form($inputs, $header, $formdata);
		}
		

	// edit an entry
	}else if(isset($_GET[$actionname]) AND $_GET[$actionname] == 'edit' AND isset($_GET['itemid']) OR $force == 'edit'){

		$ok = true;
		$form_msgs = '';
		$hasdata = false;
		
		$result = $mysqli->query('SELECT * FROM '.$tab.' WHERE id = "'.$mysqli->real_escape_string($_GET['itemid']).'"') or die(draw_error($lang['mysql_error'], $mysqli->error));
		$row = $result->fetch_assoc();
		
		if(!is_array($row))
		{
			die(draw_error($lang['error'], $lang['404']));
		}
		
		if(isset($_POST['send'])){

			$formhasfiles = '';

			$data = '';
			foreach($inputs as $input){
				
				if(!isset($input['name']) OR (isset($input['disabled']) AND  $input['disabled'] != false))continue;
				
				$hasdata = true;
				
				if(isset($input['required']) AND $input['required'] == '1' AND !isset($input['required_msg']))
				{
					$input['required_msg'] = $input['lang'].' '.get_label('required_msg');
					if(isset($input['category']))
					{
						$input['required_msg'] = get_label($input['category']).':&nbsp;'.$input['required_msg'];
					}
				}				
				
				if($input['name'] != 'crypt_user_select'){
					$db_field = $input['name'];
					$post_field = $input['name'];
					
					if(isset($input['validation_func']) && trim($input['validation_func'] != '') && isset($_POST[$post_field]) && $_POST[$post_field] != '')
					{
						$validation = $input['validation_func']($_POST[$post_field]);
						if(!$validation)
						{
							$ok = false;
							if(!isset($input['not_valid_msg']))
							{
								$input['not_valid_msg'] = $input['lang'].' '.get_label('not_valid_msg');
								if(isset($input['category']))
								{
									$input['not_valid_msg'] = get_label($input['category']).':&nbsp;'.$input['not_valid_msg'];
								}
							}
							$form_msgs .= draw_alert($input['not_valid_msg'], 'danger');	
						}
					}					
					
					if(isset($input['nodb']) && $input == true)
					{
						continue;
					}
					
					if(isset($input['conditional_field'])){
						$db_field = str_replace('_'.$input['conditional_field'], '', $input['name']);
						$post_field = $input['name'].'_'.$input['conditional_field'];
					}

					if($input['type'] == 'var'){
						$data .= $db_field.' = "'.$input['value'].'",';
					}else if($input['type'] == 'checkbox'){
						if(isset($_POST[$post_field])){
							$input['value'] = '1';
						}else{
							$input['value'] = '0';
						}
						if(isset($input['required']) AND $input['required'] == '1'){
							if($input['value'] == '0'){
								$ok = false;
								$form_msgs .= draw_alert($input['required_msg'], 'danger');									
								//die(draw_error($lang['error'], $input['required_msg']));
							}
						}
						$data .= $db_field.' = "'.$input['value'].'",';
					}else if($input['type'] == 'select' AND isset($input['multiple']) AND $input['multiple'] == '1'){
						$array = '';
						if(isset($_POST[$post_field]) AND is_array($_POST[$post_field]))foreach($_POST[$post_field] as $value){
							$array .= $value.';';
						}
						if(isset($input['required']) AND $input['required'] == '1'){
							if(!isset($_POST[$post_field]) OR $_POST[$post_field] == '' OR !is_array($_POST[$post_field])){
								$ok = false;
								$form_msgs .= draw_alert($input['required_msg'], 'danger');									
								//die(draw_error($lang['error'], $input['required_msg']));
							}
						}
						$data .= $input['name'].' = "'.substr($array, 0, -1).'",';
					}else if($input['type'] == 'password'){
						if(isset($_POST[$post_field]) AND $_POST[$post_field] != '' OR isset($_POST[$post_field.'_v']) AND $_POST[$post_field.'_v'] != ''){
							if($_POST[$post_field] != $_POST[$post_field.'_v']){
								$ok = false;
								$form_msgs .= draw_alert($lang['pass_equal_error'], 'danger');									
								//die(draw_error($lang['error'], $lang['pass_equal_error']));
							}
							$data .= $input['name'].' = "'.$mysqli->real_escape_string(password_crypt($_POST[$input['name']])).'",';
						}
					}else if($input['type'] == 'file' OR $input['type'] == 'emoji'){
						$formhasfiles .= '1';
						$input['value'] = $row[$db_field];
						$input['current_data'] = $row;
						
						$file = upload_file($input, 'edit');
						if(is_array($file)){
							$data .= $db_field.' = "'.$mysqli->real_escape_string($file['file']).'",';
							if($input['name'] != 'thumb' && $input['name'] == 'file'){
								$data .= 'file_name = "'.$mysqli->real_escape_string($file['file_name']).'",';
								$data .= 'size = "'.$mysqli->real_escape_string($file['size']).'",';
								$data .= 'type = "'.$mysqli->real_escape_string($file['type']).'",';
								$data .= 'path = "'.$mysqli->real_escape_string($file['path']).'",';
							}
						}
						if(!is_array($file) AND isset($input['crypt_user_select']) AND $input['crypt_user_select'] == '1' AND $input['current_data']['crypt'] != $_POST['crypt_user_select'] AND $input['name'] != 'thumb'){
								change_crypt($input['current_data'], $mysqli->real_escape_string($_POST['crypt_user_select']), $mysqli->real_escape_string($input['crypt_key']));
								$data .= 'crypt = "'.$mysqli->real_escape_string($_POST['crypt_user_select']).'",';
								$data .= 'crypt_key = "'.$mysqli->real_escape_string($input['crypt_key']).'",';
						}
						if(is_numeric($file))
						{
							$data .= $db_field.' = "'.$mysqli->real_escape_string($file).'",';
						}
					}else if($input['type'] != 'fix'){
						if((isset($input['simple']) AND $input['simple'] == '1') || (isset($input['wysiwyg']) AND ($input['wysiwyg'] == 'bbcode' OR $input['wysiwyg'] == 'classic'))){
							$_POST[$post_field] = simple_post(htmlspecialchars($_POST[$post_field]));
						}
						if($input['type'] == 'text'){
							if(isset($_POST[$post_field]))
							{
								$_POST[$post_field] = htmlspecialchars($_POST[$post_field]);
							}
							
							if(isset($input['entertime']) AND $input['entertime'] == '1'){
								$_POST[$post_field] = strtotime($_POST[$post_field]);
							}
							if(isset($input['timepicker']) AND $input['timepicker'] == '1'){
								$_POST[$post_field] = 0;
								if(isset($_POST[$post_field.'_d']))
								{
									$_POST[$post_field] += $_POST[$post_field.'_d']*3600*24;
								}								
								if(isset($_POST[$post_field.'_h']))
								{
									$_POST[$post_field] += $_POST[$post_field.'_h']*3600;
								}
								if(isset($_POST[$post_field.'_m']))
								{
									$_POST[$post_field] += $_POST[$post_field.'_m']*60;
								}
								if(isset($_POST[$post_field.'_s']))
								{
									$_POST[$post_field] += $_POST[$post_field.'_s'];
								}
							}
							
							if(isset($input['datepicker']))
							{
								$_POST[$post_field] = date('Y-m-d', strtotime($_POST[$post_field]));
							}							
						}
						else if($input['type'] == 'timepicker')
						{
							$timeofday = 0;
							if(isset($_POST[$post_field.'_h']))
							{
								$timeofday += $_POST[$post_field.'_h']*3600;
							}
							if(isset($_POST[$post_field.'_m']))
							{
								$timeofday += $_POST[$post_field.'_m']*60;
							}
							if(isset($_POST[$post_field.'_s']))
							{
								$timeofday += $_POST[$post_field.'_s'];
							}
							
							$datetime = strtotime($_POST[$post_field.'_date']);
							
							$time = $datetime + $timeofday;
							
							$_POST[$post_field] = $time;
						}
						
						if(isset($input['required']) AND $input['required'] == '1'){
							if(!isset($_POST[$post_field]) || $_POST[$post_field] == ''){
								$_POST[$post_field] = '';
								$ok = false;
								$form_msgs .= draw_alert($input['required_msg'], 'danger');								
								//die(draw_error($lang['error'], $input['required_msg']));
							}
						}
						
						if(isset($_POST[$post_field]))
						{
							$data .= $db_field.' = "'.$mysqli->real_escape_string($_POST[$post_field]).'",';
						}
						else
						{
							$data .= $db_field.' = "",';
						}						
					}
				}
			}
			
			if($ok && $hasdata)
			{
				$new = '
					UPDATE '.$tab.' SET
						'.substr($data, 0, -1).'
					WHERE
						id = "'.$mysqli->real_escape_string($_GET['itemid']).'"
				';
				$update = $mysqli->query($new)or die(draw_error($lang['mysql_error'], $mysqli->error));
				
				$autoform_success = 1;
				
				$autoform_last_edit_id = $mysqli->real_escape_string($_GET['itemid']);
				
				//draw_msg($header, $lang['edited'].'<br><a href="#" onclick="history.go(-2)">'.$lang['back'].'</a>', '');
				$msg_stack .= draw_alert($lang['edited'], 'success', $dismissibleAlert, $backLink, $backLinkName);					
			}
		}
		if(!isset($_POST['send']) || !$ok)
		{
			$inputs_edit = $inputs;
			$formhasfiles = '';
			foreach($inputs as $key => $input){
				
				if(!isset($input['name']))continue;
				
				if($input['name'] != 'crypt_user_select'){
					$db_field = $input['name'];
					$post_field = $input['name'];
					if(isset($input['conditional_field'])){
						$db_field = str_replace('_'.$input['conditional_field'], '', $input['name']);
						$post_field = $input['name'].'_'.$input['conditional_field'];
					}
					
					$inputs_edit[$key]['name'] = $post_field;
					
					if($input['type'] == 'text'){
						$inputs_edit[$key]['value'] = $row[$db_field];
						autoform_autocomplete($input, $tab);
					}else if($input['type'] == 'textarea'){
						if((isset($input['simple']) AND $input['simple'] == '1') || (isset($input['wysiwyg']) AND ($input['wysiwyg'] == 'bbcode' OR $input['wysiwyg'] == 'classic'))){
							$row[$db_field] = simple_post_form($row[$db_field]);
						}
						$inputs_edit[$key]['value'] = $row[$db_field];
					}else if($input['type'] == 'radio'){
						foreach($input['options'] as $value => $option){
							if($value == $row[$db_field]){
								$inputs_edit[$key]['options'][$value]['active'] = ' checked';
							}
						}
					}else if($input['type'] == 'select'){
					
						if(isset($input['groups']) AND $input['groups'] == '1'){
							
							foreach($input['options'] as $grp => $grp_array){
								if(isset($input['multiple']) AND $input['multiple'] == '1'){
									$array = explode(';', $row[$db_field]);
									foreach($array as $db_value){
										foreach($grp_array['options'] as $value => $option){
											if(!isset($option['active']))$option['active'] = '';
											if($value == $db_value){
												$inputs_edit[$key]['options'][$grp]['options'][$value]['active'] = ' selected';
											}
										}
									}
								}else{
									foreach($grp_array['options'] as $value => $option){
										if(!isset($option['active']))$option['active'] = '';
										if($value == $row[$db_field] AND $option['active'] == ''){
											$inputs_edit[$key]['options'][$grp]['options'][$value]['active'] = ' selected';
										}
									}
								}
							}
							
						}else{
					
							if(isset($input['multiple']) AND $input['multiple'] == '1'){
								$array = explode(';', $row[$db_field]);
								foreach($array as $db_value){
									if(is_array($input['options']))
									foreach($input['options'] as $value => $option){
										if(!isset($option['active']))$option['active'] = '';
										if($value == $db_value){
											$inputs_edit[$key]['options'][$value]['active'] = ' selected';
										}
									}
								}
							}else{
								if(is_array($input['options']))
								foreach($input['options'] as $value => $option){
									if(!isset($option['active']))$option['active'] = '';
									if($value == $row[$db_field] AND $option['active'] == ''){
										$inputs_edit[$key]['options'][$value]['active'] = ' selected';
									}
								}
							}
						
						}
						
					}else if($input['type'] == 'checkbox'){
						if($row[$db_field] == '1'){
							$inputs_edit[$key]['active'] = ' checked';
						}
					}else if($input['type'] == 'file' OR $input['type'] == 'emoji' OR $input['type'] == 'media'){
						$formhasfiles .= '1';
						$file_info = '';
						if(is_numeric($row[$db_field]))
						{
							$file_info = file_info($row[$db_field]);
						}

						$inputs_edit[$key]['current_data'] = $row;
						$inputs_edit[$key]['value'] = $row[$db_field];
						
						if(is_array($file_info))
						{
							$inputs_edit[$key]['current_data'] = $file_info;
							$inputs_edit[$key]['value'] = $row[$db_field];
						}
						
					}else if($input['type'] == 'timepicker'){
						$inputs_edit[$key]['value'] = $row[$db_field];
					}
				}
			}

			if($force == ''){
				$action = $actionname.'=edit';
			}else{
				$action = '';
			}
			if($formhasfiles == ''){
				$fileadd = '';
			}else{
				$fileadd  = ' enctype="multipart/form-data"';
			}
		

			$formdata = array(
				'name' => 'post',
				'action' => ''.$php.'?'.$link.''.$action.'&itemid='.$_GET['itemid'].'',
				'method' => 'POST',
				'fileadd' => $fileadd,
				'actionname' => $actionname,
				'mode' => $mode,
				'form_msgs' => $form_msgs,
				'infobox' => $infobox
			);
			
			if(!$ok && $mode == 'modal')
			{
				die(draw_error($lang['error'], draw_form($inputs, $header, $formdata)));
			}			
			
			return draw_form($inputs_edit, $header, $formdata);
			

		}
	// delete an entry
	}else if(isset($_GET[$actionname]) AND $_GET[$actionname] == 'del' AND isset($_GET['itemid']) OR $force == 'del'){

		if(isset($_GET['sure'])){
			
			$result = $mysqli->query('SELECT * FROM '.$tab.' WHERE id = "'.$mysqli->real_escape_string($_GET['itemid']).'"') or die(draw_error($lang['mysql_error'], $mysqli->error));
			$row = $result->fetch_assoc();
			
			if(!is_array($row))
			{
				die(draw_error($lang['error'], $lang['404']));
			}			
			
			$formhasfiles = '';
			if(isset($row) && is_array($row))
			foreach($inputs as $input){
				if(($input['type'] == 'file' OR $input['type'] == 'emoji' OR ($input['type'] == 'media' AND isset($input['cleanupdb']) AND $input['cleanupdb'] == true)) AND $input['name'] != 'thumb'){
					
					if(is_numeric($row[$input['name']]))
					{
						del_file($row[$input['name']]);
					}
					else
					{
						if(file_exists($phpRootPath.$input['path'].$row[$input['name']]) AND isset($row[$input['name']]) AND $row[$input['name']] !=''){
							unlink($phpRootPath.$input['path'].$row[$input['name']]);
						}
					}
				}
			}
			
			$query = '
				DELETE FROM '.$config['prefix'].'comments
				WHERE
					object = "'.$mysqli->real_escape_string($modulename).'"
					AND
					object_id = "'.$mysqli->real_escape_string($_GET['itemid']).'"
			';
			$del = $mysqli->query($query) or die(draw_error($lang['mysql_error'], $mysqli->error));				
			
			$query = '
				DELETE FROM '.$tab.'
				WHERE
					id = "'.$mysqli->real_escape_string($_GET['itemid']).'"
			';
			$del = $mysqli->query($query) or die(draw_error($lang['mysql_error'], $mysqli->error));
			
			$autoform_success = 1;
			
			$autoform_last_del_id = $mysqli->real_escape_string($_GET['itemid']);
			
			//draw_msg($header, $lang['deleted'].'<br><a href="#" onclick="history.go(-2)">'.$lang['back'].'</a>', '');
			$msg_stack .= draw_alert($lang['deleted'], 'success', $dismissibleAlert, $backLink, $backLinkName);
			
		}else{

			if($force == ''){
				$action = $actionname.'=del';
			}else{
				$action = '';
			}
			
			$noLink = $php.'?'.$link;
			if($backLink != 'back2')
			{
				$noLink = $backLink;
			}
			
			$msg = draw_confirm_dialog($lang['del_confirm'], $php.'?'.$link.''.$action.'&itemid='.$_GET['itemid'].'&sure=yes', $noLink);
			//draw_msg($header, $msg, '');
			$msg_stack .= draw_alert($msg, 'warning', $dismissibleAlert);
		}

	}

}

?>