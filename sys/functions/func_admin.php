<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function update_settings($settings)
{
	global $config, $mysqli, $lang;
	
	$data = '';
	
	foreach($settings as $setting)
	{
		if(!isset($setting['name']))continue;
		if($setting['type'] == 'checkbox')
		{
			if(isset($_POST[$setting['name']])){
				
				$value = '1';
			}
			else
			{
				$value = '0';
			}
			$new = '
				UPDATE
					'.$config['prefix'].'settings
				Set
					value = "'.$value.'"
				WHERE
					variable = "'.$setting['name'].'"
			';
			$update = $mysqli->query($new)or die(draw_error($lang['mysql_error'], $mysqli->error));
		}
		else if($setting['type'] == 'select' && isset($setting['multiple']) && $setting['multiple'] == "1")
		{
			$value_string = '';
			
			if(isset($_POST[$setting['name']]) && is_array($_POST[$setting['name']]))
			{
				foreach($_POST[$setting['name']] as $value)
				{
					$value_string .= $mysqli->real_escape_string($value).';';
				}
				$value_string = substr($value_string, 0, -1);
			}
			
			$new = '
				UPDATE
					'.$config['prefix'].'settings
				Set
					value = "'.$value_string.'"
				WHERE
					variable = "'.$setting['name'].'"
			';
			$update = $mysqli->query($new)or die(draw_error($lang['mysql_error'], $mysqli->error));
		}
		else if($setting['type'] == 'var')
		{
			$value_string = $mysqli->real_escape_string($setting['value']);
			
			$new = '
				UPDATE
					'.$config['prefix'].'settings
				Set
					value = "'.$value_string.'"
				WHERE
					variable = "'.$setting['name'].'"
			';
			$update = $mysqli->query($new)or die(draw_error($lang['mysql_error'], $mysqli->error));
		}
		else
		{
			$new = '
				UPDATE
					'.$config['prefix'].'settings
				Set
					value = "'.$mysqli->real_escape_string($_POST[$setting['name']]).'"
				WHERE
					variable = "'.$setting['name'].'"
			';
			$update = $mysqli->query($new)or die(draw_error($lang['mysql_error'], $mysqli->error));
		}
	}
	
}

function function_select_inputs($name, $category = '', $field = 'functions', $var = '')
{

	global $modules;

	$options = array(
		'mephitis' => array(
			'lang' => get_label('mep_system'),
			'options' => array(
				'' => array(
					'lang' => get_label('none')
				)			
			)
		),
	);

	if($var == '')
	{
		$var = $modules;
	}
	
	foreach($var as $key => $mod)
	{
		$mod_funcs = array();
		
		if(isset($mod[$field]) && is_array($mod[$field]))
		{
			foreach($mod[$field] as $func => $func_info)
			{
				
				if(is_array($func_info))
				{
					$func_name = $func;
					$func_label = $func_info['lang'];
				}
				else
				{
					$func_name = $func_info;
					$func_label = get_label($func_info);
				}
				
				$mod_funcs[$func_name] = array(
					'lang' => $func_label
				);
			}
		}
		
		if(!isset($mod['lang']))$mod['lang'] = $key;
		$options[$key] = array(
			'lang' => $mod['lang'],
			'options' => $mod_funcs
		);
	}

	$inputs[] = array(
		'name'	=> $name,
		'type'	=> 'select',
		'lang'	=> get_label($name),
		'category' => $category,
		'groups' => 1,
		'options' => $options
	);

	$inputs[] = array(
		'name'	=> $name.'_args',
		'type'	=> 'text',
		'category' => $category,
		'lang'	=> get_label($name.'_args')
	);
	
	return $inputs;
}

?>