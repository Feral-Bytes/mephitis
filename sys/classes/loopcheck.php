<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

class loopcheck{
	
	var $path;
	var $config;
	var $detection;
	var $db;
	
	function __construct($table, $id, $subof, $mode = 'sites'){
		
		global $mysqli;
		
		$this->db = $mysqli;
		
		$this->config['table']			= $table;
		$this->config['id']				= $this->db->real_escape_string($id);
		$this->config['sub_of']			= $this->db->real_escape_string($subof);
		$this->config['mode']			= $mode;
		
		if($mode == 'sites'){
			$this->config['parentfield']	= 'sub_of';
		}else if($mode == 'fce'){
			$this->config['parentfield']	= 'parent_id';
		}
		
		$this->check($this->config['id'], $this->config['sub_of']);
		
	}
	
	function path($id){
		if($this->config['mode'] == 'sites'){
			$result = $this->db->query('SELECT id, '.$this->config['parentfield'].' FROM '.$this->config['table'].' WHERE id = "'.$this->db->real_escape_string($id).'"');
		}else if($this->config['mode'] == 'fce'){
			$result = $this->db->query('SELECT id, '.$this->config['parentfield'].', parent_kind FROM '.$this->config['table'].' WHERE id = "'.$this->db->real_escape_string($id).'"');
		}
		
		
		$row = $result->fetch_assoc();
		
		if($this->config['mode'] == 'sites'){
			
		}else if($this->config['mode'] == 'fce'){
			if($row['parent_kind'] == 'site'){
				//force root
				$row[$this->config['parentfield']] = 0;
			}
		}
		
		if(isset($row['id']))$this->path[$row['id']] = $row[$this->config['parentfield']];
		if(is_array($row) AND isset($row[$this->config['parentfield']])){
			$this->path($row[$this->config['parentfield']]);
		}
	}
	
	function array_loop($key, $search){
		
		if($this->path[$key] == 0){
			$this->detection = 'root';
		}else if($this->path[$key] != $search){
			if(isset($this->path[$key])){
				$this->array_loop($this->path[$key], $search);
			}else{
				$this->detection = 'end';
			}
		}else if($this->path[$key] == $search){
			$this->detection = 'loop';
		}else{
			$this->detection = 'error';
		}
		
	}
	
	function check($id, $subof){
		
		//get path
		$this->path($subof);

		//simulate the movement
		if(isset($this->path[$id])){
			$this->path[$id] = $subof;
		}
		//check for loop
		$this->array_loop($subof, $subof);
		
	}
	
	
}

?>