<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

class export
{

	var $db;
	
	function __construct()
	{
		global $mysqli;
		
		$this->db = $mysqli;
		
	}
	
	function Filename($file)
	{
		//http://stackoverflow.com/questions/2021624/string-sanitizer-for-filename
		
		$file = mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $file);
		$file = mb_ereg_replace("([\.]{2,})", '', $file);
		$file = str_replace(" ", '_', $file);
		return $file;
	}
	
	function mysql_2_xls($query, $filename)
	{
		
		global $lang;
		
		$result = $this->db->query($query) or die(draw_error($lang['mysql_error'], $this->db->error));	
		
		header('Content-Type: application/xls');
		header('Content-Disposition: attachment; filename='.$this->Filename($filename).'.xls');
		header('Pragma: no-cache');
		header('Expires: 0');
		
		
		$sep = "\t"; //tabbed character
		//start of printing column names as names of MySQL fields
		for ($i = 0; $i < $result->field_count; $i++) {
			echo $result->fetch_field_direct($i)->name . "\t";
		}
		print("\n");	
		//end of printing column names  
		//start while loop to get data
		while($row = $result->fetch_row())
		{
			$schema_insert = "";
			for($j=0; $j< $result->field_count;$j++)
			{
				if(!isset($row[$j]))
				{
					$schema_insert .= "NULL".$sep;
				}
				elseif ($row[$j] != "")
				{
					$schema_insert .= "$row[$j]".$sep;
				}
				else
				{
					$schema_insert .= "".$sep;
				}
			}
			$schema_insert = str_replace($sep."$", "", $schema_insert);
			$schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
			$schema_insert .= "\t";
			print(trim($schema_insert));
			print "\n";
		}   
		exit;
		
	}
	
	function array2xls($array, $filename)
	{
		
		header('Content-Type: application/xls');
		header('Content-Disposition: attachment; filename='.$this->Filename($filename).'.xls');
		header('Pragma: no-cache');
		header('Expires: 0');
		
		if(is_array($array))
		{
			$xls = '';
			foreach($array as $fields)
			{
				foreach($fields as $key => $value)
				{
					$xls .= get_label($key)."\t";
				}
				$xls .= "\n";
				break;
			}
			
			
			
			foreach($array as $fields)
			{
				foreach($fields as $key => $value)
				{
					$value = preg_replace("/\r\n|\n\r|\n|\r/", " ", $value);
					$value = trim($value)."\t";
					$xls .= $value;
				}
				$xls .= "\n";
			}
		}
		echo $xls;
		exit;
	}
	
}

?>