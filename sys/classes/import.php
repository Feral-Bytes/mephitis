<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

class import
{
	
	var $db;
	
	function __construct()
	{
		global $mysqli;
		
		$this->db = $mysqli;
		
	}	
	
	function csv_2_mysql($table, $file, $conf = '')
	{
		
		global $lang;
		
		if(!isset($conf['delimiter']))
		{
			$conf['delimiter'] = ',';
		}
		
		if(!isset($conf['ignoreFirstField']))
		{
			$conf['ignoreFirstField'] = '0';
		}
		
		if(!isset($conf['dateFields']))
		{
			$conf['dateFields'] = '';
		}		
		
		$row = 0;
		if (($handle = fopen($file, "r")) !== FALSE)
		{
			$fields = '';
			$header = '';
			while (($data = fgetcsv($handle, 0, $conf['delimiter'])) !== FALSE)
			{							
				
				$num = count($data);
				$row++;
				$query = '';
				
				for ($c=0; $c < $num; $c++)
				{
					$data[$c] = $this->db->real_escape_string($data[$c]);
				}				
				
				if($row == 1)
				{
					for ($c=0; $c < $num; $c++)
					{
						$header[$c] = $data[$c];
					}						
					if($conf['ignoreFirstField'] == '1')
					{
						unset($data[0]);
					}						
					$fields = implode(',', $data);
				}
				else
				{
					for ($c=0; $c < $num; $c++)
					{
						if(is_array($conf['dateFields']))
						{
							for($f = 0; $f < count($conf['dateFields']); $f++)
							{
								if($conf['dateFields'][$f]['field'] == $header[$c])
								{
									$data[$c] = date($conf['dateFields'][$f]['timeformat'], strtotime($data[$c]));
								}
							}
						}
						if(is_array($conf['timeFields']))
						{
							for($f = 0; $f < count($conf['timeFields']); $f++)
							{
								if($conf['timeFields'][$f]['field'] == $header[$c])
								{
									$data[$c] = strtotime($data[$c]);
								}
							}
						}						
						$data[$c] = '"'.$data[$c].'"';
					}					
					if($conf['ignoreFirstField'] == '1')
					{
						unset($data[0]);
					}						
					
					$query = '
						INSERT INTO '.$table.'
						('.$fields.')
						VALUES
						('.implode(',', $data).')
					';
					
					$this->db->query($query) or die(draw_error($lang['mysql_error'], $this->db->error));
				}
			}
			fclose($handle);
		}		
		
	}
	
}

?>