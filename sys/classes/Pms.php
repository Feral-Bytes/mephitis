<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

class Pms
{
	var $db;
	var $tablePrefix;
	var $user;
	var $draw;
	var $unreadCount;
	var $inboxCount;
	var $outboxCount;
	
	function __construct()
	{
		global $mysqli, $user, $config;
		
		$this->db = $mysqli;
		$this->tablePrefix = $config['prefix'];
		
		$this->user = $user;
		if(!isset($config['notemplate']) || !$config['notemplate'])
		{
			$this->draw = new draw_pms($this);
			if($this->user['id'] != '0')
			{
				$this->CheckBoxes();
			}
		}
	}	
	
	function DrawPmsLinkLabel()
	{
		return $this->draw->PmsLinkLabel();
	}
	
	function DrawPms($box, $content, $form_success = false)
	{
		return $this->draw->Pms($box, $content, $form_success);
	}	
	
	function Inbox()
	{
		return $this->DrawPms('inbox', $this->Box('inbox'));
	}	

	function Outbox()
	{
		return $this->DrawPms('outbox', $this->Box('outbox'));
	}
	
	function Box($box)
	{
		$search = array();
		
		$data = array();
		
		if($box == 'inbox')
		{
			$search[] = 'to_id = "'.$this->user['id'].'"';
			$search[] = 'del_to = "0"';
		}

		if($box == 'outbox')
		{
			$search[] = 'from_id = "'.$this->user['id'].'"';
			$search[] = 'del_from = "0"';
		}		
		
		$query = '
			SELECT
				*
			FROM
				'.$this->tablePrefix.'pms
			WHERE
				'.implode(' AND ', $search).'
			ORDER BY
				time DESC
		';
		
		$result = $this->db->query($query);
		
		while($row = $result->fetch_assoc())
		{
			$data[] = $row;
		}
		
		return $data;
		
	}
	
	function ShowPm($id)
	{
		$pm = $this->Pm($id);
		
		if(isset($pm) && is_array($pm) && $pm['to_id'] == $this->user['id'])
		{
			if($pm['read_to'] == "0")
			{
				$query = '
					UPDATE 
						'.$this->tablePrefix.'pms
					SET
						read_to = "1"
					WHERE
						id = "'.$pm['id'].'"
				';
				$result = $this->db->query($query);
			}
			return $this->DrawPms('inbox', $pm);
		}
		else if(isset($pm) && is_array($pm) && $pm['from_id'] == $this->user['id'])
		{
			return $this->DrawPms('outbox', $pm);
		}
		
	}
	
	function Pm($id)
	{
	
		$id = $this->db->real_escape_string($id);
		
		$query = '
			SELECT
				*
			FROM
				'.$this->tablePrefix.'pms
			WHERE
				id = "'.$id.'"
				AND 
				(
					(
						from_id = "'.$this->user['id'].'"
						AND
						del_from = "0"
					)
					OR
					(
						to_id = "'.$this->user['id'].'"
						AND
						del_to = "0"
					)
				)
		';
		
		$result = $this->db->query($query);		
		$row = $result->fetch_assoc();
		
		return $row;
	}
	
	function Del($pm, $pms = '')
	{
		$pm = $this->Pm($pm);
		
		if(isset($pm) && is_array($pm))
		{
			$set = '';
			
			if($pm['to_id'] == $this->user['id'])
			{
				$set = 'del_to = "1"';
			}
			else if($pm['from_id'] == $this->user['id'])
			{
				$set = 'del_from = "1"';
			}
			
			$query = '
				UPDATE 
					'.$this->tablePrefix.'pms
				SET
					'.$set.'
				WHERE
					id = "'.$pm['id'].'"
			';
			$result = $this->db->query($query);
			
			$query = '
				DELETE
				FROM
					'.$this->tablePrefix.'pms
				WHERE
					(
						del_to = "1"
						AND
						del_from = "1"
					)
					OR
					(
						del_to = "1"
						AND 
						from_id = "0"
					)
			';
			$result = $this->db->query($query);
			
			if($pms != '')
			{
				$this->CheckBoxes();
				return $this->draw->MsgDelSuccess($pm, $pms);
			}
		}
	}
	
	function UserInfo($id)
	{
		$id = $this->db->real_escape_string($id);
		if($id == '0'){
			
			$array = array(
				'id' => '0',
				'name' => get_label('system')
			);

			return $array;
			
		}else{
			
			$query = '
				SELECT
					*
				FROM
					'.$this->tablePrefix.'user
				WHERE
					id = "'.$id.'"
			';
			
			$result = $this->db->query($query);
			$row = $result->fetch_assoc();	
			
			return $row;
		}
	}
	
	function GetForm()
	{
		$inputs = array();	
		
		$replyTo = '';
		if(isset($_GET['reply']))
		{
			$replyTo = $this->Pm($_GET['reply']);
		}
		
		$inputs[] = array(
			'type'	=> 'fix',
			'name'	=> 'from_id',
			'value'	=> $this->user['id']
		);
		
		$inputs[] = array(
			'type'	=> 'var',
			'name'	=> 'time',
			'value'	=> time()
		);
		
		if(isset($_GET['pm_action']) && $_GET['pm_action'] == 'new')
		{
			$search = array();
			$options = array();

			$search[] = 'id != "'.$this->user['id'].'"';
			if($replyTo != '')
			{
				$search[] = 'id = "'.$replyTo['from_id'].'"';
				$disabled = true;

			}
			else
			{
				$options['']['lang'] = '';
			}
			
			$result_options = $this->db->query('
				SELECT 
					* 
				FROM
					'.$this->tablePrefix.'user
				WHERE
					'.implode(' AND ', $search).'
				ORDER BY
					name ASC
			');
			
			while($row_options = $result_options->fetch_assoc())
			{
				$options[$row_options['id']] = array(
					'lang' => $row_options['name']
				);
			}
			$inputs[] = array(
				'type'	=> 'select',
				'name'	=> 'to_id',
				'lang'	=> get_label('form_to'),
				'options' => $options,
				'required' => '1'
			);			
		}
		
		$subject = '';
		if($replyTo != '')
		{
			$subject = get_label('pms_re').': '.$replyTo['subject'];
		}
	
		$inputs[] = array(
			'type'	=> 'text',
			'name'	=> 'subject',
			'lang'	=> get_label('subject'),
			'required' => '1',
			'value' => $subject
		);
		
		/*
		$inputs[] = array(
			'type'	=> 'textarea',
			'name'	=> 'content',
			'simple' => '1',
			'bbcode' => getvar('bbcode'),
			'emojis' => getvar('emojis'),
			'required' => '1',
			'required_msg' => get_label('required_msg_content'),
			'value'	=> '',
			'lang'	=> get_label('content')
		);
		*/
		
		$inputs[] = array(
			'type'	=> 'textarea',
			'name'	=> 'content',
			'wysiwyg' => getvar('wysiwyg'),
			'required' => '1',
			'required_msg' => get_label('required_msg_content'),
			'value'	=> '',
			'lang'	=> get_label('content')
		);
		
		return $inputs;
	}
	
	function NewPm($inputs = null, $ajaxMode = false)
	{
		global $phproot, $lang, $mysqli, $autoform_last_insert_id, $msg_stack, $autoform_success;
		
		$echo = '';
		
		if(!isset($inputs) || !is_array($inputs))
		{
			$inputs = $this->GetForm();
			$lang['posted'] = get_label('msg_pm_send');

			
			if($ajaxMode)
			{
				$msg_stack_save = $msg_stack;
				$form = autoform($this->tablePrefix.'pms', $inputs, $phproot, 'pms=action&', get_label('pm'), NULL, 'modal', 'pm_action', $phproot.'?pms=inbox', get_label('inbox'));
				$msg_stack = $msg_stack_save;				
				$echo .= $form;
			}
			else
			{
				$msg_stack_save = $msg_stack;
				$form = autoform($this->tablePrefix.'pms', $inputs, $phproot, 'pms=action&', get_label('pm'), NULL, 'noframe', 'pm_action', $phproot.'?pms=inbox', get_label('inbox'));
				$msg_stack = $msg_stack_save;				
				$echo .= $this->DrawPms('newpm', $form, $autoform_success);				
			}
			
			$this->SendEMail($autoform_last_insert_id);
			
			return $echo;
		}
	}
	
	function CheckBoxes()
	{
		$query = '
			SELECT
				id
			FROM
				'.$this->tablePrefix.'pms
			WHERE
				to_id = "'.$this->user['id'].'"
				AND
				read_to = "0"
				AND
				del_to = "0"
		';
		
		$result = $this->db->query($query);
		
		$this->unreadCount = $result->num_rows;
		if(!isset($_GET['pms']) && $this->unreadCount > 0)
		{
			$this->draw->Unread();
		}

		$query = '
			SELECT
				id
			FROM
				'.$this->tablePrefix.'pms
			WHERE
				to_id = "'.$this->user['id'].'"
				AND
				del_to = "0"
		';
		
		$result = $this->db->query($query);
		$this->inboxCount = $result->num_rows;
		
		$query = '
			SELECT
				id
			FROM
				'.$this->tablePrefix.'pms
			WHERE
				from_id = "'.$this->user['id'].'"
				AND
				del_from = "0"
		';
		
		$result = $this->db->query($query);
		$this->outboxCount = $result->num_rows;		
		
	}
	
	function SendPm($user_id, $subject, $content, $converPost = true)
	{
		if($converPost)
		{
			$content = simple_post($content);
		}
		
		$user_id = $this->db->real_escape_string($user_id);
		$subject = $this->db->real_escape_string($subject);
		$content = $this->db->real_escape_string($content);
		
		$query = '
			INSERT
			INTO
				'.$this->tablePrefix.'pms
			( to_id, time, subject, content, del_from )
			VALUES
			( "'.$user_id.'", "'.time().'", "'.$subject.'", "'.$content.'", "1")
		';
		
		$result = $this->db->query($query);		
		
		$this->SendEMail($this->db->insert_id);
	}
	
	function SendEMail($pm)
	{
		
		$pm = $this->Pm($pm);
		
		$to_user = $this->UserInfo($pm['to_id']);
		$from_user = $this->UserInfo($pm['from_id']);
		
		if($to_user['pms_sendemail'] == '1' && trim($to_user['email']) != '')
		{
			
			$subject = get_label('pms_sendemail_subject');
			$subject = str_replace('{title}', getvar('title'), $subject);
			$subject = str_replace('{subject}', $pm['subject'], $subject);
			$subject = str_replace('{user}', $from_user['name'], $subject);
			
			$linkToPm = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'?pms=show&pm='.$pm['id'];
			
			$message = $this->draw->EMail($pm, $to_user, $from_user, $linkToPm);
			
			mailer($to_user['email'], $subject, $message, getvar('admin_name'), getvar('admin_email'), 'html');
			
		}
		
	}
	
}

?>