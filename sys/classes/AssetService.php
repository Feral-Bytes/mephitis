<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes http://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license http://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

class AssetService
{
	
	var $assets;
	var $vars;
	var $varsFiltered;
	
	function __construct()
	{
		
	}
	
	private function FilterVars($vars)
	{
		$this->varsFiltered = array();
		$this->DoFilterVars($vars);
		return $this->varsFiltered;
	}
	
	private function DoFilterVars($vars)
	{
		if(is_array($vars))
		foreach($vars as $key => $value)
		{
			if(is_array($value))
			{
				$this->DoFilterVars($value);
			}
			else
			{
				$this->varsFiltered[$key] = $value;
			}
		}
	}
	
	public function SetVars($vars)
	{
		
		$vars = $this->FilterVars($vars);

		foreach($vars as $key => $value)
		{
			$this->vars[$key] = $value;
		}
	}
	
	public function Add($asset)
	{
		if(
			!isset($asset['name'])
			||
			!isset($asset['type'])
		)
		{
			return;
		}
		
		$asset['name'] = trim($asset['name']);
		$asset['type'] = trim($asset['type']);
		
		if(!isset($this->assets[$asset['name']]))
		{
			$this->assets[$asset['name']] = $asset;
		}
		
	}
	
	public function Render($type)
	{
		global $phpRootPath, $httpRootPath;
		
		$assets = array();
		$assetsStandalone = array();
		foreach($this->assets as $asset)
		{
			if($type != $asset['type'])
			{
				continue;
			}
			
			$assets[] =  $asset;
		}
		
		$filePrefix = '';
		$fileSuffix = '';
		switch($type)
		{
			case 'css':
				$filePrefix = 'style-';
				$fileSuffix = '.css';
				$type = 'style';
				break;
			case 'js':
				$filePrefix = 'script-';
				$fileSuffix = '.js';
				$type = 'script';
				break;
		}
		
		$html = '';
		$assetGroups = array();
		$group = 0;
		foreach($assets as $key => $asset)
		{
			if(
				(isset($asset['cache']) && $asset['cache'] == false)
				||
				(isset($asset['standalone']) && $asset['standalone'] == true)
			)
			{
				$group++;
				$assetGroups[$group][] = $asset;
				$group++;
				
			}
			else
			{
				$assetGroups[$group][] = $asset;
			}
		}
		
		foreach($assetGroups as $assets)
		{
			if(count($assets) == 1 && isset($assets[0]['cache']) && $assets[0]['cache'] == false)
			{
				$html .= $this->BuildHTML($assets[0]['path'], $type);
			}
			else
			{
				
				$file = $filePrefix.md5($this->GetFileName($assets)).$fileSuffix;
				$fileHttp = $httpRootPath.'cache/'.$file;
				
				if(!file_exists($phpRootPath.'cache/'.$file))
				{
					$this->BuildFile($file, $assets, $type);
				}
				
				$html .= $this->BuildHTML($fileHttp, $type);
			}
		}
		

		
		return $html;
	}
	
	private function BuildFile($file, $assets, $type)
	{
		global $phpRootPath, $httpRootPath;
		
		$fileAbs = $phpRootPath.'cache/'.$file;
		
		if(file_exists($fileAbs))
		{
			unlink($fileAbs);
		}
		
		$content = '';
		
		foreach($assets as $asset)
		{
			if(!isset($asset['content']))
			{
				$asset['content'] = file_get_contents($asset['path']);
			}
			if(isset($asset['replace']) && is_array($asset['replace']))
			{
				foreach($asset['replace'] as $key => $value)
				{
					$asset['content'] = str_replace($key, $value, $asset['content']);
				}
			}
			
			$content .= $asset['content'];
		}
		
		if($type == 'style')
		{
			$content = $this->CompileSCSS($content);
		}
		
		foreach($assets as $asset)
		{
			if(isset($asset['replaceAfter']) && is_array($asset['replaceAfter']))
			{
				foreach($asset['replaceAfter'] as $key => $value)
				{
					$content = str_replace($key, $value, $content);
				}
			}
		}
		
		file_put_contents($fileAbs, $content);
	}
	
	private function CompileSCSS($content)
	{
		$scss = new Leafo\ScssPhp\Compiler();
		$scss->setFormatter('Leafo\ScssPhp\Formatter\Crunched');
		
		
		if(is_array($this->vars))
		{
			//debug($this->vars);
			$scss->setVariables($this->vars);
		}
		
		$css = $scss->compile($content);
		
		return $css;
	}
	
	private function BuildHTML($file, $type)
	{
		$html = '';
		switch($type)
		{
			case 'style':
				$html .= '<link rel="stylesheet" type="text/css" href="'.$file.'">';
				break;
			case 'script':
				$html .= '<script src="'.$file.'"></script>';
				break;
		}
		
		return $html;
	}
	
	private function GetFileName($assets)
	{
		$name = '';
		for($i = 0; $i < count($assets); $i++)
		{
			$name .= $assets[$i]['name'];
		}
		return trim($name);
	}
	
}

?>