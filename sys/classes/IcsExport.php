<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

class IcsExport {
	
	var $events;

	private function DateToCal($timestamp) {
		return gmdate('Ymd\THis\Z', $timestamp);
	}

	private function EscapeString($string) {
		return preg_replace('/([\,;])/','\\\$1', $string);
	}

	public function AddEvent($dateStart, $dateEnd, $summary = '', $description = '', $location = '', $uri = '')
	{
		$this->events[] = array(
			'dateStart'		=> $dateStart,
			'dateEnd'		=> $dateEnd,
			'summary'		=> $summary,
			'description'	=> $description,
			'location'		=> $location,
			'uri'			=> $uri
		);
	}

	private function RenderEvents()
	{
		$data = array();
		if(is_array($this->events))
		{
			foreach($this->events as $event)
			{
				$eventData = '
					BEGIN:VEVENT
					DTSTAMP:'.$this->DateToCal(time()).'
					DTSTART:'.$this->DateToCal($event['dateStart']).'
					DTEND:'.$this->DateToCal($event['dateEnd']).'
					UID:'.uniqid().'
					CREATED:'.$this->DateToCal(time()).'
					SUMMARY:'.$this->EscapeString($event['summary']).'
					DESCRIPTION:'.$this->EscapeString($event['description']).'
					LOCATION:'.$this->EscapeString($event['location']).'
					URL;VALUE=URI:'.$this->EscapeString($event['uri']).'
					END:VEVENT
				';

				$eventData = trim(preg_replace('/\t+/', '', $eventData));

				$data[] = $eventData;
			}
		}
		return $data;
	}

	public function Render()
	{
		$data = '
			BEGIN:VCALENDAR
			VERSION:2.0
			PRODID:-//Feral Bytes//NONSGML Mephitis CMS//DE
			CALSCALE:GREGORIAN
			'.implode("\r\n", $this->RenderEvents()).'
			END:VCALENDAR
		';

		$data = trim(preg_replace('/\t+/', '', $data));

		return $data;
	}

	function Output($filename)
	{
		header('Content-type: text/calendar; charset=utf-8');
		header('Content-Disposition: attachment; filename='.$filename);
		echo $this->Render();
		exit;
	}
}

?>