<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

class searchengine
{
	
	var $conf;
	var $template;
	var $results;
	var $link;
	var $actionlink;
	var $db;
	
	function __construct($conf = null)
	{
		global $config, $mysqli, $phproot, $link, $user;
		
		$this->db = $mysqli;
		
		include_once(include_file('template', 'template_searchengine.php'));	
		$this->template = new draw_searchengine();
		$this->conf = $conf;
		
		$this->InitConf('prefix', $config['prefix']);
		$this->InitConf('action', 'search');
		$this->InitConf('actionkey', 'action');
		
		$this->InitConf('search', array(
			'content' => array(
				'lang' => get_label('sites'),
				'table' => $this->conf['prefix'].'content',
				'title' => 'name',
				'content' => 'content',
				'XMLField' => 'content',
				'link' => 'site',
				'IdField' => 'page',
				//'keywords' => array('name', 'content'),
				'if' => array(
					'online' => '1'
				),
				'groups' => array(
					'field_groups' => 'groups',
					'field_groups_mode' => 'groups_mode'
					//'invert_groups_mods' => '1'
				)
			)
		));
		
		$this->actionlink = $phproot.'?'.$link.'&'.$this->conf['actionkey'].'='.$this->conf['action'].'';
		
		$this->template->searchengine = $this;
		
		
	}
	
	function InitConf($var, $value)
	{
		if(!isset($this->conf[$var]) || $this->conf[$var] == '')
		{
			$this->conf[$var] = $value;
		}		
	}
	
	function filter($query)
	{
		$str = trim($query);
		
		$str = str_replace('&', '', $str);
		$str = str_replace(' ', '|', $str);
		
		return $str;
	}
	
	function search($query)
	{
		global $phproot, $link, $lang;
		
		$query = $this->db->real_escape_string($query);
		$query = $this->filter($query);
		
		$this->link = $phproot.'?'.$link.'&'.$this->conf['actionkey'].'='.$this->conf['action'].'&search_query='.urlencode($query);
		
		$query = htmlspecialchars($query);
		
		$this->results = array();
		$i = 0;
		if(is_array($this->conf['search']) && $query != '')
		{
			foreach($this->conf['search'] as $module => $conf)
			{	
				if($conf['link'] == null)
				{
					continue;
				}
				$i++;
				
				$queryadd = '';
				
				if(isset($conf['if']) && is_array($conf['if']))
				{
					foreach($conf['if'] as $field => $field_conf)
					{
						if(!is_array($field_conf))
						{
							$queryadd .= '
								AND ('.$field.' = "'.$field_conf.'")
							';
						}
					}
				}				
				
				$orderby = '';
				if(isset($_REQUEST[$this->conf['action'].'_orderby']))
				{
					$order = $this->db->real_escape_string($_REQUEST[$this->conf['action'].'_orderby']);
					if(
						(
							$order == 'title'
							||
							$order == 'content'
						)
					)
					{
						
						$conf['orderby']['field'] = $order;
						
						if(isset($_REQUEST[$this->conf['action'].'_ordermode']))
						{
							if($_REQUEST[$this->conf['action'].'_ordermode'] == 'DESC')
							{
								$conf['orderby']['mode'] = 'DESC';
							}
							else
							{
								$conf['orderby']['mode'] = 'ASC';
							}
						}
					}
					else
					{
						$conf['orderby']['field'] = 'title';
						$conf['orderby']['mode'] = 'ASC';
					}
				}
				
				if(!isset($conf['orderby']))
				{
					$conf['orderby']['field'] = 'title';
					$conf['orderby']['mode'] = 'ASC';
				}				
				
				if($i <= 1)
				{
					$this->link .= '&'.$this->conf['action'].'_orderby='.$conf['orderby']['field'].'&'.$this->conf['action'].'_ordermode='.$conf['orderby']['mode'];
				}
				
				$orderby = $conf['orderby']['field'].' '.$conf['orderby']['mode'];

				$fields = array();
				$wheres = array();

				if(isset($conf['title']) && $conf['title'] != '')
				{
					$fields[] = ''.$conf['title'].' as title';
					$wheres[] = ''.$conf['title'].' REGEXP "'.$query.'"';
				}
				
				if(isset($conf['content'])  && $conf['content'] != '')
				{
					$fields[] = ''.$conf['content'].' as content';
					$wheres[] = ''.$conf['content'].' REGEXP "'.$query.'"';
				}

				if(isset($conf['keywords'])  && $conf['keywords'] != '')
				{
					if(!is_array($conf['keywords']))
					{
						$keywords_array = explode(';', $conf['keywords']);
						if(count($keywords_array) > 1)
						{
							$conf['keywords'] = $keywords_array;
						}
					}
					if(is_array($conf['keywords']))
					{
						$keywordfields = implode(', ", ", ', $conf['keywords']);
						$fields[] = 'CONCAT('.$keywordfields.') AS keywords';
						$wheres[] = 'CONCAT('.$keywordfields.') REGEXP "'.$query.'"';
					}
					else
					{
						$fields[] = ''.$conf['keywords'].' AS keywords';
						$wheres[] = ''.$conf['keywords'].' REGEXP "'.$query.'"';
					}
					
				}					
				
				if(isset($conf['groups'])  && is_array($conf['groups']))
				{
					$fields[] = $conf['groups']['field_groups'];
					$fields[] = $conf['groups']['field_groups_mode'];
				}
				
				if(isset($conf['fields']) && is_array($conf['fields']))
				{
					$fields = array_merge($fields, $conf['fields']);
				}
				
				if(isset($conf['IdField']))
				{
					$fields[] = $conf['IdField'];
				}
				
				if(isset($conf['XMLField']))
				{
					$wheres = '
						id != ""
					';
				}
				else
				{
					$wheres = '
						(
							'.implode(' OR ', $wheres).'
						)
					';
				}
				
				
				$mysql_query = '
					SELECT 
						id,
						'.implode(',', $fields).'
					FROM
						'.$conf['table'].'
					WHERE
						'.$wheres.'
						'.$queryadd.'
					ORDER BY
						'.$orderby .'				
				';
				
				$result = $this->db->query($mysql_query);
				if($result)
				while($row = $result->fetch_assoc())
				{
					if(!online_time_check($row))
					{
						continue;
					}
					
					if(isset($conf['XMLField']))
					{
						$xmlResult = $this->XMLSearch($conf['XMLField'], $row[$conf['content']], $query);
						if($xmlResult == false)
						{
							continue;
						}
						else
						{
							$row[$conf['content']] = $xmlResult;
						}
					}

					if(isset($conf['groups']) && is_array($conf['groups']))
					{
						if(check_group_access($row[$conf['groups']['field_groups']], $row[$conf['groups']['field_groups_mode']]))
						{
							$this->results[$module]['results'][] = $row;
						}
					}
					else
					{
						$this->results[$module]['results'][] = $row;
					}
				}
				$this->results[$module]['config'] = $conf;
			}
		}
		
	}
	
	function XMLSearch($field, $xml, $query)
	{
		$xml = simplexml_load_string($xml);
		$string = (string)$xml->$field;
		
		if (@preg_match('/'.$query.'/', $string))
		{
			return $string;
		}
		return false;
		
	}
	
	function draw_form($cssForm = '')
	{
		return $this->template->form($cssForm);
	}
	
	function draw_serp()
	{
		return $this->template->serp();
	}
	
	
}

?>