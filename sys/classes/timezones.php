<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

class timezones{

	var $tzlist;

	function __construct(){
	
		$this->tzlist = $this->tz_list();
	
		$list_neg	= '';
		$list_pos	= '';
		$list_0		= '';
		
/*		foreach($this->tzlist as $zone){
			if($zone['GMT_offset'] == '+00:00'){
				$list_0[] = $zone;
			}else if(substr($zone['GMT_offset'], 0, 1) == '-'){
				$list_neg[] = $zone;
			}else if(substr($zone['GMT_offset'], 0, 1) == '+' AND $zone['GMT_offset'] != '+00:00'){
				$list_pos[] = $zone;
			}
		}
*/
		$list_0 = array();
		$list_pos = array();
		$list_neg = array();
		foreach($this->tzlist as $zone){
			if($zone['GMT_offset'] == '+00:00'){
				$list_0[] = $zone;
			}else if(substr($zone['GMT_offset'], 0, 1) == '-'){
				$list_neg[] = $zone;
			}else if(substr($zone['GMT_offset'], 0, 1) == '+' AND $zone['GMT_offset'] != '+00:00'){
				$list_pos[] = $zone;
			}
		}
		
		$list_neg = $this->sortbysubpart($list_neg, 'GMT_offset', SORT_DESC);
		$list_pos = $this->sortbysubpart($list_pos, 'GMT_offset', SORT_ASC);
		
		$this->tzlist = array_merge($list_neg, $list_0, $list_pos);
		
	}

	function tz_list() {
		$zones_array = array();
		$date = date('Y-m-d');
		foreach(timezone_identifiers_list() as $key => $zone) {

			$offset = new DateTime($date, new DateTimeZone($zone));
			$offset = $offset->format('P');
			
			$zones_array[$key]['zone'] = $zone;
			$zones_array[$key]['GMT_offset'] = $offset;
		}
		return $zones_array;
	}
	
	function sortbysubpart($list, $part, $sort){
	
		foreach ($list as $key => $row) {
			$offsets[$key] = $row[$part];
		}

		array_multisort($offsets, $sort, $list);
		
		return $list;
		
	}
	
	function getlist($mode = 'offsets', $sort = 'offest'){
	
		$list = '';
	
		if($mode == 'one'){
	
			foreach($this->tzlist as $zone){
				$list[$zone['GMT_offset']] = $zone;
			}
			
		}else if($mode == 'groups'){
			
			$list = array();
			foreach($this->tzlist as $zone){
				
				$zonename	= $zone['zone'];
				$zonename	= explode('/', $zonename);
				
				$continent	= $zonename[0];
				
				if($zone['zone'] == 'UTC'){
					$city = 'UTC';
				}else{
					$city = $zonename[1];
				}
				
				$list[$continent][$city]['zone']		= $zone['zone'];
				$list[$continent][$city]['GMT_offset']	= $zone['GMT_offset'];
			}
			
			if($sort == 'name'){
				array_multisort($list, SORT_ASC);
				foreach($list as $continent => $city){
					array_multisort($list[$continent], SORT_ASC);
				}
			}
			
		}else if($mode == 'offsets'){
		
			foreach($this->tzlist as $zone){
				$list[$zone['GMT_offset']] = $zone;
			}
			
		}else if($mode == 'all'){
		
			$list = $this->tzlist;
			
		}
		
		return $list;
	
	}
	
}

?>