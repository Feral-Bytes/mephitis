<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes http://www.feralbytes.ch
 * @copyright 2015 Feral Bytes
 * @license http://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

class content{
	
	var $config;
	var $db;
	var $user;
	
	function __construct($conf, $cUser = null)
	{
		global $user, $mysqli, $config;
		
		$this->config = $conf;
		$this->db = $mysqli;
		
		//error handling
		if(
			!isset($this->config['tab'])
		)
		{
			$this->config['tab'] = 'content';
		}

		//Access function
		if(!isset($this->config['funcAccess']))
		{
			$this->config['funcAccess'] = 'check_group_access';
		}
		
		//OnlineTime function
		if(!isset($this->config['funcOnlineTime']))
		{
			$this->config['funcOnlineTime'] = 'online_time';
		}
		
		//AdminCenter
		if(!isset($this->config['admin']))
		{
			$this->config['admin'] = false;
		}
		
		if($this->config['admin'])
		{
			$this->config['render'] = 'draw_preview';
		}
		else
		{
			$this->config['render'] = 'draw_view';
		}
		
		if($cUser == null)
		{
			$this->user = $user;
		}
		else
		{
			$this->user = $cUser;
		}
		
		if(!StartsWith($this->config['tab'], $config['prefix']))
		{
			$this->config['tab'] = $config['prefix'].$this->config['tab'];
		}
		
	}
	
	public function GetContent($pos, $parentId, $pageId, $isBranch = false)
	{
		$query = '
			SELECT
				*
			FROM
				'.$this->config['tab'].'
			WHERE
				parent = "'.$this->db->real_escape_string($parentId).'"
				AND
				page = "'.$this->db->real_escape_string($parentId).'"
				AND
				pos = "'.$this->db->real_escape_string($pos).'"
			ORDER BY
				rank ASC
		';
		$result = $this->db->query($query);
		$content = array();
		
		$contents = db_get(
			$this->config['tab'],
			array(
				'parent' => $parentId,
				'page' => $pageId,
				'pos' => $pos
			),
			'ORDER BY rank ASC'
		);
		
		$returnContent = array();
		$i = 0;
		$count = count($contents);
		
		foreach($contents as $content)
		{
			$i++;
			$content['i'] = $i;
			$content['count'] = $count;
			$content['hasAccess'] = true;
			$content['tab'] = $this->config['tab'];
		
			if(!$this->config['admin'])
			{
				
				if($content['hasAccess'])
				{
					if($content['online'])
					{
						$content['hasAccess'] = true;
					}
					else
					{
						$content['hasAccess'] = false;
					}
				}
				
				if($content['hasAccess'])
				{
					if($this->config['funcOnlineTime']($content))
					{
						$content['hasAccess'] = true;
					}
					else
					{
						$content['hasAccess'] = false;
					}
				}
				
				if($content['hasAccess'])
				{
					if($this->config['funcAccess']($content['groups'], $content['groups_mode'], $this->user))
					{
						$content['hasAccess'] = true;
					}
					else
					{
						$content['hasAccess'] = false;
					}
				}
			}
			
			if($content['hasAccess'])
			{
				$content['type'] = 'content';
				$this->IncludeElement($content['fce'], $this->config['templates']);
			
				$funcName = $this->config['render'].'_'.$content['fce'];
				
				if(!function_exists($funcName))
				{
					$html = $this->MissingFunction($funcName);
					
				}
				else
				{
					$content['content'] = simplexml_load_string($content['content']);
					
					$content['isBranch'] = $isBranch;
					if(isset($_GET['select_content']) && $_GET['select_content'] == $content['id'])
					{
						$content['isBranch'] = true;
					}
					
					$content['module'] = '';
					if(StartsWith($content['fce'], 'module_'))
					{
						$module = str_replace('module_', '', $content['fce']);
						$module = explode('_', $module);
						$element = $module[count($module)-1];
						unset($module[count($module)-1]);
						$module = implode('_', $module);
						
						$content['module'] = $module;
					}
					
					$html = $funcName($content);
				}
				
				$returnContent[] = array(
					'html' => $html,
					'type' => 'content',
					'contentElement' => $content
				);
			}
		}
		return $returnContent;
		
	}
	
	public function GetTemplates($page)
	{
		global $config;
		
		$templates = array();
		foreach($page['rootline'] as $element)
		{
			if($element['template'] != "0")
			{
				$templates[] = $element['template'];
			}
		}
		$templates = array_reverse($templates);
		
		if($config['default_template'] != getvar('template_dir'))
		{
			$templates[] = $config['default_template'];
		}
		
		$templates[] = getvar('template_dir');	
		
		return $templates;
	}
	
	public function GetPageContent($page)
	{
		
		if(is_numeric($page))
		{
			$page = GetPage($page);
		}
		
		$page['type'] = 'page';
		
		$templates = array();
		foreach($page['rootline'] as $element)
		{
			if($element['template'] != "0")
			{
				$templates[] = $element['template'];
			}
		}
		$templates = array_reverse($templates);
		$templates[] = getvar('template_dir');
		
		$this->config['templates'] = $this->GetTemplates($page);
		
		$this->IncludeElement('page', $templates);
		
		
		
		$funcName = $this->config['render'].'_page';
		
		if(!function_exists($funcName))
		{
			return $this->MissingFunction($funcName);
		}
		
		@$page['content'] = simplexml_load_string($page['content']);
		$page['isBranch'] = false;
		
		return $funcName($page);
	}
	
	public function MissingFunction($name)
	{
		if($this->config['admin'])
		{
			return draw_alert('Function: <b>'.$name.'</b> not forund', 'danger', false);
		}
		return '';
	}
	
	public function GetSettings($data)
	{
		$settingsFunc = 'fce_settings_'.$data['element'];
		$this->config['templates'] = $this->GetTemplates($data['page']);
		$this->IncludeElement($data['element'], $this->config['templates']);

		if(!function_exists($settingsFunc))
		{
			return;
		}
		
		if($data['parent'] != 0)
		{
			$data['parent'] = db_get($this->config['tab'], $data['parent']);
		}
		else
		{
			$data['parent'] = array(
				'id' => 0,
				'fce' => 'page'
			);
		}
		
		if(isset($data['elementId']) && $data['elementId'] != 0)
		{
			$tab = $this->config['tab'];
			if($data['element'] == 'page')
			{
				$tab = str_replace('content', 'pages', $this->config['tab']);
			}
			
			$data['elementData'] = db_get($tab, $data['elementId']);
			
			$content = $data['elementData']['content'];
			
			$content = simplexml_load_string($content);
			
		}
		
		$data['settings'] = $settingsFunc($data);
		
		if($data['settings'] != null)
		{
			if($data['element'] != 'page')
			{
				$data['settings']['fields'] = array_merge($data['settings']['fields'], $this->DefaultFields());
			}
			
			foreach($data['settings']['fields'] as $field => $conf)
			{
				$data['settings']['fields'][$field]['name'] = $field;
				$data['settings']['fields'][$field]['lang'] = get_label($field, $data['element']);
				
				if(!isset($data['settings']['fields'][$field]['category']))
				{
					$data['settings']['fields'][$field]['category'] = 'cat_common';
				}
				
				if(isset($data['elementData'][$field]))
				{
					$data['settings']['fields'][$field]['value'] = $data['elementData'][$field];
				}
				if(isset($content) && isset($content->$field))
				{	
					$data['settings']['fields'][$field]['value'] = @(string)$content->$field;
				}
			}
			
			return $data;
		}

	}
	
	public function SetFieldValue($value, $conf)
	{
		if(is_array($value))
		{
			$value = implode(';', $value);
		}
		else
		{
			
			switch($conf['type'])
			{
				case 'checkbox':
					
					$value = 1;
					
					break;
				default:
					break;
			}
		}
		
		return htmlspecialchars($value);
	}
	
	public function BuildElement($data, $post)
	{
		
		$fields = array();
		foreach($data['settings']['fields'] as $field => $conf)
		{
			$value = '';
			if(isset($post[$field]))
			{
				$value = $this->SetFieldValue($post[$field], $conf);
			}
			$fields[$field] = $value;
		}
		
		$element = array();
		foreach($this->DefaultFields() as $defaultField => $conf)
		{
			if(isset($fields[$defaultField]))
			{
				$element[$defaultField] = $fields[$defaultField];
				unset($fields[$defaultField]);
			}
		}
		$element['content'] = array2xml($fields);
		
		return $element;
		
	}
	
	public function AddElement($data, $post)
	{
		$element = $this->BuildElement($data, $_POST);
		
		$result = $this->db->query('SELECT MAX( rank ) AS rank FROM '.$this->config['tab'].' WHERE parent = "'.$data['parent']['id'].'" AND page = "'.$data['page']['id'].'" AND pos = "'.$this->db->real_escape_string($data['pos']).'"') or die(draw_error(get_label('mysql_error'), $this->db->error));
		$row = $result->fetch_assoc();
		$rank = $row['rank']+1; 
		
		$element = array_merge(array(
			'page' => $data['page']['id'],
			'parent' => $data['parent']['id'],
			'pos' => $data['pos'],
			'rank' => $rank,
			'fce' => $data['element'],
		), $element);
		
		$newElementId = db_insert($this->config['tab'], $element);
		
		return $newElementId;
		
	}
	
	public function EditElement($data, $post)
	{
		$element = $this->BuildElement($data, $_POST);
		
		$tab = $this->config['tab'];
		if($data['element'] == 'page')
		{
			$tab = str_replace('content', 'pages', $this->config['tab']);
		}
		
		db_update($tab, $element, $data['elementId']);
		
		return true;
		
	}
	
	public function DefaultFields()
	{
		$inputs = array();
		
		$inputs[] = array(
			'name' => 'name',
			'type' => 'text',
			'category' => 'cat_common'
		);
		
		$inputs[] = array(
			'name' => 'online',
			'type' => 'checkbox',
			'category' => 'cat_access'
		);
		
		$inputs = array_merge($inputs, online_time_inputs('cat_access'));
		$inputs = array_merge($inputs, groups_inputs('groups', 'cat_access'));
		
		$fields = array();
		
		foreach($inputs as $input)
		{
			switch($input['name'])
			{
				case 'online':
					$input['default'] = '1';
					break;
				case 'groups_mode':
					$input['options']['blacklist']['default'] = '1';
					break;
			}

			$fields[$input['name']] = $input;
		}
		
		return $fields;
		
	}
	
	public function DeleteElement($id)
	{
		$id = $this->db->real_escape_string($id);
		
		$query = '
			SELECT
				id, parent
			FROM
				'.$this->config['tab'].'
			WHERE
				parent = "'.$id.'"
		';
		
		$result = $this->db->query($query);
		while($child = $result->fetch_assoc())
		{
			$this->DeleteElement($child['id']);
		}
		
		$query = '
			DELETE FROM '.$this->config['tab'].'
			WHERE
				id = "'.$id.'"
		';
		$result = $this->db->query($query);
		
	}
	
	public function DeleteChildren($id)
	{
		$id = $this->db->real_escape_string($id);
		
		$query = '
			SELECT
				id, parent
			FROM
				'.$this->config['tab'].'
			WHERE
				parent = "'.$id.'"
		';
		
		$result = $this->db->query($query);
		while($child = $result->fetch_assoc())
		{
			$this->DeleteElement($child['id']);
		}
	}
	
	public function CopyElement($id, $target, $lvl = 0)
	{
		$id = $this->db->real_escape_string($id);
		
		$element = db_get($this->config['tab'], $id);
		$element['parent'] = $target;
		
		$targetElement = db_get($this->config['tab'], $target);
		
		if($lvl == 0)
		{
			$result = $this->db->query('SELECT MAX( rank ) AS rank FROM '.$this->config['tab'].' WHERE parent = "'.$target.'" AND page ="'.$targetElement['page'].'" AND pos = "'.$targetElement['pos'].'"') or die(draw_error(get_label('mysql_error'), $mysqli->error));
			$row = $result->fetch_assoc();
			$element['rank'] = $row['rank']+1; 
		}
		
		$newParentId = db_insert($this->config['tab'], $element);
		
		$query = '
			SELECT
				id
			FROM
				'.$this->config['tab'].'
			WHERE
				parent = "'.$id.'"
		';
		
		$result = $this->db->query($query);
		while($child = $result->fetch_assoc())
		{
			$this->CopyElement($child['id'], $newParentId, $lvl+1);
		}
		
		return $newParentId;
	}
	
	public function IncludeElement($element, $templates)
	{
		global $phpRootPath, $user, $lang;
		
		$found = false;
		$includePath = '';
		
		if(!is_array($templates))
		{
			$templates = array($templates);
		}
	
		foreach($templates as $template)
		{
			$path = $phpRootPath.'templates/'.$template.'/fces/'.$element.'/';
			if(file_exists($path))
			{
				$found = true;
				$includePath = $path;
				break;
			}
		}
		
		if(!$found)
		{
			$path = $phpRootPath.'templates/mephitis1/fces/'.$element.'/';
			
			if(!file_exists($path) && StartsWith($element, 'module_'))
			{
				$module = str_replace('module_', '', $element);
				$module = explode('_', $module);
				$element = $module[count($module)-1];
				unset($module[count($module)-1]);
				$module = implode('_', $module);
				
				$path = $phpRootPath.'modules/'.$module.'/templates/fces/'.$element.'/';
				
			}
			
		}
		
		$langFiles = array(
			$path.'lang.'.$user['lang'].'.php',
			$path.'lang.english.php',
			$path.'lang.php'
		);
		
		foreach($langFiles as $langFile)
		{
			if(file_exists($langFile))
			{
				include_once($langFile);
				break;
			}
		}
		
		$files = array(
			'view',
			'preview',
			'settings'
		);
		
		foreach($files as $file)
		{
			if(file_exists($path.$file.'.php'))
			{
				include_once($path.$file.'.php');
			}
		}
	}
	
	public function CleanOrdering($id)
	{
		$element = db_get($this->config['tab'], $id);
		$elements = db_get($this->config['tab'], array('parent' => $element['parent'], 'pos' => $element['pos']));
		$i = 0;
		foreach($elements as $element_)
		{
			$i++;
			db_update($this->config['tab'], array('rank' => $i), $element_['id']);
		}		
	
	}
	
	public function MoveElement($id, $mode, $after = 0)
	{
		switch($mode)
		{
			case "up":
				$this->MoveElementLinear($id, 'up');
				break;
			case "down":
				$this->MoveElementLinear($id, 'down');
				break;
			case "after":
				$this->MoveElementAfter($id, $after);
				break;
			default:
				break;
		}
	}
	
	public function Move($id, $target, $pos)
	{
		
		$element = db_get($this->config['tab'], $id);
		$target = db_get($this->config['tab'], $target);
		
		$targetElements = db_get($this->config['tab'], array('parent' => $target['id'], 'pos' => $pos), 'ORDER BY rank');
		
		$iMax = 0;
		$i = 0;
		foreach($targetElements as $targetElement)
		{
			$i++;
			
			if($i >= $iMax)
			{
				$iMax = $i;
			}
			
			db_update($this->config['tab'], array('rank' => $i), $targetElement['id']);
			
		}
	
		db_update($this->config['tab'], array('rank' => $iMax+1, 'parent' => $target['id'], 'pos' => $pos), $element['id']);
		
		$this->CleanOrdering($id);
		
	}
	
	public function MoveElementLinear($id, $dir)
	{

		$element = db_get($this->config['tab'], $id);
		$elements = db_get($this->config['tab'], array('parent' => $element['parent'], 'pos' => $element['pos']), 'ORDER BY rank', false);
		
		$offset = 1;
		if($dir == 'up')
		{
			$offset = -1;
		}
		if($dir == 'down')
		{
			$offset = 1;
		}

		for($i = 0; $i < count($elements); $i++)
		{
			if($elements[$i]['id'] == $element['id'])
			{
				if(isset($elements[$i+$offset]))
				{
					db_update($this->config['tab'], array('rank' => $elements[$i+$offset]['rank']), $element['id']);
					db_update($this->config['tab'], array('rank' => $elements[$i]['rank']), $elements[$i+$offset]['id']);
				}
				break;
			}
		}
	}
	
	public function MoveElementAfter($id, $after = 0)
	{
		$element = db_get($this->config['tab'], $id);
		$elements = db_get($this->config['tab'], array('parent' => $element['parent'], 'pos' => $element['pos']), 'ORDER BY rank');
		
		$this->CleanOrdering($id);
		
		$offset = 0;
		$pos = 0;
		if($after == 0)
		{
			$pos = 0;
			$offset = 1;
		}
		
		$i = 0;
		foreach($elements as $element_)
		{
			$i++;
			
			if($after != 0 && $element_['id'] == $after)
			{
				$offset = 1;
				$pos = $i+1;
			}
		}
		
		$i = 0;
		foreach($elements as $element_)
		{
			$i++;
			if($i >= $pos)
			{
				db_update($this->config['tab'], array('rank' => $i+$offset), $element_['id']);
			}
		}
		
		db_update($this->config['tab'], array('rank' => $pos), $element['id']);
		
	}
	
}

?>