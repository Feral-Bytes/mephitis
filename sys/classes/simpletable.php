<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

class simpletable{

	var $tab;
	var $tab_info;
	var $conf;
	var $db;
	
	function __construct($tab, $conf = NULL){
		
		global $lang, $mysqli;
		
		$this->db = $mysqli;
		
		include_once(include_file('template', 'template_simpletable.php'));
	
		$this->tab = $this->db->real_escape_string($tab);
		$conf['tab'] = $this->tab;
		$this->tab_info = array();
		
		
		
		if(!isset($conf['perpage']))
		$conf['perpage'] = 0;
		
		if(!isset($conf['header']))
		$conf['header'] = '';
		
		if(!isset($conf['ajaxkey']))
		$conf['ajaxkey'] = '';
		
		if(!isset($conf['modal_label']))
		$conf['modal_label'] = '';
		
		if(!isset($conf['cols']))
		$conf['cols'] = 'all';
	
		if(!isset($conf['search']))
		$conf['search'] = '';
		
		if(!isset($conf['form_mode']))
		$conf['form_mode'] = '';
	
		if(!isset($conf['access_write']))
		$conf['access_write'] = true;
		
		$query = 'SHOW COLUMNS FROM '.$this->tab.'';
		$result = $this->db->query($query);
		
		if (!$result) {
			die(draw_error($lang['mysql_error'], $this->db->error));
		}
		if ($result->num_rows > 0) {
			while ($row = $result->fetch_assoc()){
				
				if($conf['cols'] == 'all'){
				
				}else if($conf['cols'] == 'fields'){
					if(!isset($conf['fields'][$row['Field']])){
						continue;
					}
				}
				
				if(isset($conf['fields'][$row['Field']]['label'])){
					$label = $conf['fields'][$row['Field']]['label'];
				}else{
					$label = get_label($row['Field']);
				}
			
				$row['name'] = $label;
				$this->tab_info[$row['Field']] = $row;
				$conf['fields'][$row['Field']]['found'] = true;
			}
		}
		
		foreach($conf['fields'] as $key => $field)
		{
			if(!isset($field['found']) || $field['found'] == false)
			{
				$new = array();
				foreach ($this->tab_info as $k => $value)
				{
					$new[$k] = $value;
					if (isset($field['after']) && $k === $field['after']) 
					{
						if(!isset($field['Field']) && isset($field['query']))
						{
							$field['Field'] = $key;
							$conf['fields'][$key] = $field['Field'];
						}
						
						$new[$key] = $field;
					}
				}
				$this->tab_info = $new;
			}
		}
		
		
		if(isset($_GET['orderby']) AND isset($_GET['ordermode'])){
			$conf['orderlink'] = '&orderby='.$this->db->real_escape_string($_GET['orderby']).'&ordermode='.$this->db->real_escape_string($_GET['ordermode']);
		}
		else
		{
			$conf['orderlink'] = '';
		}
		
		if(isset($_REQUEST['page']))
		{
			$conf['link'] .= '&page='.$this->db->real_escape_string($_REQUEST['page']).'&';
		}
		
		$this->conf = $conf;
		
	}
	
	function truncate()
	{
		global $msg_stack, $phproot;
		
		if(!isset($this->conf['truncate']) || $this->conf['truncate'] == false)
		{
			return;
		}
		
		if(isset($_GET['action']) && $_GET['action'] == 'truncate')
		{
			if(isset($_GET['sure']))
			{
				$query = 'TRUNCATE TABLE '.$this->tab.'';
				$del = $this->db->query($query) or die(draw_error(get_label('mysql_error'), $this->db->error));
				
				$msg_stack .= draw_alert(get_label('truncate_success'), 'success');
			}
			else
			{
				$msg = draw_confirm_dialog(get_label('truncate_confirm'), $phproot.'?'.$this->conf['link'].'&action=truncate&sure=yes', $phproot.'?'.$this->conf['link']);
				$msg_stack .= draw_alert($msg, 'warning', true);
			}
		}
	}
	
	function order($where)
	{
		if(!isset($this->conf['rank']) || $this->conf['rank'] != true)
		{
			return;
		}
		
		if(isset($_GET['itemid']) AND isset($_GET['order']))
		{
			
			$result = $this->db->query('SELECT id, rank FROM '.$this->tab.' WHERE id = "'.$this->db->real_escape_string($_GET['itemid']).'"');
			$row = $result->fetch_assoc()or die(draw_error($lang['mysql_error'], $this->db->error));
			
			$order = '';
			$wheres = array();
			if(trim($where) != '')
			{
				$wheres[] = $where;
			}
			
			if($_GET['order'] == 'up')
			{
				$wheres[] = 'rank < "'.$row['rank'].'"';
				$order = 'DESC';
			}
			
			if($_GET['order'] == 'down')
			{
				$wheres[] = 'rank > "'.$row['rank'].'"';
				$order = 'ASC';
			}
			
			$result_next = $this->db->query('SELECT id, rank FROM '.$this->tab.' WHERE '.implode(' AND ', $wheres).' ORDER BY rank '.$order.' LIMIT 1');
			$row_next = $result_next->fetch_assoc();
			
			if(is_array($row_next))
			{

				$new = '
					UPDATE '.$this->tab.' Set
						rank = "'.$row['rank'].'"
					WHERE
						id = "'.$row_next['id'].'"
				';
				$update = $this->db->query($new)or die(draw_error(get_label('mysql_error'), $this->db->error));

				$new = '
					UPDATE '.$this->tab.' Set
						rank = "'.$row_next['rank'].'"
					WHERE
						id = "'.$row['id'].'"';
				$update = $this->db->query($new)or die(draw_error(get_label('mysql_error'), $this->db->error));

			}
			else
			{
				
			}
		}
	}
	
	function getdata(){
		
		global $lang;
		
		$data = array();
		
		$queryfields = '';
		$query = '';
		
		foreach($this->tab_info as $key => $field){
			if(isset($field['Field']) && !isset($field['query']))
			{
				$queryfields .= $this->db->real_escape_string($field['Field']).', ';
			}
			
			if(isset($field['query']))
			{
				$queryfields .= '('.$field['query'].') AS '.$this->db->real_escape_string($field['Field']).', ';
			}
		}
		
		$queryfields = substr($queryfields, 0, -2);
		
		$query .= 'SELECT '.$queryfields.' FROM '.$this->tab.'';
		
		$where = '';
		
		if(isset($this->conf['show_rows']) AND is_array($this->conf['show_rows']))
		{
			$where .= ' id IN ('.implode(',',$this->conf['show_rows']).') ';
		}
		else if($this->conf['search'] != '')
		{
			$where .= ' '.$this->conf['search'].' ';
		}
		
		if($where != '')
		{
			$query .= ' WHERE '.$where.' ';
		}
		
		$queryadd = '';
		if(isset($_GET['orderby']) AND isset($_GET['ordermode'])){
			
			$ordermode = '';
			
			if($this->db->real_escape_string($_GET['ordermode']) == 'ASC'){
				$ordermode = 'ASC';
			}
			if($this->db->real_escape_string($_GET['ordermode']) == 'DESC'){
				$ordermode = 'DESC';
			}
			
			$orderby = '';
			
			foreach($this->tab_info as $field){
				if(isset($field['Field']) && $field['Field'] == $this->db->real_escape_string($_GET['orderby'])){
					$orderby = $field['Field'];
				}
			}
			
			if($orderby != '')
			$query .= ' ORDER BY '.$orderby.' '.$ordermode;
		}
		else
		{
			if(isset($this->conf['default_order']))
			{
				$query .= ' ORDER BY '.$this->conf['default_order'].'';
			}
		}
		
		if($this->conf['perpage'] != 0){
			$query .= pages('limits', $this->tab, $this->conf['search'], $this->conf['link'].$this->conf['orderlink'], 'page', $this->conf['perpage'], FALSE);
		}
		
		$this->order($where);
		$this->truncate();
		
		$result = $this->db->query($query) or die(draw_error($lang['mysql_error'], $this->db->error));
		while($row = $result->fetch_assoc()){
			foreach($row as $key => $value){
				if(isset($this->conf['fields'][$key]['abstraction'])){
					foreach($this->conf['fields'][$key]['abstraction'] as $abskey => $absvalue){
						if($abskey == $value){
							$row[$key] = $absvalue;
						}
					}
				}
			}
			$data[] = $row;
		}
		
		
		$this->conf['count'] = $result->num_rows;
		
		return $data;
		
	}
	
	function renderdata()
	{
		$table = '';
		
		$data = $this->getdata();
		
		if(isset($this->conf['renderfunc']) && trim($this->conf['renderfunc']) != '')
		{
			$renderfunc = trim($this->conf['renderfunc']);
			if(function_exists(trim($this->conf['renderfunc'])))
			{
				if(isset($this->conf['renderfunc_args']) && trim($this->conf['renderfunc_args']) != '')
				{
					$table = $renderfunc($this->conf, $this->tab_info, $data, trim($this->conf['renderfunc_args']));
				}
				else
				{
					$table = $renderfunc($this->conf, $this->tab_info, $data);
				}
			}
		}
		else
		{
			$table = draw_simpletable($this->conf, $this->tab_info, $data);
		}
		return $table;
	}	
	
	function show(){
		
		global $phproot, $lang, $autoform_success, $autoform_lock;
				
		
		if((isset($_GET['action'])) AND ($_GET['action'] == 'new' OR $_GET['action'] == 'edit' OR $_GET['action'] == 'del')){
						
			if(!is_array($this->conf['form'])){
			
				$this->conf['form'] = array();
			
				foreach($this->tab_info as $field){
					
					if(isset($this->conf['fields'][$field['Field']]['label'])){
						$label = $this->conf['fields'][$field['Field']]['label'];
					}else{
						$label = $field['name'];
					}
					
					if(isset($this->conf['fields'][$field['Field']]['input'])){
						$input = $this->conf['fields'][$field['Field']]['input'];
						
						if($input == 'password'){
							$this->conf['form'][] = array(
								'name'	=> $field['Field'],
								'type'	=> $input,
								'lang' => $lang['user_pass'],
								'lang_v' => $lang['user_pass_v'],
								'required' => '1',
								'required_msg' => '',
								'value'	=> '',
							);
						}else{
							$this->conf['form'][] = array(
								'name'	=> $field['Field'],
								'type'	=> $input,
								'lang'	=> $label,
								'value'	=> ''
							);
						}
					}else{
						$input = 'text';
						$this->conf['form'][] = array(
							'name'	=> $field['Field'],
							'type'	=> $input,
							'lang'	=> $label,
							'value'	=> ''
						);
					}
				}
			}
			
			if(!$this->conf['access_write'])
			{
				
			}
			else
			{
				
				if(isset($this->conf['rank']) && $this->conf['rank'] == true)
				{
					$result_rank = $this->db->query('SELECT id, rank FROM '.$this->tab.'') or die(draw_error(get_label('mysql_error'), $this->db->error));
					$this->conf['form'][] = array(
						'type'	=> 'fix',
						'name'	=> 'rank',
						'value'	=> ($result_rank->num_rows + 1)
					);
					
				}
				
				if(isset($_GET['ajax']) AND $_GET['ajax'] == 'getform'){
					echo autoform($this->tab, $this->conf['form'], $phproot, $this->conf['link'].'&', $this->conf['header'], '', 'modal');
				}else{
					
					$backlink = $phproot.'?'.$this->conf['link'];
					if(isset($this->conf['form_backlink']))
					{
						$backlink = $this->conf['form_backlink'];
					}
					
					$echo = '';
					$echo .= autoform($this->tab, $this->conf['form'], $phproot, $this->conf['link'].'&', $this->conf['header'], '', $this->conf['form_mode'], 'action', '', '', true);
					
					if($_GET['action'] == 'new')
					{
						if(isset($autoform_success) && $autoform_success == 1)
						{
							$echo .= $this->renderdata();
						}		
					}
					if($_GET['action'] == 'edit')
					{
						if(isset($autoform_success) && $autoform_success == 1)
						{
							$echo .= $this->renderdata();
						}		
					}
					if($_GET['action'] == 'del')
					{
						if(isset($autoform_success) && $autoform_success == 1)
						{
							
						}						
						$echo .= $this->renderdata();
					}				
					
					return $echo;
				}
			}
			
		}else{
				return $this->renderdata();
		}
	}
	
	
}

?>