<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

class simple_recursion{
	
	var $config;
	var $data;
	var $to_echo;
	var $db;
	
	function __construct($table, $getvar, $getvar_serach, $current_itemid, $query_add = '', $col_name = 'name', $col_id = 'id', $col_sub_of = 'sub_of'){
		
		global $config, $mysqli;
		
		$this->db = $mysqli;
		
		$this->config['table']		= $table;
		$this->config['getvar']		= $getvar;
		$this->config['getvar_serach']	= $getvar_serach;
		$this->config['current_itemid']	= $current_itemid;
		$this->config['query_add']	= $query_add;
		$this->config['col_name']	= $col_name;
		$this->config['col_id']		= $col_id;
		$this->config['col_sub_of']	= $col_sub_of;
		
		$this->data = '';
		
		$this->data = $this->recursion();
		
	}
	
	function get_list($root = NULL){
		
		$this->to_echo = '';
		$root_check = 0;
		if($root != NULL){
			$this->to_echo = $root;
			$root_check = 1;
		}
		$this->mk_list($this->data, $root_check);
		
		return $this->to_echo;
	}
	
	function recursion($id = '0', $deep = '0'){
		if($id != 0)$deep++;
		$data = array();
		$result = $this->db->query('
			SELECT 
				'.$this->config['col_id'].',
				'.$this->config['col_sub_of'].',
				'.$this->config['col_name'].'
			FROM 
				'.$this->config['table'].'
			WHERE '.$this->config['col_sub_of'].' = "'.$id.'" '.$this->config['query_add'].'
		');
		while($row = $result->fetch_assoc()){
			$row['recursion_deep'] = $deep;
			$row['recursion_subs'] = $this->recursion($row[$this->config['col_id']], $deep);
			$data[$row[$this->config['col_id']]] = $row;
		}
		return $data;
		
	}
	
	function mk_list($data, $root_check){
		
		if(is_array($data))foreach($data as $key => $row){
			
			
			$loopcheck = new loopcheck($this->config['table'], $this->config['current_itemid'], $row[$this->config['col_id']]);
			
			if($loopcheck->detection != 'loop'){
				$ok = 1;
			}else{
				$ok = 0;
			}
			
			$array = '';
			
			if($ok == 1){
				if($root_check != 0){
					$array .= '--';
				}else{
					$array .= '';
				}
				for($i=1;$i<=$row['recursion_deep'];$i++){
					$array .= '--';
				}
				$array .= $row[$this->config['col_name']].'';
				
				$this->to_echo[$key]['lang'] = $array;
				$this->to_echo[$key]['active'] = '';
				if(isset($_GET[$this->config['getvar']]) AND $_GET[$this->config['getvar']] == $row[$this->config['getvar_serach']]){
					$this->to_echo[$key]['default'] = '1';
				}
				if(isset($row['recursion_subs'])){
					$this->mk_list($row['recursion_subs'], $root_check);
				}
			}
		}
		
	}
	
}

?>