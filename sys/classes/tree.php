<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes http://www.feralbytes.ch
 * @copyright 2015 Feral Bytes
 * @license http://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

class tree{
	
	var $config;
	var $db;
	var $user;
	var $tree;
	var $rootline;
	var $current;
	
	function __construct($conf, $cUser = null)
	{
		global $user, $mysqli, $config;
		
		$this->config = $conf;
		$this->db = $mysqli;
		
		//error handling
		if(
			!isset($this->config['tab'])
		)
		{
			die('missing tree table');
		}
		
		//setup defaults
		if(!isset($this->config['fieldId']))
		{
			$this->config['fieldId'] = 'id';
		}
		if(!isset($this->config['fieldParent']))
		{
			$this->config['fieldParent'] = 'parent';
		}
		
		//Access Fields
		if(!isset($this->config['fieldAccessGroups']))
		{
			$this->config['fieldAccessGroups'] = 'groups';
		}
		if(!isset($this->config['fieldAccessGroupsMode']))
		{
			$this->config['fieldAccessGroupsMode'] = 'groups_mode';
		}
		//Access function
		if(!isset($this->config['funcAccess']))
		{
			$this->config['funcAccess'] = 'check_group_access';
		}
		
		//OnlineTime fields
		if(!isset($this->config['fieldOnlineTimeMode']))
		{
			$this->config['fieldOnlineTimeMode'] = 'online_time_mode';
		}
		if(!isset($this->config['fieldOnlineTimeStart']))
		{
			$this->config['fieldOnlineTimeStart'] = 'online_time_start';
		}
		if(!isset($this->config['fieldOnlineTimeEnd']))
		{
			$this->config['fieldOnlineTimeEnd'] = 'online_time_end';
		}
		//OnlineTime function
		if(!isset($this->config['funcOnlineTime']))
		{
			$this->config['funcOnlineTime'] = 'online_time';
		}
		
		//hidden field
		if(!isset($this->config['fieldHidden']))
		{
			$this->config['fieldHidden'] = 'hidden';
		}
		
		//online field
		if(!isset($this->config['fieldOnline']))
		{
			$this->config['fieldOnline'] = 'online';
		}
		
		//rank field
		if(!isset($this->config['fieldRank']))
		{
			$this->config['fieldRank'] = 'rank';
		}
		
		//where
		if(!isset($this->config['where']))
		{
			$this->config['where'] = array();
		}
		
		
		if(!isset($this->config['fields']))
		{
			$this->config['fields'] = array();
		}
		
		if(!isset($this->config['orderBy']))
		{
			$this->config['orderBy'] = $this->config['fieldId'].' ASC';
		}
		if(!isset($this->config['checkTime']))
		{
			$this->config['checkTime'] = false;
		}
		if(!isset($this->config['checkAccess']))
		{
			$this->config['checkAccess'] = false;
		}
		if(!isset($this->config['checkOnline']))
		{
			$this->config['checkOnline'] = false;
		}
		if(!isset($this->config['checkHidden']))
		{
			$this->config['checkHidden'] = false;
		}
		if(!isset($this->config['root']))
		{
			$this->config['root'] = 0;
		}
		if(!isset($this->config['current']))
		{
			$this->config['current'] = 0;
		}
		
		//setup query data
		$fields = array();
		$fields[] = $this->config['fieldId'];
		$fields[] = $this->config['fieldParent'];
		
		if(!isset($this->config['noRank']) || !$this->config['noRank'])
		{
			$fields[] = $this->config['fieldRank'];
		}
		else
		{
			$this->config['fieldRank'] = $this->config['fieldId'];
		}
		if(!is_array($this->config['fields']))
		{
			$this->config['fields'] = explode(',', $this->config['fields']);
		}
		
		if($this->config['checkHidden'])
		{
			$fields[] = $this->config['fieldHidden'];
		}
		
		if($this->config['checkOnline'])
		{
			$fields[] = $this->config['fieldOnline'];
		}
		
		if($this->config['checkTime'])
		{
			$fields[] = $this->config['fieldOnlineTimeMode'];
			$fields[] = $this->config['fieldOnlineTimeStart'];
			$fields[] = $this->config['fieldOnlineTimeEnd'];
		}
		
		if($this->config['checkAccess'])
		{
			$fields[] = $this->config['fieldAccessGroups'];
			$fields[] = $this->config['fieldAccessGroupsMode'];
		}
		
		$this->config['queryWhere'] = '';
		if(count($this->config['where']) > 0)
		{
			$this->config['queryWhere'] = ' AND '.implode(' AND ', $this->config['where']);
		}
		
		$this->config['fieldsQuery'] = array_merge($fields, $this->config['fields']);
		
		if($cUser == null)
		{
			$this->user = $user;
		}
		else
		{
			$this->user = $cUser;
		}
		
		if(!StartsWith($this->config['tab'], $config['prefix']))
		{
			$this->config['tab'] = $config['prefix'].$this->config['tab'];
		}
		
		//$this->tree = $this->GetTree($this->config['root']);
	}
	
	public function GetTree($root = 0, $lvl = 0, $isBranch = false)
	{
		$data = array();
		
		if($lvl == 0)
		{
			$this->rootline = array();
		}
		
		$root = $this->db->real_escape_string($root);
		
		$query = '
			SELECT
				'.implode(',', $this->config['fieldsQuery']).'
			FROM
				'.$this->config['tab'].'
			WHERE
				'.$this->config['fieldParent'].' = "'.$root.'"
				'.$this->config['queryWhere'].'
			ORDER BY
				'.$this->config['orderBy'].'
		';
		
		$result = $this->db->query($query);
		while($element = $result->fetch_assoc())
		{
			
			if($this->config['checkHidden'])
			{
				if($element[$this->config['fieldHidden']])
				{
					$element['isHidden'] = true;
				}
				else
				{
					$element['isHidden'] = false;
				}
			}
			
			$element['hasAccess'] = true;
			if($this->config['checkOnline'] && $element['hasAccess'])
			{
				if($element[$this->config['fieldOnline']])
				{
					$element['hasAccess'] = true;
				}
				else
				{
					$element['hasAccess'] = false;
				}
			}
			
			if($this->config['checkTime'] && $element['hasAccess'])
			{
				if($this->config['funcOnlineTime']($element))
				{
					$element['hasAccess'] = true;
				}
				else
				{
					$element['hasAccess'] = false;
				}
			}
			
			if($this->config['checkAccess'] && $element['hasAccess'])
			{
				if($this->config['funcAccess']($element[$this->config['fieldAccessGroups']], $element[$this->config['fieldAccessGroupsMode']], $this->user))
				{
					$element['hasAccess'] = true;
				}
				else
				{
					$element['hasAccess'] = false;
				}
			}
			$element['isBranch'] = $isBranch;
			$element['isCurrent'] = false;
			$element['isActive'] = false;
			if($this->config['current'] == $element[$this->config['fieldId']])
			{
				$element['isCurrent'] = true;
				$element['isActive'] = true;
				$element['isBranch'] = true;
				$this->current = $element;
			}
			
			$element['children'] = $this->GetTree($element[$this->config['fieldId']], $lvl+1, $element['isBranch']);
			$element['childrenCount'] = count($element['children']);
			$element['hasChildren'] = $element['childrenCount'] > 0 ? true : false;
			
			for($i = 0; $i < $element['childrenCount']; $i++)
			{
				if($element['children'][$i]['isActive'])
				{
					$element['isActive'] = true;
				}
			}
			
			if($element['isActive'])
			{
				$rootlineElement = $element;
				$this->rootline[] = $rootlineElement;
			}
			
			$element['lvl'] = $lvl;
			
			$data[] = $element;
		}
		
		
		return $data;
	}
	
	public function Move($id, $target)
	{
		$id = $this->db->real_escape_string($id);
		$target = $this->db->real_escape_string($target);
		
		$element = $this->GetElement($id);
		$target = $this->GetElement($target);
		
		$targetElements = $this->GetElementsByParent($target[$this->config['fieldId']]);
		
		$iMax = 0;
		
		for($i = 0; $i < count($targetElements); $i++)
		{
			if($i >= $iMax)
			{
				$iMax = $i;
			}
			
			$this->UpdateElement(
				$targetElements[$i][$this->config['fieldId']],
				array(
					$this->config['fieldRank'] => $i
				)
			);
		}
	
		$this->UpdateElement(
			$element[$this->config['fieldId']],
			array(
				$this->config['fieldParent'] => $target[$this->config['fieldId']],
				$this->config['fieldRank'] => $iMax+1
			)
		);
		
		$this->CleanOrdering($id);
		
	}
	
	public function GetElement($id)
	{
		$id = $this->db->real_escape_string($id);
		
		$query = '
			SELECT
				'.implode(',', $this->config['fieldsQuery']).'
			FROM
				'.$this->config['tab'].'
			WHERE
				'.$this->config['fieldId'].' = "'.$id.'"
		';
		
		$result = $this->db->query($query);
		$element = $result->fetch_assoc();
		
		if(is_array($element))
		{
			return $element;
		}
		else
		{
			return null;
		}
	}
	
	public function GetElementsByParent($id)
	{
		$id = $this->db->real_escape_string($id);
		
		$query = '
			SELECT
				'.implode(',', $this->config['fieldsQuery']).'
			FROM
				'.$this->config['tab'].'
			WHERE
				'.$this->config['fieldParent'].' = "'.$id.'"
			ORDER BY
				'.$this->config['fieldRank'].' ASC
		';
		
		$elements = array();
		$result = $this->db->query($query);
		while($element = $result->fetch_assoc())
		{
			$elements[] = $element;
		}
		
		return $elements;
	}
	
	public function UpdateElement($id, $data)
	{
		$id = $this->db->real_escape_string($id);
		
		$queryData = array();
		
		if(is_array($data))
		foreach($data as $key => $value)
		{
			
			if(
				$key == $this->config['fieldRank']
				&&
				!(!isset($this->config['noRank']) || !$this->config['noRank'])
			)
			{
				continue;
			}
			
			$queryData[] = ''.$this->db->real_escape_string($key).' = "'.$this->db->real_escape_string($value).'"';
		}
		
		$query = '
			UPDATE
				'.$this->config['tab'].'
			SET
				'.implode(',', $queryData).'
			WHERE
				'.$this->config['fieldId'].' = "'.$id.'"
		';
		
		$result = $this->db->query($query);
		
		return $result;
	}
	
	public function DeleteElement($id)
	{
		$id = $this->db->real_escape_string($id);
		
		$query = '
			SELECT
				'.implode(',', $this->config['fieldsQuery']).'
			FROM
				'.$this->config['tab'].'
			WHERE
				'.$this->config['fieldParent'].' = "'.$id.'"
		';
		
		$result = $this->db->query($query);
		while($child = $result->fetch_assoc())
		{
			$this->DeleteElement($child[$this->config['fieldId']]);
		}
		
		$query = '
			DELETE FROM '.$this->config['tab'].'
			WHERE
				'.$this->config['fieldId'].' = "'.$id.'"
		';
		$result = $this->db->query($query);
		
	}
	
	public function DeleteChildren($id)
	{
		$id = $this->db->real_escape_string($id);
		
		$query = '
			SELECT
				'.implode(',', $this->config['fieldsQuery']).'
			FROM
				'.$this->config['tab'].'
			WHERE
				'.$this->config['fieldParent'].' = "'.$id.'"
		';
		
		$result = $this->db->query($query);
		while($child = $result->fetch_assoc())
		{
			$this->DeleteElement($child[$this->config['fieldId']]);
		}
	}
	
	public function MoveElement($id, $mode, $after = 0)
	{
		switch($mode)
		{
			case "up":
				$this->MoveElementLinear($id, 'up');
				break;
			case "down":
				$this->MoveElementLinear($id, 'down');
				break;
			case "after":
				$this->MoveElementAfter($id, $after);
				break;
			default:
				break;
		}
	}
	
	public function MoveElementAfter($id, $after = 0)
	{
		$element = $this->GetElement($id);
		$elements = $this->GetElementsByParent($element[$this->config['fieldParent']]);
		
		$this->CleanOrdering($id);
		
		$offset = 0;
		$pos = 0;
		if($after == 0)
		{
			$pos = 0;
			$offset = 1;
		}
		
		for($i = 0; $i < count($elements); $i++)
		{	
			if($after != 0 && $elements[$i][$this->config['fieldId']] == $after)
			{
				$offset = 1;
				$pos = $i+1;
			}
		}
		
		for($i = 0; $i < count($elements); $i++)
		{
			if($i >= $pos)
			{
				$this->UpdateElement(
					$elements[$i][$this->config['fieldId']],
					array(
						$this->config['fieldRank'] => $i+$offset
					)
				);
			}
		}
		
		$this->UpdateElement(
			$element[$this->config['fieldId']],
			array(
				$this->config['fieldRank'] => $pos
			)
		);
		
	}
	
	public function CleanOrdering($id)
	{
		$element = $this->GetElement($id);
		$elements = $this->GetElementsByParent($element[$this->config['fieldParent']]);

		for($i = 0; $i < count($elements); $i++)
		{
			$this->UpdateElement(
				$elements[$i][$this->config['fieldId']],
				array(
					$this->config['fieldRank'] => $i
				)
			);
		}		
	
	}
	
	public function MoveElementLinear($id, $dir)
	{
		$id = $this->db->real_escape_string($id);
		
		$element = $this->GetElement($id);
		$elements = $this->GetElementsByParent($element[$this->config['fieldParent']]);
		
		$offset = 1;
		if($dir == 'up')
		{
			$offset = -1;
		}
		if($dir == 'down')
		{
			$offset = 1;
		}
		
		for($i = 0; $i < count($elements); $i++)
		{
			if($elements[$i][$this->config['fieldId']] == $element[$this->config['fieldId']])
			{
				if(isset($elements[$i+$offset]))
				{
					$this->UpdateElement(
						$element[$this->config['fieldId']],
						array(
							$this->config['fieldRank'] => $elements[$i+$offset][$this->config['fieldRank']]
						)
					);
					$this->UpdateElement(
						$elements[$i+$offset][$this->config['fieldId']],
						array(
							$this->config['fieldRank'] => $elements[$i][$this->config['fieldRank']]
						)
					);
				}
				break;
			}
		}
	}
	
	public function CopyElement($id, $target, $lvl = 0)
	{
		$id = $this->db->real_escape_string($id);
		
		$element = db_get($this->config['tab'], array($this->config['fieldId'] => $id));
		$element = $element[$id];
		$element[$this->config['fieldParent']] = $target;
		if($lvl == 0)
		{
			$result = $this->db->query('SELECT MAX( '.$this->config['fieldRank'].' ) AS '.$this->config['fieldRank'].' FROM '.$this->config['tab'].' WHERE '.$this->config['fieldParent'].' = "'.$target.'"') or die(draw_error(get_label('mysql_error'), $mysqli->error));
			$row = $result->fetch_assoc();
			$element[$this->config['fieldRank']] = $row[$this->config['fieldRank']]+1; 
		}
		
		$newParentId = db_insert($this->config['tab'], $element);
		
		$query = '
			SELECT
				'.implode(',', $this->config['fieldsQuery']).'
			FROM
				'.$this->config['tab'].'
			WHERE
				'.$this->config['fieldParent'].' = "'.$id.'"
		';
		
		$result = $this->db->query($query);
		while($child = $result->fetch_assoc())
		{
			$this->CopyElement($child[$this->config['fieldId']], $newParentId, $lvl+1);
		}
		
		return $newParentId;
	}
	
	public function GetRootline()
	{
		if(!is_array($this->tree) || !is_array($this->rootline) || count($this->rootline) < 1)
		{
			$this->tree = $this->GetTree($this->config['root']);
		}
		
		return array_reverse($this->rootline);
	}

}

?>