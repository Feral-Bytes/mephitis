<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

class dbcheck{
	
	var $config;
	var $current_db;
	var $current_db_array;
	var $errors;
	var $db;

	function __construct($config, $current_db_array){
		
		global $mysqli;
		
		$this->db = $mysqli;
		
		$this->config		= $config;
		
		foreach($current_db_array as $table => $data){
			if(!isset($current_db_array[$table]['Name'])){
				$current_db_array[$table]['Name'] = $table;
			}
			
			if(!isset($current_db_array[$table]['Engine'])){
				$current_db_array[$table]['Engine'] = 'MyISAM';
			}
			
			if(!isset($current_db_array[$table]['Collation'])){
				$current_db_array[$table]['Collation'] = 'utf8_general_ci';
			}
			
			$previous = '';
			
			foreach($data['fields'] as $field => $field_data){
				if(!isset($field_data['Name'])){
					$current_db_array[$table]['fields'][$field]['Field']	= $field;
				}

				if(!isset($field_data['Null'])){
					$current_db_array[$table]['fields'][$field]['Null']	= 'NO';
				}

				if(!isset($field_data['Key'])){
					$current_db_array[$table]['fields'][$field]['Key']	= '';
				}

				if(!isset($field_data['Default'])){
					$current_db_array[$table]['fields'][$field]['Default']	= '';
				}

				if(!isset($field_data['Extra'])){
					$current_db_array[$table]['fields'][$field]['Extra']	= '';
				}
				
				$current_db_array[$table]['fields'][$field]['previous'] = $previous;
				$previous = $current_db_array[$table]['fields'][$field]['Field'];
			}
		}
		
		$this->current_db_array	= $current_db_array;
	}
	
	function load_db(){
		
		$db = array();
		
		$result = $this->db->query('SHOW TABLE STATUS FROM '.$this->config['db_name'].' LIKE "'.$this->config['prefix'].'%"');
		while($row = $result->fetch_assoc()){

			$row['keys'] = '';
			$result_keys = $this->db->query('SHOW KEYS FROM '.$row['Name'].'');
			$row_keys = $result_keys->fetch_assoc();
			$row['keys'] = $row_keys;

			$previous = '';
			$row['fields'] = array();
			$result_fields = $this->db->query('SHOW FULL FIELDS FROM '.$row['Name'].'');
			while($row_fields = $result_fields->fetch_assoc()){
				$row_fields['previous'] = $previous;
				$row['fields'][$row_fields['Field']] = $row_fields;
				$previous = $row_fields['Field'];
			}
			$db[$row['Name']] = $row;
		}
		
		if(is_array($db))
		$this->current_db = $db;
	
	}
	
	function check(){
		
		$db_chache	= '';
		$errors		= array();
		foreach($this->current_db_array as $table){
			
			if(isset($this->current_db[$table['Name']])){
				$this->current_db[$table['Name']]['check'] = 'ok';
				if($this->current_db[$table['Name']]['Engine'] != $table['Engine']){
					$errors[$table['Name']]['Engine'] = $table['Engine'];
					$this->current_db[$table['Name']]['check'] = 'engine_error';
				}
				if($this->current_db[$table['Name']]['Collation'] != $table['Collation']){
					$errors[$table['Name']]['Collation'] = $table['Collation'];
					$this->current_db[$table['Name']]['check'] = 'collation_error';
				}
				
				$this->current_db[$table['Name']]['field_check'] = 'ok';
				foreach($table['fields'] as $field){
					
					if(isset($this->current_db[$table['Name']]['fields'][$field['Field']])){
						$this->current_db[$table['Name']]['fields'][$field['Field']]['check'] = 'ok';
						if($this->current_db[$table['Name']]['fields'][$field['Field']]['Type'] != $field['Type']){
							$errors[$table['Name']]['fields'][$field['Field']]['Type'] = $field['Type'];
							$this->current_db[$table['Name']]['fields'][$field['Field']]['check'] = 'error';
							$this->current_db[$table['Name']]['field_check'] = 'error';
						}
						if($this->current_db[$table['Name']]['fields'][$field['Field']]['Null'] != $field['Null']){
							$errors[$table['Name']]['fields'][$field['Field']]['Null'] = $field['Null'];
							$this->current_db[$table['Name']]['fields'][$field['Field']]['check'] = 'error';
							$this->current_db[$table['Name']]['field_check'] = 'error';
						}
						if($this->current_db[$table['Name']]['fields'][$field['Field']]['Key'] != $field['Key']){
							$errors[$table['Name']]['fields'][$field['Field']]['Key'] = $field['Key'];
							$this->current_db[$table['Name']]['fields'][$field['Field']]['check'] = 'error';
							$this->current_db[$table['Name']]['field_check'] = 'error';
						}
						if($this->current_db[$table['Name']]['fields'][$field['Field']]['Extra'] != $field['Extra']){
							$errors[$table['Name']]['fields'][$field['Field']]['Extra'] = $field['Extra'];
							$this->current_db[$table['Name']]['fields'][$field['Field']]['check'] = 'error';
							$this->current_db[$table['Name']]['field_check'] = 'error';
						}

						if($this->current_db[$table['Name']]['fields'][$field['Field']]['previous'] != $field['previous']){
							$errors[$table['Name']]['fields'][$field['Field']]['previous'] = $field['previous'];
							$this->current_db[$table['Name']]['fields'][$field['Field']]['check'] = 'sort_error';
							$this->current_db[$table['Name']]['field_check'] = 'sort_error';
						}

						if(isset($this->current_db[$table['Name']]['fields'][$field['Field']]['Collation']) AND $this->current_db[$table['Name']]['fields'][$field['Field']]['Collation'] != 'utf8_general_ci'){
							$errors[$table['Name']]['fields'][$field['Field']]['Collation'] = 'utf8_general_ci';
							$this->current_db[$table['Name']]['fields'][$field['Field']]['check'] = 'collation_error';
							$this->current_db[$table['Name']]['field_check'] = 'collation_error';
						}

						
					}else{
						$errors[$table['Name']]['fields'][$field['Field']] = 'not_found';
						$this->current_db[$table['Name']]['field_check'] = 'error';
					}
				}
				
				if(isset($table['vars']) AND is_array($table['vars'])){
					$result = $this->db->query('SELECT * FROM '.$table['Name'].'');
					while($row = $result->fetch_assoc()){
						$this->current_db[$table['Name']]['vars'][$row['variable']] = '';
					}
					
					$this->current_db[$table['Name']]['var_check'] = 'ok';
					foreach($table['vars'] as $key => $value){
						if(isset($this->current_db[$table['Name']]['vars'][$key])){
							$this->current_db[$table['Name']]['vars'][$key] = 'ok';
						}else{
							$this->current_db[$table['Name']]['vars'][$key] = 'not_found';
						}
						
						if($this->current_db[$table['Name']]['vars'][$key] != 'ok'){
							$this->current_db[$table['Name']]['var_check'] = 'error';
							$errors[$table['Name']]['vars'][$key] = 'not_found';
						}
					}
					
				}
				
			}else{
				$errors[$table['Name']] = 'not_found';
			}
		}
	
		$this->errors = $errors;
		
	}
	
	function repair(){ 
		
		if(is_array($this->errors))foreach($this->errors as $table => $error){
			if(is_array($error) && count($error) > 0){
				// edit table
				if(isset($error['fields']) AND is_array($error['fields']))foreach($error['fields'] as $field => $data){
					$sql = '';
					if(is_array($data)){
						//edit field
						if(isset($data['previous'])){
							if($data['previous'] != ''){
								$sql = 'ALTER TABLE '.$table.' MODIFY '.$field.' '.$this->current_db_array[$table]['fields'][$field]['Type'].' AFTER '.$data['previous'].'';
							}else{
								$sql = 'ALTER TABLE '.$table.' MODIFY '.$field.' '.$this->current_db_array[$table]['fields'][$field]['Type'].' FIRST';
							}
							$this->db->query($sql)or die(draw_error('MySql Error', $this->db->error));
						}
						
						if($this->current_db_array[$table]['fields'][$field]['Null'] == 'NO'){
							$null = 'NOT NULL';
						}
						
						if(isset($this->current_db_array[$table]['fields'][$field]['Extra']) AND $this->current_db_array[$table]['fields'][$field]['Extra'] == 'auto_increment'){
							$extra = 'AUTO_INCREMENT';
						}else{
							$extra = '';
						}
						
						if(isset($data['Collation'])){
							$collation = 'CHARACTER SET utf8 COLLATE '.$data['Collation'].'';
						}else{
							$collation = '';
						}
						
						$sql = 'ALTER TABLE '.$table.' CHANGE '.$field.' '.$field.' '.$this->current_db_array[$table]['fields'][$field]['Type'].' '.$collation.' '.$null.' '.$extra.'';
						$this->db->query($sql)or die(draw_error('MySql Error', $this->db->error));
						
					}else if($data == 'not_found'){
						//new field
						$null = '';
						if($this->current_db_array[$table]['fields'][$field]['Null'] == 'NO'){
							$null = 'NOT NULL';
						}
						
						if(isset($this->current_db_array[$table]['fields'][$field]['Extra']) AND $this->current_db_array[$table]['fields'][$field]['Extra'] == 'auto_increment'){
							$extra = 'AUTO_INCREMENT';
						}else{
							$extra = '';
						}
						
						$previous = '';
						if(isset($this->current_db_array[$table]['fields'][$field]['previous'])){
							if($this->current_db_array[$table]['fields'][$field]['previous'] != ''){
								$previous = 'AFTER '.$this->current_db_array[$table]['fields'][$field]['previous'];
							}else{
								$previous = 'FIRST';
							}
						}else{
							$previous = '';
						}
						
						$sql = 'ALTER TABLE '.$table.' ADD '.$field.' '.$this->current_db_array[$table]['fields'][$field]['Type'].' '.$null.' '.$extra.' '.$previous.' ';
						$this->db->query($sql)or die(draw_error('MySql Error', $this->db->error));
						
					}
				}
				
				if(isset($error['Engine']) OR isset($error['Collation'])){
					$sql = 'ALTER TABLE '.$table.' ENGINE = '.$this->current_db_array[$table]['Engine'].' DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci';
					$this->db->query($sql)or die(draw_error('MySql Error', $this->db->error));
				}
				
				if(isset($error['vars']) AND is_array($error['vars']))foreach($error['vars'] as $var => $data){
					if($data == 'not_found'){
						$sql = 'INSERT INTO `'.$table.'` (`variable`, `value`) VALUES (\''.$var.'\', \''.$this->current_db_array[$table]['vars'][$var].'\');'; 
						$this->db->query($sql)or die(draw_error('MySql Error', $this->db->error));
					}
				}
				
			}else if($error == 'not_found'){
				
				$sql = array();
				$sql_pri = '';
				foreach($this->current_db_array[$table]['fields'] as $field => $data){
					
					$null = '';
					if($this->current_db_array[$table]['fields'][$field]['Null'] == 'NO'){
						$null = 'NOT NULL';
					}
					
					if(isset($this->current_db_array[$table]['fields'][$field]['Extra']) AND $this->current_db_array[$table]['fields'][$field]['Extra'] == 'auto_increment'){
						$extra = 'AUTO_INCREMENT';
					}else{
						$extra = '';
					}

					if($this->current_db_array[$table]['fields'][$field]['Key'] == 'PRI'){
						$sql_pri = 'PRIMARY KEY ( '.$field.' )';
					}
					
					$sql[] = ''.$field.' '.$data['Type'].' '.$null.' '.$extra.' ';
					
					
				}
				
				$sql = implode(', ', $sql);
				if($sql_pri != '')
				{
					$sql .= ', '.$sql_pri;
				}
				
				$sql = '
				CREATE TABLE '.$this->config['db_name'].'.'.$table.' (
				'.$sql.'
				) ENGINE = '.$this->current_db_array[$table]['Engine'].';'; 
				
				$this->db->query($sql)or die(draw_error('MySql Error', $this->db->error));
			}
		}
		
		
	}
	
	function del($mode, $table, $field){
		
		$table	= $this->db->real_escape_string($table);
		$field	= $this->db->real_escape_string($field);
		
		if($mode == 'field'){
			if(!isset($this->current_db[$table]['fields'][$field]['check'])){
				$sql = 'ALTER TABLE '.$table.' DROP '.$field.'';
				$this->db->query($sql)or die(draw_error('MySql Error', $this->db->error));
			}
		}

		if($mode == 'table'){
			if(!isset($this->current_db[$table]['check'])){
				$sql = 'DROP TABLE '.$table.'';
				$this->db->query($sql)or die(draw_error('MySql Error', $this->db->error));
			}
		}

		if($mode == 'variable'){
			if(isset($this->current_db[$table]['vars'][$field]) AND $this->current_db[$table]['vars'][$field] != 'ok'){
				$sql = 'DELETE FROM '.$table.' WHERE variable = "'.$field.'"';
				$this->db->query($sql)or die(draw_error('MySql Error', $this->db->error));
			}
			
		}
		
	}
	
	function backup($formdata){
		
		foreach($this->current_db as $table => $data){
			$result = $this->db->query('SHOW CREATE TABLE '.$table.'');
			$row = $result->fetch_assoc();
			
			$this->current_db[$table]['Create Table'] = $row['Create Table'];
			
		}
		
		if(!isset($formdata['breakat']) OR !is_numeric($formdata['breakat']) OR $formdata['breakat'] <= 100)$formdata['breakat'] = 100;
		
		$sql = '';
		foreach($this->current_db as $table => $data){
			
			if(isset($formdata['drop_table_if_exists']))$sql .= 'DROP TABLE IF EXISTS `'.$table.'`;'."\r\n";
			$sql .= $data['Create Table'].'; '."\r\n".''."\r\n".'';
			
			$fields = '';
			foreach($data['fields'] as $field){
				$fields .= '`'.$field['Field'].'`, ';
			}
			$fields = substr($fields, 0, -2);
			$insert = 'INSERT INTO `'.$table.'` ('.$fields.') VALUES '."\r\n";
			
			$sql_rows = '';
			$i = 0;
			$result = $this->db->query('SELECT * FROM '.$table.'');
			while($row = $result->fetch_assoc()){
				$i++;
				$cols = '';
				foreach($row as $col){
					if(isset($formdata['send_as_file'])){
						$cols .= '\''.(addslashes($col)).'\', ';
					}else{
						$cols .= '\''.(htmlspecialchars(addslashes($col))).'\', ';
					}
				}
				$sql_rows .= '('.substr($cols, 0, -2).')'.','."\r\n";
				if($i >= $formdata['breakat']){
					$i = 0;
					$sql_rows = substr($sql_rows, 0, -3).';'."\r\n"."\r\n".$insert;
				}
			}
			if($result->num_rows > 0){
				$query = $insert.''.substr($sql_rows, 0, -3).';'."\r\n";
				$sql .= $query."\r\n";
			}
		}
		
		if(isset($formdata['send_as_file'])){
			header('Content-Type: text/plain; charset=UTF-8');
			header('Content-Disposition: attachment; filename="'.getvar('title').'-'.date('Y-m-d-H-i-s').'.sql"');
			echo $sql;
			exit;
		}else{
			return $sql;
		}
	}

}

?>