<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

define('MephitisCMS', 'backend');
 
$rootDir = dirname(dirname($_SERVER['PHP_SELF']));
if(strlen($rootDir) <= 1)
{
	$rootDir = '';
}
$phpRootPath = dirname(__DIR__).'/';
$httpRootPath = $rootDir.'/';
$phproot = $rootDir.'/sys/index.php';

$stylefile = 'style_admin.php';

include($phpRootPath.'sys/init.php');
include($phpRootPath.'sys/admin_templates.php');
include_once(include_file('lang', 'lang_admin'));


header('Content-Type: text/html; charset=utf-8');
header("Cache-Control: no-cache, no-store, must-revalidate, private");
header("Pragma: no-cache");
header("X-XSS-Protection: 1; mode=block");
header("X-Content-Type-Options: nosniff");
header("X-Frame-Options: SAMEORIGIN");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

//admintools
admin_tools();

//usertools
user_tools();

if (version_compare(phpversion(), $minPhpVersion, '<')) {
    $msg_stack .= draw_alert('<h2>Warning you are using PHP '.phpversion().' which is not supported, malfunction iminent!<h2>', 'danger');
}

if(isset($_GET['cache_action']))
{
	if($_GET['cache_action'] == 'clear')
	{
		CacheClear();
		$msg = get_label('cache_cleared');
		
		if(isset($_GET['ajax']))
		{
			echo $msg;
			exit;
		}
		$msg_stack .= draw_alert(get_label('cache_cleared'), 'success');
	}
}

include_once(include_file('template', 'overrides_admin.php'));

if(isset($_GET['usercp'])  && !isset($_POST['login']) && $_GET['usercp'] != 'logout')
{
	include($phpRootPath.'sys/usercp.php');
}
else if(isset($_GET['filemanager']))
{
	include($phpRootPath.'sys/filemanager.php');
}
else if(isset($_GET['pms']))
{
	include($phpRootPath.'sys/pms.php');
}
else
{
	if($user['perm'] != 'admin'){
		$formdata = array(
			'name' => 'login',
			'action' => $phproot.'?usercp=login',
			'method' => 'POST',
			'fileadd' => ''
		);
		$content .= draw_content($lang['login'], draw_login($formdata));
	}else{

		include($phpRootPath.'sys/admin.php');

	}
}

if(!isset($_GET['ajax'])){
	$echo = new style();
	if(isset($_GET['iframe']))
	{
		if(method_exists($echo, "draw_iframe"))
		{
			echo $echo->draw_iframe($content);
		}
		else
		{
			echo $echo->draw_site($content);
		}
	}
	else
	{
		echo $echo->draw_site($content);
	}
}

if(isset($_GET['cache_action']))
{
	if($_GET['cache_action'] == 'clear')
	{
		
	}
}

//debug mode
if($config['debugmode'] == 1){

	$query = "SHOW SESSION STATUS LIKE 'Questions'";
	$result = $mysqli->query($query);
	$row = $result->fetch_assoc();
	
	debug ($row);

	$endtime = microtime(TRUE);
	debug(round($endtime-$starttime, 3));

	debug($_SESSION);
	debug($_COOKIE);
	debug($_SERVER);
	debug($_GET);
	debug($_POST);
	debug($user);
}

?>