<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

//comments

$database[$config['prefix'].'comments']['fields']['id'] = array(
	'Type' => 'int(10)',
	'Key' => 'PRI',
	'Extra' => 'auto_increment'
);
$database[$config['prefix'].'comments']['fields']['object']['Type']		= 'text';
$database[$config['prefix'].'comments']['fields']['object_id']['Type']	= 'int(10)';
$database[$config['prefix'].'comments']['fields']['user_id']['Type']	= 'int(10)';
$database[$config['prefix'].'comments']['fields']['user_name']['Type']	= 'text';
$database[$config['prefix'].'comments']['fields']['subject']['Type']	= 'text';
$database[$config['prefix'].'comments']['fields']['content']['Type']	= 'mediumtext';
$database[$config['prefix'].'comments']['fields']['ip']['Type']			= 'text';
$database[$config['prefix'].'comments']['fields']['time']['Type']		= 'int(10)';
$database[$config['prefix'].'comments']['fields']['online']['Type']		= 'int(1)';

//file directorys

$database[$config['prefix'].'file_dirs']['fields']['id'] = array(
	'Type' => 'int(10)',
	'Key' => 'PRI',
	'Extra' => 'auto_increment'
);
$database[$config['prefix'].'file_dirs']['fields']['sub_of']['Type']					= 'int(10)';
$database[$config['prefix'].'file_dirs']['fields']['name']['Type']					= 'text';;
$database[$config['prefix'].'file_dirs']['fields']['groups']['Type']					= 'text';
$database[$config['prefix'].'file_dirs']['fields']['groups_mode']['Type']			= 'varchar(255)';

//files

$database[$config['prefix'].'files']['fields']['id'] = array(
	'Type' => 'int(10)',
	'Key' => 'PRI',
	'Extra' => 'auto_increment'
);
$database[$config['prefix'].'files']['fields']['user']['Type']				= 'int(10)';
$database[$config['prefix'].'files']['fields']['dir']['Type']				= 'int(10)';
$database[$config['prefix'].'files']['fields']['module']['Type']			= 'text';
$database[$config['prefix'].'files']['fields']['module_options']['Type']	= 'text';
$database[$config['prefix'].'files']['fields']['name']['Type']				= 'text';
$database[$config['prefix'].'files']['fields']['path']['Type']				= 'text';
$database[$config['prefix'].'files']['fields']['file']['Type']				= 'text';
$database[$config['prefix'].'files']['fields']['file_name']['Type']			= 'text';
$database[$config['prefix'].'files']['fields']['type']['Type']				= 'text';
$database[$config['prefix'].'files']['fields']['size']['Type']				= 'text';
$database[$config['prefix'].'files']['fields']['thumb']['Type']				= 'int(10)';
$database[$config['prefix'].'files']['fields']['time']['Type'] 				= 'int(10)';
$database[$config['prefix'].'files']['fields']['groups']['Type']			= 'text';
$database[$config['prefix'].'files']['fields']['groups_mode']['Type']		= 'varchar(255)';

//ips

$database[$config['prefix'].'ips']['fields']['id'] = array(
	'Type' => 'int(10)',
	'Key' => 'PRI',
	'Extra' => 'auto_increment'
);
$database[$config['prefix'].'ips']['fields']['ip']['Type']				= 'text';
$database[$config['prefix'].'ips']['fields']['host']['Type']			= 'text';
$database[$config['prefix'].'ips']['fields']['user']['Type']			= 'int(10)';
$database[$config['prefix'].'ips']['fields']['request_method']['Type']	= 'text';
$database[$config['prefix'].'ips']['fields']['php_self']['Type']		= 'text';
$database[$config['prefix'].'ips']['fields']['http_user_agent']['Type']	= 'text';
$database[$config['prefix'].'ips']['fields']['http_referer']['Type']	= 'text';
$database[$config['prefix'].'ips']['fields']['time']['Type']			= 'int(10)';

//settings

$database[$config['prefix'].'settings']['fields']['variable']['Type']			= 'text';
$database[$config['prefix'].'settings']['fields']['value']['Type']				= 'text';
$database[$config['prefix'].'settings']['vars']['title']						= 'Mephitis CMS';
$database[$config['prefix'].'settings']['vars']['footer']						= '';
$database[$config['prefix'].'settings']['vars']['homepage']						= '0';
$database[$config['prefix'].'settings']['vars']['httpRootPath']					= '';
$database[$config['prefix'].'settings']['vars']['http']							= '';
$database[$config['prefix'].'settings']['vars']['domain']						= '';
$database[$config['prefix'].'settings']['vars']['homepage']						= '0';
$database[$config['prefix'].'settings']['vars']['cookie_name']					= 'mephitis';
$database[$config['prefix'].'settings']['vars']['public_register']				= '1';
$database[$config['prefix'].'settings']['vars']['public_login']					= '1';
$database[$config['prefix'].'settings']['vars']['public_show_login']			= '1';
$database[$config['prefix'].'settings']['vars']['thumbnail_size']				= '100';
$database[$config['prefix'].'settings']['vars']['norm_time']					= 'H:i';
$database[$config['prefix'].'settings']['vars']['norm_date']					= 'd.m.Y, H:i';
$database[$config['prefix'].'settings']['vars']['norm_date_notime']				= 'd.m.Y';
$database[$config['prefix'].'settings']['vars']['meta_robots']					= 'noindex,nofollow';
$database[$config['prefix'].'settings']['vars']['meta_author']					= 'Mephitis';
$database[$config['prefix'].'settings']['vars']['meta_description']				= 'Mephitis CMS';
$database[$config['prefix'].'settings']['vars']['meta_keywords']				= 'CMS, PHP, Mephitis';
$database[$config['prefix'].'settings']['vars']['shortcut_icon']				= '';
$database[$config['prefix'].'settings']['vars']['save_accesses']				= '1';
$database[$config['prefix'].'settings']['vars']['template_dir']					= 'mephitis1';
$database[$config['prefix'].'settings']['vars']['per_page']						= '10';
$database[$config['prefix'].'settings']['vars']['bbcode']						= '1';
$database[$config['prefix'].'settings']['vars']['emojis']						= '1';
$database[$config['prefix'].'settings']['vars']['wysiwyg']						= 'bbcode';
$database[$config['prefix'].'settings']['vars']['num_emojis']					= '8';
$database[$config['prefix'].'settings']['vars']['register_text']				= '';
$database[$config['prefix'].'settings']['vars']['forgotpassword_max_age']		= 24*3600;
$database[$config['prefix'].'settings']['vars']['admin_name']					= '';
$database[$config['prefix'].'settings']['vars']['admin_email']					= '';
$database[$config['prefix'].'settings']['vars']['useronline_timeout']			= '300';
$database[$config['prefix'].'settings']['vars']['new_user_groups']				= '';
$database[$config['prefix'].'settings']['vars']['guest_user_groups']			= '';
$database[$config['prefix'].'settings']['vars']['msg_box_fadeout_timer']		= '5';
$database[$config['prefix'].'settings']['vars']['comment_mod_groups']			= '';
$database[$config['prefix'].'settings']['vars']['comment_guests_can_write']		= '';
$database[$config['prefix'].'settings']['vars']['comment_members_can_write']	= '1';
$database[$config['prefix'].'settings']['vars']['comment_requires_mod_check']	= '';
$database[$config['prefix'].'settings']['vars']['global_tracking_enable']		= '';
$database[$config['prefix'].'settings']['vars']['global_tracking_code']			= '';
$database[$config['prefix'].'settings']['vars']['file_upload_groups']			= '';
$database[$config['prefix'].'settings']['vars']['reCAPTCHA']					= '0';
$database[$config['prefix'].'settings']['vars']['reCAPTCHA_key_private']		= '6LfGsAYTAAAAAFLU25ZwykUT38Y8DYgS1sSiBxpD';
$database[$config['prefix'].'settings']['vars']['reCAPTCHA_key_public']			= '6LfGsAYTAAAAADqO4FFkikZIHrwN4bDU0fCu2cU8';
$database[$config['prefix'].'settings']['vars']['email_smtp']					= '0';
$database[$config['prefix'].'settings']['vars']['email_smtp_host']				= '';
$database[$config['prefix'].'settings']['vars']['email_smtp_auth']				= '';
$database[$config['prefix'].'settings']['vars']['email_smtp_user']				= '';
$database[$config['prefix'].'settings']['vars']['email_smtp_pass']				= '';
$database[$config['prefix'].'settings']['vars']['email_smtp_secure']			= '';
$database[$config['prefix'].'settings']['vars']['email_smtp_port']				= '587';
$database[$config['prefix'].'settings']['vars']['email_smtp_from_name']			= '';
$database[$config['prefix'].'settings']['vars']['email_smtp_from_email']		= '';

//pages

$database[$config['prefix'].'pages']['fields']['id'] = array(
	'Type' => 'int(10)',
	'Key' => 'PRI',
	'Extra' => 'auto_increment'
);
$database[$config['prefix'].'pages']['fields']['parent']['Type']				= 'int(10)';
$database[$config['prefix'].'pages']['fields']['rank']['Type']					= 'int(10)';
$database[$config['prefix'].'pages']['fields']['name']['Type']					= 'text';
$database[$config['prefix'].'pages']['fields']['shortlink']['Type']				= 'varchar(255)';
$database[$config['prefix'].'pages']['fields']['icon']['Type']					= 'varchar(255)';
$database[$config['prefix'].'pages']['fields']['template']['Type']				= 'varchar(255)';
$database[$config['prefix'].'pages']['fields']['content']['Type']				= 'mediumtext';
$database[$config['prefix'].'pages']['fields']['content_kind']['Type']			= 'varchar(255)';
$database[$config['prefix'].'pages']['fields']['content_link_target']['Type']	= 'text';
$database[$config['prefix'].'pages']['fields']['online']['Type']				= 'int(1)';
$database[$config['prefix'].'pages']['fields']['online_time_mode']['Type']		= 'varchar(255)';
$database[$config['prefix'].'pages']['fields']['online_time_start']['Type']		= 'int(10)';
$database[$config['prefix'].'pages']['fields']['online_time_end']['Type']		= 'int(10)';
$database[$config['prefix'].'pages']['fields']['hidden']['Type']				= 'int(1)';
$database[$config['prefix'].'pages']['fields']['groups']['Type']				= 'text';
$database[$config['prefix'].'pages']['fields']['groups_mode']['Type']			= 'varchar(255)';

//content
$database[$config['prefix'].'content']['fields']['id'] = array(
	'Type' => 'int(10)',
	'Key' => 'PRI',
	'Extra' => 'auto_increment'
);
$database[$config['prefix'].'content']['fields']['page']['Type']				= 'int(10)';
$database[$config['prefix'].'content']['fields']['parent']['Type']				= 'int(10)';
$database[$config['prefix'].'content']['fields']['rank']['Type']				= 'int(10)';
$database[$config['prefix'].'content']['fields']['fce']['Type']					= 'varchar(255)';
$database[$config['prefix'].'content']['fields']['name']['Type']				= 'text';
$database[$config['prefix'].'content']['fields']['pos']['Type']					= 'varchar(255)';
$database[$config['prefix'].'content']['fields']['content']['Type']				= 'mediumtext';
$database[$config['prefix'].'content']['fields']['online']['Type']				= 'int(1)';
$database[$config['prefix'].'content']['fields']['online_time_mode']['Type']	= 'varchar(255)';
$database[$config['prefix'].'content']['fields']['online_time_start']['Type']	= 'int(10)';
$database[$config['prefix'].'content']['fields']['online_time_end']['Type']		= 'int(10)';
$database[$config['prefix'].'content']['fields']['hidden']['Type']				= 'int(1)';
$database[$config['prefix'].'content']['fields']['groups']['Type']				= 'text';
$database[$config['prefix'].'content']['fields']['groups_mode']['Type']			= 'varchar(255)';


//emojis

$database[$config['prefix'].'emojis']['fields']['id'] = array(
	'Type' => 'int(10)',
	'Key' => 'PRI',
	'Extra' => 'auto_increment'
);
$database[$config['prefix'].'emojis']['fields']['rank']['Type']	= 'int(10)';
$database[$config['prefix'].'emojis']['fields']['name']['Type']	= 'text';
$database[$config['prefix'].'emojis']['fields']['code']['Type']	= 'text';
$database[$config['prefix'].'emojis']['fields']['file']['Type']	= 'text';
$database[$config['prefix'].'emojis']['fields']['file_name']['Type']	= 'text';
$database[$config['prefix'].'emojis']['fields']['path']['Type']	= 'text';
$database[$config['prefix'].'emojis']['fields']['type']['Type']	= 'text';
$database[$config['prefix'].'emojis']['fields']['size']['Type']	= 'text';
$database[$config['prefix'].'emojis']['fields']['crypt']['Type'] 	= 'varchar(255)';
$database[$config['prefix'].'emojis']['fields']['crypt_key']['Type'] 	= 'varchar(255)';
$database[$config['prefix'].'emojis']['fields']['time']['Type']	= 'int(10)';

//user

$database[$config['prefix'].'user']['fields']['id'] = array(
	'Type' => 'int(10)',
	'Key' => 'PRI',
	'Extra' => 'auto_increment'
);
$database[$config['prefix'].'user']['fields']['name']['Type']			= 'text';
$database[$config['prefix'].'user']['fields']['pass']['Type']			= 'text';
$database[$config['prefix'].'user']['fields']['time']['Type']			= 'int(10)';
$database[$config['prefix'].'user']['fields']['reg_time']['Type']		= 'int(10)';
$database[$config['prefix'].'user']['fields']['perm']['Type']			= 'text';
$database[$config['prefix'].'user']['fields']['email']['Type']			= 'text';
$database[$config['prefix'].'user']['fields']['active']['Type']			= 'int(1)';
$database[$config['prefix'].'user']['fields']['ref']['Type']			= 'text';
$database[$config['prefix'].'user']['fields']['ref_time']['Type']		= 'int(10)';
$database[$config['prefix'].'user']['fields']['timezone']['Type'] 		= 'varchar(255)';
$database[$config['prefix'].'user']['fields']['lang']['Type'] 			= 'varchar(255)';
$database[$config['prefix'].'user']['fields']['avatar']['Type']			= 'int(10)';
$database[$config['prefix'].'user']['fields']['groups']['Type']			= 'text';
$database[$config['prefix'].'user']['fields']['pms_sendemail']['Type']	= 'int(1)';

//groups

$database[$config['prefix'].'groups']['fields']['id'] = array(
	'Type' => 'int(10)',
	'Key' => 'PRI',
	'Extra' => 'auto_increment'
);
$database[$config['prefix'].'groups']['fields']['name']['Type']		= 'text';

//groups

$database[$config['prefix'].'modules']['fields']['id'] = array(
	'Type' => 'int(10)',
	'Key' => 'PRI',
	'Extra' => 'auto_increment'
);
$database[$config['prefix'].'modules']['fields']['name']['Type']	= 'text';
$database[$config['prefix'].'modules']['fields']['active']['Type']	= 'int(1)';


//sessions

$database[$config['prefix'].'sessions']['fields']['id'] = array(
	'Type' => 'int(10)',
	'Key' => 'PRI',
	'Extra' => 'auto_increment'
);
$database[$config['prefix'].'sessions']['fields']['session_id']['Type']	= 'varchar(255)';
$database[$config['prefix'].'sessions']['fields']['user']['Type']		= 'int(10)';
$database[$config['prefix'].'sessions']['fields']['time']['Type']		= 'int(10)';

//pms

$database[$config['prefix'].'pms']['fields']['id'] = array(
	'Type' => 'int(10)',
	'Key' => 'PRI',
	'Extra' => 'auto_increment'
);
$database[$config['prefix'].'pms']['fields']['from_id']['Type']		= 'int(10)';
$database[$config['prefix'].'pms']['fields']['to_id']['Type']		= 'int(10)';
$database[$config['prefix'].'pms']['fields']['time']['Type']		= 'int(10)';
$database[$config['prefix'].'pms']['fields']['subject']['Type']		= 'text';
$database[$config['prefix'].'pms']['fields']['content']['Type']		= 'mediumtext';
$database[$config['prefix'].'pms']['fields']['read_to']['Type']		= 'int(1)';
$database[$config['prefix'].'pms']['fields']['del_from']['Type']	= 'int(1)';
$database[$config['prefix'].'pms']['fields']['del_to']['Type']		= 'int(1)';

//taskqueue

$database[$config['prefix'].'taskqueue']['fields']['id'] = array(
	'Type' => 'int(10)',
	'Key' => 'PRI',
	'Extra' => 'auto_increment'
);
$database[$config['prefix'].'taskqueue']['fields']['name']['Type']				= 'text';
$database[$config['prefix'].'taskqueue']['fields']['function']['Type']			= 'text';
$database[$config['prefix'].'taskqueue']['fields']['function_args']['Type']		= 'text';
$database[$config['prefix'].'taskqueue']['fields']['exec_date']['Type']			= 'date';
$database[$config['prefix'].'taskqueue']['fields']['exec_time_of_day']['Type']	= 'int(10)';
$database[$config['prefix'].'taskqueue']['fields']['last_exec']['Type']			= 'int(10)';
$database[$config['prefix'].'taskqueue']['fields']['exec_mode']['Type']			= 'varchar(255)';

//templates

$database[$config['prefix'].'templates']['fields']['id'] = array(
	'Type' => 'int(10)',
	'Key' => 'PRI',
	'Extra' => 'auto_increment'
);
$database[$config['prefix'].'templates']['fields']['name']['Type']				= 'text';
$database[$config['prefix'].'templates']['fields']['vars']['Type']				= 'mediumtext';


//mailqueue

$database[$config['prefix'].'mailqueue']['fields']['id'] = array(
	'Type' => 'int(10)',
	'Key' => 'PRI',
	'Extra' => 'auto_increment'
);
$database[$config['prefix'].'mailqueue']['fields']['from_email']['Type']		= 'text';
$database[$config['prefix'].'mailqueue']['fields']['from_name']['Type']			= 'text';
$database[$config['prefix'].'mailqueue']['fields']['recipient_email']['Type']	= 'text';
$database[$config['prefix'].'mailqueue']['fields']['recipient_name']['Type']	= 'text';
$database[$config['prefix'].'mailqueue']['fields']['subject']['Type']			= 'text';
$database[$config['prefix'].'mailqueue']['fields']['body']['Type']				= 'mediumtext';
$database[$config['prefix'].'mailqueue']['fields']['altbody']['Type']			= 'mediumtext';
$database[$config['prefix'].'mailqueue']['fields']['attachments']['Type']		= 'text';
$database[$config['prefix'].'mailqueue']['fields']['ishtml']['Type']			= 'int(1)';
$database[$config['prefix'].'mailqueue']['fields']['time_created']['Type']		= 'int(10)';
$database[$config['prefix'].'mailqueue']['fields']['issent']['Type']			= 'int(1)';


?>