<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */



if(isset($_GET['usercp']) AND $_GET['usercp'] == 'login')
{
	include($phpRootPath.'sys/usercp/login.php');
}
else if(isset($_GET['usercp']) AND $_GET['usercp'] == 'register')
{
	include($phpRootPath.'sys/usercp/register.php');
}
else if(isset($_GET['usercp']) AND $_GET['usercp'] == 'user')
{
	include($phpRootPath.'sys/usercp/user.php');
}
else if(isset($_GET['usercp']) AND $_GET['usercp'] == 'activation' AND isset($_GET['ref']))
{
	include($phpRootPath.'sys/usercp/activation.php');
}
else if(isset($_GET['usercp']) AND $_GET['usercp'] == 'forgotpassword')
{
	include($phpRootPath.'sys/usercp/forgotpassword.php');
}

?>
