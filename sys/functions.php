<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

include($phpRootPath.'sys/functions/func_basics.php');
include($phpRootPath.'sys/functions/func_db.php');
include($phpRootPath.'sys/functions/func_user.php');
include($phpRootPath.'sys/functions/func_time.php');
include($phpRootPath.'sys/functions/func_nav.php');
include($phpRootPath.'sys/functions/func_comments.php');
include($phpRootPath.'sys/functions/func_content.php');
include($phpRootPath.'sys/functions/func_file.php');
include($phpRootPath.'sys/functions/func_autoform.php');
include($phpRootPath.'sys/functions/func_mediaselect.php');

if(isset($_GET['admin']))
{
	include($phpRootPath.'sys/functions/func_admin.php');	
}

$minPhpVersion = '5.5.0';

?>