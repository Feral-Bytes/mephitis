<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

if(!isset($_SESSION[getvar('cookie_name')]['user']['session_id']))
{
	$formdata = array(
		'name' => 'login',
		'action' => $phproot.'?usercp=login',
		'method' => 'POST',
		'fileadd' => ''
	);
	$content .= draw_content($lang['login'], draw_login($formdata));
}

?>