<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */
	
if(getvar('public_register') == '0'){
	die(draw_error($lang['error'], $lang['access_denied']));
}

$ok = true;

if(isset($_POST['send'])){
	$lang['posted'] = $lang['registered']; 
	
	$query = 'SELECT * FROM '.$config['prefix'].'user WHERE name = "'.$mysqli->real_escape_string($_POST['name']).'"';
	$result = $mysqli->query($query);
	$row = $result->fetch_assoc();		
	
	if(is_array($row)){
		$msg_stack .= draw_alert($lang['error_doublename'], 'danger');
		$ok = false;
	}
	
	$query = 'SELECT * FROM '.$config['prefix'].'user WHERE email = "'.$mysqli->real_escape_string($_POST['email']).'"';
	$result = $mysqli->query($query);
	$row = $result->fetch_assoc();
	
	if(is_array($row)){
		$msg_stack .= draw_alert($lang['error_doubleemail'], 'danger');
		$ok = false;
	}
	$ref = mk_ref(16);
	
	$inputs[] = array(
		'name'	=> 'ref',
		'type'	=> 'var',
		'value'	=> $ref
	);

	$lang['posted'] = $lang['reg_email_sent']; 
	
}

$inputs[] = array(
	'name'	=> 'active',
	'type'	=> 'var',
	'value'	=> 0
);

$inputs[] = array(
	'name'	=> 'time',
	'type'	=> 'fix',
	'value'	=> time()
);

$inputs[] = array(
	'name'	=> 'reg_time',
	'type'	=> 'fix',
	'value'	=> time()
);

$inputs[] = array(
	'name'	=> 'name',
	'type'	=> 'text',
	'lang'	=> $lang['user_name'],
	'required' => '1',
	'required_msg' => $lang['required_msg_name'],
	'value'	=> ''
);

$inputs[] = array(
	'name'	=> 'email',
	'type'	=> 'text',
	'lang'	=> $lang['email'],
	'required' => '1',
	'required_msg' => $lang['required_msg_email'],
	'value'	=> ''
);	

$inputs[] = array(
	'type'	=> 'fix',
	'name'	=> 'perm',
	'value'	=> 'user'
);

$inputs[] = array(
	'type'	=> 'fix',
	'name'	=> 'groups',
	'value'	=> getvar('new_user_groups')
);	

$options = array();
$tzlist = new timezones();
$tzlist_echo = $tzlist->getlist('groups', 'name');
foreach($tzlist_echo as $continent => $citys){
	$options[$continent]['lang'] = $continent;
	foreach($citys as $city => $zone){
		$active = '';
		if($user['timezone'] == $zone['zone'])$active = ' selected';
		$options[$continent]['options'][$zone['zone']] = array(
			'lang'		=> $city,
			'active'	=> $active
		);
	}
}
$inputs[] = array(
	'type'	=> 'select',
	'name'	=> 'timezone',
	'lang'	=> $lang['timezone'],
	'groups' => 1,
	'options' => $options
);

$options = array();
foreach($config['availablelangs'] as $optlang){
	$active = '';
	if($user['lang'] == $optlang)$active = ' selected';
	$options[$optlang] = array(
		'lang'		=> $optlang,
		'active'	=> $active
	);
}
$inputs[] = array(
	'type'	=> 'select',
	'name'	=> 'lang',
	'lang'	=> $lang['lang'],
	'options' => $options
);

if(isset($installedModules) && is_array($installedModules))
foreach($installedModules as $modulename => $module)
{
	if($module['active'] == '1')
	{
		$file = $phpRootPath.'modules/'.$modulename.'/mod_additional_user_fields.php';
		if(file_exists($file))
		{
			$additionalInputs = array();
			$additionalFields = array();
			include($file);
			if(isset($additionalInputs))
			{
				foreach($additionalInputs as $key => $input)
				{
					unset($additionalInputs[$key]['category']);
					
					if(!$additionalInputs[$key]['required'] || $additionalInputs[$key]['required'] == '0' || $additionalInputs[$key]['required'] == '')
					{
						unset($additionalInputs[$key]);
						continue;
					}
				}
				$inputs = array_merge($inputs, $additionalInputs);
			}
		}
	}
}

if(!$ok)
{
	unset($_POST['send']);
}

$form = autoform($config['prefix'].'user', $inputs, $phproot, 'usercp=register', $lang['register'], 'new', NULL, 'usercp', '', '', false, getvar('register_text'));

if(!isset($autoform_success) || $autoform_success != 1)
{
	$content .= $form;
}

if($ok && isset($autoform_success) && $autoform_success == 1)
{
	
	$link = siteLink().'?usercp=activation&ref='.$ref;
	$message = $lang['reg_email1'].$link.$lang['reg_email2'];
		
	if(mailer($mysqli->real_escape_string($_POST['email']), $lang['registration'], $message, getvar('admin_name'), getvar('admin_email')) != 1){
		//die(draw_error($lang['error'], $lang['mail_error']));
		$msg_stack .= draw_alert($lang['mail_error'], 'danger');
		$ok = false;
	}

	$modulecontent	= '';
	$content		= '';
	$row			= '';
	$link			= '';
	unset($_GET['usercp']);
	unset($_GET['site']);
	unset($_GET['show']);
	
	include($phpRootPath.'sys/main.php');
}


?>