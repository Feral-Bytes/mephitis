<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */


$ref = $mysqli->real_escape_string($_GET['ref']);

$query = 'SELECT * FROM '.$config['prefix'].'user WHERE ref = "'.$ref.'" AND active = "0"';
$result = $mysqli->query($query);
$row = $result->fetch_assoc();	

if(is_array($row) AND isset($row['id'])){

	$ok = true;

	$settings[] = array(
		'name'	=> 'pass',
		'type'	=> 'password',
		'lang' => $lang['user_pass'],
		'lang_v' => $lang['user_pass_v'],
		'required' => '1',
		'validation_func' => 'password_validation',
		'value'	=> '',
	);

	$settings[] = array(
		'name'	=> 'active',
		'type'	=> 'var',
		'value'	=> '1'
	);
	
	$settings[] = array(
		'name'	=> 'ref',
		'type'	=> 'var',
		'value'	=> ''
	);
	
	if(isset($_POST['send'])){
	
		$data = '';
		foreach($settings as $setting){

			if(isset($setting['required']) AND $setting['required'] == '1' AND !isset($setting['required_msg']))
			{
				$setting['required_msg'] = $setting['lang'].' '.get_label('required_msg');
				if(isset($setting['category']))
				{
					$setting['required_msg'] = get_label($setting['category']).':&nbsp;'.$setting['required_msg'];
				}
			}
		
			if(isset($setting['required']) AND $setting['required'] == '1')
			{
				if(!isset($_POST[$setting['name']]) || $_POST[$setting['name']] == '')
				{
					$ok = false;
					$msg_stack .= draw_alert($setting['required_msg'], 'danger');	
				}
			}
		
			if(isset($setting['validation_func']) && trim($setting['validation_func'] != '') && isset($_POST[$setting['name']]) && $_POST[$setting['name']] != '')
			{
				$validation = $setting['validation_func']($_POST[$setting['name']]);
				if(!$validation)
				{
					$ok = false;
					if(!isset($setting['not_valid_msg']))
					{
						$setting['not_valid_msg'] = $setting['lang'].' '.get_label('not_valid_msg');
						if(isset($setting['category']))
						{
							$setting['not_valid_msg'] = get_label($setting['category']).':&nbsp;'.$setting['not_valid_msg'];
						}
					}
					$msg_stack .= draw_alert($setting['not_valid_msg'], 'danger');
				}
			}
		
			if($setting['type'] == 'password'){
				if(isset($_POST[$setting['name']]) AND $_POST[$setting['name']] != '' OR isset($_POST[$setting['name'].'_v']) AND $_POST[$setting['name'].'_v'] != ''){
					if($_POST[$setting['name']] != $_POST[$setting['name'].'_v']){
						$msg_stack .= draw_alert($lang['pass_equal_error'], 'danger');
						$ok = false;
					}
					$data .= $setting['name'].' = "'.$mysqli->real_escape_string(password_crypt($_POST[$setting['name']])).'",';
				}
			}else if($setting['type'] == 'checkbox'){
				if(isset($_POST[$setting['name']])){
					$value = '1';
				}else{
					$value = '0';
				}
				$data .= $setting['name'].' = "'.$value.'",';
			}else if($setting['type'] == 'var'){
				$data .= $setting['name'].' = "'.$setting['value'].'",';
			}else{
				$data .= $setting['name'].' = "'.$mysqli->real_escape_string($_POST[$setting['name']]).'",';
			}
			
		}
		
		if($ok)
		{
			$new = 'UPDATE '.$config['prefix'].'user Set
			'.substr($data, 0, -1).'
			WHERE id = "'.$row['id'].'"';
			$update = $mysqli->query($new) or die(draw_error($lang['mysql_error'], $mysqli->error));
			$msg_stack .= draw_alert($lang['useracc_activated'], 'success', false);
			
			
			$modulecontent	= '';
			$content		= '';
			$row			= '';
			$_GET['usercp'] = 'login';
			unset($_GET['site']);
			unset($_GET['show']);			
			include($phpRootPath.'sys/main.php');
		}
	}

	if(!isset($_POST['send']) || !$ok)
	{
		$formdata = array(
			'name' => 'activation',
			'action' => $phproot.'?usercp=activation&ref='.$ref.'',
			'method' => 'POST',
			'fileadd' => '',
			'infobox' => $lang['useracc_activation1'].$row['name'].$lang['useracc_activation2']
		);			
		$content .= draw_form($settings, $lang['useracc_activation'].': '.$row['name'], $formdata);
	}
	
}else{
	die(draw_error($lang['error'], $lang['404']));
}
	

?>