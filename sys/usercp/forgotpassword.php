<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

if(isset($_GET['ref']))
{
	$maxAge = time()-getvar('forgotpassword_max_age');
	
	$ref = $mysqli->real_escape_string($_GET['ref']);
	
	$query = 'SELECT * FROM '.$config['prefix'].'user WHERE ref = "'.$ref.'" AND active = "1" AND ref_time > "'.$maxAge.'"';
	$result = $mysqli->query($query);
	$row = $result->fetch_assoc();
	
	if(is_array($row) AND isset($row['id'])){

		$ok = true;

		$settings[] = array(
			'name'	=> 'pass',
			'type'	=> 'password',
			'lang' => $lang['user_pass'],
			'lang_v' => $lang['user_pass_v'],
			'required' => '1',
			'validation_func' => 'password_validation',
			'value'	=> '',
		);
		
		$settings[] = array(
			'name'	=> 'ref',
			'type'	=> 'var',
			'value'	=> ''
		);
		
		if(isset($_POST['send'])){
		
			$data = '';
			foreach($settings as $setting){

				if(isset($setting['required']) AND $setting['required'] == '1' AND !isset($setting['required_msg']))
				{
					$setting['required_msg'] = $setting['lang'].' '.get_label('required_msg');
					if(isset($setting['category']))
					{
						$setting['required_msg'] = get_label($setting['category']).':&nbsp;'.$setting['required_msg'];
					}
				}
			
				if(isset($setting['required']) AND $setting['required'] == '1')
				{
					if(!isset($_POST[$setting['name']]) || $_POST[$setting['name']] == '')
					{
						$ok = false;
						$msg_stack .= draw_alert($setting['required_msg'], 'danger');	
					}
				}
			
				if(isset($setting['validation_func']) && trim($setting['validation_func'] != '') && isset($_POST[$setting['name']]) && $_POST[$setting['name']] != '')
				{
					$validation = $setting['validation_func']($_POST[$setting['name']]);
					if(!$validation)
					{
						$ok = false;
						if(!isset($setting['not_valid_msg']))
						{
							$setting['not_valid_msg'] = $setting['lang'].' '.get_label('not_valid_msg');
							if(isset($setting['category']))
							{
								$setting['not_valid_msg'] = get_label($setting['category']).':&nbsp;'.$setting['not_valid_msg'];
							}
						}
						$msg_stack .= draw_alert($setting['not_valid_msg'], 'danger');
					}
				}
			
				if($setting['type'] == 'password'){
					if(isset($_POST[$setting['name']]) AND $_POST[$setting['name']] != '' OR isset($_POST[$setting['name'].'_v']) AND $_POST[$setting['name'].'_v'] != ''){
						if($_POST[$setting['name']] != $_POST[$setting['name'].'_v']){
							$msg_stack .= draw_alert($lang['pass_equal_error'], 'danger');
							$ok = false;
						}
						$data .= $setting['name'].' = "'.$mysqli->real_escape_string(password_crypt($_POST[$setting['name']])).'",';
					}
				}else if($setting['type'] == 'checkbox'){
					if(isset($_POST[$setting['name']])){
						$value = '1';
					}else{
						$value = '0';
					}
					$data .= $setting['name'].' = "'.$value.'",';
				}else if($setting['type'] == 'var'){
					$data .= $setting['name'].' = "'.$setting['value'].'",';
				}else{
					$data .= $setting['name'].' = "'.$mysqli->real_escape_string($_POST[$setting['name']]).'",';
				}
				
			}
			
			if($ok)
			{
				$new = 'UPDATE '.$config['prefix'].'user Set
				'.substr($data, 0, -1).'
				WHERE id = "'.$row['id'].'"';
				$update = $mysqli->query($new) or die(draw_error($lang['mysql_error'], $mysqli->error));
				$msg_stack .= draw_alert($lang['msg_password_restored'], 'success', false);
				
				$modulecontent	= '';
				$content		= '';
				$row			= '';
				$link			= '';
				$_GET['usercp'] = 'login';
				unset($_GET['site']);
				unset($_GET['show']);
				
				include($phpRootPath.'sys/main.php');
			}
		}

		if(!isset($_POST['send']) || !$ok)
		{
			$formdata = array(
				'name' => 'forgotpassword',
				'action' => $phproot.'?usercp=forgotpassword&ref='.$ref.'',
				'method' => 'POST',
				'fileadd' => ''
			);			
			$content .= draw_form($settings, $lang['forgotpassword'].': '.$row['name'], $formdata);
		}
	}
	else
	{
		$modulecontent	= '';
		$content		= '';
		$row			= '';
		unset($_GET['usercp']);
		unset($_GET['site']);
		unset($_GET['show']);
		
		include($phpRootPath.'sys/main.php');
	}
}
else
{
	$ok = false;
	
	$captcha = chk_captcha();
	
	if(isset($_POST['send']))
	{
		
		if(isset($_POST['name']) && $_POST['name'] != '')
		{
			$name  = $mysqli->real_escape_string(trim($_POST['name']));
			
			$field = '';
			
			if(mailcheck($name))
			{
				$field = 'email';
			}
			else
			{
				$field = 'name';
			}
			
			$query = 'SELECT * FROM '.$config['prefix'].'user WHERE '.$field.' = "'.$name.'" AND active = "1"';
			$result = $mysqli->query($query);
			$rowUser = $result->fetch_assoc();
			if(is_array($rowUser) && isset($rowUser['id']) && $rowUser['id'] != 0)
			{
				
				$ok = true;
				
				if($captcha != '1')
				{
					$ok = false;
					$msg_stack .= draw_alert($lang['captcha_wrong'], 'danger');
				}
				
				$ref = md5(mk_ref(16).random_string(100));
				
				$link = siteLink().'?usercp=forgotpassword&ref='.$ref;
				
				$message = get_label('mail_forgotpassword');
				$message = str_replace('{link}', '<a href="'.$link.'">'.$link.'</a>', $message);
				
				if($ok && mailer($mysqli->real_escape_string($rowUser['email']), $lang['forgotpassword'], $message, getvar('admin_name'), getvar('admin_email'), 'html') != 1){
					$msg_stack .= draw_alert($lang['mail_error'], 'danger');
					$ok = false;
				}
				
				if($ok)
				{
					$update = $mysqli->query('
						UPDATE '.$config['prefix'].'user SET
							ref = "'.$ref.'",
							ref_time = "'.time().'"
						WHERE
							id = '.$rowUser['id'].'
					') or die(draw_error($lang['error'], $mysqli->error));
					
					$msg_stack .= draw_alert(get_label('msg_forgotpassword'), 'info');
					
					$modulecontent	= '';
					$content		= '';
					$row			= '';
					unset($_GET['usercp']);
					unset($_GET['site']);
					unset($_GET['show']);
					
					include($phpRootPath.'sys/main.php');
				}
			}
			else
			{
				$ok = false;
				$msg_stack .= draw_alert(get_label('login_name').' '.get_label('not_valid_msg'), 'danger');
			}
		}
		else
		{
			$ok = false;
			$msg_stack .= draw_alert(get_label('login_name').' '.get_label('not_valid_msg'), 'danger');
		}
	}
	if(!$ok)
	{
		$settings[] = array(
			'name' => 'name',
			'type' => 'text',
			'lang' => get_label('login_name'),
			'required' => '1',
			'validation_func' => 'mailcheck'
		);
		
		chk_captcha('submit');
		
		$formdata = array(
			'name' => 'forgotpassword',
			'action' => $phproot.'?usercp=forgotpassword',
			'method' => 'POST',
			'fileadd' => '',
			'captcha' => true
		);			
		$content .= draw_form($settings, $lang['forgotpassword'], $formdata);
	}
}
?>