<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

function get_uscp_form()
{
	
	global $config, $mysqli, $lang, $msg_stack, $ok, $user, $installedModules, $phpRootPath;
	
	$settings = array();

	$options = array();
	$tzlist = new timezones();
	$tzlist_echo = $tzlist->getlist('groups', 'name');
	foreach($tzlist_echo as $continent => $citys){
		$options[$continent]['lang'] = $continent;
		foreach($citys as $city => $zone){
			$active = '';
			if($user['timezone'] == $zone['zone'])$active = ' selected';
			$options[$continent]['options'][$zone['zone']] = array(
				'lang'		=> $city,
				'active'	=> $active
			);
		}
	}
	$settings[] = array(
		'type'	=> 'select',
		'name'	=> 'timezone',
		'category' => 'usercp_cat_common',		
		'lang'	=> $lang['timezone'],
		'groups' => 1,
		'options' => $options
	);
	
	$options = array();
	foreach($config['availablelangs'] as $optlang){
		$active = '';
		if($user['lang'] == $optlang)$active = ' selected';
		$options[$optlang] = array(
			'lang'		=> $optlang,
			'active'	=> $active
		);
	}
	$settings[] = array(
		'type'	=> 'select',
		'name'	=> 'lang',
		'category' => 'usercp_cat_common',		
		'lang'	=> $lang['lang'],
		'options' => $options
	);	
	
	$settings[] = array(
		'type' => 'custom',
		'category' => 'usercp_cat_crit',
		'html' => draw_alert(get_label('usercp_cat_crit_passcheck'), 'info', false),
		'cols' => 1,
		'lang' => ''
	);
	
	$settings[] = array(
		'type' => 'custom',
		'key' => 'passcheck_crit',
		'category' => 'usercp_cat_crit',
		'cols' => 1,
		'lang' => $lang['user_pass_check'],
		'input' => 'password',
		'input_name' => 'passcheck_crit'
	);
	
	$settings[] = array(
		'name'	=> 'email',
		'type'	=> 'text',
		'category' => 'usercp_cat_crit',
		'lang'	=> $lang['email'],
		'value'	=> $user['email'],
		'validation_func' => 'mailcheck',
		'required' => 1
	);
	if(isset($_POST['send'])){
		
		$query = 'SELECT * FROM '.$config['prefix'].'user WHERE email = "'.$mysqli->real_escape_string($_POST['email']).'"';
		$result = $mysqli->query($query);
		$row = $result->fetch_assoc();
		
		if(is_array($row) AND $row['id'] != $user['id']){
			$msg_stack .= draw_alert($lang['error_doubleemail'], 'danger');
			$ok = false;
		}
	}
	
	$settings[] = array(
		'name'	=> 'pass',
		'type'	=> 'password',
		'category' => 'usercp_cat_crit',
		'lang' => $lang['user_change_pass'],
		'lang_v' => $lang['user_change_pass_v'],
		'validation_func' => 'password_validation',
		'value'	=> '',
	);
	
	$settings[] = array(
		'type'	=> 'file',
		'name'	=> 'avatar',
		'category' => 'usercp_cat_avatar',		
		'path'	=> 'files/avatars/',
		'value' => $user['avatar'],
		'lang'	=> $lang['avatar'],
		'id'	=> '',
		'delbtn' => '',
		'opt' => array(
			'onlyimage' => true
		)
	);
	
	$active = '';
	if($user['pms_sendemail'] == '1')
	{
		$active = ' checked';
	}
	$settings[] = array(
		'type'	=> 'checkbox',
		'category' => 'usercp_cat_pms',
		'name'	=> 'pms_sendemail',
		'lang'	=> $lang['pms_sendemail'],
		'active' => $active
	);
	
	if(is_array($installedModules))
	foreach($installedModules as $modulename => $module)
	{
		if($module['active'] == '1')
		{
			$file = $phpRootPath.'modules/'.$modulename.'/mod_additional_user_fields.php';
			if(file_exists($file))
			{
				$additionalSettings = array();
				include($file);
				if(isset($additionalSettings))
				{
					$settings = array_merge($settings, $additionalSettings);
				}
			}
		}
	}
	
	return $settings;
}

if(isset($user['id']) AND $user['id'] == '0'){
	$formdata = array(
		'name' => 'login',
		'action' => $phproot.'?usercp=login',
		'method' => 'POST',
		'fileadd' => ''
	);
	$content .= draw_content($lang['login'], draw_login($formdata));
}
else
{
	$ok = true;

	if(isset($_POST['send'])){
		update_user_settings(get_uscp_form());
	}
	
	if(isset($user['id']) AND $user['id'] == '0')
	{
		$formdata = array(
			'name' => 'login',
			'action' => $phproot.'?usercp=login',
			'method' => 'POST',
			'fileadd' => ''
		);
		$content .= draw_content($lang['login'], draw_login($formdata));
	}
	else
	{
		$formdata = array(
			'name' => 'user',
			'action' => $phproot.'?usercp=user',
			'method' => 'POST',
			'fileadd' => ' enctype="multipart/form-data"'
		);
		$content .= draw_form(get_uscp_form(), $lang['user_account'], $formdata);
	}
}
?>