<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

include_once(include_file('lang', 'lang_admin'));

if(isset($_GET['frontend_admin']) AND $_GET['frontend_admin'] == 'sites' AND isset($_GET['action']))
{
	include($phpRootPath.'sys/admintools/admin_frontend_sites.php');	
}
else if(isset($_GET['frontend_admin']) AND $_GET['frontend_admin'] == 'comments')
{
	include($phpRootPath.'sys/admintools/admin_frontend_comments.php');	
}
else
{
	$modulename = $mysqli->real_escape_string($_GET['frontend_admin']);
	$subadmin = '';
	if(isset($installedModules[$modulename]) && $installedModules[$modulename]['active'] == 1 && isset($modules[$modulename]['frontend_admin']) && $modules[$modulename]['frontend_admin'] == true && ((isset($modules[$modulename]['frontend_admin_access']) && $modules[$modulename]['frontend_admin_access']) || !isset($modules[$modulename]['frontend_admin_access'])))
	{
		$file = $phpRootPath.'modules/'.$modulename.'/mod_frontend_admin.php';
		if(isset($installedModules[$modulename]) && $installedModules[$modulename]['active'] == "1" && file_exists($file)){
			$link .= 'frontend_admin='.$modulename;
			if(isset($_GET['subadmin']))
			{
				$subadmin = $mysqli->real_escape_string($_GET['subadmin']);
				$link .= '&subadmin='.$subadmin;
			}
			include($file);
			if(isset($modulecontent) && $modulecontent != '')
			{
				$content .= $modulecontent;
			}
		}
	}
}


?>