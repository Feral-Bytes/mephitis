<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */
session_start();

$starttime = microtime(TRUE);

//system modules
include($phpRootPath.'sys/functions.php');
include($phpRootPath.'sys/classes.php');

$config['availablelangs'] = availablelangs();
$user['lang'] = browser_lang($config['availablelangs']);

if(file_exists($phpRootPath.'config.php')){
	include($phpRootPath.'config.php');
	
	$config['version'] = '1.1 dev (unstable)';
	
	//language
	include_once(include_file('lang', 'lang_main'));

	//mysql connection
	$mysqli = new mysqli($config['db_host'], $config['db_user'], $config['db_pass'], $config['db_name']);
	$mysqli->query("SET NAMES 'utf8'");
	$mysqli->set_charset("utf8");

	//config from db
	$config['template'] = getvar('template_dir');
	
	$user = user_guest();
	
	$assetService = new AssetService();
	
	$page = GetPage();
	
	$pageTree = new tree(
		array(
			'tab' => 'pages',
			'fields' => 'name,template,content_kind,content_link_target,shortlink,icon',
			'checkAccess' => true,
			'checkTime' => true,
			'checkHidden' => true,
			'checkOnline' => true,
			'current' => $page['id'],
			'orderBy' => 'rank ASC'
		)
	);
	$pageContent = new content(array());
	$rootline = $pageTree->GetRootline();
	$page['rootline'] = $rootline;
	$config['selected_template'] = get_parent_template($page['rootline']);

	if(!isset($config['notemplate']) || !$config['notemplate'])
	{
		include($phpRootPath.'sys/templates.php');
	}
	
	
	
	//sys variables
	$msg_stack		= '';
	$user			= user();
	$link			= cms_link();
	$searchengine	= new searchengine();
	$links = array();
	$content = '';
	$modulecontent = '';
	$libs				= array();
	$libs['html_header']	= array();
	$libs['html_footer']	= array();

	//more language
	include_once(include_file('lang', 'lang_main'));

	//tracking
	if(getvar('save_accesses') == '1'){
		ipsave();
	}

	$modules	= array();
	$installedModules = get_installed_modules();
	$database	= array();

	$tpl_data['charset'] = $config['charset'];

	//timezone
	date_default_timezone_set($user['timezone']);
	
	//libs
	include($phpRootPath.'libs/libs.php');
	
	//page renderer
	$echo = new style();
	
	//module loading
	$sysmoduleloader	= array();
	$moduleloader		= array();
	if ($handle = opendir($phpRootPath.'/modules')) {
		while (false !== ($file = readdir($handle))) {
			if ($file != "." && $file != ".." && is_dir($phpRootPath.'/modules/'.$file) && substr($file, 0, 1) != '_')
			{
				if(substr($file, 0, strlen('sys_')) === 'sys_'){
					//system module will be loaded before normal modules
					$sysmoduleloader[]	= $file;
				}else{
					$moduleloader[]		= $file;
				}
				
			}
		}
		closedir($handle);
	}

	$mod_files = array(
		'model',
		'mod_lang',
		'mod_conf',
		'mod_functions',
		'mod_classes',
		'mod_header'
	);
	if(isset($user['perm']) AND $user['perm'] == 'admin' AND isset($_GET['admin']) AND $_GET['admin'] == 'db'){
		$mod_files[] = 'mod_db';
	}

	foreach(array_merge($sysmoduleloader, $moduleloader) as $modulename){
		
		if(isset($installedModules[$modulename]) && $installedModules[$modulename]['active'] == "1")
		{
			foreach($mod_files as $mod_file){
				if($mod_file == 'mod_lang'){
					$mod_availablelangs = availablelangs($phpRootPath.'modules/'.$modulename.'/'.$mod_file);
					
					$file			= $phpRootPath.'modules/'.$modulename.'/'.$mod_file.'.'.$user['lang'].'.php';
					$file_fallback1	= $phpRootPath.'modules/'.$modulename.'/'.$mod_file.'.en.php';
					$file_fallback2	= $phpRootPath.'modules/'.$modulename.'/'.$mod_file.'.php';
					
					if(file_exists($file)){
						include($file);
					}else if(file_exists($file_fallback1)){
						include($file_fallback1);
					}else if(file_exists($file_fallback2)){
						include($file_fallback2);
					}
					
				}else{
					$file = $phpRootPath.'modules/'.$modulename.'/'.$mod_file.'.php';
					if(file_exists($file)){
						include($file);
					}
				}
			}
		}
	}

	$modulename = '';

	//pms
	$pms = new Pms();	
	
}else{
	include(include_file('lang', 'lang_install'));
	die($lang['runinstalltool']);
}

?>