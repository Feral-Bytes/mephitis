<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

//title
$lang['title']						= 'Mephitis Admin Center';

//dashboard
$lang['dashboard']					= 'Dashboard';

//navbar
$lang['cms_tools_dir']				= 'CMS Tools';
$lang['cms_modules_dir']			= 'Modules';

//global
$lang['id']							= 'ID';
$lang['ips_del']					= 'Clear table';
$lang['lastaccess']					= 'Last access';
$lang['error_sub_of_loop']			= 'Loop is created. Error in the assignment.';
$lang['no_site']					= 'no';
$lang['wrong_data']					= 'Incorrect Entries';
$lang['nothing']					= 'Please select an area';
$lang['logic_error']				= 'Logic Error';
$lang['install_tool']				= 'Install Tool';
$lang['credits']					= 'Credits';

//settings
$lang['common_settings']			= 'System settings';
$lang['cms_title']					= 'Website title';
$lang['cms_footer']					= 'Website footer';
$lang['coookie_name']				= 'Cookie name';
$lang['meta_robots']				= 'Commands to search engines';
$lang['meta_author']				= 'Meta-Tag author';
$lang['meta_description'] 			= 'Meta-Tag description';
$lang['meta_keywords']				= 'Meta-Tag keywords';
$lang['shortcut_icon']				= 'Path to icon';
$lang['cms_homepage']				= 'Startpage';
$lang['template_dir']				= 'Template directory';
$lang['admin_name']					= 'Admin name';
$lang['admin_email']				= 'Admin mail';
$lang['thumbnail_size']				= 'Thumbnail max size in px';
$lang['norm_time']					= 'Time format (PHP)';
$lang['norm_date']					= 'Date format (PHP) with time';
$lang['norm_date_notime']			= 'Date format (PHP) without time';
$lang['per_page']					= 'Entries per page';
$lang['perpage']					= $lang['per_page'];
$lang['num_emojis']					= 'Number of emojis';
$lang['useronline_timeout']			= 'Timeout for "Users online"';
$lang['msg_box_fadeout_timer']		= 'Timeout for message box fade out';
$lang['allow_bbcode']				= 'Activate BBCode';
$lang['allow_emojis']				= 'Activate Emojis';
$lang['wysiwyg_user']				= 'WYSIWYG editor';
$lang['wysiwyg_bbcode']				= 'TinyMCE (BBCode)';
$lang['wysiwyg_classic']			= 'Classic';
$lang['forgotpassword_max_age']		= 'Max. age of "forgot password" E-Mails in sec.';
$lang['register_text']				= 'Registration info text';
$lang['register_method_email']		= 'E-Mail activation';
$lang['register_method_direct']		= 'Password activation';
$lang['save_accesses']				= 'Access logging';
$lang['public_show_login']			= 'Login button visible';
$lang['public_register']			= 'Activate registration';
$lang['public_login']				= 'Activate login';
$lang['guest_user_groups']			= 'Groups for guests';
$lang['new_user_groups']			= 'Groups for new users';
$lang['comment_mod_groups']			= 'Groups for comment-moderators';
$lang['comment_guests_can_write']	= 'Guests can write comments';
$lang['comment_members_can_write']	= 'Members can write comments';
$lang['comment_requires_mod_check']	= 'Comments requires check by a comment-moderator';
$lang['file_upload_groups']			= 'Groups for fileupload';
$lang['common_settings_edited'] 	= 'Settings were changed';
$lang['reCAPTCHA']					= 'reCAPTCHA activate';
$lang['reCAPTCHA_key_private']		= 'reCAPTCHA private key';
$lang['reCAPTCHA_key_public']		= 'reCAPTCHA public key';
$lang['global_tracking_enable']		= 'Enable Global Tracking';
$lang['global_tracking_code']		= 'Global Tracking Code';
$lang['settings_cat_common']		= 'Common';
$lang['settings_cat_admin']			= 'Admin';
$lang['settings_cat_content']		= 'Content';
$lang['settings_cat_user_content']	= 'User Content';
$lang['settings_cat_user']			= 'Content';
$lang['settings_cat_tracking']		= 'Tracking';

//email
$lang['email_system']				= 'E-Mail System';
$lang['email_settings']				= 'Settings';
$lang['email_settings_ok']			= 'Settings saved, connection test successful';
$lang['email_settings_notok']		= 'Settings saved, connection test failed';
$lang['email_queue']				= 'Queue';

$lang['email_smtp']					= 'Use SMTP for E-Mail delivery';
$lang['email_smtp_host']			= 'Host';
$lang['email_smtp_auth']			= 'Server needs authentication';
$lang['email_smtp_user']			= 'Username';
$lang['email_smtp_pass']			= 'Password';
$lang['email_smtp_secure']			= 'Crypt Method';
$lang['email_smtp_port']			= 'Port';
$lang['email_smtp_from_name']		= 'Sender name';
$lang['email_smtp_from_email']		= 'Sender E-Mail';

$lang['recipient_email']			= 'Recipient E-Mail';
$lang['recipient_name']				= 'Recipient name';
$lang['email_send_queue']			= 'Process queue';

//content
$lang['dirs']						= 'Folder';
$lang['dir']						= 'Folder'; //fix doppelt
$lang['icon']						= 'icon';
$lang['sites']						= 'Pages';
$lang['site_new']					= 'new page';
$lang['dir_new']					= 'new folder';
$lang['dir_new_this_lvl']			= 'new folder';
$lang['dir_new_next_lvl']			= 'new sub folder';
$lang['template']					= 'Template';
$lang['template_parent']			= 'Template of the higher-level element';
$lang['template_current']			= 'Current Template';
$lang['none']						= '-none-';
$lang['sub_dir_of']					= 'Sub directory of';
$lang['border']						= 'Border';
$lang['publish']					= 'Publish';
$lang['shortlink']					= 'Short link';
$lang['hidden_nav']					= 'Hide in navigation';
$lang['notempty']					= 'Object is not empty...';
$lang['site_edit']					= 'Edit site';
$lang['site_del']					= 'Delete site';
$lang['dir_edit']					= 'Edit folder';
$lang['dir_del']					= 'Delete folder';
$lang['nodirs']						= 'No folder available...<br><a href="'.$phproot.'">Home</a>';
$lang['placeholder']				= 'placeholder';
$lang['tooltip']					= 'tooltip';
$lang['site_tracking_enable']		= 'Enable Site Tracking';
$lang['site_tracking_code']			= 'Site Tracking Code';
$lang['clear_cache']				= 'clear Cache';
$lang['cache_cleared']				= 'Cache cleared';

//accesslog
$lang['accesses']					= 'Access protocol';
$lang['host']						= 'Host';
$lang['http_user_agent']			= 'Browser Agent';
$lang['http_referer']				= 'External access from';
$lang['request_method']				= 'Method';
$lang['php_self']					= 'PHP-File';

//sessions
$lang['sessions']					= 'Sessions';
$lang['session_id']					= 'Session ID';

//user
$lang['user_active']				= 'Account active';
$lang['reg_time']					= 'Date of registration';
$lang['status']						= 'Status';
$lang['selfdelete_error']			= 'You can not delete your own profile!';
$lang['active']						= 'active';
$lang['inactive']					= 'inactive';
$lang['perm']						= 'Permission';
$lang['admin']						= 'Administrator';
$lang['mod']						= 'Moderator';
$lang['user']						= 'User';
$lang['online']						= 'Online';
$lang['offline']					= 'Offline';
$lang['groups']						= 'Groups';
$lang['groups_mode']				= 'Groups access mode';
$lang['std']						= 'Standart';
$lang['whitelist']					= 'Whitelist';
$lang['blacklist']					= 'Blacklist';
$lang['edit_groups']				= 'Groups with edit permissons';

//module and fce
$lang['module_error'] 				= 'Cannot find module file...<br>';
$lang['module_error_admin']			= 'Correct errors';
$lang['module']						= 'Module';
$lang['module_args']				= 'Module arguments';
$lang['nomodule']					= 'No Module';
$lang['mod_instance']				= 'Module instance';
$lang['content_kind'] 				= 'Kind of content';
$lang['content_kind_this']			= 'Only this element';
$lang['content_kind_fce']			= 'Flexible Content Elements';
$lang['content_kind_link']			= 'Link';
$lang['content_kind_link_blank']	= 'Link in new window';
$lang['content_link_target']		= 'Link Target';
$lang['fces']						= 'Flexible Content Elements';
$lang['possible_fce_containers']	= 'Flexible Content Elements can contain';
$lang['no_fce']						= 'no flexible content elements';
$lang['fce_name_meppostborder']		= 'Normal border';
$lang['fce_name_bscontainer']		= 'Bootstrap container';
$lang['fce_name_bs3cols']			= '3 bootstrap columns';
$lang['fce-bs-Col1']				= 'Left';
$lang['fce-bs-Col2']				= 'Mid';
$lang['fce-bs-Col3']				= 'Right';
$lang['fce-module-container']		= 'Module container';
$lang['fce-text-field']				= 'Text field';
$lang['fce-html-field']				= 'HTML';
$lang['del_confirm_recursive'] 		= 'Rekursive Datenl&ouml;schung. Sind Sie sicher das Sie dieses und alle Objekte darin l&ouml;schen wollen?';
$lang['order_dirs']					= 'order directorys';
$lang['order_sites']				= 'order sites';
$lang['type']						= 'Type';
$lang['select']						= 'Select';
$lang['copypastehere']				= 'paste here';
$lang['movehere']					= 'move here';
$lang['moved']						= 'moved';
$lang['moved_txt1']					= 'has been moved';
$lang['cssClasses']					= 'CSS Classes';
$lang['mod_fix_db']					= 'repair Database';
$lang['mod_db_fixed']				= 'Database repaired';
$lang['mod_update']					= 'update';
$lang['mod_updated']				= 'updated';
$lang['mod_updated_fail']			= 'Update failed';
$lang['mod_updater_notfound']		= 'Updater was not found';

//carousel
$lang['fce_name_bscarousel']		= 'Bootstrap Carousel';
$lang['fce_name_bscarouselitem']	= 'Bootstrap Carousel Item';
$lang['picture']					= 'Image';
$lang['picture_alt']				= 'Alternative text';
$lang['caption']					= 'Caption';

//modules
$lang['mail']						= 'Mail address';
$lang['website']					= 'Website';
$lang['website_form']				= 'Website http://';

//modul manager
$lang['modules_manager']			= 'Module Manager';

//database
$lang['db']							= 'Database';
$lang['db_tables']					= 'Tables';
$lang['db_table']					= 'Table';
$lang['db_fields']					= 'Fields';
$lang['db_status']					= 'Status';
$lang['db_ok']						= 'ok';
$lang['db_unknown']					= 'source unknown';
$lang['db_not_found']				= 'not found';
$lang['db_error']					= 'error';
$lang['db_sort_error']				= 'sort error';
$lang['db_engine_error']			= 'engine error';
$lang['db_collation_error']			= 'collation error';
$lang['db_repair']					= 'Try to fix error or create tables';
$lang['db_repaired']				= 'Errors has been fixed or tables has been created';
$lang['db_need_repair']				= 'need repair';
$lang['db_toinstall']				= 'There are new tables to create';
$lang['db_table_show']				= 'show';
$lang['db_field_del1']				= 'are you sure to delete the field';
$lang['db_field_del2']				= 'in table';
$lang['db_field_del3']				= '?';
$lang['db_field_deled1']			= 'The field';
$lang['db_field_deled2']			= 'in table';
$lang['db_field_deled3']			= 'was deleted';
$lang['db_var_del1']				= 'are you sure to delete the variable';
$lang['db_var_del2']				= 'in table';
$lang['db_var_del3']				= '?';
$lang['db_var_deled1']				= 'The variable';
$lang['db_var_deled2']				= 'in table';
$lang['db_var_deled3']				= 'was deleted';
$lang['db_table_del1']				= 'Are you sure to delete the table';
$lang['db_table_del2']				= '?';
$lang['db_table_deled1']			= 'The table';
$lang['db_table_deled2']			= 'was deleted';
$lang['db_backup']					= 'Backup';
$lang['db_drop_table_if_exists']	= 'Add DROP TABLE IF EXISTS';
$lang['db_breakat_rows']			= 'Cut INSERT after n records';
$lang['db_send_as_file']			= 'Send as file';
$lang['db_var_error']				= 'variable error';
$lang['db_vars']					= 'variables';
$lang['db_var']						= 'variable';

//taskqueue
$lang['mep_system']					= 'system';
$lang['taskqueue']					= 'Scheduled tasks';
$lang['function']					= 'Function';
$lang['function_args']				= 'Function arguments';
$lang['exec_date']					= 'Execute date';
$lang['exec_time_of_day']			= 'Execute time';
$lang['last_exec']					= 'Last Excecute';
$lang['exec_mode']					= 'Excecute mode';
$lang['exec_mode_simple']			= 'Simple';
$lang['exec_mode_simple_once']		= 'Once ('.$lang['exec_date'].' + '.$lang['exec_time_of_day'].')';
$lang['exec_mode_simple_daily']		= 'Daily ('.$lang['exec_time_of_day'].')';
$lang['exec_mode_weekly']			= 'Weekly ('.$lang['exec_time_of_day'].')';

$lang['simple_once']				= $lang['exec_mode_simple_once'];
$lang['simple_daily']				= $lang['exec_mode_simple_daily'];
$lang['weekly_mon']					= $lang['exec_mode_weekly'].'('.$lang['mon'].')';
$lang['weekly_tue']					= $lang['exec_mode_weekly'].'('.$lang['tue'].')';
$lang['weekly_wed']					= $lang['exec_mode_weekly'].'('.$lang['wed'].')';
$lang['weekly_thu']					= $lang['exec_mode_weekly'].'('.$lang['thu'].')';
$lang['weekly_fri']					= $lang['exec_mode_weekly'].'('.$lang['fri'].')';
$lang['weekly_sat']					= $lang['exec_mode_weekly'].'('.$lang['sat'].')';
$lang['weekly_sun']					= $lang['exec_mode_weekly'].'('.$lang['sun'].')';

$lang['Task_FixDB']					= 'Check and fix Database';
$lang['Task_BackupDB']				= 'Backup Database';
$lang['Task_FixBackupDB']			= 'Check, fix and backup Database';
$lang['Task_MailQueue']				= 'Process E-Mail queue';
$lang['Task_ClearCache']			= 'Clear Cache';

//updater
$lang['updater']					= 'Updater';
$lang['install']					= 'install';
$lang['cancel']						= 'cancel';
$lang['install_update']				= 'install update';
$lang['md5_checksum']				= 'md5 checksum';
$lang['admin_pw']					= 'administrator password';
$lang['wrong_md5_checksum']			= 'wrong md5 checksum';
$lang['wrong_admin_pw']				= 'wrong administrator password';
$lang['error_file']					= 'file error';
$lang['install_update_warning']		= 'If you run this update installation, you CMS can become unusable, so be really sure to do this!';
$lang['install_update_success']		= 'Installation successful!';
$lang['install_update_fail']		= 'Installation failed! Maybe is the install archive broken';

//templates
$lang['templates']					= 'Templates';
$lang['tpl_create_db_vars']			= 'write template data to database';
$lang['tpl_edit_db_vars']			= 'edit template data';
$lang['tpl_vars_created']			= 'template data has been created';
$lang['tpl_vars_updated']			= 'template data has been updated';
$lang['color']						= 'color';
$lang['size']						= 'size';

//style_admin.php from template
$lang['path']						= 'Path';

$lang['hidden']						= 'hidden'; // will get a refresh - soon

?>