<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

//button
$lang['usercp']					= 'Account Management';
$lang['admincp']				= 'Admin-Center';

//login form
$lang['user']					= 'User';
$lang['login']					= 'Login';
$lang['logout']					= 'Logout';
$lang['login_name']				= 'Username or E-Mail';
$lang['user_name']				= 'Username';
$lang['user_pass']				= 'Password';
$lang['user_pass_v']			= 'Confirm password';
$lang['login_remember']			= 'Remain signed in';
$lang['login_msg']				= 'Login successful...';
$lang['logout_msg']				= 'Logout successful...';
$lang['login_error']			= 'Username or password incorrect...';

//password registration
$lang['register']				= 'Not yet registered?';
$lang['forgotpassword']			= 'Forgot password';
$lang['registration']			= 'Registration';
$lang['registered']				= 'You can now login to your account.';

//mail registration
$lang['reg_email_sent']			= 'A mail with further information to your registration is in your mailbox.';
$lang['reg_email1']				= 'Hi,'."\r\n".'thanks for registration. Please activate your account with the following link'."\r\n";
$lang['reg_email2']				= "\r\n".'Best regards'."\r\n".'the System';
$lang['useracc_activation']		= 'Account activation';
$lang['useracc_activation1']	= 'Welcome ';
$lang['useracc_activation2']	= '!<br>Please set a password for completing the activation.';
$lang['useracc_activated']		= 'Account was activated. You can now login to your account.';
$lang['mail_error']				= 'Error with mail delivery, try again later.';
$lang['mag_forgotpassword']		= 'An E-Mail with further instructions was sent to you';
$lang['mail_forgotpassword']	= 'Hi,<br>you can reset yout password with the following link<br>{link}<br>Best regards<br>the System';
$lang['msg_password_restored']	= 'Password has been set';

//user account
$lang['user_account']			= 'My Profil';
$lang['user_account_edited']	= 'Your profil was changed. Maybe you must login again.';
$lang['email']					= 'E-Mail';
$lang['email_successful']		= 'E-Mail successfully sent.';
$lang['successful']				= 'successful';
$lang['success']				= 'Success';
$lang['user_pass_check']		= 'Please enter your current password to confirm';
$lang['usercp_passcheck_fail']	= 'Please enter your current password!';
$lang['mail_fail']				= 'Please enter a valid mail address!';
$lang['user_change_pass']		= 'New password';
$lang['user_change_pass_v']		= 'Confirm new password';
$lang['required_msg_email']		= 'E-Mail address is required';
$lang['avatar']					= 'Avatar';
$lang['pms_sendemail']			= 'Send PMs as E-Mail to me';
$lang['usercp_cat_common']		= 'Common';
$lang['usercp_cat_crit']		= 'E-Mail and Password';
$lang['usercp_cat_avatar']		= 'Avatar';
$lang['usercp_cat_pms']			= 'Private Messages';
$lang['usercp_cat_crit_passcheck'] = 'You need to enter your current password for changes in this category';
$lang['member']					= 'Member';
$lang['members']				= 'Members';
$lang['guest']					= 'Guest';

//locales
$lang['lang']					= 'Language';
$lang['timezone']				= 'Timezone';

//captcha
$lang['captcha']				= 'Security Code';
$lang['captcha_confirm']		= 'Please confirm';
$lang['captcha_wrong']			= 'Code wrong';
$lang['norm_send']				= 'Send';

//general
$lang['new']					= 'New';
$lang['edit']					= 'edit';
$lang['del']					= 'delete';
$lang['remove']					= 'remove';
$lang['content']				= 'Content';
$lang['edited']					= 'Object changed...';
$lang['posted']					= 'Object was written into the database...';
$lang['deleted']				= 'Deleted';
$lang['del_confirm']			= 'Are you sure, that you want delete this object?';
$lang['homepage']				= 'Startpage';
$lang['yes']					= 'Yes';
$lang['no']						= 'No';
$lang['name']					= 'Name';
$lang['close']					= 'Close';
$lang['ip']						= 'IP';
$lang['asc']					= 'ascending';
$lang['desc']					= 'descending';
$lang['action']					= 'Action';
$lang['time']					= 'Time';
$lang['time_created']			= 'created';
$lang['time_edit']				= 'last edit';
$lang['refresh']				= 'refresh';
$lang['allow_comments']			= 'Allow comments';
$lang['access']					= 'Access';
$lang['nav']					= 'Navigation';
$lang['root_dir']				= 'Root directory';
$lang['to_top']					= 'to top';
$lang['to_bottom']				= 'top bottom';
$lang['up']						= 'move up';
$lang['down']					= 'move down';
$lang['footer']					= 'Mephitis CMS &copy; by <a href="https://www.feralbytes.ch" target="_blank">Feral Bytes</a>';
$lang['public']					= 'Public';
$lang['hidden']					= 'hidden';
$lang['set_public']				= 'set Public';
$lang['unset_public']			= 'unset Public';
$lang['download_ics']			= 'download for calendar';
$lang['truncate']				= 'emtpy Table';
$lang['truncate_confirm']		= 'Are you sure to emtpy the table?';
$lang['truncate_success']		= 'Table is now empty';

//days
$lang['mon']					= 'Monday';
$lang['tue']					= 'Tuesday';
$lang['wed']					= 'Wednesday';
$lang['thu']					= 'Thursday';
$lang['fri']					= 'Friday';
$lang['sat']					= 'Saturday';
$lang['sun']					= 'Sunday';

//errors
$lang['required_msg']			= 'is required';
$lang['not_valid_msg']			= 'not valid';
$lang['error_doublename']		= 'Name already in use...';
$lang['error_doubleemail']		= 'Mail address is already in use...';
$lang['required_msg_name']		= 'No name given...';
$lang['required_msg_content']	= 'No content...';
$lang['required_msg_file']		= 'No file attached...';
$lang['pass_equal_error']		= 'Passwords are not the identical...';
$lang['no_pass_error']			= 'No password set...';
$lang['mysql_error']			= 'MySQL Error';
$lang['404']					= 'Object not found';
$lang['error']					= 'Error';
$lang['isnopic']				= 'is not a picture';
$lang['access_denied']			= 'Access denied';
$lang['no_upload_permission']	= 'no upload permission';
$lang['template_error']			= 'Template file not found...';

//comments
$lang['no_comments']			= 'No comments there.';
$lang['comments']				= 'Comments';
$lang['subject']				= 'Subject';
$lang['new_comment']			= 'Write comment';
$lang['new_comment_posted']		= 'comment posted';
$lang['new_comment_posted_mc']	= 'your comment will be published after a check';
$lang['edit_comment']			= 'Edit comment';
$lang['del_comment']			= 'Delete comment';
$lang['more']					= 'more'; //emoji
$lang['show']					= 'show';
$lang['offline_comments']		= 'Comments Offline';
$lang['online_comments']		= 'Comments Online';

//page navigation
$lang['page']					= 'Page';
$lang['go_to_page']				= 'go to page';
$lang['next']					= 'Next';
$lang['back']					= 'Back';

//searchengine
$lang['search_placeholder']		= 'search...';
$lang['search_btn']				= 'search';
$lang['search_nothingfound']	= 'nothing found';
$lang['search_result']			= 'search result';

//files
$lang['thumb_admin']				= 'Preview image';
$lang['file']						= 'File';
$lang['files']						= 'Filemanager';
$lang['file_module']				= 'Module';
$lang['link']						= 'Link';
$lang['lastchange']					= 'Last change';
$lang['thumb']						= 'Preview';
$lang['upload']						= 'Upload';
$lang['uploading']					= 'uploading ...';
$lang['media_filebrowser']			= 'serach in media';
$lang['file_name']					= 'filename';
$lang['thumb_link']					= 'linked preview image';
$lang['thumb_nolink']				= 'only preview image';
$lang['fullsize']					= 'fullsize';

//time
$lang['online_time_mode']			= 'Timer';
$lang['online_time_mode_none']		= 'no timer';
$lang['online_time_mode_start']		= 'set online at startdate';
$lang['online_time_mode_end']		= 'set online at enddate';
$lang['online_time_mode_startend']	= 'set online beteween startdate and enddate';
$lang['online_time_start']			= 'Startdate';
$lang['online_time_end']			= 'Enddate';
$lang['online_in']					= 'online in';
$lang['online_since']				= 'online since';
$lang['offline_in']					= 'offline in';
$lang['offline_since']				= 'offline since';

$lang['time_duration_sec']			= '{num} second';
$lang['time_duration_secs']			= '{num} seconds';
$lang['time_duration_min']			= '{num} minute';
$lang['time_duration_mins']			= '{num} minutes';
$lang['time_duration_hour']			= '{num} hour';
$lang['time_duration_hours']		= '{num} hours';
$lang['time_duration_day']			= '{num} day';
$lang['time_duration_days']			= '{num} days';
$lang['time_duration_year']			= '{num} year';
$lang['time_duration_years']		= '{num} years';

//pms
$lang['pms']					= 'Messages';
$lang['pm']						= 'message';
$lang['pms_nav']				= 'Navigation';
$lang['pms_re']					= 'Re';
$lang['newpm']					= 'New Message';
$lang['inbox']					= 'Inbox';
$lang['outbox']					= 'Outbox';
$lang['from_to_inbox']			= 'From';
$lang['from_to_outbox']			= 'To';
$lang['form_to']				= 'Recipient';
$lang['msg_pm_send']			= 'Message was sent';
$lang['msg_inbox_confirm_del']	= 'Are you sure that you want to delete {subject} from {user}?';
$lang['msg_inbox_success_del']	= 'The message {subject} from {user} has been deletet';
$lang['msg_outbox_confirm_del']	= 'Are you sure that you want to delete {subject} to {user}?';
$lang['msg_outbox_success_del']	= 'The message {subject} to {user} has been deletet';
$lang['msg_unread_pms']			= 'You have {count} unread message/s';
$lang['msg_box_empty']			= 'You have no new message/s';
$lang['pms_sendemail_subject']	= '{title} Messagesystem: you have got a '.$lang['pm'].' from {user} with subject {subject}';
$lang['pms_view_in_cms']		= 'show online';
$lang['reply']					= 'Reply';

//emojis
$lang['code']					= 'Code';
$lang['emojis']					= 'Emojis';
$lang['emoji']					= 'Emoji';

//local standards 
$lang['dateFormat']				= 'm/d/Y';
$lang['dateFormatJs']			= 'mm/dd/yy';

$lang['numDecPoint']			= '.';
$lang['numThousandsSep']		= ',';

?>