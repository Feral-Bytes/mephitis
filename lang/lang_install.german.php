<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

$lang['title']				= 'Mephitis Installation';

$lang['runinstalltool']		= '<a href="sys/install.php">Die Installation starten</a>';

$lang['not_installed']		= 'Mephitis CMS wurde noch nicht auf dem Server installiert. Bitte eine der folgenen Aktionen w&auml;hlen:';
$lang['already_installed']	= 'Mephitis CMS wurde bereits auf dem Server installiert. Bitte eine der folgenen Aktionen w&auml;hlen:';

$lang['need_auth_file']		= 'Um die Installation zu starten müssen Sie folgende Datei im <b>Wurzelverzeichnis</b> des CMS anlegen:';
$lang['need_auth_file_content']	= 'Diese Datei <b>muss</b> folgenden Inhalt haben:';
$lang['deleted_auth_file']	= 'Die folgende Datei wurde gel&ouml;scht. Bitte das <b>Wurzelverzeichnis</b> des CMS pr&uuml;fen.';
$lang['need_auth_file_next']	= 'Installation fortfahren';

$lang['need_special_auth_file']	= 'Um diese Aktion durchf&uuml;hren zu k&ouml;nnen muss die folgende Datei angepasst werden:';
$lang['need_special_auth_file_content']	= 'Bitte den vorhandenen Inhalt mit diesen &uuml;berschreiben:';

$lang['status']				= 'Status';
$lang['repair']				= 'reparieren';
$lang['install']			= 'Installation';
$lang['logout']				= 'Abmelden';

$lang['next']				= 'weiter';
$lang['db_host']			= 'Datenbank Host';
$lang['db_name']			= 'Datenbank Name';
$lang['db_user']			= 'Datenbank Benutzer';
$lang['db_pass']			= 'Datenbank Passwort';
$lang['prefix']				= 'Tabellen Prefix';

$lang['install_dberror']	= 'Datenbankfehler';
$lang['install_dberror_no_db'] = 'Datenbank nicht gefunden (Angaben korrekt?)';
$lang['install_dberror_no_connection'] = 'Keine Verbindung zum Host m&ouml;glich (Angaben korrekt?)';
$lang['install_dberror_no_admin'] = 'Admin Benutzer konnte nicht erstellt werden.';

$lang['step_1']				= 'Schritt 1';
$lang['step_1_info']		= 'Datenbank Informationen';
$lang['step_2']				= 'Schritt 2';
$lang['step_2_info']		= 'Admin Benutzer';
$lang['step_3']				= 'Schritt 3';
$lang['step_3_info']		= 'Weitere Informationen, &Auml;nderungen nicht n&ouml;tig.';

$lang['pass']				= 'Passwort';
$lang['installed']			= 'Mephits CMS wurde erfolgreich installiert.';

$lang['warning']			= 'Warnung';
$lang['wrong_login_try']	= 'Ein Anmeldeversuch ist gescheitert';

?>
