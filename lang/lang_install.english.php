<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

$lang['title']				= 'Mephitis Installer';

$lang['runinstalltool']		= '<a href="sys/install.php">Start the installation</a>';

$lang['not_installed']		= 'Mephitis CMS is not installed on this server. Please choose one of the following actions:';
$lang['already_installed']	= 'Mephitis CMS is already installed on this server. Please choose one of the following actions:';

$lang['need_auth_file']		= 'To start the installation, you need to create the following file in the <b>root directory</b> of the CMS:';
$lang['need_auth_file_content']	= 'The file <b>must</b> contain the following content:';
$lang['deleted_auth_file']	= 'The file was successfully deleted. Please check the <b>root directory</b> of the CMS.';
$lang['need_auth_file_next']	= 'Continue installation';

$lang['need_special_auth_file']	= 'In order to perform this action, the following has to be adjusted:';
$lang['need_special_auth_file_content']	= 'Please overwrite the existing content with these:';

$lang['status']				= 'Status';
$lang['repair']				= 'repair';
$lang['install']			= 'Installation';
$lang['logout']				= 'Logout';

$lang['next']				= 'next';
$lang['db_host']			= 'Database Host';
$lang['db_name']			= 'Database Name';
$lang['db_user']			= 'Database Account';
$lang['db_pass']			= 'Database Password';
$lang['prefix']				= 'Table Prefix';

$lang['install_dberror']	= 'Database Error';
$lang['install_dberror_no_db'] = 'Database not found (information correct?)';
$lang['install_dberror_no_connection'] = 'Unable to connect to the host (information correct?)';
$lang['install_dberror_no_admin'] = 'Admin account could not created.';

$lang['step_1']				= 'Step 1';
$lang['step_1_info']		= 'Database Information';
$lang['step_2']				= 'Step 2';
$lang['step_2_info']		= 'Admin Account';
$lang['step_3']				= 'Step 3';
$lang['step_3_info']		= 'Further information, changes are not necessary.';

$lang['pass']				= 'Password';
$lang['installed']			= 'Mephits CMS was successfully installed.';

$lang['warning']			= 'Warning';
$lang['wrong_login_try']	= 'Login failed';

?>
