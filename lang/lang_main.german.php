<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

//button
$lang['usercp']					= 'Benutzerverwaltung';
$lang['admincp']				= 'Admin-Center';

//login form
$lang['user']					= 'Benutzer';
$lang['login']					= 'Login';
$lang['logout']					= 'Logout';
$lang['login_name']				= 'Benutzername oder E-Mail';
$lang['user_name']				= 'Benutzername';
$lang['user_pass']				= 'Passwort';
$lang['user_pass_v']			= 'Passwort best&auml;tigen';
$lang['login_remember']			= 'Eingeloggt bleiben';
$lang['login_msg']				= 'Login war erfolgreich';
$lang['logout_msg']				= 'Logout war erfolgreich';
$lang['login_error']			= 'Benutzername oder Passwort ist falsch';

//password registration
$lang['register']				= 'Registrieren';
$lang['forgotpassword']			= 'Passwort vergessen';
$lang['registration']			= 'Registrierung';
$lang['registered']				= 'Sie k&ouml;nnen sich nun mit Ihren Daten anmelden';

//mail registration
$lang['reg_email_sent']			= 'Eine Best&auml;tigungsmail mit weiteren Angaben wurde versendet';
$lang['reg_email1']				= 'Hallo'."\r\n".'Vielen Dank für Ihre Registrierung. Bitte aktivieren Sie Ihren Account über folgenden Link'."\r\n";
$lang['reg_email2']				= "\r\n".'Mit freundlichen Grüssen'."\r\n".'das System';
$lang['useracc_activation']		= 'Benutzer Account Aktivierung';
$lang['useracc_activation1']	= 'Willkommen ';
$lang['useracc_activation2']	= '!<br>Bitte vergeben Sie ein Passwort, um die Aktivierung abzuschliessen.';
$lang['useracc_activated']		= 'Benutzer Account wurde aktiviert. Sie k&uuml;nnen sich nun mit Ihrem Passwort anmelden.';
$lang['mail_error']				= 'Fehler bei der E-Mail Zustellung. Bitte versuchen Sie es sp&auml;ter noch einmal.';
$lang['mail_forgotpassword']	= 'Hallo<br>Bitte benutzen Sie folgenden Link um Ihr Passwort zur&uuml;ckzusetzen<br>{link}<br>Mit freundlichen Gr&uuml;ssen<br>das System';
$lang['msg_forgotpassword']		= 'Es wurde eine E-Mail mit weiteren Anweisungen an Sie versendet';
$lang['msg_password_restored']	= 'Passwort wurde gesetzt';

//user account
$lang['user_account']			= 'Mein Profil';
$lang['user_account_edited']	= 'Ihr Profil wurde bearbeitet. Eventuell m&uuml;ssen Sie sich erneut anmelden.';
$lang['email']					= 'E-Mail';
$lang['email_successful']		= 'E-Mail wurde erfolgreich versendet';
$lang['successful']				= 'erfolgreich';
$lang['success']				= 'Erfolg';
$lang['user_pass_check']		= 'Aktuelles Passwort';
$lang['usercp_passcheck_fail']	= 'Bitte tragen Sie Ihr akutelles Passwort ein!';
$lang['mail_fail']				= 'Bitte tragen Sie eine g&uuml;ltige E-Mail Adresse ein!';
$lang['user_change_pass']		= 'Neues Passwort';
$lang['user_change_pass_v']		= 'Neues Passwort best&auml;tigen';
$lang['required_msg_email']		= 'E-Mail Adresse wird ben&ouml;tigt';
$lang['avatar']					= 'Avatar';
$lang['pms_sendemail']			= 'PMs als E-Mail an mich senden';
$lang['usercp_cat_common']		= 'Allgemein';
$lang['usercp_cat_crit']		= 'E-Mail und Passwort';
$lang['usercp_cat_avatar']		= 'Avatar';
$lang['usercp_cat_pms']			= 'Nachrichtensystem';
$lang['usercp_cat_crit_passcheck'] = 'Sie m&uuml;ssen Ihr aktuelles Passwort eingeben um &Auml;nderungen in dieser Kategorie vorzunehmen';
$lang['member']					= 'Mitglied';
$lang['members']				= 'Mitglieder';
$lang['guest']					= 'Gast';

//locales
$lang['lang']					= 'Sprache';
$lang['timezone']				= 'Zeitzone';

//captcha
$lang['captcha']				= 'Sicherheitscode';
$lang['captcha_confirm']		= 'Bitte best&auml;tigen';
$lang['captcha_wrong']			= 'Sicherheitscode falsch';
$lang['norm_send']				= 'Absenden';

//general
$lang['new']					= 'Neu';
$lang['edit']					= 'Bearbeiten';
$lang['del']					= 'L&ouml;schen';
$lang['remove']					= 'Entfernen';
$lang['content']				= 'Inhalt';
$lang['edited']					= 'Objekt wurde bearbeitet';
$lang['posted']					= 'Objekt wurde in die Datenbank geschrieben';
$lang['deleted']				= 'Gel&ouml;scht';
$lang['del_confirm']			= 'Sind Sie sicher, dass sie dieses Objekt l&ouml;schen wollen?';
$lang['homepage']				= 'Startseite';
$lang['yes']					= 'Ja';
$lang['no']						= 'Nein';
$lang['name']					= 'Name';
$lang['close']					= 'Schliessen';
$lang['ip']						= 'IP';
$lang['asc']					= 'Aufsteigend';
$lang['desc']					= 'Absteigend';
$lang['action']					= 'Aktion';
$lang['time']					= 'Zeit';
$lang['time_created']			= 'Erstellt';
$lang['time_edit']				= 'Zuletzt bearbeitet';
$lang['refresh']				= 'Aktualisieren';
$lang['allow_comments']			= 'Kommentarfunktion aktivieren';
$lang['access']					= 'Zugriff';
$lang['nav']					= 'Navigation';
$lang['root_dir']				= 'Hauptverzeichnis';
$lang['to_top']					= 'Nach Oben';
$lang['to_bottom']				= 'Nach Unten';
$lang['up']						= 'Nach oben verschieben';
$lang['down']					= 'Nach unten verschieben';
$lang['footer']					= 'Mephitis CMS &copy; by <a href="https://www.feralbytes.ch" target="_blank">Feral Bytes</a>';
$lang['public']					= '&Ouml;ffentlich';
$lang['hidden']					= 'Unsichtbar';
$lang['set_public']				= 'Setze &Ouml;ffentlich';
$lang['unset_public']			= 'Setze nicht &Ouml;ffentlich';
$lang['download_ics']			= 'F&uuml;r Kalender herunterladen';
$lang['truncate']				= 'Tabelle leeren';
$lang['truncate_confirm']		= 'Sind Sie sicher, dass Sie die Tabelle leeren wollen?';
$lang['truncate_success']		= 'Tabelle geleert';

//days
$lang['mon']					= 'Montag';
$lang['tue']					= 'Dienstag';
$lang['wed']					= 'Mittwoch';
$lang['thu']					= 'Donnerstag';
$lang['fri']					= 'Freitag';
$lang['sat']					= 'Samstag';
$lang['sun']					= 'Sonntag';

//errors
$lang['required_msg']			= 'wird ben&ouml;tigt';
$lang['not_valid_msg']			= 'ung&uuml;ltig';
$lang['error_doublename']		= 'Der Name ist bereits vergeben';
$lang['error_doubleemail']		= 'E-Mail Adresse ist bereits vergeben';
$lang['required_msg_name']		= 'Kein Name vergeben';
$lang['required_msg_content']	= 'Kein Inhalt';
$lang['required_msg_file']		= 'Keine Datei angegeben';
$lang['pass_equal_error']		= 'Passw&ouml;rter stimmen nicht &uuml;berein';
$lang['no_pass_error']			= 'Passwort ist nicht gesetzt';
$lang['mysql_error']			= 'MySQL Fehler';
$lang['404']					= 'Objekt nicht gefunden';
$lang['error']					= 'Fehler';
$lang['isnopic']				= 'ist kein Bild';
$lang['access_denied']			= 'Zugang verweigert';
$lang['no_upload_permission']	= 'Keine Berechtigung zum Hochladen';
$lang['template_error']			= 'Template Datei nicht gefunden';

//comments
$lang['no_comments']			= 'Noch keine Kommentare vorhanden';
$lang['comments']				= 'Kommentare';
$lang['subject']				= 'Betreff';
$lang['new_comment']			= 'Kommentar verfassen';
$lang['new_comment_posted']		= 'Kommentar verfasst';
$lang['new_comment_posted_mc']	= 'Ihr Kommentar wird nach einer Pr&uuml;fung ver&ouml;ffentlicht';
$lang['edit_comment']			= 'Kommentar bearbeiten';
$lang['del_comment']			= 'Kommentar l&ouml;schen';
$lang['more']					= 'Mehr'; //emoji
$lang['show']					= 'Anzeigen';

//page navigation
$lang['page']					= 'Seite';
$lang['go_to_page']				= 'gehe zu Seite';
$lang['next']					= 'Vor';
$lang['back']					= 'Zur&uuml;ck';

//searchengine
$lang['search_placeholder']		= 'suchen...';
$lang['search_btn']				= 'suchen';
$lang['search_nothingfound']	= 'Es wurde nichts gefunden';
$lang['search_result']			= 'Suchergebnis';

//files
$lang['thumb_admin']				= 'Vorschaubild';
$lang['file']						= 'Datei';
$lang['files']						= 'Dateimanager';
$lang['file_module']				= 'Modul';
$lang['link']						= 'Link';
$lang['lastchange']					= 'Letzte &Auml;nderung';
$lang['thumb']						= 'Vorschau';
$lang['upload']						= 'Hochladen';
$lang['uploading']					= 'lade hoch ...';
$lang['media_filebrowser']			= 'Medien durchsuchen';
$lang['file_name']					= 'Dateiname';
$lang['thumb_link']					= 'verlinkte Vorschau';
$lang['thumb_nolink']				= 'nur Vorschau';
$lang['fullsize']					= 'volle Gr&ouml;&szlig;e';

//time
$lang['online_time_mode']			= 'Zeitschaltung';
$lang['online_time_mode_none']		= 'Keine Zeitschaltung';
$lang['online_time_mode_start']		= 'Zum Startzeitpunkt als online markieren';
$lang['online_time_mode_end']		= 'Zum Endzeitpunkt als nicht online markieren';
$lang['online_time_mode_startend']	= 'Zum Zeitraum zwischen Start- und Endzeitpunkt als online markieren';
$lang['online_time_start']			= 'Startzeitpunkt';
$lang['online_time_end']			= 'Endzeitpunkt';
$lang['online_in']					= 'online in';
$lang['online_since']				= 'online seit';
$lang['offline_in']					= 'offline in';
$lang['offline_since']				= 'offline seit';

$lang['time_duration_sec']			= '{num} Sekunde';
$lang['time_duration_secs']			= '{num} Sekunden';
$lang['time_duration_min']			= '{num} Minute';
$lang['time_duration_mins']			= '{num} Minuten';
$lang['time_duration_hour']			= '{num} Stunde';
$lang['time_duration_hours']		= '{num} Stunden';
$lang['time_duration_day']			= '{num} Tag';
$lang['time_duration_days']			= '{num} Tage';
$lang['time_duration_year']			= '{num} Jahr';
$lang['time_duration_years']		= '{num} Jahre';

//pms
$lang['pms']					= 'Nachrichten';
$lang['pm']						= 'Nachricht';
$lang['pms_re']					= 'Re';
$lang['newpm']					= 'Neue Nachricht';
$lang['pms_nav']				= 'Navigation';
$lang['inbox']					= 'Posteingang';
$lang['outbox']					= 'Postausgang';
$lang['from_to_inbox']			= 'Von';
$lang['from_to_outbox']			= 'An';
$lang['form_to']				= 'Empf&auml;nger';
$lang['msg_pm_send']			= 'Nachricht versendet';
$lang['msg_inbox_confirm_del']	= 'Sind Sie sicher das {subject} von {user} gel&ouml;scht werden soll?';
$lang['msg_inbox_success_del']	= 'Die Nachricht {subject} von {user} wurde gel&ouml;scht';
$lang['msg_outbox_confirm_del']	= 'Sind Sie sicher das {subject} an {user} gel&ouml;scht werden soll?';
$lang['msg_outbox_success_del']	= 'Die Nachricht {subject} an {user} wurde gel&ouml;scht';
$lang['msg_unread_pms']			= 'Sie haben {count} ungelesene Nachricht/en';
$lang['msg_box_empty']			= 'Sie haben keine neuen Nachrichten';
$lang['pms_sendemail_subject']	= '{title} Nachrichtensystem: Ihnen wurde eine '.$lang['pm'].' von {user} mit dem Betreff {subject} gesendet';
$lang['pms_view_in_cms']		= 'zeige online';
$lang['reply']					= 'Antworten';

//emojis
$lang['code']					= 'Code';
$lang['emojis']					= 'Emojis';
$lang['emoji']					= 'Emoji';

//local standards 
$lang['dateFormat']				= 'd.m.Y';
$lang['dateFormatJs']			= 'dd.mm.yy';

$lang['numDecPoint']			= ',';
$lang['numThousandsSep']		= '.';

?>