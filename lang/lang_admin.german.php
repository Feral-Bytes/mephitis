<?php
/*
 * Mephits CMS
 * 
 * @package Mephitis
 * @author Feral Bytes https://www.feralbytes.ch
 * @copyright 2017 Feral Bytes
 * @license https://www.feralbytes.ch/index.php?show=licenses GNU Affero General Public License (AGPL) 3.0
 * 
 * @version 1.1
 */

//title
$lang['title']						= 'Mephitis Admin-Center';

//dashboard
$lang['dashboard']					= 'Dashboard';

//navbar
$lang['cms_tools_dir']				= 'CMS Tools';
$lang['cms_modules_dir']			= 'Module';

//global
$lang['id']							= 'ID';
$lang['ips_del']					= 'Tabelle leeren';
$lang['lastaccess']					= 'Letzter Zugriff';
$lang['error_sub_of_loop']			= 'Schleife wird erzeugt, Fehler in der Zuordnung';
$lang['no_site']					= 'keine';
$lang['wrong_data']					= 'Falsche Eingaben';
$lang['nothing']					= 'Bitte einen Bereich w&auml;hlen';
$lang['logic_error']				= 'Logikfehler';
$lang['install_tool']				= 'Install Tool';
$lang['credits']					= 'Credits';

//settings
$lang['common_settings']			= 'Systemeinstellungen';
$lang['cms_title']					= 'Titel der Webseite';
$lang['cms_footer']					= 'Footer der Website';
$lang['coookie_name']				= 'Cookiename';
$lang['meta_robots']				= 'Anweisungen an Suchmaschinen';
$lang['meta_author']				= 'Meta-Tag Autor';
$lang['meta_description'] 			= 'Meta-Tag Beschreibung';
$lang['meta_keywords']				= 'Meta-Tag Keywords';
$lang['shortcut_icon']				= 'Pfad zum Icon';
$lang['cms_homepage']				= 'Startseite';
$lang['template_dir']				= 'Template Ordner';
$lang['admin_name']					= 'Admin Name';
$lang['admin_email']				= 'Admin E-Mail';
$lang['thumbnail_size']				= 'Max. Gr&ouml;sse der Vorschaubilder in Pixel';
$lang['norm_time']					= 'Zeitformat (PHP)';
$lang['norm_date']					= 'Datumsformat (PHP) mit Zeit';
$lang['norm_date_notime']			= 'Datumsformat (PHP) ohne Zeit';
$lang['per_page']					= 'Eintr&auml;ge pro Seite';
$lang['perpage']					= $lang['per_page'];
$lang['num_emojis']					= 'Anzahl Emojis';
$lang['useronline_timeout']			= 'Timeout f&uuml;r "Benutzer online" Anzeige';
$lang['msg_box_fadeout_timer']		= 'Timeout f&uuml;r Message Box Ausblendung';
$lang['allow_bbcode']				= 'BBCode aktivieren';
$lang['allow_emojis']				= 'Emojis aktivieren';
$lang['wysiwyg_user']				= 'WYSIWYG Editor';
$lang['wysiwyg_bbcode']				= 'TinyMCE (BBCode)';
$lang['wysiwyg_classic']			= 'Klassisch';
$lang['forgotpassword_max_age']		= 'max. Alter von "passwort vergessen" E-Mails  in sek.';
$lang['register_text']				= 'Registrierungs Info Text';
$lang['register_method_email']		= 'E-Mail Aktivierung';
$lang['register_method_direct']		= 'Direkte Passworteingabe';
$lang['save_accesses']				= 'Zugriffe protokollieren';
$lang['public_show_login']			= 'Zeige Anmelden Button';
$lang['public_register']			= 'Registrierung aktivieren';
$lang['public_login']				= 'Anmeldung aktivieren';
$lang['guest_user_groups']			= 'Gruppen f&uuml;r G&auml;ste';
$lang['new_user_groups']			= 'Gruppen f&uuml;r neue Benutzer';
$lang['comment_mod_groups']			= 'Gruppen f&uuml;r Kommentar-Moderatoren';
$lang['comment_guests_can_write']	= 'G&auml;ste k&ouml;nnen Kommentare schreiben';
$lang['comment_members_can_write']	= 'Mitglieder k&ouml;nnen Kommentare schreiben';
$lang['comment_requires_mod_check']	= 'Kommentare m&uuml;ssen von einen Kommentar-Moderator gepr&uuml;ft werden';
$lang['file_upload_groups']			= 'Dateiupload f&uuml;r folg. Gruppen erlauben';
$lang['common_settings_edited'] 	= 'Einstellungen wurden ge&auml;ndert';
$lang['reCAPTCHA']					= 'reCAPTCHA aktivieren';
$lang['reCAPTCHA_key_private']		= 'reCAPTCHA privater Schl&uuml;ssel';
$lang['reCAPTCHA_key_public']		= 'reCAPTCHA &ouml;ffentlicher Schl&uuml;ssel';
$lang['global_tracking_enable']		= 'Globales tracking aktivieren';
$lang['global_tracking_code']		= 'Globaler Tracking Code';
$lang['settings_cat_common']		= 'Allgemein';
$lang['settings_cat_admin']			= 'Admin';
$lang['settings_cat_content']		= 'Inhalt';
$lang['settings_cat_user_content']	= 'Benutzerinhalte';
$lang['settings_cat_user']			= 'Benutzer';
$lang['settings_cat_tracking']		= 'Tracking';

//email
$lang['email_system']				= 'E-Mail System';
$lang['email_settings']				= 'Einstellungen';
$lang['email_settings_ok']			= 'Einstellungen gepeichtert, Verbindung zum Server erfolgreich';
$lang['email_settings_notok']		= 'Einstellungen gepeichtert, Verbindung zum Server gescheitert';
$lang['email_queue']				= 'Warteschlange';

$lang['email_smtp']					= 'Benutze SMTP Server f&uuml;r E-Mail Versand';
$lang['email_smtp_host']			= 'Host';
$lang['email_smtp_auth']			= 'Server ben&ouml;tigt Authentifikation';
$lang['email_smtp_user']			= 'Benutzername';
$lang['email_smtp_pass']			= 'Passwort';
$lang['email_smtp_secure']			= 'Verschl&uuml;sslung';
$lang['email_smtp_port']			= 'Port';
$lang['email_smtp_from_name']		= 'Name des Versenders';
$lang['email_smtp_from_email']		= 'E-Mail des Versenders';

$lang['recipient_email']			= 'Empf&auml;nger E-Mail';
$lang['recipient_name']				= 'Empf&auml;nger Name';
$lang['email_send_queue']			= 'Warteschlange abarbeiten';

//content
$lang['dirs']						= 'Ordner';
$lang['dir']						= 'Ordner'; //fix doppelt
$lang['icon']						= 'Icon';
$lang['sites']						= 'Seiten';
$lang['site_new']					= 'neue Seite';
$lang['dir_new']					= 'neuer Ordner';
$lang['dir_new_this_lvl']			= 'neuer Ordner';
$lang['dir_new_next_lvl']			= 'neuer Unterordner';
$lang['dir_move_here']				= 'Ordner hierhin verschieben';
$lang['template']					= 'Template';
$lang['template_parent']			= 'Template des &uuml;bergeordneten Elements';
$lang['template_current']			= 'Aktuelles Template';
$lang['none']						= '-keine-';
$lang['sub_dir_of']					= 'Unterordner von';
$lang['border']						= 'Rahmen';
$lang['publish']					= 'Publizieren';
$lang['shortlink']					= 'Kurzlink';
$lang['hidden_nav']					= 'Unsichtbar in der Navigation';
$lang['notempty']					= 'Objekt nicht leer...';
$lang['site_edit']					= 'Seite bearbeiten';
$lang['site_del']					= 'Seite l&ouml;schen';
$lang['dir_edit']					= 'Ordner bearbeiten';
$lang['dir_del']					= 'Ordner l&ouml;schen';
$lang['nodirs']						= 'Keine Ordner vorhanden...<br><a href="'.$phproot.'">Zur Startseite</a>';
$lang['placeholder']				= 'Platzhalter';
$lang['tooltip']					= 'Tooltip';
$lang['site_tracking_enable']		= 'Seiten Tracking aktivieren';
$lang['site_tracking_code']			= 'Seiten Tracking Code';
$lang['clear_cache']				= 'Cache leeren';
$lang['cache_cleared']				= 'Cache geleert';

//accesslog
$lang['accesses']					= 'Zugriffsprotokoll';
$lang['host']						= 'Host';
$lang['http_user_agent']			= 'Browserkennung';
$lang['http_referer']				= 'Externer Zugriff von';
$lang['request_method']				= 'Methode';
$lang['php_self']					= 'PHP-Datei';

//sessions
$lang['sessions']					= 'Sessions';
$lang['session_id']					= 'Session ID';

//user
$lang['user_active']				= 'Account aktiv';
$lang['reg_time']					= 'Datum der Registrierung';
$lang['status']						= 'Status';
$lang['selfdelete_error']			= 'Das eigene Profil kann nicht gel&ouml;scht werden!';
$lang['active']						= 'aktiv';
$lang['inactive']					= 'inaktiv';
$lang['perm']						= 'Berechtigung';
$lang['admin']						= 'Administrator';
$lang['mod']						= 'Moderator';
$lang['user']						= 'Benutzer';
$lang['online']						= 'Online';
$lang['offline']					= 'Offline';
$lang['groups']						= 'Gruppen';
$lang['groups_mode']				= 'Art des Gruppenzugriffs';
$lang['std']						= 'Standard';
$lang['whitelist']					= 'Whitelist';
$lang['blacklist']					= 'Blacklist';
$lang['edit_groups']				= 'Gruppen mit Bearbeitungsrechten';

//module and fce
$lang['module_error'] 				= 'Modul Datei nicht gefunden...<br>';
$lang['module_error_admin']			= 'Fehler beheben';
$lang['module']						= 'Modul';
$lang['modulearguments']			= 'Modul Argumente';
$lang['nomodule']					= 'Kein Modul';
$lang['moduleinstance']				= 'Module Instanz';
$lang['content_kind'] 				= 'Art des Inhalts';
$lang['content_kind_this']			= 'Nur dieses Element';
$lang['content_kind_fce']			= 'Flexible Inhaltselemente';
$lang['content_kind_link']			= 'Link';
$lang['content_kind_link_blank']	= 'Link im neuen Fenster';
$lang['content_link_target']		= 'Link Ziel';
$lang['fces']						= 'Flexible Inhaltselemente';
$lang['possible_fce_containers']	= 'Elemente die flexible Inhaltselemente beinhalten k&ouml;nnen';
$lang['no_fce']						= 'keine flexiblen Inhaltselemente';
$lang['fce_name_meppostborder']		= 'Normaler Rahmen';
$lang['fce_name_bscontainer']		= 'Bootstrap Container';
$lang['fce_name_bs3cols']			= '3 Bootstrap Spalten';
$lang['fce-bs-Col1']				= 'Links';
$lang['fce-bs-Col2']				= 'Mitte';
$lang['fce-bs-Col3']				= 'Rechts';
$lang['fce-module-container']		= 'Modul Container';
$lang['fce-text-field']				= 'Textfeld';
$lang['fce-html-field']				= 'HTML';
$lang['del_confirm_recursive'] 		= 'Rekursive Datenl&ouml;schung. Sind Sie sicher das Sie dieses und alle Objekte darin l&ouml;schen wollen?';
$lang['order_dirs']					= 'Ordner ordnen';
$lang['order_sites']				= 'Seiten ordnen';
$lang['type']						= 'Typ';
$lang['select']						= 'Ausw&auml;hlen';
$lang['copypastehere']				= 'Hierher kopieren und einf&uuml;gen';
$lang['movehere']					= 'Hierher verschieben';
$lang['moved']						= 'Verschoben';
$lang['moved_txt1']					= 'wurde verschoben';
$lang['cssClasses']					= 'CSS Klassen';
$lang['mod_fix_db']					= 'Datenbank reparieren';
$lang['mod_db_fixed']				= 'Datenbank repariert';
$lang['mod_update']					= 'update';
$lang['mod_updated']				= 'geupdatet';
$lang['mod_updated_fail']			= 'Update fehlgeschlagen';
$lang['mod_updater_notfound']		= 'Updater nicht gefunden';

$lang['page']							= 'Seite';
$lang['page_content_element_select']	= 'Neues Inhaltselement auf der Seite erstellen';
$lang['page_new_this_lvl']				= 'Neue Seite auf diese Ebene';
$lang['page_new_next_lvl']				= 'Neue Unterseite in dieser Seite';
$lang['page_edit']						= 'Seite bearbeiten';
$lang['page_edit_content']				= 'Seiten Basis-Element bearbeiten';
$lang['page_del']						= 'Seite l&ouml;schen';
$lang['page_select']					= 'Seite ausw&auml;hlen';
$lang['page_move_here']					= 'Seite hierhin verschieben';
$lang['page_move_here_after']			= 'Seite hierhin nach Seite verschieben';

$lang['content_new']					= 'Neues Inhaltselement';
$lang['content_edit']					= 'Inhaltselement bearbeiten';
$lang['content_del']					= 'Inhaltselement l&ouml;schen';
$lang['content_select']					= 'Inhaltselement ausw&auml;hlen';
$lang['content_move_here']				= 'Inhaltselement hierhin verschieben';

$lang['fce_content']					= 'Inhaltselement';
$lang['fce_added']						= 'Inhaltselement hinzugef&uuml;gt';
$lang['fce_edited']						= 'Inhaltselement bearbeitet';
$lang['fce_del_confirm']				= 'Sind Sie sicher, das Sie dieses Inhaltselement l&ouml;schen wollen?';
$lang['fce_del_success']				= 'Inhaltselement gel&ouml;scht';

$lang['cat_access']						= $lang['access'];
$lang['cat_common']						= 'Allgemein';


	//carousel
$lang['fce_name_bscarousel']		= 'Bootstrap Carousel';
$lang['fce_name_bscarouselitem']	= 'Bootstrap Carousel Item';
$lang['picture']					= 'Bild';
$lang['picture_alt']				= 'Bild alternativ Text';
$lang['caption']					= '&Uuml;berschrift';
	//modules
$lang['mail']						= 'E-Mail Adresse';
$lang['website']					= 'Webseite';
$lang['website_form']				= 'Webseite http://';

//modul manager
$lang['modules_manager']			= 'Modulmanager';

//database
$lang['db']							= 'Datenbank';
$lang['db_tables']					= 'Tabellen';
$lang['db_table']					= 'Tabelle';
$lang['db_fields']					= 'Felder';
$lang['db_status']					= 'Status';
$lang['db_ok']						= 'Ok';
$lang['db_unknown']					= 'Quelle unbekannt';
$lang['db_not_found']				= 'nicht gefunden';
$lang['db_error']					= 'Fehler';
$lang['db_sort_error']				= 'Sortierung fehlerhaft';
$lang['db_engine_error']			= 'Engine fehlerhaft';
$lang['db_collation_error']			= 'Koalition fehlerhaft';
$lang['db_repair']					= 'Versuchen Fehler zu beheben oder Tabelle(n) anzulegen';
$lang['db_repaired']				= 'Fehler behoben oder Tabelle(n) angelegt';
$lang['db_need_repair']				= 'Fehler beheben ausf&uuml;hren';
$lang['db_toinstall']				= 'Es k&ouml;nnen neue Tabellen angelegt werden';
$lang['db_table_show']				= 'anzeigen';
$lang['db_field_del1']				= 'Sind Sie sicher, dass Sie das Feld';
$lang['db_field_del2']				= 'in der Tabelle';
$lang['db_field_del3']				= 'l&ouml;schen wollen?';
$lang['db_field_deled1']			= 'Das Feld';
$lang['db_field_deled2']			= 'in der Tabelle';
$lang['db_field_deled3']			= 'wurde gel&ouml;scht.';
$lang['db_var_del1']				= 'Sind Sie sicher, dass Sie die Variable';
$lang['db_var_del2']				= 'in der Tabelle';
$lang['db_var_del3']				= 'l&ouml;schen wollen?';
$lang['db_var_deled1']				= 'Die Variable';
$lang['db_var_deled2']				= 'in der Tabelle';
$lang['db_var_deled3']				= 'wurde gel&ouml;scht.';
$lang['db_table_del1']				= 'Sind Sie sicher, dass Sie die Tabelle';
$lang['db_table_del2']				= 'l&ouml;schen wollen?';
$lang['db_table_deled1']			= 'Die Tabelle';
$lang['db_table_deled2']			= 'wurde gel&ouml;scht.';
$lang['db_backup']					= 'Backup';
$lang['db_drop_table_if_exists']	= 'DROP TABLE IF EXISTS einbeziehen';
$lang['db_breakat_rows']			= 'INSERT Befehl bei n Datens&auml;tzen unterbrechen';
$lang['db_send_as_file']			= 'als Datei senden';
$lang['db_var_error']				= 'Variablen Fehler';
$lang['db_vars']					= 'Variablen';
$lang['db_var']						= 'Variable';

//taskqueue
$lang['mep_system']					= 'System';
$lang['taskqueue']					= 'Geplante Aufgaben';
$lang['function']					= 'Funktion';
$lang['function_args']				= 'Funktion Argumente';
$lang['exec_date']					= 'Ausf&uuml;hrungsdatum';
$lang['exec_time_of_day']			= 'Ausf&uuml;hrungszeit';
$lang['last_exec']					= 'Letzte Ausf&uuml;hrung';
$lang['exec_mode']					= 'Ausf&uuml;hrungsart';
$lang['exec_mode_simple']			= 'Einfach';
$lang['exec_mode_simple_once']		= 'Einmaling ('.$lang['exec_date'].' + '.$lang['exec_time_of_day'].')';
$lang['exec_mode_simple_daily']		= 'T&auml;glich ('.$lang['exec_time_of_day'].')';
$lang['exec_mode_weekly']			= 'W&ouml;chentlich ('.$lang['exec_time_of_day'].')';

$lang['simple_once']				= $lang['exec_mode_simple_once'];
$lang['simple_daily']				= $lang['exec_mode_simple_daily'];
$lang['weekly_mon']					= $lang['exec_mode_weekly'].'('.$lang['mon'].')';
$lang['weekly_tue']					= $lang['exec_mode_weekly'].'('.$lang['tue'].')';
$lang['weekly_wed']					= $lang['exec_mode_weekly'].'('.$lang['wed'].')';
$lang['weekly_thu']					= $lang['exec_mode_weekly'].'('.$lang['thu'].')';
$lang['weekly_fri']					= $lang['exec_mode_weekly'].'('.$lang['fri'].')';
$lang['weekly_sat']					= $lang['exec_mode_weekly'].'('.$lang['sat'].')';
$lang['weekly_sun']					= $lang['exec_mode_weekly'].'('.$lang['sun'].')';

$lang['Task_FixDB']					= 'Datenbank pr&uuml;fen und reparieren';
$lang['Task_BackupDB']				= 'Datenbank Backup anlegen';
$lang['Task_FixBackupDB']			= 'Datenbank pr&uuml;fen, reparieren und Backup anlegen';
$lang['Task_MailQueue']				= 'E-Mail Warteschlange abarbeiten';
$lang['Task_ClearCache']			= 'Cache leeren';

//updater
$lang['updater']					= 'Updater';
$lang['install']					= 'installieren';
$lang['cancel']						= 'abbrechen';
$lang['install_update']				= 'Update installieren';
$lang['md5_checksum']				= 'md5 Pr&uuml;fsumme';
$lang['admin_pw']					= 'Administrator Passwort';
$lang['wrong_md5_checksum']			= 'md5 Pr&uuml;fsumme falsch';
$lang['wrong_admin_pw']				= 'Administrator Passwort falsch';
$lang['error_file']					= 'Fehler in der Datei';
$lang['install_update_warning']		= 'Durch das Ausf&uuml;hren der Update-Installation kann Ihre CMS Installation unbrauchbar werden, verwenden Sie sie nur wenn Sie sich absolut sicher sind und ein Backup angelegt haben!';
$lang['install_update_success']		= 'Installation erfolgreich!';
$lang['install_update_fail']		= 'Installation fehlgeschlagen, ggf. ist das Archiv besch&auml;digt!';

//templates
$lang['templates']					= 'Templates';
$lang['tpl_create_db_vars']			= 'Template Daten in Datenbank schreiben';
$lang['tpl_edit_db_vars']			= 'Template Daten bearbeiten';
$lang['tpl_vars_created']			= 'Template Daten in Datenbank geschrieben';
$lang['tpl_vars_updated']			= 'Template Daten bearbeitet';
$lang['color']						= 'Farbe';
$lang['size']						= 'Gr&ouml;&szlig;e';

//style_admin.php from template
$lang['path']						= 'Pfad';

$lang['hidden']						= 'unsichtbar'; // will get a refresh - soon

?>